////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/ObserverService.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/LogService.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/Manager.hpp>
#include <The2D/Rtti.hpp>

the::Watcher::Watcher(std::function<void (GameEntity*, WatchAction)> _watcher,
    WatchAction _actionFlags,
    WatchStrategy _strategy)
    : watcher(_watcher), actionFlags(_actionFlags), strategy(_strategy), handleCount(0)
{
}

the::WatcherHandle::WatcherHandle(): mWatcher(nullptr), mWatcherList(nullptr)
{
}

the::WatcherHandle::WatcherHandle(const WatcherHandle& cpy): WatcherHandle(cpy.mWatcher, cpy.mWatcherList)
{
}

the::WatcherHandle::WatcherHandle(Watcher* watcher, std::vector<Watcher*>* watchersList)
{
    set(watcher, watchersList);
}

the::WatcherHandle& the::WatcherHandle::operator=(const WatcherHandle& handle)
{
    reset();
    set(handle.mWatcher, handle.mWatcherList);
    return *this;
}

the::WatcherHandle::~WatcherHandle()
{
    reset();
}

bool the::WatcherHandle::operator==(const WatcherHandle& handle) const
{
    return equal(handle);
}

bool the::WatcherHandle::operator!=(const WatcherHandle& handle) const
{
    return !equal(handle);
}
bool the::WatcherHandle::equal(const WatcherHandle& handle) const
{
    return mWatcher == handle.mWatcher;
}

void the::WatcherHandle::reset()
{
    if (valid())
    {
        mWatcher->handleCount--;
        if (mWatcher->handleCount == 0)
        {
            for (auto it = mWatcherList->begin(); it != mWatcherList->end(); it++)
            {
                if ((*it) == mWatcher)
                {
                    mWatcherList->erase(it);
                    delete mWatcher;
                    break;
                }
            }
        }
    }
    mWatcher = nullptr;
}

bool the::WatcherHandle::valid()
{
    return mWatcher != nullptr;
}
the::WatcherHandle::operator bool()
{
    return valid();
}

the::Watcher* the::WatcherHandle::get()
{
    return mWatcher;
}

void the::WatcherHandle::set(Watcher* watcher, std::vector<Watcher*>* watchersList)
{
    mWatcher = watcher;
    mWatcherList = watchersList;
    mWatcher->handleCount++;
}


the::ObserverService::ObserverService(InfrastructureCore* core)
    :  mCore(core), mEntityCounter(1)
{
    mEntityList.reserve(100000);
    mEntityList.push_back(nullptr);
}

the::ObserverService::~ObserverService()
{
}

the::WatcherHandle the::ObserverService::beginWatch(
    std::function<void (GameEntity*, WatchAction)> watcher,
    WatchAction actionFlags,
    WatchStrategy strategy)
{
    // delete see in WatcherHandle
    Watcher* ptr = new Watcher(watcher, actionFlags, strategy);
    mWatchers.push_back(ptr);
    return WatcherHandle(ptr, &mWatchers);
}

void the::ObserverService::endWatch(WatcherHandle& handle)
{
    handle.reset();
}

const std::vector<the::GameEntity*>& the::ObserverService::getEntityList() const
{
    return mEntityList;
}

the::GameEntity* the::ObserverService::getEntity(GameEntityID id) const
{
    ASSERT(id >=0 && id < (int)mEntityList.size(), "Invalid id");
    return mEntityList[id];
}

const std::vector<the::GameEntity*>& the::ObserverService::getManagerList() const
{
    return mManagerList;
}

int the::ObserverService::getRegisteredEntityCount() const
{
    return mEntityCounter;
}

void the::ObserverService::onRegister(GameEntity* ent)
{
    ent->acceptAll(std::bind(&ObserverService::addToList, this, std::placeholders::_1));
    noticeWatchers(ent, ENTITY_REGISTERED, WATCH_SUBTREE);
}

void the::ObserverService::onUnregister(GameEntity* ent)
{
    noticeWatchers(ent, ENTITY_REGISTERED, WATCH_SUBTREE);
    ent->acceptAll(std::bind(&ObserverService::removeFromList, this, std::placeholders::_1));
}

void the::ObserverService::onRegisterManager(GameEntity* ent)
{
    mManagerList.push_back(ent);
}
void the::ObserverService::onUnregisterManager(GameEntity* ent)
{
    for (auto it = mManagerList.begin(); it != mManagerList.end(); it++)
    {
        if (*it == ent)
        {
            mManagerList.erase(it);
            break;
        }
    }
}
void the::ObserverService::onInitializing(GameEntity* ent)
{
    noticeWatchers(ent, ENTITY_INITIALIZING);
}
void the::ObserverService::onInitialized(GameEntity* ent)
{
    noticeWatchers(ent, ENTITY_INITIALIZED);
}
void the::ObserverService::onDeinitialized(GameEntity* ent)
{
    noticeWatchers(ent, ENTITY_DEINITIALIZED);
}

void the::ObserverService::noticeWatchers(GameEntity* ent, WatchAction action, WatchStrategy strategy)
{
    // This loop strange, because during call w.watcher(ent, action) watcher can remove himself from mWatchers.
    for (auto watcherIt = mWatchers.rbegin(); watcherIt != mWatchers.rend();)
    {
        Watcher& w = *(*watcherIt);
        watcherIt++;
        if (((w.actionFlags & action) > 0) && w.strategy == strategy)
        {
            w.watcher(ent, action);
        }
    }
}

void the::ObserverService::addToList(GameEntity* ent)
{
    ASSERT(ent->id() == 0, "Trying to register already registered entity (something impossible).");

    //mEntityList.push_back(ent);
    mEntityCounter++;
    ent->mEngineID = mEntityCounter; //mEntityList.size() - 1;
    ent->setCore(mCore);
    noticeWatchers(ent, ENTITY_REGISTERED, WATCH_EACH_ENTITY);
}
void the::ObserverService::removeFromList(GameEntity* ent)
{
    // Dirty hack, while binds not became entities (not params)
    if (ent->getName() != "DefaultContainer")
    {
        ASSERT(ent->id() > 0, "Trying unregister not registered entity (something impossible).");
        //ASSERT(ent->id() < (int)mEntityList.size(), "Invalid engine id of the entity (something impossible).");

        //mEntityList[ent->mEngineID] = nullptr;
        ent->mEngineID = 0;
        ent->setCore(nullptr);
        noticeWatchers(ent, ENTITY_UNREGISTERED, WATCH_EACH_ENTITY);
    }
}
