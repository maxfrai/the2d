////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/ParametersBase.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/Utils/Utils.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>
#include <algorithm>

//////////////////////////////////////////////////
//// Parameter implementation ///////////////////
/////////////////////////////////////////////////

bool the::Parameter::StoreParsedXml = true;

the::Parameter::Parameter()
    : mParent(nullptr), mRelativeTo(nullptr), mNoParent(false),
    mRequired(false), mLocal(false), mHadParent(false), mParsed(false)
{
}
the::Parameter::~Parameter()
{
    if (!mNoParent)
    {
        ASSERT(mHadParent,
           "Unregistered parameter '%s'"
           " Register it via REGISTER"
           " or mark as parameter without parent by calling setNoParent.", mXmlName);
    }
}

void the::Parameter::setParent(Parameter* parent)
{
    mParent = parent;
    if (parent != nullptr)
    {
        mHadParent = true;
    }
}

the::Parameter* the::Parameter::getParent() const
{
    return mParent;
}

the::GameEntity* the::Parameter::tryFindParentEntity() const
{
    if (getParent() != nullptr) return getParent()->tryFindParentEntity();
    else return nullptr;
}

bool the::Parameter::getLocked() const
{
    if (mParent != nullptr) return mParent->getLocked();
    else return false;
}

bool the::Parameter::getRequired() const
{
    // If parameter present in xml, then it is required by user
    if (mParsed) return true;

    // If I or some parent is not required, then I not required too
    if (mRequired == false) return false;
    else
    {
        if (mParent != nullptr) return mParent->getRequired();
        else return true;
    }
}


the::Parameter& the::Parameter::setRequired(bool required)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    mRequired = required;

    return *this;
}

the::Parameter& the::Parameter::setXmlName(string name)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    mXmlName = name;
    return *this;
}

void the::Parameter::setNoParent()
{
    mNoParent = true;
    mParent = nullptr;
}

the::Parameter& the::Parameter::setLocal(Local local)
{
    mLocal = local == LOCAL;
    return *this;
}

the::Parameter& the::Parameter::setRelativeTo(Parameter* relativeTo)
{
    mRelativeTo = relativeTo;
    return *this;
}


the::string the::Parameter::getXmlName() const
{
    return mXmlName;
}

the::string the::Parameter::formXmlName(string fieldName) const
{
    string xmlName = fieldName;
    if (xmlName.size() > 3 && xmlName[0] == 'p' && xmlName[1] == 'r' && xmlName[2] == 'm')
    {
        xmlName = xmlName.substr(3, xmlName.size()-3);
    }
    if (xmlName.size() > 2 && xmlName[0] == 't' && xmlName[1] == 'o')
    {
        xmlName = xmlName.substr(2, xmlName.size()-2);
    }
    return xmlName;
}

bool the::Parameter::getLocal()
{
    return findLocalRec(this);
}

the::Parameter* the::Parameter::getRelativeTo()
{
    return findRelativeToRec(this);
}

void the::Parameter::setParsed(bool parsed)
{
    mParsed = parsed;
}
bool the::Parameter::getParsed() const
{
    return mParsed;
}

the::Parameter* the::Parameter::findRelativeToRec(Parameter* current)
{
    if (current->mRelativeTo != nullptr)
    {
        return current->mRelativeTo;
    }
    else
    {
        if (current->getParent() != nullptr)
        {
            findLocalRec(current->getParent());
        }
        else
        {
            return nullptr;
        }
    }
    return nullptr; // stupid compile warning: control reaches end of non-void function
}
bool the::Parameter::findLocalRec(Parameter* current)
{
    if (current->mLocal)
    {
        return true;
    }
    else
    {
        if (current->getParent() != nullptr)
        {
            return findLocalRec(current->getParent());
        }
        else
        {
            return false;
        }
    }
}

//////////////////////////////////////////////////
//// ValueParameter implementation //////////////
//////////////////////////////////////////////////



the::ValueParameter::ValueParameter(): mInOnParse(false)
{
}
the::ValueParameter::~ValueParameter()
{
}

bool the::ValueParameter::parse(string& strValue)
{
    ASSERT(!mInOnParse, "Looks like 'parse' recursivelly called from 'onParse'."
        "Check, if you call in your override of onParse BaseParameter::onParse, not BaseParameter::parse.");

    mInOnParse = true;
    bool ok = onParse(strValue);
    mInOnParse = false;
    postParse(strValue, ok);
    return ok;
}

void the::ValueParameter::postParse(string& strVal, bool& parsedOk)
{
    if (StoreParsedXml)
    {
        mParsedXml = strVal;
    }
    setParsed(true);
    SigParsed(strVal, parsedOk);
}

bool the::ValueParameter::isGroup() const
{
    return false;
}

the::ParameterTypes the::ValueParameter::getType() const
{
    return USER_VALUE_PARAM;
}

the::string the::ValueParameter::getParsedXml()
{
    return mParsedXml;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//// ParameterGroup implementation ///////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

the::ParameterGroup::ParameterGroup(): mInOnParse(false)
{
}
the::ParameterGroup::~ParameterGroup()
{
}

the::ParameterGroup* the::ParameterGroup::getParentGroup() const
{
    return dynamic_cast<ParameterGroup*>(mParent);
}

bool the::ParameterGroup::isMyTag(TiXmlElement *element)
{
    return compareCaseInsensitive(getXmlName(),  element->ValueStr());
}

bool the::ParameterGroup::isGroup()const
{
    return true;
}

const std::vector<the::Parameter*>& the::ParameterGroup::getItems() const
{
    return mItems;
}
void the::ParameterGroup::addItem(Parameter* item)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    if (item->getParent() == nullptr)
    {
        item->setParent(this);
    }

    mItems.push_back(item);
}
void the::ParameterGroup::addItem(std::shared_ptr<Parameter> param)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    mStoredItems.push_back(param);
    addItem(param.get());
}
void the::ParameterGroup::removeItem(Parameter* item)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    auto itemIt = std::find(mItems.begin(), mItems.end(), item);
    mItems.erase(itemIt);

    // Remove item from mStoredItems, if it contrains item
    auto it = mStoredItems.begin();
    for(; it != mStoredItems.end(); it++)
    {
        if (it->get() == item)
        {
            break;
        }
    }
    if(it != mStoredItems.end())
    {
        mStoredItems.erase(it);
    }
}

void the::ParameterGroup::clear()
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    mItems.clear();
    mStoredItems.clear();
}

void the::ParameterGroup::resetAll()
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    for (Parameter* child : getItems())
    {
        if (child->isGroup())
        {
            ParameterGroup* childParam = static_cast<ParameterGroup*>(child);
            childParam->resetAll();
        }
        else if (child->isGroup())
        {
            ValueParameter* childParam = static_cast<ValueParameter*>(child);
            childParam->reset();
        }
    }
}

void the::ParameterGroup::registerChild(Parameter* p, string xmlName)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    ASSERT(p != nullptr, "Invalid argument.");

    addItem(p);
    p->setParent(this);
    p->setXmlName(formXmlName(xmlName));
}

void the::ParameterGroup::unregisterChild(Parameter* p)
{
    removeItem(p);
}

std::list<the::ParsingError> the::ParameterGroup::parse(TiXmlElement* el)
{
    ASSERT(!mInOnParse, "Looks like 'parse' recursivelly called from 'onParse'."
        "Check, if you call in your override of onParse BaseParameter::onParse, not BaseParameter::parse.");

    mInOnParse = true;
    std::list<the::ParsingError> errs = onParse(el);
    mInOnParse = false;

    postParse(el, errs);
    return errs;
}


std::list<the::ParsingError> the::ParameterGroup::onParse(TiXmlElement* el)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    ASSERT(isMyTag(el), "Expected '%s', got '%s'", getXmlName(), el->ValueStr());


    setParsed(true);

    el->SetUserData(NODE_VIEWED);

    std::list<ParsingError> errors;

    for (Parameter* child : getItems())
    {
        if (!child->getParsed())
        {
            if (!child->isGroup()) // Value Parameter
            {
                ValueParameter* childParam = static_cast<ValueParameter*>(child);
                string paramName = childParam->getXmlName();
                std::transform(paramName.begin(), paramName.end(), paramName.begin(), ::tolower);

                const char* strVal = nullptr;

                // Look for attribute
                for (TiXmlAttribute* attr = el->FirstAttribute(); attr != nullptr; attr = attr->Next())
                {
                    string attrName = attr->Name();
                    std::transform(attrName.begin(), attrName.end(), attrName.begin(), ::tolower);
                    if (attrName == paramName && attr->GetUserData() != NODE_VIEWED)
                    {
                        strVal = attr->Value();
                        attr->SetUserData(NODE_VIEWED);
                        break;
                    }
                }

                // If there is no attribute, look child element
                if(strVal == nullptr)
                {
                    for (TiXmlElement* childElem = el->FirstChildElement();
                        childElem != nullptr; childElem = childElem->NextSiblingElement())
                    {
                        string elemName = childElem->ValueStr();
                        std::transform(elemName.begin(), elemName.end(), elemName.begin(), ::tolower);
                        if (elemName == paramName && childElem->GetUserData() != NODE_VIEWED)
                        {
                            strVal = childElem->GetText();
                            childElem->SetUserData(NODE_VIEWED);
                            break;
                        }
                    }
                    }

                if (strVal != nullptr)
                {
                    string str = strVal;
                    bool ok = childParam->parse(str);
                    if (!ok)
                    {
                        errors.push_back({el->ValueStr(), true,
                            "Value '" + the::string(strVal) + "' has invalid format.", el->Row()});
                    }
                }
                else if (childParam->getRequired())
                {
                    errors.push_back({el->ValueStr(), true,
                        "Missed required property '" + childParam->getXmlName() + "'.", el->Row()});
                }

            }
            else // Group parameter
            {
                ParameterGroup* childParam = static_cast<ParameterGroup*>(child);
                TiXmlElement* childParamElem = nullptr;
                string paramName = childParam->getXmlName();
                std::transform(paramName.begin(), paramName.end(), paramName.begin(), ::tolower);

                for (TiXmlElement* childElem = el->FirstChildElement();
                        childElem != nullptr; childElem = childElem->NextSiblingElement())
                {
                    string elemName = childElem->ValueStr();
                    std::transform(elemName.begin(), elemName.end(), elemName.begin(), ::tolower);
                    if (elemName == paramName && childElem->GetUserData() != NODE_VIEWED)
                    {
                        childParamElem = childElem;
                        childElem->SetUserData(NODE_VIEWED);
                        break;
                    }
                }
                if (childParamElem != nullptr)
                {
                    auto childErrors = childParam->parse(childParamElem);
                    errors.splice(errors.begin(), childErrors);
                }
                else if (childParam->getRequired())
                {
                    errors.push_back({el->ValueStr(), true,
                        "Missed required element '" + childParam->getXmlName() + "'.", el->Row()});
                }
            }
        }
    }

    // Note if there not recognized nodes
    for (TiXmlAttribute* attr = el->FirstAttribute(); attr != nullptr; attr = attr->Next())
    {
        if (attr->GetUserData() != NODE_VIEWED)
        {
            errors.push_back({el->ValueStr(), false,
                    "Attribute '" + string(attr->Name()) + "' is not recognized by any parameter.", el->Row()});
        }
    }
    for (TiXmlElement* childElem = el->FirstChildElement();
        childElem != nullptr; childElem = childElem->NextSiblingElement())
    {
        if (childElem->GetUserData() != NODE_VIEWED)
        {
            errors.push_back({childElem->ValueStr(), false,
                    "Element '" + childElem->ValueStr() + "' is not recognized by any parameter.", childElem->Row()});
        }
    }

    return errors;
}

TiXmlElement* the::ParameterGroup::serialize()
{
    TiXmlElement* root = new TiXmlElement(getXmlName());
    bool containsSmth = false;
    for (Parameter* child : getItems())
    {
        if (!child->isGroup())
        {
            ValueParameter* childParam = static_cast<ValueParameter*>(child);

            if (childParam->modified())
            {
                TiXmlElement* childElem = new TiXmlElement(childParam->getXmlName());
                TiXmlText * childElemText = new TiXmlText(childParam->serialize());
                childElem->LinkEndChild(childElemText);
                root->LinkEndChild(childElem);
                containsSmth = true;
            }
        }
        else
        {
            ParameterGroup* childParam = static_cast<ParameterGroup*>(child);
            TiXmlElement* childElem = childParam->serialize();
            if (childElem != nullptr)
            {
                root->LinkEndChild(childElem);
                containsSmth = true;
            }
        }
    }

    return (containsSmth)? root : nullptr;
}

void the::ParameterGroup::postParse(TiXmlElement* el, std::list<ParsingError>& err)
{
    setParsed(true);
    mParsingErrors = err;
    if (StoreParsedXml)
    {
        mParsedXml = std::shared_ptr<TiXmlElement>(el->Clone()->ToElement());
    }
    SigParsed(el, err);
}

the::ParameterTypes the::ParameterGroup::getType() const
{
    return USER_GROUP_PARAM;
}

void the::ParameterGroup::readDataFrom(const Parameter* src)
{
    ASSERT(src->isGroup(), "Parameter group can't read data from value parameter.");
    const ParameterGroup* srcParam = static_cast<const ParameterGroup*>(src);

    for (int i = 0; i< (int)std::min(mItems.size(), srcParam->mItems.size()); i++)
    {
        mItems[i]->readDataFrom(srcParam->mItems[i]);
    }
}

const std::list<the::ParsingError>& the::ParameterGroup::getParsingErrors()
{
    return mParsingErrors;
}

std::shared_ptr<const TiXmlElement> the::ParameterGroup::getParsedXml()
{
    ASSERT(StoreParsedXml, "Storing parsed xml disabled. Set Parameter::StoreParsedXml to enable.");
    return mParsedXml;
}

////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
// GameEntityInitData implementation

the::GameEntityInitData::GameEntityInitData(GameEntity* parent): mParentEntity(parent)
{
    setNoParent();
}

the::GameEntityInitData::~GameEntityInitData()
{
}

the::ParameterTypes the::GameEntityInitData::getType() const
{
    return GAME_ENTITY_INIT_DATA_PARAM;
}

bool the::GameEntityInitData::getLocked() const
{
    return (mParentEntity == nullptr)? false : (mParentEntity->isParametersLocked());
}

the::GameEntity* the::GameEntityInitData::tryFindParentEntity() const
{
    return mParentEntity;
}
void the::GameEntityInitData::setParentEntity(GameEntity* ent)
{
    mParentEntity = ent;
}

std::list<the::ParsingError> the::GameEntityInitData::onParse(TiXmlElement* element)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    ASSERT(isMyTag(element), "Expected '%s', got '%s'", getXmlName(), element->ValueStr());

    setParsed(true);

    element->SetUserData(NODE_VIEWED);

    std::list<ParsingError> errors;

    if (mParentEntity != nullptr)
    {
        for (GameEntity* child : mParentEntity->getChildren())
        {
            IXmlParser* childParser = child->getParser();
            if (childParser != nullptr)
            {
                TiXmlElement* childParamElem = nullptr;
                for (TiXmlElement* childElem = element->FirstChildElement();
                        childElem != nullptr; childElem = childElem->NextSiblingElement())
                {
                    if (childParser->isMyTag(childElem))
                    {
                        childParamElem = childElem;
                        break;
                    }
                }
                if (childParamElem != nullptr)
                {
                    childParamElem->SetUserData(NODE_VIEWED); ///  \todo When attached will be normal, palce after parse call
                    auto childErrors = childParser->parse(childParamElem);
                    errors.splice(errors.begin(), childErrors);
                }
                else if (childParser->required())
                {
                    errors.push_back({element->ValueStr(), true,
                        "Missed required element for '" + child->getPathString() + "'.", element->Row()});
                }
            }
        }
    }

    auto baseErrs = ParameterGroup::onParse(element);
    errors.splice(errors.begin(), baseErrs);

    return errors;
}
TiXmlElement* the::GameEntityInitData::serialize()
{
    TiXmlElement* root = ParameterGroup::serialize();
    if (root == nullptr)
    {
        root = new TiXmlElement(getXmlName());
    }

    if (mParentEntity != nullptr)
    {
        for (GameEntity* child : mParentEntity->getChildren())
        {
            if (child->getParser() != nullptr)
            {
                TiXmlElement* childElem = child->getParser()->serialize();
                if (childElem != nullptr)
                {
                    root->LinkEndChild(childElem);
                }
            }
        }
    }

    bool containsSmth = root->FirstChildElement() != nullptr || root->FirstAttribute() != nullptr;
    return (containsSmth)? root : nullptr;
}

////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////

the::ParameterList::ParameterList(string itemTagName)
{
    setItemTagName(itemTagName);
}

the::ParameterList::ParameterList(): ParameterList("Item")
{
}

the::ParameterList::~ParameterList()
{
}

the::ParameterList& the::ParameterList::setItemTagName(string itemTagName)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    mItemTagName = itemTagName;
    return *this;
}

the::string the::ParameterList::getItemTagName()
{
    return mItemTagName;
}

int the::ParameterList::size() const
{
    return mItems.size();
}

std::list<the::ParsingError> the::ParameterList::onParse(TiXmlElement* el)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    ASSERT(isMyTag(el), ":(");

    setParsed(true);

    std::list<ParsingError> errors;

    ParameterGroup::clear();

    for (TiXmlElement* childElem = el->FirstChildElement();
            childElem != nullptr; childElem = childElem->NextSiblingElement())
    {
        string elemNameLower = childElem->ValueStr();
        std::transform(elemNameLower.begin(), elemNameLower.end(), elemNameLower.begin(), ::tolower);
        string itemTagNameLower = mItemTagName;
        std::transform(itemTagNameLower.begin(), itemTagNameLower.end(), itemTagNameLower.begin(), ::tolower);
        if (elemNameLower == itemTagNameLower)
        {
            std::shared_ptr<Parameter> item = createItem();
            item->setXmlName(mItemTagName);
            if(item->isGroup())
            {
                ParameterGroup* groupItem = static_cast<ParameterGroup*>(item.get());
                auto itemErrors = groupItem->parse(childElem);
                errors.splice(errors.begin(), itemErrors);
            }
            else
            {
                ValueParameter* valueItem = static_cast<ValueParameter*>(item.get());
                string strVal = childElem->GetText();
                bool ok = valueItem->parse(strVal);
                if (!ok)
                {
                    errors.push_back({el->ValueStr() + "/" + childElem->ValueStr(), true,
                        "Value '" + strVal + "' has invalid format.", el->Row()});
                }
            }
            item->setRequired(mRequired);
            addItem(item);
        }
        else
        {
            errors.push_back({el->ValueStr(), true,
                        "Ignored element '" + childElem->ValueStr() + "'", el->Row()});
        }
    }

    return errors;
}

void the::ParameterList::readDataFrom(const Parameter* src)
{
    ASSERT(dynamic_cast<const ParameterList*>(src) != 0, "Invalid source parameter type.");

    const ParameterList* srcParam = static_cast<const ParameterList*>(src);
    clear();
    for (Parameter* srcChild : srcParam->getItems())
    {
        std::shared_ptr<Parameter> item = createItem();
        item->setXmlName(mItemTagName);
        item->readDataFrom(srcChild);
        addItem(item);
    }
}

the::ParameterTypes the::ParameterList::getType() const
{
    return LIST_PARAM;
}


the::MappedValueParameter::~MappedValueParameter()
{
}
