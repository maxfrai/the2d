////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/BindService.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/LogService.hpp>
#include <The2D/GameEntity.hpp>

using namespace the;

BindService::BindService()
    :  mDebugOutputLevel(-1)
{
    REGISTER(atIncomingBindings);
}

BindService::~BindService()
{
}

ParsedPath BindService::parsePath(string _path, GameEntity* relativeTo)
{
    string path = _path;
    trim(path);
    std::replace(path.begin(), path.end(), '\\', '/');

    ParsedPath parsedPath;

    if (path.empty())
    {
        parsedPath.searchRoot = relativeTo;
        return parsedPath;
    }

    string& strPath = path;
    std::vector<string> itemsStrings;
    itemsStrings.reserve(5);
    split(strPath, '/', itemsStrings);
    parsedPath.pathItems.reserve(itemsStrings.size()+1);

    // First path item should be search root.
    // Search root is nearest GameTree or core()->root() if '~' specified.
    parsedPath.searchRoot = nullptr;
    string rootRawString;
    if (itemsStrings[0] == "~")
    {
        parsedPath.searchRoot = core()->tree();
        rootRawString = "~";
    }
    else if (itemsStrings[0] == ".")
    {
        parsedPath.searchRoot = relativeTo;
        rootRawString = ".";
    }
    else
    {
        // Find parent GameTree
        GameEntity* current = relativeTo->getParent();
        if (current != nullptr)
        {
            while (parsedPath.searchRoot == nullptr)
            {
                if (core()->rtti()->is<GameTree>(current)) parsedPath.searchRoot = current;
                else current = current->getParent();
            }
        }
        else
        {
            parsedPath.searchRoot = core()->tree();
        }
    }
    // Add root as first item
    {
        parsedPath.pathItems.emplace_back(parsedPath.searchRoot->getNameHash(), parsedPath.searchRoot->getGroupsHashes(), type(parsedPath.searchRoot).nameHash);
        parsedPath.pathItems.back().rawString = rootRawString;
    }

    for (string& itemStr : itemsStrings)
    {
        if (itemStr == "~" || itemStr == ".")
        {
            // Already added
        }
        else if (itemStr == "..")
        {
            PathItem item(false);
            item.goParent = true;
            item.rawString = "..";
            parsedPath.pathItems.push_back(item);
        }
        else if (itemStr == "?")
        {
            parsedPath.pathItems.emplace_back(false);
            parsedPath.pathItems.back().rawString = "?";
        }
        else if (itemStr.empty()) // construction '//'
        {
            parsedPath.pathItems.emplace_back(true);
            parsedPath.pathItems.back().rawString = "//";
        }
        else
        {
            // Path item has format name(type:Type,group:group)
            // name or braces can be skipped, in braces all elements also not required

            trim(itemStr);

            if (itemStr.size() > 0)
            {
                // Find name part and braces
                string namePart;
                string bracesPart; // this variable will contains braces part, without braces symbols ('(', ')')
                // Skipped name
                if (itemStr[0] == '(')
                {
                    if (itemStr[itemStr.size() -1] == ')')
                    {
                        bracesPart = itemStr;
                        bracesPart[bracesPart.size() -1] = ' ';
                        bracesPart[0] = ' ';
                    }
                    else
                    {
                        ERR("Missed closing brace in item '%s' of path '%s'", itemStr, _path);
                        break;
                    }
                }
                else
                {
                    std::vector<string> itemParts;
                    itemParts.reserve(3);
                    split(itemStr, '(', itemParts);
                    // Specified name and braces
                    if (itemParts.size() == 2)
                    {
                        namePart = itemParts[0];
                        bracesPart = itemParts[1];
                        if (bracesPart[bracesPart.size() -1] == ')')
                        {
                            bracesPart[bracesPart.size() -1] = ' ';
                            bracesPart[0] = ' ';
                        }
                        else
                        {
                            ERR("Looks like in path item %s there is not closing brace", itemStr);
                            break;
                        }
                    }
                    else if (itemParts.size() == 1) // Skipped braces
                    {
                        namePart = itemParts[0];
                    }
                    else
                    {
                        ERR("Invalid path item %s. Split by '(' should return 1 or 2 elements, but returned %i",
                                itemStr, itemParts.size());
                        break;
                    }
                }

                PathItem item(false);
                item.rawString = itemStr;
                trim(namePart);
                if (!namePart.empty())
                {
                    item.name = getHashCode(namePart);
                }
                // Parse braces
                std::vector<string> bracesParts;
                bracesParts.reserve(2);
                split(bracesPart, ',', bracesParts);
                for (string& part : bracesParts)
                {
                    std::vector<string> nameValue;
                    nameValue.reserve(2);
                    split(part, ':', nameValue);
                    if (nameValue.size() == 2)
                    {
                        trim(nameValue[0]);
                        trim(nameValue[1]);
                        if (compareCaseInsensitive(nameValue[0], "group"))
                        {
                            std::vector<string> groups;
                            groups.reserve(2);
                            split(nameValue[1], ';', groups);
                            item.groups.reserve(groups.size());
                            for (string& groupStr : groups)
                            {
                                trim(groupStr);
                                item.groups.push_back(getHashCode(groupStr));
                            }
                        }
                        else if (compareCaseInsensitive(nameValue[0], "type"))
                        {
                            item.type = getHashCode(nameValue[1]);
                        }
                    }
                    else
                    {
                        ERR("Invalid syntax name:value in braces of item '%s' of path '%s'", itemStr, _path);
                        break;
                    }
                }

                parsedPath.pathItems.push_back(item);
            }
            else
            {
                ERR("Invalid '/ /' construction in path '%s'", _path);
                break;
            }
        }
    }

    return parsedPath;
}

eastl::list<GameEntity*> BindService::findAll(ParsedPath path, std::function<bool(GameEntity* item)> visitor, string* debugLog)
{
    mDebugOutputLevel = -1;
    mMaxDeep = 0;

    eastl::list<GameEntity*> result;
    if (debugLog && !path.pathItems.empty())
    {
        *debugLog += format("Search root: %s\n", path.searchRoot->getPathString());
    }

    _findAll(path, 0, path.searchRoot, result, visitor, debugLog);

    if (debugLog && !path.pathItems.empty())
    {
        *debugLog += format("\n\n**Maximum deep: [%i]**\n", mMaxDeep);
    }

    return result;
}
bool BindService::matchPath(GameEntity* ent, ParsedPath path, string* debugLog)
{
    mDebugOutputLevel = -1;

    return _matchPath(path, path.pathItems.size() -1, ent, debugLog);
}

bool BindService::_findAll(
    ParsedPath& pathItems,
    int current,
    GameEntity* object,
    eastl::list<GameEntity*>& result,
    std::function<bool(GameEntity* item)>& visitor,
    string* debugLog)
{
    if (pathItems.pathItems.size() == 0) return false;

    mDebugOutputLevel++;
    mMaxDeep = std::max(mMaxDeep, mDebugOutputLevel);
    
    bool found = false;
    PathItem& curItem = pathItems[current];

    std::string debugData;
    debugData.reserve(80);
    if (debugLog)
    {
        *debugLog +=
            format("[%i]%sLook at '%s' for '%s' - ",
                mDebugOutputLevel,
                std::string(mDebugOutputLevel*4, '.'),
                object->getShortInfoString(),
                curItem.rawString
            );
    }

    bool mathNameGroupAndType = true;
    bool mathName = true; 
    bool mathGroup = true;
    bool mathType = true;
    bool goChildrenRec = pathItems[current].goChildrenRec;
    bool goParent = pathItems[current].goParent;
    if (!goChildrenRec && !goParent)
    {
        // Check type
        if (curItem.type != INVALID_HASH && curItem.type != object->getTypeInfo().nameHash)
        {
            mathNameGroupAndType = false;
            mathType = false;
        }
        // Check groups
        for (auto& group : curItem.groups)
        {
            if (!object->inGroup(group))
            {
                mathNameGroupAndType = false;
                mathGroup = false;
                break;
            }
        }
        // Check name
        if (curItem.name != INVALID_HASH && curItem.name != object->getNameHash())
        {
            mathNameGroupAndType = false;
            mathName = false;
        }
    }
    else
    {
        mathNameGroupAndType = false;
    }

    if (mathNameGroupAndType || goChildrenRec || goParent)
    {
        if (debugLog)
        {
            *debugLog += " match!\n";
        }

        if (current+1 < (int)pathItems.pathItems.size())
        {
            if (goParent)
            {
                if (object->getParent() != nullptr)
                {
                    if (pathItems[current+1].goParent)
                    {
                        found = _findAll(pathItems, current+1, object->getParent(), result, visitor, debugLog);
                    }
                    else
                    {
                        auto children = object->getParent()->getChildren();
                        for (auto childIt = children.begin(); childIt != children.end(); childIt++)
                        {
                            GameEntity* child = *childIt;
                            found = _findAll(pathItems, current+1, child, result, visitor, debugLog);
                            if (found) break;
                        }
                    }
                }
                else
                {
                    if (debugLog)
                    {
                        *debugLog += "there is no parent!\n";
                    }
                }
            }
            else if (!found && pathItems[current+1].goParent)
            {
                found = _findAll(pathItems, current+1, object, result, visitor, debugLog);
            }
            else if (!found)
            {
                if (goChildrenRec)
                {
                    found = _findAll(pathItems, current+1, object, result, visitor, debugLog);
                }
                if (!found)
                {
                    auto children = object->getChildren();
                    for (auto childIt = children.begin(); childIt != children.end(); childIt++)
                    {
                        GameEntity* child = *childIt;
                        found = _findAll(pathItems, current+1, child, result, visitor, debugLog);
                        if (found)
                        {
                            break;
                        }
                        else if (goChildrenRec)
                        {
                            // Dots ('..') may mean any count of nested items
                            found = _findAll(pathItems, current, child, result, visitor, debugLog);
                            if (found)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            GameEntity* res = object;
            if (goParent)
            {
                res = object->getParent();
            }
            
            result.push_back(res);

            found = !visitor(res);
            if (debugLog)
            {
                if (found)
                {
                    *debugLog += format("%sItem Added!!! %s %s type\n",
                        std::string(mDebugOutputLevel*4, '.'),
                        res->getName(),
                        res->getType()
                    );
                }
                else
                {
                    *debugLog += format("%s Declined by visitor...\n",
                        std::string(mDebugOutputLevel*4, '.')
                    );
                }
            }
        }
    }
    else
    {
        if (debugLog)
        {
            *debugLog += " decline: ";
            if (!mathName) *debugLog += "invalid name; ";
            if (!mathType) *debugLog += "invalid type; ";
            if (!mathGroup) *debugLog += "invalid group;";
            *debugLog += "\n";
        }
    }

    mDebugOutputLevel--;
    return found;
}
bool BindService::_matchPath(ParsedPath& pathItems, int current, GameEntity* object, string* debugLog)
{
    if (pathItems.pathItems.empty()) return false;

    bool match = false;
    if (debugLog)
    {
        *debugLog += format("%sLook at '%s' of '%s' for '%s' \n",
            std::string(mDebugOutputLevel*4, '.'),
            object->getName(),
            object->getType(),
            pathItems[current].rawString
        );
    }
    PathItem& curItem = pathItems[current];

    bool mathNameGroupAndType = true;
    bool goChildrenRec = pathItems[current].goChildrenRec;
    bool goParent = pathItems[current].goParent;
    if (!goChildrenRec && !goParent)
    {
        // Check type
        if (curItem.type != INVALID_HASH && curItem.type != object->getTypeInfo().nameHash)
        {
            mathNameGroupAndType = false;
        }
        // Check groups
        for (auto& group : curItem.groups)
        {
            if (!object->inGroup(group))
            {
                mathNameGroupAndType = false;
                break;
            }
        }
        // Check name
        if (curItem.name != INVALID_HASH && curItem.name != object->getNameHash())
        {
            mathNameGroupAndType = false;
        }
    }
    else
    {
        mathNameGroupAndType = false;
    }


    if (mathNameGroupAndType || goChildrenRec || goParent)
    {
        if (debugLog)
        {
            *debugLog += " match!\n";
        }
        if (current-1 >= 0)
        {
            if (goParent)
            {
                // We are going from back to front, so goParent mean go child
                auto children = object->getChildren();
                for (auto childIt = children.begin(); childIt != children.end(); childIt++)
                {
                    GameEntity* child = *childIt;
                    match = _matchPath(pathItems, current-1, child, debugLog);
                    if (match)
                    {
                        break;
                    }
                }
            }
            else
            {
                if (goChildrenRec)
                {
                    match = _matchPath(pathItems, current-1, object, debugLog);
                }
                GameEntity* parent = object->getParent();
                if (parent != nullptr)
                {
                    match = _matchPath(pathItems, current-1, parent, debugLog);
                    if (!match && goChildrenRec)
                    {
                        match = _matchPath(pathItems, current, parent, debugLog);
                    }
                }
            }
        }
        else
        {
            if (object == pathItems.searchRoot)
            {
                if (debugLog)
                {
                    *debugLog += format("%sEntity match!! %s %s type\n",
                        std::string(mDebugOutputLevel*4, '.'),
                        object->getName(),
                        object->getType()
                    );
                }
                match = true;
            }
        }
    }
    else
    {
        if (debugLog)
        {
            *debugLog +=" decline...\n";
        }
    }
    return match;
}