////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StatisticService.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/LogService.hpp>
#include <The2D/GameTree.hpp>


the::StatisticTarget::~StatisticTarget()
{
}

the::string the::StatisticTarget::getShortInfoString() const
{
    return "unknown";
}
the::string the::StatisticTarget::getDetailedInfoString() const
{
    return "unknown";
}

the::StatisticService::StatisticService(InfrastructureCore* core)
    : mCore(core), mStatistic(1, 1024, 1024), mStatisticEnabled(false)
{
}

the::StatisticService::~StatisticService()
{
}

void the::StatisticService::resetData()
{
    ASSERT(mStatisticEnabled, "Call beginStatisticsGathering first");

    mStatistic.clear();

    beginEntry(mCore->tree(), "root");
    mStatistic.front().parentEntry = nullptr;
}

bool the::StatisticService::statisticsIsGathering()
{
    return mStatisticEnabled;
}

void the::StatisticService::beginStatisticsGathering()
{
    ASSERT(!statisticsIsGathering(), "Sorry, but calling several times beginStatisticsGathering without endStatisticsGathering not supported.");

    mStatistic.clear();
    mStatistic.defragAndShrink();

    mStatisticEnabled = true;
    beginEntry(mCore->tree(), "root");
    mStatistic.front().parentEntry = nullptr;
}
void the::StatisticService::endStatisticsGathering()
{
    if (statisticsIsGathering())
    {
        endEntry(mCore->tree(), "root");
    }
}

the::ChunkList<the::StatisticEntry>& the::StatisticService::getStatistics()
{
    return mStatistic;
}

the::StatisticService::ResultTree::ResultTree()
    : averageElapsedMilisec(0), minElapsedMilisec(99999999999), maxElapsedMilisec(0),
      count(0), target(nullptr), parent(nullptr)
{
}

void the::StatisticService::ResultTree::accept(std::function<void (ResultTree* cur, int level)> visitor, int level)
{
    visitor(this, level);
    for (auto& child : children)
    {
        child->accept(visitor, level+1);
    }
}

std::shared_ptr<the::StatisticService::ResultTree> the::StatisticService::calculateResultTree(StatisticTarget* _target)
{
    auto target = _target;
    if (_target == nullptr)
    {
        target = mCore->tree();
    }

    using namespace eastl;
    typedef std::shared_ptr<ResultTree> ResultTreePtr;

    map<StatisticTarget*, map<string, ResultTreePtr>> nodes;

    auto derivedFromTarget = [&](StatisticEntry& entry){
        for (StatisticEntry* en = &entry; en != nullptr; en = en->parentEntry)
        {
            if (en->target == target) return true;
        }
        return false;
    };

    for (ChunkItem<StatisticEntry>* item = mStatistic.begin(); item != mStatistic.end(); item = item->next())
    {
        if (derivedFromTarget(item->data))
        {
            // Find target tags map, or create new one, if not exists
            map<string, ResultTreePtr>& tagsMap = nodes[item->data.target];

            // Find tag entry in target, or create new one, if not exists
            ResultTreePtr& tagEntry = tagsMap[item->data.tag];
            if (tagEntry.get() == nullptr)
            {
                tagEntry = ResultTreePtr(new ResultTree());
                tagEntry->tag = item->data.tag;
                tagEntry->target = item->data.target;
                if (item->data.parentEntry != nullptr)
                {
                    tagEntry->parentTag = item->data.parentEntry->tag;
                    tagEntry->parentTarget = item->data.parentEntry->target;
                }
            }

            double elapsed = item->data.elapsedMilisec();
            tagEntry->averageElapsedMilisec += elapsed;
            tagEntry->count++;
            if (elapsed > tagEntry->maxElapsedMilisec) tagEntry->maxElapsedMilisec = elapsed;
            if (elapsed < tagEntry->minElapsedMilisec) tagEntry->minElapsedMilisec = elapsed;
        }
    }

    std::list<ResultTreePtr> allEntries;
    std::list<ResultTree*> allEntriesRaw;
    for (auto& targetRoot : nodes)
    {
        for (auto& tagEntry : targetRoot.second)
        {
            allEntries.push_back(tagEntry.second);
            allEntriesRaw.push_back(tagEntry.second.get());
        }
    }

    // Calculate average
    for (ResultTreePtr entry : allEntries)
    {
        entry->averageElapsedMilisec = entry->averageElapsedMilisec / float(entry->count);
    }

    // Build tree
    for (ResultTreePtr entry : allEntries)
    {
        for (ResultTreePtr child : allEntries)
        {
            if (child->parentTarget == entry->target && child->parentTag == entry->tag)
            {
                entry->children.push_back(child);
                child->parent = entry.get();
            }
        }
    }

    // Find root
    ResultTreePtr root;
    for (ResultTreePtr entry : allEntries)
    {
        if (entry->parent == nullptr)
        {
            root = entry;
            break;
        }
    };



    ASSERT(root->target == target, "Smth wrong..");

    return root;
}

