////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/AttachedService.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/LogService.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/AttachedBase.hpp>
#include <The2D/GameTree.hpp>
#include <The2D/Rtti.hpp>

using namespace the;

AttachedService::AttachedService(InfrastructureCore* core)
    :  mCore(core)
{
    core->rtti()->SigTypeRegistered.add(std::bind(
        &AttachedService::spreadAttachedDownToHierarchyRec,
        this, std::placeholders::_1));
}

AttachedService::~AttachedService()
{
}

void AttachedService::registerProperty(AttachedBase* prop)
{
    if (prop->isStatic())
    {
        StaticAttachedID id = {getHashCode(prop->getStaticAttachedParentType()), getHashCode(prop->getName()),
            getHashCode(prop->getValueType()), getHashCode(prop->getTargetType()) };

        // Find, if there static attached with such signature
        StaticAttachedInfo* found = nullptr;
        for (auto it = mStaticInfo.begin(); it != mStaticInfo.end(); it++)
        {
            if (it->first == id)
            {
                found = &it->second;
                break;
            }
        }
        // If found, use its source
        if (found != nullptr)
        {
            prop->setSource(found->source);
            prop->setID(found->id, found->classID);
        }
        else
        {
            prop->createSource();
            StaticAttachedInfo info = { prop->getSource(),  prop->getSource()->getID(),  prop->getSource()->getClassID() };
            mStaticInfo.push_back(eastl::make_pair(id, info));
        }
    }
    else
    {
        // Determine class
        int classID = -1;
        for (int i=0; i < (int)mClasses.size(); i++)
        {
            if (mClasses[i] == prop->getTargetType())
            {
                classID = i;
                break;
            }
        }
        if (classID < 0)
        {
            mClasses.push_back(prop->getTargetType());
            classID = mClasses.size()-1;
        }

        eastl::vector<AttachedBase*>& props = mProperties[classID];
        props.push_back(prop);
        prop->setID(props.size()-1, classID);
    }
}

void AttachedService::unregisterProperty(AttachedBase* property)
{
    auto it = mProperties.find(property->getClassID());
    ASSERT(it != mProperties.end(), "Invalid classID");
    eastl::vector<AttachedBase*>& props = it->second;
    ASSERT(property->getID() < (int)props.size() && property->getID() >= 0, "Invalid id");

    /// \todo Prevent fragmentation of props
    props[property->getID()] = nullptr;
}

AttachedBase* AttachedService::getAttached(int id, int classID)
{
    auto it = mProperties.find(classID);
    ASSERT(it != mProperties.end(), "Invalid classID");
    eastl::vector<AttachedBase*>& props = it->second;
    ASSERT(id < (int)props.size() && id >= 0, "Invalid id");

    return props[id];
}

void AttachedService::freeAllUnusedMemory()
{
    for (auto it = mProperties.begin(); it != mProperties.end(); it++)
    {
        eastl::vector<AttachedBase*>& props = it->second;
        for (auto at : props)
        {
            if (at != nullptr)
            {
                at->defragAndShrink();
                for (ISupportAttachedInternal* ent : *at->getAttachiers().get())
                {
                    ent->shrinkAttachedIndexes();
                }
            }
        }
    }
}

size_t AttachedService::getTotalUsedMemory()
{
    size_t total = 0;
    for (auto it = mProperties.begin(); it != mProperties.end(); it++)
    {
        eastl::vector<AttachedBase*>& props = it->second;
        for (auto at : props)
        {
            if (at != nullptr)
            {
                total += at->getUsedMemory();
            }
        }
    }
    mCore->tree()->acceptAll([&total](ISupportAttached* ent){ total += ent->getMemoryUsedByAttached(); });
    return total;
}
size_t AttachedService::getTotalWastedMemory()
{
    size_t total = 0;
    for (auto it = mProperties.begin(); it != mProperties.end(); it++)
    {
        eastl::vector<AttachedBase*>& props = it->second;
        for (auto at : props)
        {
            if (at != nullptr)
            {
                total += at->getWastedMemory();
            }
        }
    }
    mCore->tree()->acceptAll([&total](GameEntity* ent){ total += ent->getMemoryWastedByAttached(); });
    return total;
}

const eastl::map<int, eastl::vector<AttachedBase*>>& AttachedService::getProperties()
{
    return mProperties;
}

void the::AttachedService::resetAllAttached(ISupportAttached* obj)
{
    for (auto it = obj->mAttachedIndexes.begin(); it != obj->mAttachedIndexes.end(); it++)
    {
        eastl::vector<ChunkVectorIndex>& indexes = it->second;
        for (int id = 0; id < (int)indexes.size(); id++)
        {
            auto attached = getAttached(id, it->first);
            if (attached != nullptr)
            {
                attached->reset(obj);
            }
        }
        indexes.clear();
        if ((int)indexes.capacity() != ISupportAttached::INITIAL_ATTACHED_INDEXES_CAPACITY)
        {
            indexes.set_capacity(ISupportAttached::INITIAL_ATTACHED_INDEXES_CAPACITY, INVALID_INDEX);
        }
    }
}

void the::AttachedService::spreadAttachedDownToHierarchyRec(const The2DTypeInfo* typeC)
{
    The2DTypeInfo* type = const_cast<The2DTypeInfo*>(typeC);    
    
    for(auto& baseC : typeC->baseTypes)
    {
        for (auto& pair : baseC->mAttachedIndexes)
        {
            auto& baseIndexesVector = pair.second;
            int typeInd = pair.first;
            for (int i = 0; i < (int)baseIndexesVector.size(); i++)
            {
                if (baseIndexesVector[i] != INVALID_INDEX)
                {
                    type->setAttachedIndex(i, typeInd, baseIndexesVector[i]);
                }
            }
        }
    }

    for (auto& derived : typeC->derivedTypes)
    {
        spreadAttachedDownToHierarchyRec(derived);
    }
}