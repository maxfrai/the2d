////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/GameTree.hpp>
#include <The2D/Attached.hpp>
#include <The2D/Component.hpp>
#include <The2D/LogService.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>



the::Attached::Attached()
{
}

the::Attached::~Attached()
{
    for (auto pair : mAttachedProperties)
    {
        delete pair.second;
    }
    mAttachedProperties.clear();
}

std::list<the::ParsingError> the::Attached::parse(TiXmlElement* element)
{
    ASSERT(isMyTag(element), ":(");

    std::list<ParsingError> errors;

     // Parse attached objects
    for (auto objTag = element->FirstChild(); objTag != nullptr; objTag = objTag->NextSibling())
    {
        // Skip this iteration if children isn't an element (comment or smth else)
        if (objTag->Type() != TiXmlNode::TINYXML_ELEMENT) continue;
        TiXmlElement *el = objTag->ToElement();

        string key = el->Value();
        mAttachedProperties[key] = el->Clone()->ToElement();

    }
    return errors;
}

bool the::Attached::isMyTag(TiXmlElement *element)
{
    string tagName = element->Value();
    return tagName == type(this).name;
}

TiXmlElement* the::Attached::serialize()
{
    auto p = GameEntity::getParser();
    TiXmlElement* attachedTag = p->serialize();
    if (attachedTag == nullptr)
    {
        attachedTag = new TiXmlElement(getInitData()->getXmlName());
    }
    for (auto it = mAttachedProperties.begin(); it != mAttachedProperties.end(); it++)
    {
        TiXmlElement* propertyElem = it->second->Clone()->ToElement();
        attachedTag->LinkEndChild(propertyElem);
    }
    bool containsSmth = attachedTag->FirstChildElement() != nullptr || attachedTag->FirstAttribute() != nullptr;
    return (containsSmth)? attachedTag : nullptr;
}

the::IXmlParser* the::Attached::getParser()
{
    return this;
}

void the::Attached::attach(std::shared_ptr<GameEntity>  ent, InitOrder initOrder)
{
    ASSERT(getParent() != nullptr, ":(");

    ent.get()->setParent(getParent(), "", initOrder);
    mAttached.push_back(ent);
}

void the::Attached::detach(GameEntity* ent)
{
    for (auto it = mAttached.begin(); it != mAttached.end(); it++)
    {
        if (it->get() == ent)
        {
            ent->resetParent();
            mAttached.erase(it);
            break;
        }
    }
}
void the::Attached::detach(std::shared_ptr<GameEntity> ent)
{
    for (auto it = mAttached.begin(); it != mAttached.end(); it++)
    {
        if (it->get() == ent.get())
        {
            ent->resetParent();
            mAttached.erase(it);
            break;
        }
    }
}

bool the::Attached::hasProperty(string name)
{
    return mAttachedProperties.find(name) != mAttachedProperties.end();
}

TiXmlElement* the::Attached::getProperty(string name)
{
    auto it = mAttachedProperties.find(name);
    if (it != mAttachedProperties.end())
    {
        return it->second;
    }
    else
    {
        return nullptr;
    }
}

the::InitState the::Attached::onInit()
{
    /*for(auto &p : mAttachedProperties)
    {
        TiXmlElement* el = p.second;
        if (core()->script()->objectTypeValid(el->Value()))
        {
            std::shared_ptr<Object> obj = core()->tree()->createObject(el->Value());
            auto parser = obj->getParser();
            auto objErrors = parser->parse(el);
            for (auto errIt = objErrors.begin(); errIt != objErrors.end(); errIt++)
            {
                ParsingError &err = *errIt;
                string errStr = err.toString();

                if (err.Critical) {
                    core()->log()->CRITICAL(errStr.c_str());
                } else {
                    core()->log()->WARN(errStr.c_str());
                }
            }
            mAttached.push_back(obj); // to prevent object removal
            registerChild(obj.get());
            bool objOk = obj->init();
            if(!objOk)
            {
                core()->log()->WARN("Failed init of the attached object");
                ok = false;
            }
        }
    }*/
    return true;
}

void the::Attached::onDeinit()
{
    for(auto &p : mAttachedProperties)
    {
        delete p.second;
    }
    mAttachedProperties.clear();
}

the::string the::Attached::getText(TiXmlElement* el)
{
    return el->GetText();
}
