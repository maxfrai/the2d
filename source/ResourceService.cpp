////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/ResourceService.hpp>
#include <The2D/Utils/Filesystem.hpp>

the::ResourceService* the::_the_globalResource = nullptr;

void the::ResourceService::setInterface(std::shared_ptr<ResourceInterface> interf)
{
	mInterface = interf;
}

bool the::ResourceService::isPathExists(const std::string& relPath) const
{
	if (mInterface)
	{
		return mInterface->isPathExists(relPath);
	}
	else
	{
		return the::Filesystem::exists(relPath);
	}
}

const the::string the::ResourceService::getPath(const std::string& relPath) const
{
	if (mInterface)
	{
	    return mInterface->getPath(relPath);
	}
	else
	{
		// If interface is not set, we will return the same string
		// It should work on the PC platform, where path to the media
		// Is relative to binary and fully compatible. On another platforms
		// We should define new interface and pass it into service
		return relPath;
	}
}