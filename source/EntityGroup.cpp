////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/EntityGroup.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>
#include <The2D/XmlLoader.hpp>



the::EntityGroup::EntityGroup(): mDefaultInitOrder(AFTER_PARENT), mInDctor(false)
{
    setInitOrder(BEFORE_PARENT);
}

the::EntityGroup::~EntityGroup()
{
    mInDctor = true;
    if (isInitialized())
    {
        deinit();
    }
    for (auto child : mStoredChildren)
    {
        if (child->isInitialized()) child->deinit();
    }
    for (auto child : mStoredChildren)
    {
        if (child->isInitialized())
        {
            child->deinit();
        }
    }
    mStoredChildren.clear();
    mInDctor = false;
};

void the::EntityGroup::addChild(std::shared_ptr<GameEntity> entity, InitOrder initOrder)
{
    mStoredChildren.push_back(entity);
    if (entity->getParent() == nullptr)
    {
        entity->setParent(this, initOrder);
    }
}

void the::EntityGroup::removeChild(GameEntity* entity)
{
    if (entity->getParent() == this)
    {
        entity->resetParent();
    }

    for(auto target = mStoredChildren.begin(); target != mStoredChildren.end(); target++)
    {
        if((*target).get() == entity)
        {
            mStoredChildren.erase(target);
            break;
        }
    }
}

the::IXmlParser* the::EntityGroup::getParser()
{
    return this;
}

std::list<the::ParsingError> the::EntityGroup::parse(TiXmlElement* element)
{
    GameEntity::getParser()->parse(element); // ignore its errors

    std::list<the::ParsingError> errors;

    for (TiXmlElement *el = element->FirstChildElement(); el != nullptr; el = el->NextSiblingElement())
    {
        std::string type = el->Value();
        std::shared_ptr<GameEntity> object = core()->xml()->createEntity(type);

        if(object.get() != nullptr)
        {
            //INFO("\tParsing entity of '%s' type.", type.c_str());
            addChild(object, mDefaultInitOrder);

            auto childErrors = object->getParser()->parse(el);
            errors.splice(errors.begin(), childErrors);

            //INFO("\tFinished parsing entity of '%s' type with name %s.", type.c_str(), object->getName().c_str());
        }
        else
        {
            errors.push_back({el->ValueStr(), false,
                the::format("Element '%s' is not recognized by any creator,"
                    " registered in core()->xml() service.", type),
                    el->Row()});
        }
    }
    return errors;
}
bool the::EntityGroup::isMyTag(TiXmlElement *element)
{
    return GameEntity::getParser()->isMyTag(element);
}
TiXmlElement* the::EntityGroup::serialize()
{
    TiXmlElement* el =  GameEntity::getParser()->serialize(); // Serialize GameEntity fields.
    /*if (el == nullptr)
    {
        el = new TiXmlElement(getType());
    }
    for (GameEntity* child : getChildren())
    {
        TiXmlElement* childEl = child->getParser()->serialize();
        if (childEl != nullptr)
        {
            el->LinkEndChild(childEl);
        }
    }*/
    return el;
}

the::InitOrder the::EntityGroup::getDefaultInitOrder()
{
    return mDefaultInitOrder;
}
void the::EntityGroup::setDefaultInitOrder(InitOrder order)
{
    mDefaultInitOrder = order;
}

void the::EntityGroup::onUnregisterChild(GameEntity* child)
{
    if (!mInDctor)
    {
        removeChild(child);
    }
}


