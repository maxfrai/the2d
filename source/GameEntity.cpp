////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/GameEntity.hpp>
#include <The2D/MessageCore.hpp>
#include <The2D/MessageHelper.hpp>
#include <The2D/Bind.hpp>
#include <The2D/Rtti.hpp>
#include <The2D/LogService.hpp>
#include <The2D/ObserverService.hpp>
#include <The2D/AttachedService.hpp>



the::InitState::InitState(bool _success, the::string _comment)
    : initialized(_success), comment(_comment)
{
}

the::InitState the::READY(true);
the::InitState the::NOT_READY(false);

the::GameEntity::GameEntity():
     /*mZIndex(0),*/
     mHadParent(false), mNoParent(false),
     mXmlTag(), mParameters(this),
     mVisibility(PUBLIC), mEngineID(0),
     mInitState(false, "Not initialized"),
     mAutoSubscribeToLoop(true), mForceParamsUnlock(false),
     mParentEntity(nullptr), mCore(nullptr),
     mEntityTypeToLookInDebugger(nullptr),
     mOrderOfInit(AUTO)
{
    setName("");
    REGISTER(prmName).setRequired(false);
    REGISTER(prmGroup).setRequired(false);
    //REGISTER(prmZIndex).setRequired(false);
}

the::GameEntity::~GameEntity()
{
    ASSERT(!isInitialized(), "Do deinitialization before deleting component.");

    // Check was entity registered
    if (!mNoParent)
    {
        ASSERT(mHadParent,
           "Unregistered entity."
           " Register it via setParent(parent) or parent->registerChild "
           " or mark as parameter without parent by calling setNoParent.");
    }

    clearDependencies();

    // Unregister from tree
    resetParent();
}

void the::GameEntity::logLogic(const the::string& formattedLine, the::LogType type,
    const char* func, const char* file, const char* cmdate, const char* cmtime, int line)
{
    the::string desc = the::format("Path: %s;\nName: %s;\nGroup: %s\nType: %s;\n",
        getPathString(), getName(), getGroup(), the::type(this).fullName);

    // Note the '|' symbol. ILogInterface will split the string
    // at this symbol and pass the second part as an additional info
    the::string fullContent = the::format("%s|%s", formattedLine, desc);

    LogInterface::logLogic(fullContent, type, func, file, cmdate, cmtime, line);
}

void the::GameEntity::registerChild(GameEntity* child)
{
    ASSERT(child->getParent() == nullptr, "Entity already has parent. Call resetParent first.");
    ASSERT(child != this, "Entity cant be child of itself.");
    /*ASSERT(!(mInitializing && (child->getInitOrder() == BEFORE_PARENT ||
                             child->getInitOrder() == AFTER_PARENT ||
                             child->getInitOrder() == AUTO)),
           "Registering children with init order BEFORE_PARENT, AFTER_PARENT and AUTO during "
           "initialization process is not supported");*/

    child->onParentChanging(this);
    child->SigParentChanging(this);

    mChildren.push_back(child);
    child->mParentEntity = this;
    child->mHadParent = true;

    if(isInGameTree())
    {
        core()->observer()->onRegister(child);
    }

    updateChildInitDependencies(child);

    onRegisterChild(child);
    SigChildRegistered(child);
    child->onParentChanged();
    child->SigParentChanged();
    child->updateInitData();
}

void the::GameEntity::unregisterChild(GameEntity* child, bool deinitialize)
{
    child->onParentChanging(nullptr);
    child->SigParentChanging(nullptr);

    if (child->isInitialized())
    {
        if (deinitialize) child->deinit();
        else ASSERT(!child->isInitialized(), "It is not possible unregister initialized child.");
    }
    child->mParentEntity = nullptr;
    mChildren.remove(child);

    // Reset dependencies
    removeInitDependency(child);
    child->clearDependencies();

    child->onParentChanged();
    child->SigParentChanged();
    onUnregisterChild(child);
    SigChildUnregistered(child);
    if(child->isInGameTree())
    {
        child->core()->observer()->onUnregister(child);
    }

    // Reset incoming bindings
    // BindBase::removeItem will try to remove itself from mIncomingBindings.
    // So, we create there copy, and work with it.
    if (isInGameTree() && core()->bind()->atIncomingBindings.hasValue(child))
    {
        eastl::list<BindBase*> copyOfIncomingBindings = core()->bind()->atIncomingBindings[child];
        core()->bind()->atIncomingBindings.beginEdit(child).clear();
        core()->bind()->atIncomingBindings.endEdit(child);
        for (auto it : copyOfIncomingBindings)
        {
            /// \todo Not reset, only remove myself (multibind can contains another values)
            it->reset();
        }
    }
}

void the::GameEntity::registerChild(GameEntity* p, the::string _xmlName)
{
    the::string xmlName(_xmlName);
    if (xmlName.size() > 1 && xmlName[0] == 'm')
    {
        xmlName = xmlName.substr(1, xmlName.size()-1);
    }
    if (xmlName.size() > 3 && xmlName[0] == 'p' && xmlName[1] == 'r' && xmlName[2] == 'm')
    {
        xmlName = xmlName.substr(3, xmlName.size()-3);
    }
    if (xmlName.size() > 2 && xmlName[0] == 't' && xmlName[1] == 'o')
    {
        xmlName = xmlName.substr(2, xmlName.size()-2);
    }
    p->mXmlTag = xmlName;
    p->setName(xmlName);

    registerChild(p);
}

void the::GameEntity::setParent(GameEntity* parent, the::string name, InitOrder order)
{
    ASSERT(parent != nullptr, "Call reset parent instead");

    setInitOrder(order);
    setName(name);
    setXmlTagName(name);

    parent->registerChild(this);
}
void the::GameEntity::setParent(GameEntity* parent, InitOrder order)
{
    ASSERT(parent != nullptr, "Call reset parent instead");

    setInitOrder(order);
    parent->registerChild(this);
}
void the::GameEntity::resetParent(bool deinitialize)
{
    if (isInitialized() && deinitialize)
    {
        deinit();
    }
    if (getParent() != nullptr)
    {
        getParent()->unregisterChild(this);
    }
}


void the::GameEntity::onRegisterChild(GameEntity* child)
{
}

void the::GameEntity::onUnregisterChild(GameEntity* child)
{
}

void the::GameEntity::onChildInit(GameEntity* child)
{
}
void the::GameEntity::onChildDeinit(GameEntity* child)
{
}

void the::GameEntity::onParentChanged()
{
}

void the::GameEntity::onParentChanging(GameEntity* newParent)
{
}

void the::GameEntity::preInit()
{
    mEntityTypeToLookInDebugger = &type(this);

    /*if(prmZIndex.containsValue())
    {
        mZIndex = prmZIndex();
    }*/
    if(prmName.containsValue())
    {
        setName(prmName());
    }
    if(prmGroup.containsValue())
    {
        addGroup(prmGroup());
    }

    onPreInit();

    core()->observer()->onInitializing(this);
}
void the::GameEntity::onPreInit()
{
}

void the::GameEntity::postInit()
{
    onPostInit();

    if (isInitialized())
    {
        if (mAutoSubscribeToLoop)
        {
            core()->loop()->subscribeToDraw(this);
            draw();
            core()->loop()->subscribeToUpdate(this);
            update();
        }

        if (getParent() != nullptr)
        {
            getParent()->onChildInit(this);
            getParent()->SigChildInitialized(this);
        }
    }
}
void the::GameEntity::onPostInit()
{
}

the::InitState the::GameEntity::initGotStuck()
{
    if (isInitialized()) return getInitState();

    setInitState(onInitGotStuck());
    return getInitState();
}
the::InitState the::GameEntity::onInitGotStuck()
{
    return getInitState();
}

void the::GameEntity::preDeinit()
{
    onPreDeinit();
}
void the::GameEntity::onPreDeinit()
{
}

void the::GameEntity::postDeinit()
{
    onPostDeinit();

    if (!isInitialized())
    {
        core()->loop()->unsubscribeFromDraw(this);
        core()->loop()->unsubscribeFromUpdate(this);

        if (getParent() != nullptr)
        {
            getParent()->onChildDeinit(this);
            getParent()->SigChildDeinitialized(this);
        }
    }
}
void the::GameEntity::onPostDeinit()
{
}

the::InitState the::GameEntity::deinitGotStuck()
{
    if (!isInitialized()) return getInitState();

    the::string depMsg;
    depMsg.reserve(200);
    bool depCheck = checkDependencies(getIncomingInitDependencies(), false, depMsg, true);
    if (depCheck)
    {
        onDeinit();
        clearStateChildren();

        setInitState(NOT_READY("Deinitialized"));
        core()->observer()->onDeinitialized(this);
        return getInitState();
    }
    else
    {
        setInitState(READY(depMsg));

        InitState onDeinitGotStuckResult = onDeinitGotStuck();
        setInitState(onDeinitGotStuckResult);

        return getInitState();
    }
}

the::InitState the::GameEntity::onDeinitGotStuck()
{
    return getInitState();
}

the::string the::GameEntity::forceDeinit()
{
    if (!isInitialized()) return "";

    onDeinit();
    clearStateChildren();

    setInitState(NOT_READY(getInitState().comment));
    return getInitState().comment;
}

void the::GameEntity::updateChildInitDependencies(GameEntity* child)
{
    removeInitDependency(child);
    child->removeInitDependency(this);

    if (child->getInitOrder() == BEFORE_PARENT)
    {
        addInitDependency(child, "it is a parent", "it is a child", "child init order is BEFORE_PARENT");
    }
    else if (child->getInitOrder() == AFTER_PARENT)
    {
        child->addInitDependency(this, "it is a child", "it is a parent", "child init order is AFTER_PARENT");
    }
}

the::InitOrder the::GameEntity::getInitOrder() const
{
    return mOrderOfInit;
}

the::GameEntity& the::GameEntity::setInitOrder(the::InitOrder order)
{
    ASSERT(!isInitialized(), "Changing initialization order of initialized entity %s.", getShortInfoString());
    mOrderOfInit = order;
    if (getParent() != nullptr)
    {
        getParent()->updateChildInitDependencies(this);
    }
    return *this;
}
the::GameEntity& the::GameEntity::setVisibility(Visibility vis)
{
    mVisibility = vis;
    return *this;
}
const std::list<the::GameEntity*>& the::GameEntity::getChildren() const
{
    return mChildren;
}

the::GameEntity* the::GameEntity::getParent() const
{
    return mParentEntity;
}

bool the::GameEntity::forefatherOf(GameEntity* ent)
{
    for (GameEntity* current = ent->getParent(); current != nullptr;  current = current->getParent())
    {
        if (current == this) return true;
    }
    return false;
}

void the::GameEntity::manageStateChild(std::shared_ptr<GameEntity> child, the::string group, the::string name)
{
    mStateChildren.push_back(child);
    child->setParent(this, BY_PARENT);
    if (!group.empty())
    {
        child->addGroup(group);
    }
    if (!name.empty())
    {
        child->setName(name);
    }
}

void the::GameEntity::registerChild(Parameter* p, the::string xmlName)
{
    getInitData()->registerChild(p, xmlName);
}

void the::GameEntity::unregisterChild(Parameter* p)
{
    getInitData()->unregisterChild(p);
}

void the::GameEntity::setNoParent()
{
    mNoParent = true;
}

the::VistorBehaviour the::GameEntity::accept(std::function<VistorBehaviour(GameEntity*)> enter, std::function<void(GameEntity*)> leave)
{
    auto action = enter(this);
    if (action == CONTINUE)
    {
        for (auto ent : getChildren())
        {
            auto childAction = ent->accept(enter, leave);
            if (childAction == STOP_VISIT_NO_LEAVE || childAction == STOP_VISIT)
            {
                action = childAction;
                break;
            }
        }
    }
    if (action != STOP_VISIT_NO_LEAVE)
    {
        leave(this);
    }
    return action;
}
the::VistorBehaviour the::GameEntity::accept (std::function<VistorBehaviour(GameEntity*)> enter)
{
    return accept(enter, [](GameEntity*){});
}

void the::GameEntity::acceptAll(std::function<void (GameEntity*)> visit)
{
    visit(this);
    for (auto ent : getChildren())
    {
        ent->acceptAll(visit);
    }
}

void the::GameEntity::setName(const the::string& name)
{
    mName = name;
    mNameHash = getHashCode<string>(name);
}

void the::GameEntity::setGroup(const the::string& group)
{
    mGroup = "";
    mGroupsHashes.clear();
    addGroup(group);
}
void the::GameEntity::addGroup(const the::string& group)
{
    if (mGroup.empty())
    {
        mGroup = group;
    }
    else
    {
        mGroup = format("%s;%s", mGroup, group);
    }
    std::vector<string> groups;
    split(group, ';', groups);
    for (string& groupStr : groups)
    {
        trim(groupStr);
        mGroupsHashes.push_back(getHashCode<string>(groupStr));
    }
}

the::IXmlParser* the::GameEntity::getParser()
{
    updateInitData();
    return getInitData();
}

the::InitState the::GameEntity::initInternal()
{
    //INFO("init internal %s", getPathString());
    if (isInitialized()) return getInitState();

    // Check if all required parameters has values
    std::list<ValueParameter*> unsetRequired;
    findUnsetRequiredParams(getInitData(), unsetRequired);
    if (!unsetRequired.empty())
    {
        the::string reason;
        reason.reserve(150);
        for (auto& p : unsetRequired)
        {
            if (p->getParsed())
            {
                reason += format("Parsed parameter '%s' has no value. Parsed xml: '%s'\n",
                                        p->getXmlName(), p->getParsedXml());
            }
            else
            {
                reason += format("Required parameter '%s' has no value (not even parsed).", p->getXmlName());
            }
        }
        setInitState(NOT_READY(reason));
        return getInitState();
    }

    // Check dependencies, to prevent onInit call, if not all ready
    the::string depMsg;
    bool depCheckResult = checkDependencies(getInitDependencies(), true, depMsg);
    if (depCheckResult == false)
    {
        setInitState(NOT_READY(depMsg));
        //INFO("decline");
        return getInitState();
    }

    auto onInitResult = onInit();
    setInitState(onInitResult);

    // Look for new dependencies, that can appear in onInit
    if (onInitResult.initialized == true)
    {
        depCheckResult = checkDependencies(getInitDependencies(), true, depMsg);
        if (depCheckResult == false)
        {
            setInitState(NOT_READY(depMsg));
            //INFO("decline: %s", getPathString());
            return getInitState();
        }
    }
    else
    {
        //INFO("onInit failed");
    }

    if (isInitialized())
    {
        core()->observer()->onInitialized(this);
    }
    else
    {
        //INFO("new dependecies failed");
    }
    //INFO("end init internal: %s", getPathString());

    return getInitState();
}

bool the::GameEntity::checkDependencies(
    const std::vector<InitDependency>& list, bool checkInitialzed, string& msg, bool ignoreWeak)
{
    for (auto& dependency : list)
    {
        if (!dependency.entity->isInitialized() && checkInitialzed)
        {
            msg = format("Dependency %s (%s) is not initialized."
                " Dependency is because %s. Dependency path: %s",
                dependency.entity->getShortInfoString(), dependency.entityRole,
                dependency.dependencyReason, dependency.entity->getPathString());
            return false;
        }
        // the last brace with || means: while no forceDeinit, ignore "weakOnDeinit"
        // this needed to compute correct deinit order, if dependency is weak on deinit
        else if (dependency.entity->isInitialized() && !checkInitialzed && (!ignoreWeak || !dependency.weakOnDeinit))
        {
            msg = format("Incoming dependency %s (%s) is not deinitialized. "
                "Dependency is because %s. Dependent entity path: %s",
                dependency.entity->getShortInfoString(), dependency.entityRole,
                dependency.dependencyReason, dependency.entity->getPathString());
            return false;
        }
    }
    return true;;
}

the::InitState the::GameEntity::onInit()
{
    return READY;
}

the::InitState the::GameEntity::deinitInternal()
{
    if (!isInitialized()) return getInitState();

    string depMsg;
    bool depCheck = checkDependencies(getIncomingInitDependencies(), false, depMsg);
    if (depCheck == false)
    {
        setInitState(READY(depMsg));
        return getInitState();
    }

    onDeinit();
    clearStateChildren();

    setInitState(NOT_READY("Deinitialized"));
    core()->observer()->onDeinitialized(this);
    return getInitState();
}

void the::GameEntity::onDeinit()
{
}

void the::GameEntity::updateInitData()
{
    if (!mParameters.getLocked())
    {
        mParameters.setXmlName(getXmlTagName());
    }
}

the::ParameterGroup* the::GameEntity::getParameterGroup()
{
    return getInitData();
}

std::shared_ptr<the::GameEntity> the::GameEntity::clone() const
{
    ASSERT(type(this).constructor != nullptr, ":(");

    GameEntity* copy = static_cast<GameEntity*>(type(this).constructor->create());
    copy->readInitDataFrom(this);
    return std::shared_ptr<GameEntity>(copy);
}

const the::GameEntityInitData* the::GameEntity::getInitData() const
{
    return &mParameters;
}


the::GameEntityInitData* the::GameEntity::getInitData()
{
    return &mParameters;
}


void the::GameEntity::readInitData(const GameEntityInitData* initData)
{
    getInitData()->readDataFrom(initData);
}

void the::GameEntity::readInitDataFrom(const GameEntity* src)
{
    getInitData()->readDataFrom(src->getInitData());
}
void the::GameEntity::setInitialized(bool initialized)
{
    mInitState = initialized;
}

const the::InitState& the::GameEntity::getInitState() const
{
    return mInitState;
}

bool the::GameEntity::getBool()
{
    return b;
}
void the::GameEntity::setBool(bool s)
{
    b = s;
}

bool the::GameEntity::isParametersLocked()
{
    return isInitialized() && !mForceParamsUnlock;
}

void the::GameEntity::saveState()
{
    ASSERT(isInitialized(), "Not initialized entity has no state.");

    acceptAll([](GameEntity* ent){
        ent->mForceParamsUnlock = true;
    });
    acceptAll([](GameEntity* ent){
        if (ent->getXmlTagName() != ent->getName())
            ent->prmName = ent->getName();
        string group = (ent->prmGroup.containsValue()? ent->prmGroup() : "");
        if (group != ent->getGroup())
            ent->prmGroup = ent->getGroup();
        ent->onSaveState();
    });
    acceptAll([](GameEntity* ent){
        ent->mForceParamsUnlock = false;
    });
}

void the::GameEntity::update()
{
    core()->loop()->unsubscribeFromUpdate(this);
}
void the::GameEntity::draw()
{
    core()->loop()->unsubscribeFromDraw(this);
}

void the::GameEntity::setActive(bool active)
{
    core()->loop()->setActive(this, active);
}

the::string the::GameEntity::getPathString() const
{
    the::string path = format("%s(%s)", getName(), getType());
    path.reserve(100);
    for (const the::GameEntity* parent = getParent(); parent != nullptr; parent = parent->getParent())
    {
        path = format("%s(%s)/%s", parent->getName(), parent->getType(), path);
    }

    return path;
}

the::string the::GameEntity::getShortInfoString() const
{
    return format("'%s' (type: '%s', group: '%s', id: '%i')", getName(), getType(), getGroup(), mEngineID);
}

the::string the::GameEntity::getDetailedInfoString() const
{
    the::string initOrderStr;
    switch(getInitOrder())
    {
        CASE(NOT_SET) initOrderStr = "NOT_SET"; END_CASE;
        CASE(BEFORE_PARENT) initOrderStr = "BEFORE_PARENT"; END_CASE;
        CASE(AFTER_PARENT) initOrderStr = "AFTER_PARENT"; END_CASE;
        CASE(BY_PARENT) initOrderStr = "BY_PARENT"; END_CASE;
        CASE(AUTO) initOrderStr = "AUTO"; END_CASE;
        CASE(NEVER) initOrderStr = "NEVER"; END_CASE;
        default: initOrderStr = format("UNKNOWN VALUE '%i'", (int)getInitOrder());
    };

    return format("%s\nfull type name: %s\nxml tag name: %s\ninit data parsed: %s\n"
        "parsed xml: \n%s\npath: %s\ninit state: %s %s\ninit order: %s",
        getShortInfoString(), type(this).fullName, getXmlTagName(), getInitData()->getParsed(),
        string("<not available yet, because TinyXml does not support saving to string>"),
        getPathString(), (isInitialized())? "READY" : "NOT_READY", getInitState().comment, initOrderStr);

}

the::string the::GameEntity::getXmlTagName() const
{
    return (mXmlTag.empty())? type(this).name : mXmlTag;
}

the::GameEntity& the::GameEntity::setXmlTagName(the::string xmlTag)
{
    mXmlTag = xmlTag;
    return *this;
}

the::InitState the::GameEntity::init()
{
    core()->initService()->init(this);
    return getInitState();
}

void the::GameEntity::deinit()
{
    core()->initService()->deinit(this);
}

// sf::FloatRect the::GameEntity::getRectangle() const
// {
//     if (mChildren.size() > 0)
//     {
//         float left = std::numeric_limits<float>::max();
//         float top = std::numeric_limits<float>::max();
//         float right = 0;
//         float bottom = 0;
//         for (auto it = mChildren.begin(); it != mChildren.end(); it++)
//         {
//             sf::FloatRect childRect = (*it)->getRectangle();
//             if (childRect.width > 0 && childRect.height > 0)
//             {
//                 float childRight = childRect.left + childRect.width;
//                 float childBottom = childRect.top + childRect.height;
//                 if (left > childRect.left) left = childRect.left;
//                 if (top > childRect.top) top = childRect.top;
//                 if (right < childRight) right = childRight;
//                 if (bottom < childBottom) bottom = childBottom;
//             }
//         }
//         return sf::FloatRect(left, top, right - left, bottom - top);
//     }
//     // else
//     return sf::FloatRect();
// }
/*
the::int8 the::GameEntity::getZIndex() const
{
    return mZIndex;
}
void the::GameEntity::setZIndex(int8 _ZIndex)
{
    mZIndex = _ZIndex;
}*/

void the::GameEntity::onIncomingMessage(Message* msg)
{
}

void the::GameEntity::processMessage(Message* msg)
{
    onIncomingMessage(msg);

    if (!msg->mProcessed)
    {
        ASSERT(msg->mDirrection != Message::MESSAGE_CORE, "Use MessagManager::Get()->Broadcast(msg) inside.");
        switch(msg->mDirrection)
        {
            case Message::UP:
                if (mParentEntity != nullptr) mParentEntity->processMessage(msg);
                break;
            case Message::DOWN:
                for (auto it = mChildren.begin(); it!= mChildren.end(); it++)
                {
                    (*it)->processMessage(msg);
                    if (msg->mProcessed) break;
                }
                break;
            default: break;
        }
    }
}

int the::GameEntity::id()
{
    return mEngineID;
}

const std::vector<the::GameEntity::InitDependency>& the::GameEntity::getInitDependencies()
{
    return mDependencies;
}
const std::vector<the::GameEntity::InitDependency>& the::GameEntity::getIncomingInitDependencies()
{
    return mIncomingDependencies;
}
bool the::GameEntity::initDependsOn(GameEntity* ent)
{
    for (auto& dep : mDependencies)
    {
        if (dep.entity == ent) return true;
    }
    return false;
}
void the::GameEntity::addInitDependency(GameEntity* ent, the::string myRole,
    the::string entRole, the::string dependencyReason, bool weakOnDeinit)
{
    if (!initDependsOn(ent))
    {
        mDependencies.push_back({ent, entRole, dependencyReason, weakOnDeinit});
        ent->mIncomingDependencies.push_back({this, myRole, dependencyReason, weakOnDeinit});
    }
    else
    {
        for (auto &dep : mDependencies)
        {
            if (dep.entity == ent)
            {
                ASSERT(dep.weakOnDeinit == weakOnDeinit,
                    "Can't add init dependency %s (myRole: %s, entRole: %s reason: %s, weakOnDeinit: %i) "
                    "because already exists dependency to this entity ("
                    " entityRole: %s, reason: %s) with different weakOnDeinit value.",
                    ent->getShortInfoString(), myRole, entRole, dependencyReason, (int)weakOnDeinit,
                    dep.entityRole, dep.dependencyReason);

                dep.entityRole += format("\n%s", entRole);
                dep.dependencyReason += format("\n%s", dependencyReason);
                break;
            }
        }
        for (auto &dep : ent->mIncomingDependencies)
        {
            if (dep.entity == this)
            {
                dep.entityRole += format("\n%s", myRole);
                dep.dependencyReason += format("\n%s", dependencyReason);
                break;
            }
        }
    }
}
void the::GameEntity::removeInitDependency(GameEntity* ent)
{
    if (ent != nullptr)
    {
        for (auto dep = mDependencies.begin(); dep != mDependencies.end(); dep++)
        {
            if (dep->entity == ent)
            {
                mDependencies.erase(dep);
                break;
            }
        }
        for (auto dep = ent->mIncomingDependencies.begin(); dep != ent->mIncomingDependencies.end(); dep++)
        {
            if (dep->entity == this)
            {
                ent->mIncomingDependencies.erase(dep);
                break;
            }
        }
    }
}

void the::GameEntity::clearDependencies()
{
    for (auto it = mIncomingDependencies.rbegin(); it != mIncomingDependencies.rend();)
    {
        auto curIt = it;
        it++;
        curIt->entity->removeInitDependency(this);
    }
    for (auto it = mDependencies.rbegin(); it != mDependencies.rend();)
    {
        auto curIt = it;
        it++;
        removeInitDependency(curIt->entity);
    }
}

void the::GameEntity::findUnsetRequiredParams(ParameterGroup* current, std::list<ValueParameter*>& unsetRequired)
{
    ASSERT(current!= nullptr, "Invalid parameter");

    // Call recursively for all children
    for(Parameter* childP : current->getItems())
    {
        if (childP->isGroup())
        {
             findUnsetRequiredParams(static_cast<ParameterGroup*>(childP), unsetRequired);
        }
        else
        {
            ValueParameter* valueChildP = static_cast<ValueParameter*>(childP);
            if ((valueChildP->getRequired() || valueChildP->getParsed()) && !valueChildP->containsValue())
            {
                unsetRequired.push_back(valueChildP);
            }
        }
    }
}

void the::GameEntity::setAutoSubscribeToLoop(bool subscribe)
{
    mAutoSubscribeToLoop = subscribe;
}

void the::GameEntity::setCore(InfrastructureCore* core)
{
    if (mCore != core)
    {
        if (mCore != nullptr)
        {
            mCore->attached()->resetAllAttached(this);
        }
        auto old = mCore;
        mCore = core;
        onSetCore(old, core);
    }
}

 void the::GameEntity::onSetCore(InfrastructureCore* oldValue, InfrastructureCore* newValue)
 {
 }

void the::GameEntity::clearStateChildren()
{
    for (auto child : mStateChildren)
    {
        child->deinit();
        child->resetParent();
    }
    mStateChildren.clear();
}