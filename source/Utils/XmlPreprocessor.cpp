////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Utils/XmlPreprocessor.hpp>
#include <The2D/LogService.hpp>
#include <The2D/Utils/Filesystem.hpp>
#include <The2D/Utils/Utils.hpp>
#include <The2D/ResourceService.hpp>

#include <fstream>
#include <streambuf>


the::XmlPreprocessor::XmlPreprocessor(InfrastructureCore* core): mCore(core), mDebugLogIndent(0)
{
    // TODO: read file path from config
    commonTemplateFile = "Media/Templates/common.tpl";
    templateFolder = "Media/Templates";
    paramTagName = "Param";
    paramPlaceholderTagName = "ParamPlaceholder";
    paramPlaceholderParamAttributeName = "param";
    debugLogAttributeName = "preprocessorLog";
    universalTagName = "xxx";
    notSetParamValue = "notset";
}

the::XmlPreprocessor::XmlPreprocessor(): XmlPreprocessor(nullptr)
{
}

the::DocumentPtr the::XmlPreprocessor::process(const std::string& sceneFile)
{
    ASSERT(the::Filesystem::exists(sceneFile), "Scene '%s' file doesn't exist!", sceneFile);

    resetState();
    //std::cout<<"Processing templates at " + sceneFile + "." << std::endl;
    DocumentPtr document = DocumentPtr(new TiXmlDocument(sceneFile.c_str()));
    document->LoadFile();
    TiXmlElement* document_element = document->RootElement();
    processTag(document_element, document, sceneFile);
    std::list<TiXmlElement*> paramStack;
    processParamsRec(document_element, paramStack);
    clearStuffRec(document_element);
    // document->SaveFile("ParamsTest.xml");
    //std::cout<<std::string("Templates processing completed.") << std::endl;

    printLog(format("Preprocessed file %s", sceneFile), document_element);

    return document;
}

TiXmlElement* the::XmlPreprocessor::process(TiXmlElement* tag, DocumentPtr baseDocument, const std::string& documentFile)
{
    ASSERT(the::Filesystem::exists(documentFile), "Scene '%s' file doesn't exist!", documentFile);
    ASSERT(tag != nullptr, ":(");
    ASSERT(baseDocument.get() != nullptr, ":(");

    resetState();
    //std::cout<<"Processing templates at " + sceneFile + "." << std::endl;
    TiXmlElement* result = tag->Clone()->ToElement();
    processTag(result, baseDocument, documentFile);
    std::list<TiXmlElement*> paramStack;
    processParamsRec(result, paramStack);
    clearStuffRec(result);
    //std::cout<<std::string("Templates processing completed.") << std::endl;

    printLog(format("Preprocessed tag '%s' in file %s[%i]", tag->ValueStr(), documentFile, tag->Row()), result);

    return result;
}

the::DocumentPtr the::XmlPreprocessor::processXml(const std::string &xml)
{
    resetState();
    DocumentPtr document = DocumentPtr(new TiXmlDocument(xml.c_str()));
    document->Parse(xml.c_str());
    TiXmlElement* document_element = document->RootElement();
    processTag(document_element, document, "");
    std::list<TiXmlElement*> paramStack;
    processParamsRec(document_element, paramStack);

    printLog(format("Preprocessed xml '%s...'", std::string(xml, 0, 20)), document_element);

    return document;
}

TiXmlElement* the::XmlPreprocessor::processParams(TiXmlElement* tag)
{
    resetState();
    TiXmlElement* result = tag->Clone()->ToElement();
    std::list<TiXmlElement*> paramStack;
    processParamsRec(result, paramStack);
    printLog(format("Preprocessed params in xml with root '%s'", tag->ValueStr()), result);
    return result;
}

TiXmlElement* the::XmlPreprocessor::processTemplates(TiXmlElement* tag, DocumentPtr baseDocument, const std::string& documentFile)
{
    resetState();
    TiXmlElement* result = tag->Clone()->ToElement();
    processTag(result, baseDocument, documentFile);
    clearStuffRec(result);
    printLog(format("Preprocessed params in tag '%s' in file %s[%i]", tag->ValueStr(), documentFile, tag->Row()), result);
    return result;
}

void the::XmlPreprocessor::define(std::string var)
{
    ASSERT(var != "true" && var != "false", "These words a keywords.");

    if (!CONTAINS(mDefinedVariables, var))
    {
        mDefinedVariables.push_back(var);
    }
}

void the::XmlPreprocessor::undefine(std::string var)
{
    if (CONTAINS(mDefinedVariables, var))
    {
        mDefinedVariables.erase(std::find(mDefinedVariables.begin(), mDefinedVariables.end(), var));
    }
}

bool the::XmlPreprocessor::defined(std::string var)
{
    if (var == "true") return true;
    else if (var == "false") return false;
    else return CONTAINS(mDefinedVariables, var);
}

void the::XmlPreprocessor::clearDefines()
{
    mDefinedVariables.clear();
}
const std::vector<std::string>& the::XmlPreprocessor::getDefined() const
{
    return mDefinedVariables;
}

void the::XmlPreprocessor::beginDebugLogBlock(std::string msg = "")
{
    if (msg != "") debugLog(msg);
    mDebugLogIndent += 2;
}
void the::XmlPreprocessor::endDebugLogBlock(std::string msg = "")
{
    mDebugLogIndent -= 2;
    ASSERT(mDebugLogIndent >= 0, "Ident is less than zero");
    if (msg != "") debugLog(msg);
}
void the::XmlPreprocessor::debugLog(std::string msg, bool newLine)
{
    if (mDebugLogElements.size() > 0)
    {
        mLogMessage << format("%s%s%s", std::string(mDebugLogIndent, '.'), msg, (newLine)? "\n" : "");
    }
}

void the::XmlPreprocessor::resetState()
{
    mLogMessage.str("");
    mLogMessage.clear(); // Clear state flags.
    mDebugLogElements = std::stack<TiXmlElement*>();
    mDebugLogIndent = 0;
    mFileToDocument.clear();
    mIncludeChain.clear();
}

void the::XmlPreprocessor::printLog(std::string header, TiXmlElement* result)
{
    /*TiXmlDocument doc;
    doc.LinkEndChild(result);
    doc.SaveFile("XmlPreprocessor_temp_file.xml");
    std::ifstream t("XmlPreprocessor_temp_file.xml");
    std::string resultXml((std::istreambuf_iterator<char>(t)),
       std::istreambuf_iterator<char>());*/

    std::string defined;
    for (auto v : mDefinedVariables)
    {
        defined += v + "\n";
    }

    std::string log = mLogMessage.str();
    if (!log.empty())
    {
        log = format("\nProcessing log:\n%s", log);
    }

    INFO("%s|Defined variables:\n%s\n%s", header, defined, log);
}

void the::XmlPreprocessor::processTag(TiXmlElement* tag, DocumentPtr baseDocument, const std::string& baseFile)
{
    bool passFilter = true;
    std::string filter = "";

    if (tag->Attribute("ifdef") != nullptr)
    {
        filter = format("ifdef=\"%s\"", tag->Attribute("ifdef"));
        passFilter = parseIfdef(tag->Attribute("ifdef"));
    }
    if (tag->Attribute("ifndef") != nullptr)
    {
        filter = format("ifndef=\"%s\"", tag->Attribute("ifndef"));
        passFilter = !parseIfdef(tag->Attribute("ifndef"));
    }

    if (passFilter)
    {
        if (tag->Attribute(debugLogAttributeName) != nullptr && std::string(tag->Attribute(debugLogAttributeName.c_str())) == "true")
        {
            mDebugLogElements.push(tag);
        }

        beginDebugLogBlock("Begin processing tag " + tag->ValueStr());

        //std::cout << std::string("Process tag ") + tag->Value() << std::endl;
        if (tag->Attribute("template") != nullptr)
        {
             std::string tplLocation = tag->Attribute("template");
             TiXmlElement tagTemplate = getTemplate(tplLocation, baseDocument, baseFile);
             applyTemplate(tag, &tagTemplate);
        }
        else if (strcmp(tag->Value(), "Template") == 0 && tag->Attribute("parent") != nullptr)
        {
             std::string tplLocation = tag->Attribute("parent");
             TiXmlElement tagTemplate = getTemplate(tplLocation, baseDocument, baseFile);
             applyParent(tag, &tagTemplate);
        }

        TiXmlElement* next = nullptr;
        for (TiXmlElement* child = tag->FirstChildElement();
             child != nullptr;
             child = next)
        {
            next = child->NextSiblingElement();
            processTag(child, baseDocument, baseFile);
        }

        endDebugLogBlock("Finished processing tag " + tag->ValueStr());

        if (mDebugLogElements.size() > 0 && mDebugLogElements.top() == tag)
        {
            mDebugLogElements.pop();
        }
    }
    else
    {
        debugLog(format("Tag '%s' filtered out via %s", tag->ValueStr(), filter));
        tag->Parent()->RemoveChild(tag);
    }
}

TiXmlElement the::XmlPreprocessor::getTemplate(const std::string& spec, DocumentPtr baseDocument,
        const std::string& baseFile)
{
    //std::cout << "Get template " + spec << std::endl;
    std::vector<std::string> parts;
    parts = split(spec, ',');
    TiXmlElement* result = nullptr;
    bool first = true;
    std::vector<std::string>::reverse_iterator partsIt;
    for (partsIt = parts.rbegin(); partsIt != parts.rend(); ++partsIt)
    {
        std::string templateSpec = *partsIt;
        the::trim(templateSpec);
        TiXmlElement*  ptpl = getSingleTemplate(templateSpec, baseDocument, baseFile);
        if (ptpl != nullptr)
        {
            if (first)
            {
                first = false;
                /// \todo Optimize memory using (what the fuck this Clone?)
                result = ptpl->Clone()->ToElement();
            }
            else
            {
                applyParent(result, ptpl);
            }
        }
        else
        {
            throw InvalidSceneFormatException("Template '" + spec + "' not found.");
        }
    }
    TiXmlElement res = *result;
    delete result;
    return res;
}

// Attention! Recursive function!
TiXmlElement*
the::XmlPreprocessor::getSingleTemplate(const std::string& _spec,
                                      DocumentPtr baseDocument,
                                      const std::string& baseFile)
{
    //std::cout << "Get single template " + _spec + "; Base file: " + baseFile << std::endl;
    std::string spec(_spec);
    the::trim(spec);
    // Anti circle include
    std::pair<std::string, std::string> currentPair = std::pair<std::string, std::string>(spec, baseFile);
    std::list< std::pair<std::string, std::string> >::iterator includeChainIt;
    for (includeChainIt = mIncludeChain.begin(); includeChainIt != mIncludeChain.end(); ++includeChainIt)
    {
        if (*includeChainIt == currentPair)
        {
            throw InvalidSceneFormatException("Circle include. Template: " + spec + "; File: " + baseFile + ".");
        }
    }
    mIncludeChain.push_back(currentPair);
    TiXmlElement* result = nullptr;

    // No file, no local
    if (spec.find("/") == spec.npos && spec.find("\\") == spec.npos)
    {
        std::string templateName = spec;
        // Check local
        TiXmlElement* localSearchResult = getLocalTemplate(templateName, baseDocument, baseFile);
        if (localSearchResult)
        {
            //std::cout<<"Found local" << std::endl;
            result = localSearchResult;
            if (result != nullptr)
            {
                processTag(result, baseDocument, baseFile);
            }
        }
        // Check common.tpl
        else
        {
            if (the::Filesystem::exists(commonTemplateFile) &&
               baseFile != commonTemplateFile)
            {
                DocumentPtr commonDocument = getFileDocument(commonTemplateFile);
                TiXmlElement* comonSearchResult =
                    getSingleTemplate(templateName, commonDocument, commonTemplateFile);
                if (comonSearchResult)
                {
                    //std::cout << "Found common" << std::endl;
                    result = comonSearchResult;
                    if (result != nullptr)
                    {
                        processTag(result, commonDocument, commonTemplateFile);
                    }
                }
            }
            // Check in included files
            if (result == nullptr)
            {
                std::vector<std::string> includedFiles = getIncludedFiles(baseDocument, baseFile);
                std::vector<std::string>::iterator filesIt;
                for (filesIt = includedFiles.begin(); filesIt != includedFiles.end(); ++filesIt)
                {
                    //std::cout << " check file " << *filesIt << std::endl;
                    DocumentPtr fileDocument = getFileDocument(*filesIt);
                    TiXmlElement* fileSearchResult =
                        getSingleTemplate(templateName, fileDocument, *filesIt);
                    if (fileSearchResult) //std::cout<<"Found in include file: " + *filesIt << std::endl;
                    if (fileSearchResult)
                    {
                        if (result == nullptr)
                        {
                            result = fileSearchResult;
                        }
                        else
                        {
                            throw InvalidSceneFormatException("Several templates matches to specifier '" + templateName + "'.");
                        }
                    }
                }
                if (result)
                {
                    processTag(result, baseDocument, baseFile);
                }
            }
        }
    }
    // Local only
    else if (spec.find("local") == 0)
    {
        std::vector<std::string> parts;
        std::replace(spec.begin(), spec.end(), '\\', '/');
        parts = the::split(spec, '/');
        if (parts.size() == 2)
        {
            std::string templateName = parts.back();
            result = getLocalTemplate(templateName, baseDocument, baseFile);
            if (result)
            {
                //std::cout<<"Found local" << std::endl;
                processTag(result, baseDocument, baseFile);
            }
        }
        else
        {
            throw InvalidSceneFormatException("Invalid use of keyword 'local': " + spec);
        }
    }
    // File
    else
    {
        std::string templateName = the::Filesystem::leaf_path(spec);

        std::string src = spec;

        std::string branch = the::Filesystem::branch_path(src);

        std::string path = _the_globalResource->getPath(branch);
        // std::string filePath = solveFileName(src, baseFile);

        DocumentPtr fileDocument = getFileDocument(path);
        result = getSingleTemplate(templateName, fileDocument, path);
        if (result)
        {
            processTag(result, fileDocument, path);
        }
    }

    mIncludeChain.pop_back();

    return result;
}

TiXmlElement*
the::XmlPreprocessor::getTemplatesTag(DocumentPtr baseDocument, const std::string& baseFile)
{
    TiXmlElement* result = nullptr;

    std::vector<TiXmlElement*> templatesElements;
    if (strcmp(baseDocument->RootElement()->Value(), "Templates") == 0)
    {
        templatesElements.push_back(baseDocument->RootElement());
    }
    else
    {
        for (TiXmlElement* child = baseDocument->RootElement()->FirstChildElement();
            child != nullptr;
            child = child->NextSiblingElement())
        {
            if (strcmp(child->Value(), "Templates") == 0)
            {
                templatesElements.push_back(child);
            }
        }
    }
    if (templatesElements.size() == 1)
    {
        TiXmlElement* templatesElement = templatesElements.front();
        result = templatesElement;
    }
    else if (templatesElements.size() > 1)
    {
        throw InvalidSceneFormatException("Several tag 'Templates' in '" +baseFile +"'.");
    }
    else
    {
        debugLog("'Templates' tag not found in " + baseFile);
        // no Templates element, it's ok, result is nullptr
    }
    return result;
}

std::vector<std::string>
the::XmlPreprocessor::getIncludedFiles(DocumentPtr baseDocument,
                                     const std::string& baseFile)
{
    //std::cout << "Get included files. " << std::endl;
    std::vector<std::string> files;
    TiXmlElement* templatesTag = getTemplatesTag(baseDocument, baseFile);
    if (templatesTag)
    {
        for (TiXmlElement* include = templatesTag->FirstChildElement();
            include != nullptr;
            include = include->NextSiblingElement())
        {
            if (strcmp(include->Value(), "Include") == 0)
            {
                std::string file;
                if (include-> GetText() != nullptr)
                {
                    file = include->GetText();
                }
                if (file != "" || include->QueryStringAttribute("path", &file) == TIXML_SUCCESS)
                {
                    std::string filePath = solveFileName(file, baseFile);
                    files.push_back(filePath);
                }
                else
                {
                    throw InvalidSceneFormatException("Include tag without content.");
                }
            }
        }
    }
    //std::cout<<"Found " << files.size() << " included files." << std::endl;
    return files;
}

TiXmlElement*
the::XmlPreprocessor::getLocalTemplate(const std::string& _name,
                                     DocumentPtr baseDocument,
                                     const std::string& baseFile)
{
    beginDebugLogBlock(std::string("Looking for local template ") + _name + std::string(" in ") + std::string(baseFile));

    TiXmlElement* result = nullptr;
    std::string name = _name;
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);

    TiXmlElement* templatesTag = getTemplatesTag(baseDocument, baseFile);
    if (templatesTag)
    {
        std::vector<TiXmlElement*> templates;
        for (TiXmlElement* tpl = templatesTag->FirstChildElement();
            tpl != nullptr;
            tpl = tpl->NextSiblingElement())
        {
            if (strcmp(tpl->Value(), "Template") == 0)
            {
                const char* tplName = tpl->Attribute("name");
                if (tplName != nullptr)
                {
                    std::string tplNameStr = tplName;
                    std::transform(tplNameStr.begin(), tplNameStr.end(), tplNameStr.begin(), ::tolower);
                    debugLog("Looking if " + tplNameStr + " == " + name, false);
                    if (tplNameStr == name)
                    {
                        debugLog(" - match! ", true);
                        templates.push_back(tpl);
                    }
                    else
                    {
                         debugLog(" - decline... ", true);
                    }
                }
            }
        }
        if (templates.size() == 1)
        {
            result = templates.front();
        }
        else if (templates.size() > 1)
        {
            throw InvalidSceneFormatException("Several templates with equal names in local scope.");
        }
    }

    endDebugLogBlock();
    return result;
}

std::string the::XmlPreprocessor::solveFileName(const std::string& file, const std::string& basePath)
{
    std::string resultPath = "";

    beginDebugLogBlock("Solving file name '" + file + "' relative to '" + basePath + "'");

    // Find in base dir
    std::string scenePath = basePath;
    std::string baseDir = the::Filesystem::branch_path(scenePath);

    std::string templatePath = baseDir;
    debugLog("Looking in base directory '" + baseDir + "'", false);
    // templatePath /= file;
    templatePath += "/";
    templatePath += file;
    // templatePath /= filesystem;
    if ( _the_globalResource->isPathExists(templatePath) )
    {
        debugLog(" - found!");
        resultPath = std::string(templatePath);
        //std::cout << "Found in base dir: " << resultPath << std::endl;
    }
    // Find in media/templates dir
    else
    {
        debugLog(" - nope");
        debugLog("Looking in " + templateFolder, false);
        std::string templatesDirRelativePath = templateFolder;
        templatesDirRelativePath += "/";
        templatesDirRelativePath += file;
        //std::cout<<"FIND IN " + templatesDirRelativePath << std::endl;
        if ( _the_globalResource->isPathExists(templatesDirRelativePath) )
        {
            debugLog(" - found!");
            resultPath = std::string(templatesDirRelativePath);
            //std::cout << "Found in templates dirrectory: " << resultPath << std::endl;
        }
        else
        {
            debugLog(" - nope");
            debugLog("Looking in root", false);
            std::string rootRelativePath = file;
            //std::cout<<"FIND IN " + templatesDirRelativePath << std::endl;
            if ( _the_globalResource->isPathExists(rootRelativePath) )
            {
                debugLog(" - found!");
                resultPath = std::string(rootRelativePath);
            }
            else
            {
                debugLog(" - nope");
                throw InvalidSceneFormatException( "Template file '" + file + "' not found.");
            }
        }
    }
    endDebugLogBlock();
    return resultPath;
}

the::DocumentPtr the::XmlPreprocessor::getFileDocument(const std::string& filePath)
{
    DocumentPtr document;

    std::map<std::string, DocumentPtr>::iterator documentIt= mFileToDocument.find(filePath);
    if ( documentIt != mFileToDocument.end())
    {
        document = documentIt->second;
    }
    else
    {
        if (_the_globalResource->isPathExists(filePath))
        {
            document = DocumentPtr(new TiXmlDocument(filePath.c_str()));
            document->LoadFile();
            mFileToDocument.insert(std::pair<std::string, DocumentPtr>(filePath, document));
        }
        else
        {
            throw InvalidSceneFormatException("File " + filePath + " not exists.");
        }
    }
    return document;
}

bool the::XmlPreprocessor::tagsEquivalent(const TiXmlElement*  tag1, const TiXmlElement* tag2)
{
    debugLog("Equivalent check of " + tag1->ValueStr() + std::string(" and ") + tag2->ValueStr(), false);
    std::string id1 = "";
    if (tag1->Attribute("id") != nullptr )
    {
        id1 = tag1->Attribute("id");
        the::trim(id1);
        std::transform(id1.begin(), id1.end(), id1.begin(), ::tolower);
    }
    std::string id2 = "";
    if (tag2->Attribute("id") != nullptr )
    {
        id2 = tag2->Attribute("id");
        the::trim(id2);
        std::transform(id2.begin(), id2.end(), id2.begin(), ::tolower);
    }
    const char* val1 = "";
    std::string val1Str;
    if (tag1->Value() != nullptr)
    {
        val1 = tag1->Value();
        val1Str = val1;
        std::transform(val1Str.begin(), val1Str.end(), val1Str.begin(), ::tolower);
    }
    const char* val2 = "";
    std::string val2Str;
    if (tag2->Value() != nullptr)
    {
        val2 = tag2->Value();
        val2Str = val2;
        std::transform(val2Str.begin(), val2Str.end(), val2Str.begin(), ::tolower);
    }
    if (val1Str == universalTagName || val2Str == universalTagName)
    {
        debugLog(" - equivalent (universal tag name found)");
        return true;
    }
    if (id1 != id2)
    {
        debugLog(" - failed: different id");
        return false;
    }
    else if (val1Str != val2Str)
    {
        debugLog( " - failed: different names");
        return false;
    }
    else
    {
        debugLog(" - equivalent!");
        return true;
    }
}

void the::XmlPreprocessor::applyTemplate(TiXmlElement* _tag, TiXmlElement* templateTag)
{
//if (templateTag == nullptr) //std::cout << "nullptr fuck";
    beginDebugLogBlock("Applying template to " + _tag->ValueStr());
    //std::cout << "Apply template call. Node_name: \n"; //<< _tag->Value() << "; template node_name: " << templateTag->Value() << "\n" << std::endl;
    if (_tag->Attribute("_templates_processed_") == nullptr)
    {
        TiXmlElement* templateElement = templateTag->FirstChildElement();
        if (tagsEquivalent(templateElement, _tag))
        {
            applyTemplateRecursive(_tag, templateElement);
            _tag->SetAttribute("_templates_processed_", "true");
        }
        else
        {
            throw InvalidSceneFormatException("Applying template to the tag different from required in template.");
        }
    }
    endDebugLogBlock();
}

void the::XmlPreprocessor::applyParent(TiXmlElement* tag, TiXmlElement* parentTag)
{

    if (tag->Attribute("_templates_processed_") == nullptr)
    {
        TiXmlElement* templateElement = parentTag->FirstChildElement();
        TiXmlElement* element = tag->FirstChildElement();
        if (tagsEquivalent(templateElement, element))
        {
            applyTemplateRecursive(element, templateElement);
            tag->SetAttribute("_templates_processed_", "true");
        }
        else
        {
            tagsEquivalent(templateElement, element);
            throw InvalidSceneFormatException("Applying parent to the uncompatible template.");
        }
    }

}

void the::XmlPreprocessor::applyTemplateRecursive(TiXmlElement* _tag, TiXmlElement* templateTag)
{
    TiXmlElement* tag = _tag;

    beginDebugLogBlock();

   if (tagsEquivalent(tag, templateTag))
   {
        std::string action = "merge";
        if (tag->Attribute("action") != nullptr)
        {
            action = tag->Attribute("action");
        }

        // If ignore - remove element from xml-tree
        if (action == "ignore")
        {
            debugLog("action = 'ingnore', removing tag");
            tag->Parent()->RemoveChild(tag);
        }
        else if (action == "override")
        {
            debugLog("action = override, nothing to do");
            // Nothing to do
            return;
        }
        else if (action == "merge")
        {
            beginDebugLogBlock("action = 'merge', begin merging...");
            copyAttributes(tag, templateTag);
            std::vector<TiXmlElement*> templateChildren;
            for (TiXmlElement* templateChild = templateTag->FirstChildElement();
                templateChild != nullptr;
                templateChild = templateChild->NextSiblingElement())
            {
                templateChildren.push_back(templateChild);
            }
            // Check on equivalent dublicates in template
            /*for (uint8 i=0; i < templateChildren.size(); ++i)
            {
                for (uint8 j=i+1; j < templateChildren.size(); ++j)
                {
                    if (tagsEquivalent(templateChildren[i], templateChildren[j]))
                    {
                        throw InvalidSceneFormatException("Several equivalent tags in template.");
                    }
                }
            }*/
            // Apply template childs to equivalent tags in current tag
            // or simple add, if no equivalent tags.
            std::vector<TiXmlElement*> tagChildren;
            for (TiXmlElement* tagChild = tag->FirstChildElement();
                tagChild != nullptr;
                tagChild = tagChild->NextSiblingElement())
            {
                tagChildren.push_back(tagChild);
            }
            for (uint8 tpli=0; tpli < templateChildren.size(); ++tpli)
            {
                // Find equivalent tags
                bool applied = false;
                TiXmlElement* templateChild = templateChildren[tpli];
                //std::cout << "\t - Iteration through template children. Tag name: " << templateChild->Value() << "\n";
                for (uint8 ti=0; ti < tagChildren.size(); ++ti)
                {
                    TiXmlElement* tagChild = tagChildren[ti];
                    if (tagsEquivalent(tagChild, templateChild))
                    {
                        debugLog("Found equivalent children " + tagChild->ValueStr() + ", merging them");
                        applyTemplateRecursive(tagChild, templateChild);
                        applied = true;
                    }
                }
                if (!applied)
                {
                    //std::cout << "No equivalent tags, add children\n";
                    debugLog("No equivalent to template tag " + templateChild->ValueStr() + ", adding it");
                    TiXmlNode* toAdd = templateChild->Clone();
                    tag->LinkEndChild(toAdd);
                }
            }
            endDebugLogBlock("Merge finished");
        }
        else
        {
            throw InvalidSceneFormatException("Unknown action '" + action + "'.");
        }
    }
    // Not equivalent
    else
    {
        //std::cout << "Tags are NOT equivalent!\n";
        debugLog("Template tag " + tag->ValueStr() + " and " + tag->ValueStr() + " are not equivalent, adding sibling ");
        TiXmlNode* parent = tag->Parent();
        TiXmlNode* toAdd = templateTag->Clone();
        parent->InsertAfterChild(tag, *toAdd);
    }

    endDebugLogBlock();
}

void the::XmlPreprocessor::copyAttributes(TiXmlElement* tag, TiXmlElement* templateTag)
{
    beginDebugLogBlock("Copying attributes...");
    for (TiXmlAttribute* atr = templateTag->FirstAttribute();
      atr != nullptr;
      atr = atr->Next())
    {
        if (tag->Attribute(atr->Name()) == nullptr)
        {
            debugLog("add " + std::string(atr->Name()) + std::string(" = ") + std::string(atr->Value()));
            tag->SetAttribute(atr->Name(),  atr->Value());
        }
        else
        {
            debugLog("skip " + std::string(atr->Name()) + std::string(" - already specified in target"));
        }
    }
    endDebugLogBlock();
}

void the::XmlPreprocessor::processParamsRec(TiXmlElement* elem, std::list<TiXmlElement*>& paramStack)
{
    if (elem->ValueStr() != "Template")
    {
        beginDebugLogBlock("Processing params at " + elem->ValueStr());
        // push current element
        paramStack.push_back(elem);
        // push children elements with name `Param`
        for (TiXmlElement* paramElem = elem->FirstChildElement(paramTagName); paramElem != nullptr;
            paramElem = paramElem->NextSiblingElement(paramTagName))
        {
            paramStack.push_back(paramElem);
            debugLog(std::string("Found param with id=") + std::string(paramElem->Attribute("id")));
        }
        // Process attributes of current element
        beginDebugLogBlock("Looking for attributes");
        std::list<TiXmlAttribute*> atrToRemove;
        for (TiXmlAttribute* atr = elem->FirstAttribute(); atr != nullptr; atr = atr->Next())
        {
            std::string atrValue = atr->ValueStr();
            the::trim(atrValue);
            std::transform(atrValue.begin(), atrValue.end(), atrValue.begin(), ::tolower);

            for (auto stackTagIt = paramStack.rbegin(); stackTagIt != paramStack.rend(); stackTagIt++)
            {
                auto stackTag = *stackTagIt;
                if (stackTag->Value() == paramTagName && stackTag->Attribute("id") != nullptr )
                {
                    std::string id = stackTag->Attribute("id");
                    the::trim(id);
                    std::transform(id.begin(), id.end(), id.begin(), ::tolower);
                    if (id == atrValue)
                    {
                        if (getParamValueStr(stackTag) != notSetParamValue)
                        {
                            debugLog(std::string("Attribute ") + std::string(atr->Name()) + std::string(" contains param ") + std::string(atr->Value()) +
                                std::string(", replacing with ") + getParamValueStr(stackTag));
                            atr->SetValue(getParamValueStr(stackTag));
                        }
                        else
                        {
                            debugLog(std::string("Attribute ") + std::string(atr->Name()) + std::string(" contains param ") + std::string(atr->Value()) +
                                std::string(", param not set, removing attribute "));
                            atrToRemove.push_back(atr);
                        }
                        break; // for (auto &stackTag : paramStack)
                    }
                }
            }
        }
        for (auto &atr: atrToRemove)
        {
            elem->RemoveAttribute(atr->Name());
        }
        endDebugLogBlock();

        // Process Text children of current element
        beginDebugLogBlock("Looking for element content");
        for (TiXmlNode* textChild = elem->FirstChild(); textChild != nullptr;)
        {
            TiXmlNode* nextChild = textChild->NextSibling();

            std::string elemValue = "";
            if (textChild->Type() == TiXmlNode::TINYXML_TEXT)
            {
                elemValue = textChild->ValueStr();
            }
            else if (textChild->Type() == TiXmlNode::TINYXML_ELEMENT && textChild->ValueStr() == paramPlaceholderTagName)
            {
                ((TiXmlElement*)textChild)->QueryStringAttribute(paramPlaceholderParamAttributeName.c_str(), &elemValue);
            }

            the::trim(elemValue);
            std::transform(elemValue.begin(), elemValue.end(), elemValue.begin(), ::tolower);

            if (elemValue != "")
            {
                for (auto stackTagIt = paramStack.rbegin(); stackTagIt != paramStack.rend(); stackTagIt++)
                {
                    auto stackTag = *stackTagIt;
                    if (stackTag->Value() == paramTagName && stackTag->Attribute("id") != nullptr )
                    {
                        std::string id = stackTag->Attribute("id");
                        the::trim(id);
                        std::transform(id.begin(), id.end(), id.begin(), ::tolower);
                        if (id == elemValue)
                        {
                            // Replace textChild with it's children
                            if (getParamValueStr(stackTag) != notSetParamValue)
                            {
                                auto paramValue = getParamValue(stackTag);
                                int count = 0;
                                for (auto stackTagChild = paramValue.begin(); stackTagChild != paramValue.end(); stackTagChild++)
                                {
                                    elem->LinkEndChild((*stackTagChild)->Clone());
                                    count++;
                                }
                                debugLog("Found param " + std::string(stackTag->Attribute("id")) + std::string(" replacing with param content"));
                            }
                            else
                            {
                                debugLog("Found param " + std::string(stackTag->Attribute("id")) + std::string(". Param value not set."));
                            }
                            elem->RemoveChild(textChild);
                            break; // for (auto &stackTag : paramStack)
                        }
                    }
                } // for (auto &stackTag : paramStack)
            } // if (textChild->Type() == TiXmlNode::Text)
            textChild = nextChild;
        }
        endDebugLogBlock();
        // Do recurse for children elements
        for (TiXmlElement* child = elem->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
        {
            if (child->Value() != paramTagName)
            {
                processParamsRec(child, paramStack);
            }
        }
        // Pop all up to current element, include current element, and delete Param tags
        for (TiXmlElement* stackElem = paramStack.back(); stackElem != elem; stackElem = paramStack.back())
        {
            paramStack.erase(--paramStack.end());
            elem->RemoveChild(stackElem);
        }
        paramStack.remove(elem);

        endDebugLogBlock("Finished params processing at " + elem->ValueStr());
    }
}

void the::XmlPreprocessor::clearStuffRec(TiXmlElement* current)
{
    current->RemoveAttribute("_templates_processed_");
    current->RemoveAttribute("template");
    current->RemoveAttribute("id");
    current->RemoveAttribute("ifdef");
    current->RemoveAttribute("ifndef");
    current->RemoveAttribute(debugLogAttributeName);
    for (TiXmlElement* child = current->FirstChildElement(); child != nullptr; child = child->NextSiblingElement())
    {
        clearStuffRec(child);
    }
}

std::string the::XmlPreprocessor::getParamValueStr(TiXmlElement* paramElem)
{
    std::string value;
    if (paramElem->QueryStringAttribute("value", &value) == TIXML_SUCCESS)
    {
        return value;
    }
    else
    {
        if (paramElem->GetText() != nullptr)
        {
            return paramElem->GetText();
        }
        else
        {
            return "";
        }
    }

}

std::list<TiXmlNode*> the::XmlPreprocessor::getParamValue(TiXmlElement* paramElem)
{
    std::list<TiXmlNode*> result;

    std::string value;
    if (paramElem->QueryStringAttribute("value", &value) == TIXML_SUCCESS)
    {
        result.push_back(new TiXmlText(value));
    }
    else
    {
        for (TiXmlNode* child = paramElem->FirstChild(); child != nullptr;
                                child = child->NextSibling())
        {
            result.push_back(child);
        }
    }

    return result;
}

bool the::XmlPreprocessor::parseIfdef(std::string def)
{
    // Check valid
    int counter = 0;
    for (int i=0; i< (int)def.size(); i++)
    {
        if (def[i] == '(') counter++;
        else if(def[i] == ')') counter--;
    }
    if (counter != 0)
    {
        throw InvalidSceneFormatException("Braces mismatch in '" + def + "'.");
    }

    // Solve each deepest brace one by one, by replacing it with result
    std::string expr = def;
    size_t open = expr.find_last_of("(");
    size_t close = expr.find_first_of(")", open);
    while (open != expr.npos)
    {
        if (open != expr.npos)
        {
            std::string flatSubstr = expr.substr(open+1, (close - open)-1);
            bool flatSubstrSolve = parseFlatIfdef(flatSubstr);
            std::string substrReplace = (flatSubstrSolve)? "true" : "false";
            expr.replace(open, (close - open)+1, substrReplace);
        }
        open = expr.find_last_of("(");
        close = expr.find_first_of(")", open);
    }
    return parseFlatIfdef(expr);
}

bool the::XmlPreprocessor::parseFlatIfdef(std::string def)
{
    // Parse OR entries
    std::vector<std::string> orSplit = split(def, '|');
    bool conditionSuccess = false;
    for (auto orEntry : orSplit)
    {
        trim(orEntry);
        if (!orEntry.empty())
        {
            // Parse AND entries
            bool andSuccess = true;
            std::vector<std::string> andSplit = split(orEntry, '&');
            for (auto andEntry : andSplit)
            {
                trim(andEntry);
                if (!andEntry.empty())
                {
                    // Process negative symbol
                    bool negative = false;
                    std::string var = andEntry;
                    if (andEntry[0] == '!')
                    {
                        std::string var = std::string(andEntry, 1, andEntry.npos);
                        trim(var);
                        negative = true;
                    }
                    if ((defined(var) && negative) || (!defined(var) && !negative))
                    {
                        andSuccess = false;
                        break;
                    }
                }
            }
            if (andSuccess)
            {
                conditionSuccess = true;
                break;
            }
        }
    }
    return conditionSuccess;
}
