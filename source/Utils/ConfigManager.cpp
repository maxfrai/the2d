////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/InfrastructureCore.hpp>
#include <The2D/Utils/ConfigManager.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/Utils/Utils.hpp>
#include <assert.h>

the::ConfigManager::ConfigManager()
    : mTreePath( "Settings.info")
{
    mUUIDs = 0;
    // read_info(mTreePath, mTreeHandle);
}

std::vector<std::string> the::ConfigManager::getListValue(const std::string &key) const
{
    // using     property_tree::ptree;
    std::vector<std::string> listValues;

    // for (const ptree::value_type &value : mTreeHandle.get_child(key))
    //     listValues.push_back(value.second.data());

    return listValues;
}

the::int32 *the::ConfigManager::Int(const std::string &key)
{
    assert(mIntegerStorage.find(key) != mIntegerStorage.end() && "No such key in world settings!" );
    return &mIntegerStorage.at(key);
}
float *the::ConfigManager::Float(const std::string &key)
{
    assert(mFloatStorage.find(key) != mFloatStorage.end() && "No such key in world settings!" );
    return &mFloatStorage.at(key);
}
std::string *the::ConfigManager::String(const std::string &key)
{
    assert(mStringStorage.find(key) != mStringStorage.end() && "No such key in world settings!" );
    return &mStringStorage.at(key);
}

the::int32 *the::ConfigManager::setInt(const std::string &key, int32 value)
{
    mIntegerStorage[key] = value;
    return &mIntegerStorage.at(key);
}
float *the::ConfigManager::setFloat(const std::string &key, float value)
{
    mFloatStorage[key] = value;
    return &mFloatStorage.at(key);
}
std::string *the::ConfigManager::setString(const std::string &key, std::string value)
{
    mStringStorage[key] = value;
    return &mStringStorage.at(key);
}

std::string the::ConfigManager::randomUUID()
{
    return std::string("id#").append(the::lexical_cast<std::string>(mUUIDs++));
}
