////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/XmlLoader.hpp>
#include <The2D/ResourceService.hpp>
#include <The2D/Utils/Filesystem.hpp>

the::XmlLoader::XmlLoader(InfrastructureCore* core)
{
    mCore = core;

#ifdef DEBUG
    mPreprocessor.define("DEBUG");
#endif
#ifdef WIN32
    mPreprocessor.define("WIN32");
    mPreprocessor.define("WINDOWS");
#endif
#ifdef WIN64
    mPreprocessor.define("WIN64");
    mPreprocessor.define("WINDOWS");
#endif
#ifdef NIX
    mPreprocessor.define("NIX");
    mPreprocessor.define("LINUX");
#endif
#ifdef NIX64
    mPreprocessor.define("NIX64");
    mPreprocessor.define("NIX");
    mPreprocessor.define("LINUX");
#endif
#ifdef PC_PLATFORM_BUILD
    mPreprocessor.define("PC");
    mPreprocessor.define("PC_PLATFORM_BUILD");
#else
    mPreprocessor.define("NONPC_BUILD_PLATFORM");
    mPreprocessor.define("ANDROID");
    mPreprocessor.define("IOS");
#endif

}

the::XmlLoader::~XmlLoader()
{
}

the::XmlPreprocessor* the::XmlLoader::getPreprocessor()
{
    return &mPreprocessor;
}

void the::XmlLoader::registerCreator(std::shared_ptr<IEntityCreator> realization)
{
    ASSERT(realization.get() != nullptr, ":(");

    mCreators.push_back(realization.get());
    mStoredCreators.push_back(realization);
}

void the::XmlLoader::registerCreator(IEntityCreator* realization)
{
    ASSERT(realization != nullptr, ":(");

    mCreators.push_back(realization);
}

void the::XmlLoader::unregisterCreator(const IEntityCreator* realization)
{
    for(auto target = mCreators.begin(); target != mCreators.end(); target++)
    {
        if(*target == realization)
        {
            mCreators.erase(target);
            break;
        }
    }
}

std::shared_ptr<the::GameEntity> the::XmlLoader::createEntity(const the::string& type) const
{
    ASSERT(!mCreators.empty(), "Creators list is empty!");

    for (IEntityCreator* creator : mCreators)
    {
        std::shared_ptr<GameEntity> ent = creator->createEntity(type);
        if (ent.get() != nullptr)
        {
            return ent;
        }
    }

    return std::shared_ptr<GameEntity>();
}

the::GameEntity* the::XmlLoader::loadSubTree(const the::string& xmlFilePath, bool doInitialization)
{
    auto tree = load(xmlFilePath, mCore->tree());
    mCore->tree()->addChild(std::static_pointer_cast<GameEntity>(tree), BEFORE_PARENT);
    if (doInitialization)
    {
        INFO(">> Initializing '%s'", xmlFilePath);

        bool ok = tree->init();
        if (!ok)
        {
            ERR("Errors occured during initialization of tree loaded from %s", xmlFilePath);
        }

        INFO("<< Finished initialization of '%s'", xmlFilePath);
    }
    return tree.get();
}

the::GameEntity* the::XmlLoader::loadSubTree(TiXmlElement* xml, DocumentPtr baseDocument, const string& documentFile, bool doInitialization)
{
    auto loadtree = load(xml, baseDocument, documentFile, mCore->tree());
    mCore->tree()->addChild(std::static_pointer_cast<GameEntity>(loadtree), BEFORE_PARENT);
    if (doInitialization)
    {
        INFO(">> Initializing '%s'", documentFile);

        bool ok = loadtree->init();
        if (!ok)
        {
            ERR("Errors occured during initialization of tree loaded from xml");
        }

        INFO("<< Finished initialization of '%s'", documentFile);
    }
    return loadtree.get();
}

void the::XmlLoader::unloadSubTree(GameEntity* subTree)
{
    ASSERT(subTree != nullptr, ":(");

    subTree->deinit();
    if (subTree->getParent() == mCore->tree())
    {
        mCore->tree()->removeChild(subTree);
    }
    else
    {
        TH_FAIL("XmlLoader can unload only these subtrees, that it loaded before.");
    }
}

std::shared_ptr<the::GameEntity> the::XmlLoader::load(const the::string& xmlFilePath, GameEntity* parent)
{
    DocumentPtr document = DocumentPtr(new TiXmlDocument(xmlFilePath.c_str()));
    if (!document->LoadFile())
    {
        ERR("Failed to load `%s` xml-file, xmlFilePath", xmlFilePath);
        return std::shared_ptr<the::GameEntity>();
    }

    TiXmlElement* document_element = document->RootElement();
    return load(document_element, document, xmlFilePath, parent);
}

void the::XmlLoader::parse(const the::string& xmlFilePath, GameEntity* tree)
{
    DocumentPtr document = DocumentPtr(new TiXmlDocument(xmlFilePath.c_str()));
    if (!document->LoadFile())
    {
        ERR("Failed to load `%s` xml-file, xmlFilePath", xmlFilePath);
    }

    TiXmlElement* document_element = document->RootElement();
    parse(document_element, document, xmlFilePath, tree);
}

std::shared_ptr<the::GameEntity> the::XmlLoader::load(TiXmlElement* xml, DocumentPtr baseDocument, const string& documentFile, GameEntity* parent)
{
    ASSERT(parent != nullptr, "Parsing cant process outside of game tree");

    std::shared_ptr<the::GameEntity> tree(createEntity(xml->Value()));
    if (tree.get() != nullptr)
    {
        tree->setParent(parent, BEFORE_PARENT);
        tree->setName(Filesystem::leaf_path(documentFile));
        parse(xml, baseDocument, documentFile, tree.get());
    }
    else
    {
        ERR("There is no creator, registered in core()->xml(), that can create '%s'.", xml->Value());
    }
    return tree;
}
void the::XmlLoader::parse(TiXmlElement* xml, DocumentPtr baseDocument, const string& documentFile, GameEntity* tree)
{
    ASSERT(tree!= nullptr, "Invalid parameter");
    ASSERT(tree->isInGameTree(), "Parsing cant process outside of game tree");
    INFO(">> Parsing '%s' scene file", documentFile);

    // Templates processing
    TiXmlElement* root = nullptr;
    try
    {
        root = mPreprocessor.process(xml, baseDocument, documentFile);
    }
    catch(const InvalidSceneFormatException &e)
    {
        ERR("Failed to parse scene: %s", e.Message);
        return;
    }

    if (tree->getParser()->isMyTag(root))
    {

        auto errors = tree->getParser()->parse(root);
        for (auto errIt = errors.begin(); errIt != errors.end(); errIt++)
        {
            ParsingError &err = *errIt;

            if (err.Critical)
            {
                CRITICAL("%s[%i]: %s", documentFile.c_str(), err.Row, err.Message.c_str());
            } else {
                WARN("%s[%i]: %s", documentFile.c_str(), err.Row, err.Message.c_str());
            }
        }
    }
    else
    {
        CRITICAL("Can't load %s of %s to %s, because tag is not correct",
                 xml->Value(), documentFile, tree->getShortInfoString());
    }

    delete root;
    root = nullptr;

    INFO("<< Finished parsing '%s' scene file", documentFile);
}










