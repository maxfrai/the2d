////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/InfrastructureCore.hpp>
#include <The2D/LogService.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/Config.hpp>

#include <chrono>

std::string the::LogInterface::ADDITIONAL_INFO_SEPARATOR = "|";
std::string the::LogCoroutine::SEPARATOR = "[:]";
the::LogService* the::_the_globalLog = nullptr;
bool the::__THE_RUN_SIMULATION = false;

void the::LogInterface::logLogic(const std::string& formattedLine, the::LogType type,
    const char* func, const char* file, const char* cmdate, const char* cmtime, int line) const
{
    the::LogService::logLogic(formattedLine, type, func, file, cmdate, cmtime, line);
}

// LogService --------------------
void the::LogService::logLogic(const std::string& formattedLine, the::LogType type,
    const char* func, const char* file, const char* cmdate, const char* cmtime, int line)
{
        // Now we have a formatted string where title and subtext should be
        // splitted using | character
        std::vector<std::string> x = the::split(formattedLine, '|');

        std::string title = (x.size() > 0)? x[0] : "<empty message>";
        std::string data;
        if (x.size() > 1)
            data = x[1];

        const char* dataFormat = "%s[%s:%d] compiled at %s - %s\nin function: %s\n";
        std::string buf = the::format(
            dataFormat, static_cast<char>(type), file, line, cmdate, cmtime, func);
        if (!data.empty())
            buf.append("Additional information:\n");
        buf.append(data);
        if (type == the::LogType::LT_ERROR)
        {
            buf.append("Back trace:\n");
            buf.append(getStacktrace());
        }

        // LOGD(title.c_str());
        _the_globalLog->pushMessage(title, buf);

        if (type == the::LogType::LT_ERROR || type == the::LogType::LT_WARNING)
        {
           // _the_globalLog->waitAllMessagesSent();
        }
}

the::LogService::LogService()
{
    mCoroutineHandle = new the::LogCoroutine;
}

the::LogService::~LogService()
{
    delete mCoroutineHandle;
}

void the::LogService::pushMessage(const the::string& title, const the::string& data)
{
    std::cout << "[MSG!!!!!!!!] " << title << std::endl << data << std::endl;
    mCoroutineHandle->pushMessage({title, data});
}

void the::LogService::stopService()
{
    mCoroutineHandle->stopLogic();
}

void the::LogService::waitAllMessagesSent(int maxTimeToWaitMs)
{
    int begin = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    int current = begin;
    while (mCoroutineHandle->messageQueueSize() > 0 && (current - begin) < maxTimeToWaitMs)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds((int)(mCoroutineHandle->WAIT_TIME_MS * 3.5f)));
        current = std::chrono::duration_cast<std::chrono::milliseconds>(
                        std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    }
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

the::LogCoroutine::LogCoroutine()
    : mInited(false), mRunning(false)
{
    std::cout << "Okay, ctor, init everything...\n";
    if (onInit())
    {
        createServer();
        runLogic();
    }
    else
    {
        std::cout << "Oh shit =(\n";
    }
}

the::LogCoroutine::~LogCoroutine()
{
    std::cout << "Closing server.\n";
    onDeinit();
}

// -----------------------------------------------------
bool the::LogCoroutine::onInit()
{
    if (enet_initialize () != 0)
    {
        std::cout << "An error occurred while initializing ENet.\n";
        return false;
    }
    else
    {
        std::cout << "Waiting for log clients before running simulation...\n";
    }
    enet_time_set(0);

    return true;
}

void the::LogCoroutine::onDeinit()
{
    for (auto peer : mClients)
    {
        enet_peer_disconnect_later(peer, 42);
        enet_peer_reset(peer);
    }

    enet_host_destroy(mServer);
    enet_deinitialize();
}
// -----------------------------------------------------
void the::LogCoroutine::createServer()
{
    if (mInited)
        return;

    mAddress.host = ENET_HOST_ANY;
    mAddress.port = SERVER_PORT;

    mServer = enet_host_create(
        &mAddress,       // The address to bind the server host to
        MAX_CONNECTIONS, // Allow up to 32 clients and/or outgoing connections
        2,               // Allow up to 2 channels to be used, 0 and 1
        0,               // Assume any amount of incoming bandwidth
        0                // Assume any amount of outgoing bandwidth
    );

    if (nullptr == mServer)
        std::cout << "An error occurred while trying to create an ENet server host.\n";
    else
        mRunning = true;
}

void the::LogCoroutine::runLogic()
{
    mBackgroundWorker = std::thread(&the::LogCoroutine::logic, this);
    mBackgroundWorker.detach();
}

void the::LogCoroutine::stopLogic()
{
    std::lock_guard<std::mutex> lock(mMutex);
    mRunning = false;
}

void the::LogCoroutine::logic()
{
    bool runState = false;
    mMutex.lock();
    runState = mRunning;
    mMutex.unlock();
    while (runState)
    {
        _serverLogic();
        _senderLogic();

        // Okay, handle the pause by ourself
        std::this_thread::sleep_for(std::chrono::milliseconds(WAIT_TIME_MS));

        mMutex.lock();
        runState = mRunning;
        mMutex.unlock();
    }
}

void the::LogCoroutine::_serverLogic()
{
    ENetEvent event;
    while (enet_host_service(mServer, &event, 0) > 0)
    {
        switch (event.type)
        {
            case ENET_EVENT_TYPE_CONNECT:
                std::cout << "Somebody has connected\n";
                pushPeer(event.peer);
                break;

            case ENET_EVENT_TYPE_RECEIVE:
                // This would be used when clients will be able to send commands
                std::cout << "Received some shit\n";
                break;

            case ENET_EVENT_TYPE_DISCONNECT:
                // Remove peer from list here
                std::cout << "Somebody has disconnected\n";
                removePeer(event.peer);
                break;

            case ENET_EVENT_TYPE_NONE:
            default:
                break;
        }
    }
}

void the::LogCoroutine::_senderLogic()
{
    while (!mMessages.empty() && !mClients.empty())
    {
        mMutex.lock();
        LogMessage nextMessage = mMessages.front();
        mMutex.unlock();

        _sendMessage(nextMessage);

        mMutex.lock();
        mMessages.pop_front();
        mMutex.unlock();
    }
}
// -----------------------------------------------------
void the::LogCoroutine::pushPeer(ENetPeer* pPeer)
{
    if (nullptr == pPeer)
        return;

    std::lock_guard<std::mutex> lock(mMutex);
    the::__THE_RUN_SIMULATION = true;
    mClients.push_back(pPeer);
}

void the::LogCoroutine::removePeer(ENetPeer* pPeer)
{
    if (nullptr == pPeer)
        return;

    std::lock_guard<std::mutex> lock(mMutex);
    mClients.remove(pPeer);
}

void the::LogCoroutine::pushMessage(const LogMessage& message)
{
    std::lock_guard<std::mutex> lock(mMutex);
    mMessages.push_back(message);
}
int the::LogCoroutine::messageQueueSize()
{
    std::lock_guard<std::mutex> lock(mMutex);
    return mMessages.size();
}
// -----------------------------------------------------
void the::LogCoroutine::_sendMessage(const LogMessage& message)
{
    // Don't call this function directly!
    // It has no thread synhronisation, and don't even try to insert it
    if (!mClients.empty())
    {
        std::string data = message.title + SEPARATOR + message.message;
        ENetPacket* packet =
            enet_packet_create(data.c_str(),
                data.size() + 1,
                ENET_PACKET_FLAG_RELIABLE);

        for (auto peer : mClients)
            _sendData(peer, packet);

        enet_host_flush(mServer);
    }
    else
    {
#ifdef PC_PLATFORM_BUILD
        std::cout << "[MSG!!!!!!!!] " << message.title << std::endl << message.message << std::endl;
#else
        LOGD("%s |%s", message.title.c_str(), message.message.c_str());
#endif
    }
}
void the::LogCoroutine::_sendData(ENetPeer* pPeer, ENetPacket* pData)
{
    if (nullptr == pPeer || nullptr == pData)
        return;

    enet_peer_send(pPeer, 0, pData);
}