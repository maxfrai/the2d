////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/InfrastructureCore.hpp>
#include <The2D/MessageCore.hpp>
#include <The2D/MessageHelper.hpp>
#include <The2D/Component.hpp>


the::MessageCore::MessageCore()
{
}

the::MessageCore::~MessageCore()
{
}

void the::MessageCore::subscribe(uint32 msgType, GameEntity* subscriber, std::function<void (Message*)> callback)
{
    uint16 libID = getLibraryId(msgType);
    uint8 componentID = getOwnerId(msgType);
    uint8 messageID = getMessageId(msgType);
    mMap[libID][componentID][messageID].push_back(Subscriber(subscriber, callback));

    // Notify, that someone subscribed
    MessageSendData<SubscriberInfo> msg(SubscriberInfo(msgType, subscriber, callback),
        MSG_SUBSCRIBED, Message::MESSAGE_CORE, nullptr);
    broadcast(&msg);
}

void the::MessageCore::unsubscribe(GameEntity* subscriber, uint32 msgType)
{
    uint16 libID = getLibraryId(msgType);
    uint8 componentID = getOwnerId(msgType);
    uint8 messageID = getMessageId(msgType);
    std::function<void (Message*)>* callback = nullptr;
    auto libIt = mMap.find(libID);
    if (libIt != mMap.end())
    {
        ComponentsToMessages& compMap = libIt->second;
        auto compIt = compMap.find(componentID);
        if (compIt != compMap.end())
        {
            MessagesToCallbacks& msgMap = compIt->second;
            auto msgIt = msgMap.find(messageID);
            std::list<Subscriber>::iterator toRemove = msgIt->second.end();
            for (auto sit = msgIt->second.begin(); sit != msgIt->second.end(); sit++)
            {
                if (sit->first == subscriber)
                {
                    callback = &sit->second;
                    toRemove = sit;
                    break;
                }
            }
            if (toRemove != msgIt->second.end())
            {
                 //Notify, that someone unsubscribed
                MessageSendData<SubscriberInfo> msg(SubscriberInfo(msgType, subscriber, *callback),
                    MSG_UNSUBSCRIBED, Message::MESSAGE_CORE, nullptr);
                broadcast(&msg);

                msgIt->second.erase(toRemove);
            }

            if (msgIt->second.size() == 0)
            {
                msgMap.erase(msgIt);
            }

            if (msgMap.size() == 0)
            {
                compMap.erase(compIt);
            }
        }
        if (compMap.size() == 0)
        {
            mMap.erase(libIt);
        }
    }
}

the::uint16 the::MessageCore::broadcast(Message* msg)
{
    int subscribersCount = 0;
    uint32 msgType = msg->mType;
    uint16 libID = getLibraryId(msgType);
    uint8 componentID = getOwnerId(msgType);
    uint8 messageID = getMessageId(msgType);
    auto libIt = mMap.find(libID);
    if (libIt != mMap.end())
    {
        ComponentsToMessages& compMap = libIt->second;
        auto compIt = compMap.find(componentID);
        if (compIt != compMap.end())
        {
            MessagesToCallbacks& msgMap = compIt->second;
            auto msgIt = msgMap.find(messageID);
            if (msgIt != msgMap.end())
            {
                for (auto sit = msgIt->second.begin(); sit != msgIt->second.end(); sit++)
                {
                    sit->second(msg);
                    subscribersCount++;
                }
            }
        }
    }
    return subscribersCount;
}

void the::MessageCore::init()
{
    enum MessageType
    {
        ID_MSG_TREE_UPDATED,
        ID_MSG_GET_PARENT_OBJECT,
        ID_MSG_GET_MAGNET_PROPERTY,
        ID_MSG_GET_TRIANGULATION,
        ID_MSG_GET_ATOMIZATION
    };
}

std::list<the::Subscriber> the::MessageCore::getSubscribers(uint32 msgType)
{
    uint16 libID = getLibraryId(msgType);
    uint8 componentID = getOwnerId(msgType);
    uint8 messageID = getMessageId(msgType);
    auto libIt = mMap.find(libID);
    if (libIt != mMap.end())
    {
        ComponentsToMessages& compMap = libIt->second;
        auto compIt = compMap.find(componentID);
        if (compIt != compMap.end())
        {
            MessagesToCallbacks& msgMap = compIt->second;
            auto msgIt = msgMap.find(messageID);
            if (msgIt != msgMap.end())
            {
                return msgIt->second;
            }
        }
    }
    return std::list<the::Subscriber>();
}
