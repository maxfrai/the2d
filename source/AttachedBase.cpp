////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/AttachedBase.hpp>

using namespace the;

AttachedBase::AttachedBase(const string& targetType, bool isStatic)
    : mTargetType(targetType), mIsStatic(isStatic), mID(INVALID_ID), mClassID(INVALID_ID), mParent(nullptr)
{
}

AttachedBase::~AttachedBase()
{
}


bool AttachedBase::isStatic() const
{
    return mIsStatic;
}
const string& AttachedBase::getTargetType()
{
    return mTargetType;
}

void AttachedBase::setID(int id, int classID)
{
    mID = id;
    mClassID = classID;
}

sptr<AttachedBase> AttachedBase::getSource()
{
    return sptr<AttachedBase>();
}

void AttachedBase::setSource(sptr<AttachedBase> source)
{
}
void AttachedBase::createSource()
{
}
string AttachedBase::getStaticAttachedParentType()
{
    return "";
}

void AttachedBase::onSetCore(InfrastructureCore* old, InfrastructureCore* newVal)
{
    if (old != nullptr)
    {
        old->attached()->unregisterProperty(this);
    }
    if (newVal != nullptr)
    {
        newVal->attached()->registerProperty(this);
    }
}
