////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Object.hpp>
#include <The2D/MessageHelper.hpp>
#include <The2D/LogService.hpp>
#include <The2D/Utils/ConfigManager.hpp>




the::Object::Object()
{
    mAlwaysDraw = false;
}

the::Object::~Object()
{
}


bool the::Object::getAlwaysDraw() const
{
    return mAlwaysDraw;
}
void the::Object::setAlwaysDraw(bool state)
{
    mAlwaysDraw = state;
}

void the::Object::onIncomingMessage(Message* msg)
{
}
