////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/RttiSupport.hpp>
#include <functional>

const the::string the::RemoveNamespaceAndTemplateSpec(the::string str)
{
    int lastDotsPos = -1;
    for(int i =str.length()-1; i>=0; i--)
    {
        if(str[i] == ':')
        {
            lastDotsPos = i;
            break;
        }
    }
    int firstBracePos = str.length();
    for(int i =0; i< (int)str.length(); i++)
    {
        if(str[i] == '<')
        {
            firstBracePos = i;
            break;
        }
    }

    return the::string(str, lastDotsPos+1, firstBracePos - lastDotsPos-1);
}

bool the::The2DTypeInfo::equalTo(const The2DTypeInfo& type) const
{
    if (fullNameHash != type.fullNameHash)
    {
        return false;
    }
    else
    {
        return fullName == type.fullName;
    }
}

bool the::The2DTypeInfo::operator==(const The2DTypeInfo& type) const
{
    return equalTo(type);
}

bool the::The2DTypeInfo::operator!=(const The2DTypeInfo& type) const
{
    return !equalTo(type);
}

bool the::The2DTypeInfo::is(const The2DTypeInfo& type) const
{
    if(*this == type) return true;

    for(auto &base : baseTypes)
    {
        if(*base == type) return true;
        else if (base->is(type)) return true;
    }
    return false;
}

the::The2DTypeInfo::The2DTypeInfo(the::string Name,
    the::string FullName,
    uint16 EngineID,
    size_t Size,
    the::IEntityConstructor* Constructor,
    std::vector<const The2DTypeInfo*>  BaseTypes):
        name(Name),
        nameHash(getHashCode(Name)),
        fullName(FullName),
        engineID(EngineID),
        fullNameHash(getHashCode(FullName)),
        size(Size),
        constructor(Constructor),
        baseTypes(BaseTypes)
{
}

the::The2DTypeInfo::~The2DTypeInfo()
{
    delete constructor;
}

std::ostream& operator << (std::ostream& s, const the::The2DTypeInfo& d)
{
    s << d.fullName;
    return s;
}

const the::The2DTypeInfo the::The2DTypeInfoTypeInfo("The2DTypeInfo", typeid(the::The2DTypeInfo).name(), 0,
        sizeof(the::The2DTypeInfo), nullptr, {});