////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/LogService.hpp>
#include <The2D/GameEntity.hpp>
#include <assert.h>
#include <cstring>

bool the::StateMachine::mAdvanceState = false;

void the::StateMachine::init()
{
    mActiveState = nullptr;
}

void the::StateMachine::deinit()
{
    for (auto &state : mStates)
        state->shutdown();
}

void the::StateMachine::push(std::shared_ptr<the::IState> state)
{
    mStateChanged = true;
    mStates.push_back(state);
}

void the::StateMachine::readyToAdvance()
{
    mAdvanceState = true;
}

void the::StateMachine::update(float dSeconds)
{
    if (mStates.empty() && !mStateChanged) return;
    if (mActiveState == nullptr) this->pop();

    if (mAdvanceState)
    {
        mActiveState->shutdown();
        INFO("Exiting from '%s' game state", mActiveState->type());
        mAdvanceState = false;

        /*TODO: Remove all entities, etc. */

        this->pop();
    }
    else
    {
        mActiveState->update(dSeconds);
    }
}

the::StateMachine::StateMachine(InfrastructureCore* core)
    : mStateChanged(false), mCore(core)
{}


the::StateMachine::~StateMachine()
{
    //TODO: Delete all entities
    while (!mStates.empty())
    {
        mStates.pop_back();
    }
}

void the::StateMachine::pop()
{
    assert(!mStates.empty() && "There aren't any states now. Maybe, you didn't push one.");

    mActiveState = mStates.front(); // Store pointer for next state to use
    mStates.pop_front();            // And remove it from list

    INFO("Activating '%s' game state", mActiveState->type());

    mActiveState->init(mCore);
}

std::shared_ptr<the::IState> the::StateMachine::getActiveState() const
{
    assert(mActiveState == nullptr && "There is no active state. Maybe, you didn't push one.");
    return mActiveState;
}
