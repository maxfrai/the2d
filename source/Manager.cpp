////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Manager.hpp>
#include <The2D/ObserverService.hpp>




the::Manager::Manager()
{
}

the::Manager::~Manager()
{
}

void the::Manager::onParentChanged()
{
    if (isInGameTree())
    {
        core()->observer()->onRegisterManager(this);
    }
}

void the::Manager::onParentChanging(GameEntity* ent)
{
    if (isInGameTree() && (ent == nullptr || !ent->isInGameTree()))
    {
        core()->observer()->onUnregisterManager(this);
    }
}