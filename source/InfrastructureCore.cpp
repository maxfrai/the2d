////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/InfrastructureCore.hpp>
#include <The2D/GameTree.hpp>
#include <The2D/StateMachine.hpp>
#include <The2D/MessageCore.hpp>
#include <The2D/GameLoopProcessor.hpp>
#include <The2D/XmlLoader.hpp>
#include <The2D/Rtti.hpp>
#include <The2D/InitService.hpp>
#include <The2D/ObserverService.hpp>
#include <The2D/ResourceService.hpp>
#include <The2D/StatisticService.hpp>
#include <The2D/BindService.hpp>
#include <The2D/AttachedService.hpp>

#include <The2D/LogService.hpp>
#include <The2D/Utils/ConfigManager.hpp>
#include <The2D/StateMachine/FiniteStateMachine.hpp>
#include <The2D/StateMachine/UnitedState.hpp>
#include <The2D/StateMachine/Exec.hpp>
#include <The2D/StateMachine/SetBool.hpp>
#include <The2D/StateMachine/TransGroup.hpp>
#include <The2D/AttachedBase.hpp>

#include <iostream>

// hardware info
// #include <thread>

the::InfrastructureCore::InfrastructureCore()
{
    mInitialized = false;

    mRtti = new Rtti();
    mRtti->registerType<GameTree>();
    mRtti->registerType<FiniteStateMachine>();
    mRtti->registerType<State>();
    mRtti->registerType<UnitedState>();
    mRtti->registerType<Action>();
    mRtti->registerType<Exec>();
    mRtti->registerType<SetBool>();
    mRtti->registerType<Trans>();
    mRtti->registerType<TransGroup>();
    mRtti->registerType<AttachedBase>();

    mLog = new LogService;
    mAttached = new AttachedService(this);
    mStatistics = new StatisticService(this);
    _the_globalLog = mLog;
    SRVCMD("StartGame");
    mConfig = new ConfigManager;
    mInitService = new InitService(this);
    mTree = new GameTree();
    mTree->setName("root");
    mTree->setXmlTagName("root");
    mTree->setNoParent();
    mState = new StateMachine(this);
    //mScript = nullptr; //new ScriptManager(mConfig, mLog);
    mMessages = new MessageCore;
    mLoopProcessor = new GameLoopProcessor(this);
    mXmlLoader = new XmlLoader(this);
    mXmlLoader->registerCreator(mRtti);
    mObserver = new ObserverService(this);
    mObserver->onRegister(mTree);
    mResource = new ResourceService();
    _the_globalResource = mResource;
    mBind = new BindService();

    REGISTER_IN(tree(), mBind);
}

the::InfrastructureCore::~InfrastructureCore()
{
    INFO("======================================================================");
    INFO(">>>>>>>>>>>>>>>   [Destroying all systems]");
    INFO("======================================================================");
    //delete mResource;

    delete mState;
    
    delete mBind;
    delete mTree;
    
    //if (mScript) delete mScript;
       
    delete mConfig;
    delete mXmlLoader;
    
    delete mMessages;
    delete mLoopProcessor;
    
    delete mInitService;
    delete mObserver;

    delete mStatistics;
    delete mAttached;
    delete mLog;
    delete mRtti;
}

int the::InfrastructureCore::runGame()
{
    init();
    //mScript->runFile("Objects/Default.py");
    #ifdef PC_PLATFORM_BUILD
        loop()->start();
    #else
        #warning "Don't call runGame method in non pc build!"
    #endif
    deinit();
    return 0;
}
void the::InfrastructureCore::exitGame()
{
    #ifdef PC_PLATFORM_BUILD
        loop()->stop();
    #else
        #warning "Don't call exitGame method in non pc build!"
    #endif
}

void the::InfrastructureCore::init()
{
    // Reduce twice-called initialization
    if (!mInitialized)
    {
        INFO("======================================================================");
        INFO(">>>>>>>>>>>>>>>   [Services initialization]");
        INFO("======================================================================");

        //INFO("----- Scripting tools...");
        //mScript->init();
        INFO("----- Game states...");
        state()->init();
        INFO("----- Messagging system...");
        msg()->init();
        INFO("----- Loop processor...");
        loop()->init();

        // INFO("======================================================================");
        // INFO(">>>>>>>>>>>>>>>   [Information about system]");
        // INFO("======================================================================");
        // INFO("Graphic vendor : %s", glGetString(GL_VENDOR));
        // INFO("Graphic adapter: %s", glGetString(GL_RENDERER));
        // INFO("Graphic version: %s", glGetString(GL_VERSION));
        //INFO("Shader version : %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
        //INFO("CPU information:%s x %d", cpuInfo().c_str(), std::thread::hardware_concurrency());
    }
}

void the::InfrastructureCore::deinit()
{
    loop()->deinit();
    tree()->deinit();
}

void the::InfrastructureCore::setLog(LogService* log)
{
    ASSERT(log != nullptr, ":(");

    delete mLog;
    _the_globalLog = log;
    mLog = log;
}
