////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include "The2D/ISupportAttachedInternal.hpp"

void the::ISupportAttachedInternal::setAttachedIndex(int id, int classID, ChunkVectorIndex value)
{
    eastl::vector<ChunkVectorIndex>& indexes = mAttachedIndexes[classID];

    if (id >= (int)indexes.size())
    {
        // id numerates from zero, so id+1 (i.e. for id = 0, size should be 1
        indexes.resize(id+1, INVALID_INDEX);
    }
    indexes[id] = value;
    //INFO("Setted attached %i index %i %i for %s,now size: %i",
    //    id, value.chunkIndex, value.itemIndex, getPathString(), (int)mAttachedIndexes.size());
}

void the::ISupportAttachedInternal::shrinkAttachedIndexes()
{
    std::vector<eastl::map<int, eastl::vector<ChunkVectorIndex>>::iterator> toRemove;
    for (auto it = mAttachedIndexes.begin(); it != mAttachedIndexes.end(); it++)
    {
        eastl::vector<ChunkVectorIndex>& indexes = it->second;
        while (indexes.back() == INVALID_INDEX)
        {
            indexes.pop_back();
        }
        if (indexes.size() == 0)
        {
            toRemove.push_back(it);
        }
        else if (indexes.capacity() > indexes.size())
        {
            indexes.set_capacity(indexes.size(), INVALID_INDEX);
        }
    }
    for (auto it : toRemove)
    {
        mAttachedIndexes.erase(it);
    }
}

size_t the::ISupportAttachedInternal::getMemoryWastedByAttached()
{
    size_t wasted = 0;
    for (auto it = mAttachedIndexes.begin(); it != mAttachedIndexes.end(); it++)
    {
        eastl::vector<ChunkVectorIndex>& indexes = it->second;
        for (int id = 0; id < (int)indexes.size(); id++)
        {
            if (indexes[id] == INVALID_INDEX)
            {
                wasted += sizeof(ChunkVectorIndex);
            }
        }
    }
    return wasted;
}

size_t the::ISupportAttachedInternal::getMemoryUsedByAttached()
{
    size_t used = sizeof(mAttachedIndexes);
    for (auto it = mAttachedIndexes.begin(); it != mAttachedIndexes.end(); it++)
    {
        eastl::vector<ChunkVectorIndex>& indexes = it->second;
        used += indexes.size() * sizeof(ChunkVectorIndex) + sizeof(eastl::vector<ChunkVectorIndex>);
    }
    return used;
}