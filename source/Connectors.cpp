////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "The2D/Connectors.hpp"
#include <The2D/Rtti.hpp>

using namespace the;


//////////////////////////////////////////////////
//// Connector implementation ///////////////////
/////////////////////////////////////////////////

Connector::Connector()
{
    setInitOrder(BEFORE_PARENT);
}

Connector::~Connector()
{
}

std::list<Connector*> Connector::getChildConnectors() const
{
    std::list<Connector*> childConnectors;
    for (auto child : getChildren())
    {
        if (core()->rtti()->is<Connector>(child))
        {
            childConnectors.push_back(static_cast<Connector*>(child));
        }
    }
    return childConnectors;
}

IConversionConnection::IConversionConnection(IConversion* conv, Connector* from, Connector* to)
    : mConversion(conv), mSource(from), mTarget(to)
{
}

IConversionConnection::~IConversionConnection()
{
}

bool IConversion::canConvert(Connector* from, Connector* to, string* errMsg)
{
    return false;
}
IConversionConnection* IConversion::connect(Connector* from, Connector* to)
{
    return new IConversionConnection(this, from, to);
}
void IConversion::disconnect(IConversionConnection* con)
{
    delete con;
}