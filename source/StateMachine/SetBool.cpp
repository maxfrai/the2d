////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/SetBool.hpp>

using namespace the;



SetBool::SetBool()
{
    REGISTER(prmValue);
    REGISTER(toConnector).setInitOrder(BEFORE_PARENT);
    REGISTER(prmConnectorPath).setRequired(false);
}
SetBool::~SetBool()
{
}

void SetBool::onPreInit()
{
    if (prmConnectorPath.containsValue())
    {
        if (!toConnector.path.containsValue() || toConnector.path().empty())
        {
            toConnector.path = prmConnectorPath();
        }
        else
        {
            WARN("Specified values for both, prmCommandPath and toCommand.path; prmCommandPath ignored.");
        }
    }
}

void SetBool::onExec()
{
    if (toConnector.containsValue())
    {
        *toConnector() = prmValue();
    }
}