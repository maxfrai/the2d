////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/TransGroup.hpp>

using namespace the;



TransGroup::TransGroup()
{
    REGISTER(mTransitions).setInitOrder(AFTER_PARENT);

}
TransGroup::~TransGroup()
{

}

class ExecAnotherAction: public Action
{
    public:
      virtual void onExec() override
      {
          mAction->exec();
      }

      Action* mAction;
};

InitState TransGroup::onInit()
{
    for (auto childTrans : mTransitions())
    {
        for (auto evBindPtr : toEvents())
        {
            std::shared_ptr<Bind<CEvent>> copy(new Bind<CEvent>());
            copy->set(evBindPtr->Get());
            childTrans->toEvents.add(copy.get());
            manageStateChild(copy, "Added by TransGroup");
        }
        for (auto stateBindPtr : toStates())
        {
            std::shared_ptr<Bind<State>> copy(new Bind<State>());
            copy->set(stateBindPtr->Get());
            childTrans->toStates.add(copy.get());
            manageStateChild(copy, "Added by TransGroup");
        }
        for (auto stateBindPtr : toNextStates())
        {
            std::shared_ptr<Bind<State>> copy(new Bind<State>());
            copy->set(stateBindPtr->Get());
            childTrans->toNextStates.add(copy.get());
            manageStateChild(copy, "Added by TransGroup");
        }
        for (auto actionPtr : mAction())
        {
            std::shared_ptr<ExecAnotherAction> redirect(new ExecAnotherAction());
            redirect->mAction = actionPtr;
            childTrans->mAction.add(redirect.get());
            manageStateChild(redirect, "Added by TransGroup");
        }
        childTrans->prmStatesUnion = prmStatesUnion();
    }
    
    return true;
}
void TransGroup::onDeinit()
{

}