////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/FiniteStateMachine.hpp>

using namespace the;





FiniteStateMachine::FiniteStateMachine()
{
   REGISTER(toStartState);
   REGISTER(toFinalState).setRequired(false);
   REGISTER(mTransitionTable).setInitOrder(BEFORE_PARENT);

   REGISTER(prmAutoStart).defval(true);

   REGISTER(CWorking) = false;

   REGISTER(DoStart).setFunc(std::bind(&FiniteStateMachine::start, this));
   REGISTER(DoForceFinish).setFunc(std::bind(&FiniteStateMachine::forceFinish, this));
   REGISTER(EvStarted);
   REGISTER(EvFinished);

   mStates.reserve(10);
}
FiniteStateMachine::~FiniteStateMachine()
{
}

InitState FiniteStateMachine::onInit()
{
    mTransitionTable.acceptAll([&](GameEntity* ent){
        if (core()->rtti()->is<Bind<State>>(ent))
        {
            mStates.push_back(static_cast<Bind<State>*>(ent)->Get());
        }
    });

    if (toFinalState.containsValue())
    {
        toFinalState->EvActivated.SigRaised.add(this, std::bind(&FiniteStateMachine::onFinished, this));
    }

    return READY;
}
void FiniteStateMachine::onPostInit()
{
    if (prmAutoStart)
    {
        start();
    }
}
void FiniteStateMachine::onDeinit()
{
    mStates.clear();
    if (toFinalState.containsValue())
    {
        toFinalState->EvActivated.SigRaised.remove(this);
    }
}

const std::vector<State*> FiniteStateMachine::getStates()
{
    return mStates;
}

void FiniteStateMachine::start()
{
    ASSERT(isInitialized(), "");
    toStartState->DoActivate();
    CWorking = true;
    EvStarted.raise();
}
void FiniteStateMachine::forceFinish()
{
    for (auto state : mStates)
    {
        if (state->CActive)
        {
            state->DoDeactivate();
        }
    }
    if (toFinalState.containsValue())
    {
        toFinalState->DoActivate();
    }
}

void FiniteStateMachine::onFinished()
{
    CWorking = false;
    EvFinished.raise();
}