////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/Action.hpp>

using namespace the;



Action::Action()
{
    REGISTER(prmOrder).defval(1);

    REGISTER(DoExec).setFunc(std::bind(&Action::exec, this));
    REGISTER(EvPreExec);
    REGISTER(EvPostExec);
}
Action::~Action()
{

}

void Action::exec()
{
    EvPreExec.raise();
    
    onExec();

    for (auto child : get())
    {
        child->exec();
    }
    
    EvPostExec.raise();
}

void Action::onRegisterChild(GameEntity* child)
{
    // Call parent, that will add child to mAllChildren
    ChildList<Action>::onRegisterChild(child);
    
    // Buble sort http://cybern.ru/sortirovka-puzyrkom-realizaciya-na-c.html
    for (int i = 0; i < (int)mAllChildren.size(); i++)
    {
        for (int j = mAllChildren.size() - 1; j > i; j--)
        {
            if (mAllChildren[j]->prmOrder < mAllChildren[j - 1]->prmOrder)
            {
                std::swap(mAllChildren[j], mAllChildren[j - 1]);
            }
        }
    }
}

void Action::onExec()
{
}