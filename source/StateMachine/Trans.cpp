////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/Trans.hpp>

using namespace the;



Trans::Trans()
{
    REGISTER(toStates).setInitOrder(BEFORE_PARENT);
    REGISTER(prmState).setRequired(false);
    REGISTER(prmStatesUnion).defval(AND)
        .addToMap("AND", AND)
        .addToMap("OR", OR);
            
    REGISTER(toEvents).setInitOrder(BEFORE_PARENT).setChildTagName("Event");
    REGISTER(prmEvent).setRequired(false);

    REGISTER(mAction);
    REGISTER(prmCommand).setRequired(false);

    REGISTER(toNextStates).setInitOrder(BEFORE_PARENT);
    REGISTER(prmNextState).setRequired(false);
    
    

    REGISTER(EvPrePerform);
    REGISTER(EvPostPerform);
    
}
Trans::~Trans()
{

}

void Trans::onPreInit()
{
    if (prmState.containsValue())
    {
        mCreatedStateBind = std::shared_ptr<Bind<State>>(new Bind<State>());
        mCreatedStateBind.get()->path = prmState();
        toStates.add(mCreatedStateBind.get(), BEFORE_PARENT);
    }
    if (prmEvent.containsValue())
    {
        mCreatedEventBind = std::shared_ptr<Bind<CEvent>>(new Bind<CEvent>());
        mCreatedEventBind.get()->path = prmEvent();
        toEvents.add(mCreatedEventBind.get(), BEFORE_PARENT);
    }
    if (prmNextState.containsValue())
    {
        mCreatedNextStateBind = std::shared_ptr<Bind<State>>(new Bind<State>());
        mCreatedNextStateBind.get()->path = prmNextState();
        toNextStates.add(mCreatedNextStateBind, BEFORE_PARENT);
    }
    if (prmCommand.containsValue())
    {
        mCreatedAction = std::shared_ptr<Exec>(new Exec());
        mCreatedAction->toCommand.path = prmCommand();
        mAction.add(mCreatedAction.get());
    }

 
}
InitState Trans::onInit()
{
    if (toStates.size() == 0)
    {
        return NOT_READY("There is no states specified for the transition");
    }
    if (toEvents.size() == 0)
    {
        return NOT_READY("There is no events specified for the transition");
    }

    for (auto ev : toEvents())
    {
        (*ev)->SigRaised.add(this, std::bind(&Trans::onEvent, this, ev->Get()));
    }
    
    return true;
}
void Trans::onDeinit()
{
    for (auto ev : toEvents())
    {
        (*ev)->SigRaised.remove(this);
    }
}
void Trans::onPostDeinit()
{
    if (mCreatedEventBind.get() != nullptr)
    {
        mCreatedEventBind->deinit();
        toEvents.remove(mCreatedEventBind.get());
        mCreatedEventBind = std::shared_ptr<Bind<CEvent>>();
    }
    if (mCreatedStateBind.get() != nullptr)
    {
        mCreatedStateBind->deinit();
        toStates.remove(mCreatedStateBind.get());
        mCreatedStateBind = std::shared_ptr<Bind<State>>();
    }
    if (mCreatedNextStateBind.get() != nullptr)
    {
        mCreatedNextStateBind->deinit();
        toNextStates.remove(mCreatedNextStateBind.get());
        mCreatedNextStateBind = std::shared_ptr<Bind<State>>();
    }
    if (mCreatedAction.get() != nullptr)
    {
        mCreatedAction->deinit();
        mAction.remove(mCreatedAction.get());
        mCreatedAction = std::shared_ptr<Exec>();
    }
}

void Trans::onEvent(CEvent* ev)
{
    bool statesOk = false;
    if (prmStatesUnion == AND)
    {
        bool allStatesActive = true;
        for (auto stateBind : toStates.get())
        {
            if (!stateBind->Get()->CActive())
            {
                allStatesActive = false;
                break;
            }
        }
        statesOk = allStatesActive;
    }
    else if(prmStatesUnion == OR)
    {
         bool anyStateActive = true;
        for (auto stateBind : toStates.get())
        {
            if (stateBind->Get()->CActive())
            {
                anyStateActive = true;
                break;
            }
        }
        statesOk = anyStateActive;
    }
    if (statesOk)
    {
        perform();
        //INFO("Performed transition %s|Event: %s", getName(), ev->getDetailedInfoString());
    }
}

void Trans::perform()
{
    mAction.exec();

    if (toNextStates.size() > 0)
    {
        for (auto curState : toStates())
        {
            curState->Get()->DoDeactivate();
        }
        for (auto nextState : toNextStates())
        {
            nextState->Get()->DoActivate();
        }
    }
}