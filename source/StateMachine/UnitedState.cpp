////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/UnitedState.hpp>

using namespace the;



UnitedState::UnitedState()
{
    REGISTER(toStates);
}
UnitedState::~UnitedState()
{
}

void UnitedState::setActive(bool active)
{
    for(Bind<State>* childState : toStates())
    {
        childState->Get()->CActive = active;
    }

    State::setActive(active);
}