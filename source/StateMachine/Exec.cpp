////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/Exec.hpp>

using namespace the;



Exec::Exec()
{
    REGISTER(toCommand).setInitOrder(AFTER_PARENT);
    REGISTER(prmCommandPath).setRequired(false);;
}
Exec::~Exec()
{
}

void Exec::onExec()
{
    toCommand->exec();
}

void Exec::onPreInit()
{
    if (prmCommandPath.containsValue())
    {
        if (!toCommand.path.containsValue() || toCommand.path().empty())
        {
            toCommand.path = prmCommandPath();
        }
        else
        {
            WARN("Specified values for both, prmCommandPath(%s) and toCommand.path (%s). Value of prmCommandPath is ignored.",
                prmCommandPath(), toCommand.path());
        }
    }
}