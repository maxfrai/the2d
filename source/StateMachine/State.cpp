////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/StateMachine/State.hpp>

using namespace the;



State::State(): mActive(false)
{
    REGISTER(mActivateAction);
    REGISTER(mDeactivateAction);

    REGISTER(prmDescription).setRequired(false);

    REGISTER(CActive)
        .setSetter(std::bind(&State::setActive, this, std::placeholders::_1))
        .setGetter(&mActive);

    REGISTER(DoActivate).setFunc(std::bind(&State::setActive, this, true));
    REGISTER(DoDeactivate).setFunc(std::bind(&State::setActive, this, false));
    REGISTER(EvActivated);
    REGISTER(EvDeactivated);
}
State::~State()
{
}

void State::setActive(bool active)
{
    // Deactivating parent state should deactivate all children
    if (!active)
    {
        for(auto childState : mAllChildren)
        {
            childState->setActive(false);
        }
    }

    mActive = active;
    core()->loop()->setActive(this, active);
    if (active) 
    {
        mActivateAction.DoExec();
        onActivate();
        EvActivated.raise();
    }
    else 
    {
        mDeactivateAction.DoExec();
        onDeactivate();
        EvDeactivated.raise();
    }    
}

void State::onActivate()
{
}

void State::onDeactivate()
{
}
