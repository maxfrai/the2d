////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Rtti.hpp>

the::Rtti::Rtti(uint inheritanceMatrixCapacity)
    : mFirstTypeStub("not_a_type", "not_a_type", 0,
        0, nullptr, std::vector<const the::The2DTypeInfo*>())
{
    mInheritanceMatrixCapacity = inheritanceMatrixCapacity;
    mInheritanceMatrix = new int8[inheritanceMatrixCapacity*inheritanceMatrixCapacity];
    for(int i =0; i< (int)(inheritanceMatrixCapacity*inheritanceMatrixCapacity); i++)
    {
        mInheritanceMatrix[i] = 0;
    }
    mRegisteredTypes.push_back(&mFirstTypeStub);
}

the::Rtti::~Rtti()
{
    delete[] mInheritanceMatrix;
}

bool the::Rtti::typeRegistered(const The2DTypeInfo& type)
{
    return type.engineID != 0 &&
        type.engineID <= mRegisteredTypes.size() &&
        *(mRegisteredTypes[type.engineID]) == type;
}

void the::Rtti::registerType(const The2DTypeInfo& type)
{
    __registerTypeRec(&type, &type, 1);
}

void the::Rtti::__registerTypeRec(const The2DTypeInfo* type, const The2DTypeInfo* base, int deep)
{
    // Execution starts with __registerTypeRec(type, type) call
    // Then, it recursively continue for type and all its superclasses
    // Then, it recursively continue for __registerTypeRec(base, base)

    // If type does not registered
    // Checking only base, because first call is __registerTypeRec(type, type), and then __registerTypeRec(type, itsBase)
    bool registered = base->engineID != 0;
    for (auto t : mRegisteredTypes)
    {
        if (t->equalTo(*base))
        {
            base->engineID = t->engineID;
            registered = true;
            break;
        }
    }

    if (!registered)
    {
        mRegisteredTypes.push_back(base);
        mNameHashToType.insert(std::make_pair(base->nameHash, base));
        base->engineID = mRegisteredTypes.size()-1;

        // Fill derivedTypes array for types
        // It can't be fulfilled at static init time, because
        // order of static initialization unspecified.
        for (auto& baseBase : base->baseTypes)
        {
            baseBase->derivedTypes.push_back(base);
        }

        SigTypeRegistered(base);

        // Check needed to prevent forever recurse
        if(type != base)
        {
            __registerTypeRec(base, base, 1);
        }
    }
    else
    {
        // Type already registered
        ASSERT(base->engineID < mRegisteredTypes.size(), "Invalid type info object. Maybe, type already registered in another Rtti instance.");
        ASSERT(*base == *(mRegisteredTypes[base->engineID]), "Invalid type info object. Maybe, type already registered in another Rtti instance.");
    }

    mInheritanceMatrix[base->engineID*mInheritanceMatrixCapacity + type->engineID] = -deep;
    mInheritanceMatrix[type->engineID*mInheritanceMatrixCapacity + base->engineID] = deep;

    for(auto &baseBase : base->baseTypes)
    {
        __registerTypeRec(type, baseBase, deep+1);
    }
}

std::shared_ptr<the::GameEntity> the::Rtti::createEntity(const the::string& typeName) const
{
    ISupportRtti* result = nullptr;
    size_t nameHash = getHashCode<string>(typeName);
    auto it = mNameHashToType.find(nameHash);
    if (it != mNameHashToType.end())
    {
        result = it->second->constructor->create();
    }
    return std::shared_ptr<the::GameEntity>(dynamic_cast<GameEntity*>(result));
}

const std::vector<const the::The2DTypeInfo*>& the::Rtti::getRegisteredTypes()
{
    return mRegisteredTypes;
}

const the::The2DTypeInfo* the::Rtti::getType(const string& typeName) const
{
    size_t nameHash = getHashCode<string>(typeName);
    auto it = mNameHashToType.find(nameHash);
    if (it != mNameHashToType.end())
    {
        return it->second;
    }
    else
    {
        return nullptr;
    }
}