////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/GameEntity.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/ObserverService.hpp>
#include <The2D/LogService.hpp>

#include "The2D/Component.hpp"

namespace the
{
    the::string initOrderToString(InitOrder ord)
    {
        switch(ord)
        {
            case NOT_SET: return "NOT_SET";
            case BEFORE_PARENT: return "BEFORE_PARENT";
            case AFTER_PARENT: return "AFTER_PARENT";
            case BY_PARENT: return "BY_PARENT";
            case AUTO: return "AUTO";
            case NEVER: return "NEVER";
            default: return format("Unknown(%i)", (int)ord);
        }
    }
}

the::InitService::InitService(InfrastructureCore* core): mCore(core),
    mInitLevel(0), mDeinitLevel(0), mDebugLog(false)

{
    ASSERT(core != nullptr, "InitService needs core to send ERR message to log and notice observer about entity init and deinit.");
}

void the::InitService::enableDebugLog(bool enable)
{
    mDebugLog = enable;
}
bool the::InitService::debugLogEnabled()
{
    return mDebugLog;
}

bool the::InitService::init(the::GameEntity* root)
{
    ASSERT(root, "Can not init null game entity!");
    ASSERT(!root->mInitState, "Double initialization.");

    if (mInitLevel == 0)
    {
        SigTreeInitBegin();
    }

    mInitLevel++;

    // Note! in deinit **SAME** logic, copypast for perfomance

    // Build init list
    EntitiesList initList;
    // root must be initialized in any case, because called init for it
    root->preInit();
    initList.push_front(root);
    if (mDebugLog)
    {
        INFO("Init root added to init list %s (%i)",
            root->getPathString(), root->id());
    }
    
    // Children of root must be looked, even if init order of root is not automatic
    for (auto child : root->getChildren())
    {
        buildInitListRec(child, initList);
    }

    bool newEntitesAppeared = false;
    // Watch to the entites, appeared during initialization
    WatcherHandle registerWatcher = mCore->observer()->beginWatch([&](GameEntity* ent, WatchAction act){
        if (root->forefatherOf(ent))
        {
            buildInitListRec(ent, initList);
            newEntitesAppeared = true;
        }
    }, ENTITY_REGISTERED, WATCH_SUBTREE);

    // init
    int initializedAtIteration = 0;
    do
    {
        newEntitesAppeared = false;
        initializedAtIteration = 0;
        // Call initInternal, and if it return true, remove entity from initList
        for (auto entIt = initList.begin(); entIt != initList.end(); )
        {
            auto currentIt = entIt;
            entIt = entIt->next();

            // Init order can be changed after registration
            if (!automaticInitOrder(currentIt->data) && currentIt->data != root)
            {
                if (mDebugLog)
                {
                    INFO("Skipping %s (%i) - changed init order|Init order: %i",
                        currentIt->data->getPathString(), currentIt->data->id(), currentIt->data->getInitOrder());
                }
                initList.erase(currentIt);
                initializedAtIteration++;
            }
            if (mDebugLog)
            {
                INFO("Initializing %s (%i) ...",
                    currentIt->data->getPathString(), currentIt->data->id());
            }
            if (currentIt->data->initInternal())
            {
                initList.erase(currentIt);
                initializedAtIteration++;
                if (mDebugLog)
                {
                    INFO("Initialized ok %s (%i)",
                        currentIt->data->getPathString(), currentIt->data->id());
                }
            }
            else if (mDebugLog)
            {
                INFO("Defer %s (%i) - initInternal returned NOT_READY|Init state: %s",
                    currentIt->data->getPathString(), currentIt->data->id(),
                    currentIt->data->getInitState().comment);
            }
        }

        // If at this iteration no one entity initialized, call initGotStuck,
        // and if it return true, remove entity from initList
        if (initializedAtIteration == 0 && !newEntitesAppeared)
        {
            for (auto entIt = initList.begin(); entIt != initList.end(); )
            {
                auto currentIt = entIt;
                entIt = entIt->next();
                if (mDebugLog)
                {
                    INFO("Calling initGotStuck for %s (%i) ...",
                        currentIt->data->getPathString(), currentIt->data->id());
                }
                if (currentIt->data->initGotStuck())
                {
                    initList.erase(currentIt);
                    initializedAtIteration++;
                    if (mDebugLog)
                    {
                        INFO("Initialized via initGotStuck %s (%i)",
                            currentIt->data->getPathString(), currentIt->data->id());
                    }
                }
                else if (mDebugLog)
                {
                    INFO("Defer %s (%i) - initGotStuck returned NOT_READY|Init state: %s",
                        currentIt->data->getPathString(), currentIt->data->id(),
                        currentIt->data->getInitState().comment);
                }
            }
        }

    } while (initList.size() > 0 && (initializedAtIteration > 0 || newEntitesAppeared));

    for (auto entIt = initList.removed_begin(); entIt != initList.removed_end(); entIt = entIt->next())
    {
        if (mDebugLog)
        {
            INFO("postInit of %s (%i)",
                entIt->data->getPathString(), entIt->data->id());
        }
        entIt->data->postInit();
    }
    for (auto entIt = initList.begin(); entIt != initList.end(); entIt = entIt->next())
    {
        if (mDebugLog)
        {
            INFO("postInit of %s (%i)",
                entIt->data->getPathString(), entIt->data->id());
        }
        entIt->data->postInit();
    }

    // Err if smth not initialized
    int errsCount = 0;
    for (auto entIt = initList.begin(); entIt != initList.end(); entIt = entIt->next())
    {
        // Entity can be initialized at onPostInit, so check needed
        if (!entIt->data->isInitialized())
        {
            std::string reason = (entIt->data->getInitState().comment.empty())? "not specified" : entIt->data->getInitState().comment;
            ERR("Entity %s not initialized after init call |Init fail reason: %s\n\n\nEntity info:\n%s",
                entIt->data->getShortInfoString(), reason,  entIt->data->getDetailedInfoString());
            errsCount++;
        }
    }

    mCore->observer()->endWatch(registerWatcher);

    mInitLevel--;

    if (mInitLevel == 0)
    {
        SigTreeInitEnd();
    }

    return errsCount == 0;
}

void the::InitService::deinit(GameEntity* root)
{
    ASSERT(root, "Can not deinit null game entity");

    mDeinitLevel++;
    EntitiesList deinitList;
    // root must be deinitialized in any case, because called deinit for it
    root->preDeinit();
    deinitList.push_front(root);
    if (mDebugLog)
    {
        INFO("Deinit root added to deinit list %s (%i)",
            root->getPathString(), root->id());
    }
    
    // Children of root must be looked, even if init order of root is not automatic
    for (auto child : root->getChildren())
    {
        buildDeinitListRec(child, deinitList);
    }

    EntitiesList unregisteredEntities;
    // Watch to the entites, appeared during initialization
    WatcherHandle unregisterWatcher = mCore->observer()->beginWatch([&](GameEntity* ent, WatchAction act){
        if (automaticInitOrder(ent))
        {
            unregisteredEntities.push_back(ent);
        }
    }, ENTITY_UNREGISTERED, WATCH_EACH_ENTITY);

    // Deinit
    int deinitializedAtIteration = 0;
    do
    {
        deinitializedAtIteration = 0;
        for (auto entIt = deinitList.begin(); entIt != deinitList.end(); )
        {
            auto currentIt = entIt;
            entIt = entIt->next();
            if (containsRemove(unregisteredEntities, currentIt->data))
            {
                // currentIt can contains pointer to deleted object there
                deinitList.erase(currentIt);
                deinitializedAtIteration++;
                continue;
            }

            if (mDebugLog)
            {
                INFO("Deinitializing %s (%i) ...",
                    currentIt->data->getPathString(), currentIt->data->id());
            }
            if (!currentIt->data->deinitInternal())
            {
                deinitList.erase(currentIt);
                deinitializedAtIteration++;
                if (mDebugLog)
                {
                    INFO("Deinitialized ok %s (%i)",
                        currentIt->data->getPathString(), currentIt->data->id());
                }
            }
            else if (mDebugLog)
            {
                INFO("Defer %s (%i) - deinitInternal returned NOT_READY|Init state: %s",
                    currentIt->data->getPathString(), currentIt->data->id(),
                    currentIt->data->getInitState().comment);
            }
        }
        if (deinitializedAtIteration == 0)
        {
            // Try deinitGotStuck
            for (auto entIt = deinitList.begin(); entIt != deinitList.end(); )
            {
                auto currentIt = entIt;
                entIt = entIt->next();
                if (containsRemove(unregisteredEntities, currentIt->data))
                {
                    // currentIt can contains pointer to deleted object there
                    deinitList.erase(currentIt);
                    deinitializedAtIteration++;
                    continue;
                }
                if (mDebugLog)
                {
                    INFO("Calling deinitGotStuck for %s (%i) ...",
                        currentIt->data->getPathString(), currentIt->data->id());
                }
                if (!currentIt->data->deinitGotStuck())
                {
                    deinitList.erase(currentIt);
                    deinitializedAtIteration++;
                    if (mDebugLog)
                    {
                        INFO("Deinitialized via deinitGotStuck %s (%i)",
                            currentIt->data->getPathString(), currentIt->data->id());
                    }
                }
                else if (mDebugLog)
                {
                    INFO("Defer %s (%i) - deinitGotStuck returned NOT_READY|Init state: %s",
                        currentIt->data->getPathString(), currentIt->data->id(),
                        currentIt->data->getInitState().comment);
                }
            }
            // deinitGotStuck does not helped, force deinit
            if (deinitializedAtIteration == 0)
            {
                for (auto entIt = deinitList.begin(); entIt != deinitList.end(); entIt = entIt->next())
                {
                    WARN("Forcing deinit of %s|Normal deinit failed because of next: '%s'",
                             entIt->data->getPathString(), entIt->data->getInitState().comment);
                }
                for (auto entIt = deinitList.begin(); entIt != deinitList.end(); )
                {
                    auto currentIt = entIt;
                    entIt = entIt->next();
                    if (containsRemove(unregisteredEntities, currentIt->data))
                    {
                        // currentIt can contains pointer to deleted object there
                        deinitList.erase(currentIt);
                        deinitializedAtIteration++;
                        continue;
                    }
                    else
                    {
                        GameEntity* ent = currentIt->data;
                        std::string comment = ent->forceDeinit();
                        if (!comment.empty())
                        {
                            WARN("Entity %s deinitialized with warning|Warning: %s\n\n\n**Entity info**:\n%s\n",
                                    ent->getShortInfoString(), comment, ent->getDetailedInfoString());
                        }
                        deinitList.erase(currentIt);
                        deinitializedAtIteration++;
                    }
                }
            }
        }

    } while (deinitList.size() > 0 && (deinitializedAtIteration > 0 || unregisteredEntities.size() > 0));

    for (auto entIt = deinitList.removed_begin(); entIt != deinitList.removed_end(); entIt = entIt->next())
    {
        if (mDebugLog)
        {
            INFO("postDeinit of %s (%i)",
                entIt->data->getPathString(), entIt->data->id());
        }
        entIt->data->postDeinit();
    }
    for (auto entIt = deinitList.begin(); entIt != deinitList.end(); entIt = entIt->next())
    {
        if (mDebugLog)
        {
            INFO("postDeinit of %s (%i)",
                entIt->data->getPathString(), entIt->data->id());
        }
        entIt->data->postDeinit();
    }

    mCore->observer()->endWatch(unregisterWatcher);
    mDeinitLevel--;
 }

void the::InitService::buildInitListRec(GameEntity* current, EntitiesList& initList)
{
    ASSERT(current, "Can not build init list for null game entity");

    if (automaticInitOrder(current))
    {
        if(!current->isInitialized())
        {
            current->preInit();
            initList.push_back(current);
            if (mDebugLog)
            {
                INFO("Added to init list: %s (%i)",
                    current->getPathString(), current->id());
            }
        }
        else
        {
            if (mDebugLog)
            {
                INFO("Skipping %s (%i) - already initialized|Init state: %i %s",
                    current->getPathString(), current->id(),
                    current->getInitState().initialized, current->getInitState().comment);
            }
        }

        for (auto &child : current->getChildren())
        {
            buildInitListRec(child, initList);
        }
    }
    else
    {
        if (mDebugLog)
        {
            INFO("Skipping %s (%i)|Reason: init order is not BEFORE_PARENT, AFTER_PARENT or AUTO, but '%s'",
                current->getPathString(), current->id(), initOrderToString(current->getInitOrder()));
        }
    }
}

void the::InitService::buildDeinitListRec(GameEntity* current, EntitiesList& deinitList)
{
    ASSERT(current, "Can not build deinit list for null game entity");

    if (automaticInitOrder(current))
    {
        if(current->isInitialized())
        {
            current->preDeinit();
            
            deinitList.push_back(current);           
        }
        else
        {
            if (mDebugLog)
            {
                INFO("Skipping %s (%i) - not initialized|Init state: %i %s",
                    current->getPathString(), current->id(),
                    current->getInitState().initialized, current->getInitState().comment);
            }
        }

        for (auto &child : current->getChildren())
        {
            buildDeinitListRec(child, deinitList);
        }
    }
    else
    {
        if (mDebugLog)
        {
            INFO("Skipping %s (%i)|Reason: init order is not BEFORE_PARENT, AFTER_PARENT or AUTO, but '%s'",
                current->getPathString(), current->id(), initOrderToString(current->getInitOrder()));
        }
    }
}
bool the::InitService::automaticInitOrder(const GameEntity* ent) const
{
    the::InitOrder ord = ent->getInitOrder();
    return ord == the::BEFORE_PARENT ||
        ord == the::AFTER_PARENT ||
        ord == the::AUTO;
}

bool the::InitService::containsRemove(EntitiesList& list, GameEntity* ent)
{
    return list.remove(ent);
}