////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Component.hpp>
#include <The2D/Attached.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>

the::Component::Component()
{
    mAttachedProcessor = new the::Attached();
    mAttachedProcessor->setParent(this, "Attached", AFTER_PARENT);
    REGISTER(mAttachedChildren);
}

the::Component::~Component()
{
    delete mAttachedProcessor;
    mAttachedProcessor = nullptr;
}
