////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Parameters.hpp>
#include <The2D/LogService.hpp>
#include <The2D/ResourceService.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>
#include <ctime>

the::PFloat::~PFloat()
{
}

bool the::PFloat::onParse(string& strValue)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    bool ok = false;
    try
    {
        setValue(the::lexical_cast<float>(strValue.c_str()));
        ok = true;
    }
    catch(...)
    {
        ok = false;
    }

    return ok;
}

the::string the::PFloat::serialize() const
{
    return the::lexical_cast<the::string>(mValue);
}

the::ParameterTypes the::PFloat::getType() const
{
    return FLOAT_PARAM;
}

void the::PFloat::operator=(float val)
{
    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PDouble::~PDouble()
{
}

bool the::PDouble::onParse(string& strValue)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    bool ok = false;
    try
    {
        setValue(the::lexical_cast<double>(strValue.c_str()));
        ok = true;
    }
    catch(...)
    {
        ok = false;
    }

    return ok;
}

the::string the::PDouble::serialize() const
{
    return the::lexical_cast<the::string>(mValue);
}

the::ParameterTypes the::PDouble::getType() const
{
    return DOUBLE_PARAM;
}

void the::PDouble::operator=(double val)
{
    setValue(val);
}
///////////////////////////////////////////////////////////////////////////////////////////

the::PInt::~PInt()
{
}

bool the::PInt::onParse(string& strValue)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    bool ok = false;
    try
    {
        setValue(the::lexical_cast<int>(strValue.c_str()));
        ok = true;
    }
    catch(...)
    {
        ok = false;
    }

    return ok;
}

the::string the::PInt::serialize() const
{
    return the::lexical_cast<the::string>(mValue);
}

the::ParameterTypes the::PInt::getType() const
{
    return INT_PARAM;
}

void the::PInt::operator=(int val)
{
    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PUInt::~PUInt()
{
}

bool the::PUInt::onParse(string& strValue)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");
    bool ok = false;
    try
    {
        setValue(the::lexical_cast<int>(strValue.c_str()));
        ok = true;
    }
    catch(...)
    {
        ok = false;
    }

    return ok;
}

the::string the::PUInt::serialize() const
{
    return the::lexical_cast<the::string>(mValue);
}

the::ParameterTypes the::PUInt::getType() const
{
    return INT_PARAM;
}

void the::PUInt::operator=(uint val)
{
    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PBool::~PBool()
{
}

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

// trim from start
std::string& PBool_ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
std::string& PBool_rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
std::string& PBool_trim(std::string &s) {
        return PBool_ltrim(PBool_rtrim(s));
}

bool the::PBool::onParse(string& strValue)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    bool ok = true;
    std::string val = PBool_trim(strValue);
    std::transform(val.begin(), val.end(), val.begin(), ::tolower);
    if (val == "true" || val == "1") setValue(true);
    else if (val == "false" || val == "0") setValue(false);
    else ok = false;

    return ok;
}

the::string the::PBool::serialize() const
{
    return (mValue)? "true" : "false";
}

the::ParameterTypes the::PBool::getType() const
{
    return BOOL_PARAM;
}

void the::PBool::operator=(bool val)
{
    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PString::~PString()
{
}

bool the::PString::onParse(string& strValue)
{
    ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    setValue(strValue);
    return true;
}

the::string the::PString::serialize() const
{
    return mValue;
}

the::ParameterTypes the::PString::getType() const
{
    return STRING_PARAM;
}

void the::PString::operator=(const char* val)
{
    setValue(val);
}
void the::PString::operator=(string val)
{
    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PColor::PColor()
{
    REGISTER(a).defval(0);
    REGISTER(r).defval(0);
    REGISTER(g).defval(0);
    REGISTER(b).defval(0);
}
the::PColor::~PColor()
{
}

the::ParameterTypes the::PColor::getType() const
{
    return COLOR_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////
the::PTimeSpan::PTimeSpan()
{
    REGISTER(h).defval(0);
    REGISTER(m).defval(0);
    REGISTER(s).defval(0);
    REGISTER(ms).defval(0);
}
the::PTimeSpan::~PTimeSpan()
{
}

float the::PTimeSpan::getTotalHours() const
{
    return getTotalMinutes() / 60.0f;
}
float the::PTimeSpan::getTotalMinutes() const
{
    return getTotalSeconds() / 60.0f;
}
float the::PTimeSpan::getTotalSeconds() const
{
    return getTotalMiliseconds() / 1000.0f;
}
int the::PTimeSpan::getTotalMiliseconds() const
{
    return ms() + s()*1000 + m()*1000*60 + h()*1000*60*60;
}

the::ParameterTypes the::PTimeSpan::getType() const
{
    return TIME_SPAN_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PDate::PDate()
{
    time_t now = std::time(0);
    std::tm *ltm = std::localtime(&now);

    REGISTER(year).defval(ltm->tm_year + 1900);
    REGISTER(month).defval(ltm->tm_mon);
    REGISTER(day).defval(ltm->tm_mday);
}
the::PDate::~PDate()
{
}

the::ParameterTypes the::PDate::getType() const
{
    return DATE_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PTime::PTime()
{
    time_t now = std::time(0);
    std::tm *ltm = std::localtime(&now);

    REGISTER(h).defval(ltm->tm_hour);
    REGISTER(m).defval(ltm->tm_min);
    REGISTER(s).defval(ltm->tm_sec);
    REGISTER(ms).defval(0);
}
the::PTime::~PTime()
{
}

the::ParameterTypes the::PTime::getType() const
{
    return TIME_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PTimeStamp::PTimeStamp()
{
    time_t now = std::time(0);
    std::tm *ltm = std::localtime(&now);

    REGISTER(year).defval(ltm->tm_year + 1900);
    REGISTER(month).defval(ltm->tm_mon);
    REGISTER(day).defval(ltm->tm_mday);
    REGISTER(h).defval(ltm->tm_hour);
    REGISTER(m).defval(ltm->tm_min);
    REGISTER(s).defval(ltm->tm_sec);
    REGISTER(ms).defval(0);
}
the::PTimeStamp::~PTimeStamp()
{
}

the::ParameterTypes the::PTimeStamp::getType() const
{
    return TIME_STAMP_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PPoint::PPoint()
{
    REGISTER(x);
    REGISTER(y);
}

the::PPoint::~PPoint()
{
}

void the::PPoint::set(float px, float py)
{
    x.setValue(px);
    y.setValue(py);
}

the::ParameterTypes the::PPoint::getType() const
{
    return POINT_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PSize::PSize()
{
    REGISTER(width);
    REGISTER(height);
}
the::PSize::~PSize()
{
}

the::ParameterTypes the::PSize::getType() const
{
    return SIZE_PARAM;
}

void the::PSize::set(float width, float height)
{
    this->width = width;
    this->height = height;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PPosition::~PPosition()
{
}

the::ParameterTypes the::PPosition::getType() const
{
    return POSITION_PARAM;
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PVector::~PVector()
{
}

the::ParameterTypes the::PVector::getType() const
{
    return VECTOR_PARAM;
}

the::PRadius::~PRadius()
{
}

the::ParameterTypes the::PRadius::getType() const
{
    return RADIUS_PARAM;
}

void the::PRadius::operator=(float val)
{
    ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PAngle::~PAngle()
{
}

the::ParameterTypes the::PAngle::getType() const
{
    return ANGLE_PARAM;
}

void the::PAngle::operator=(float val)
{
    ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    setValue(val);
}

///////////////////////////////////////////////////////////////////////////////////////////

the::PPercent::~PPercent()
{
}

the::ParameterTypes the::PPercent::getType() const
{
    return PERCENT_PARAM;
}

void the::PPercent::operator=(float val)
{
    ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    setValue(val);
}
///////////////////////////////////////////////////////////////////////////////////////////

the::PMedia::~PMedia()
{
}

the::ParameterTypes the::PMedia::getType() const
{
    return MEDIA_PARAM;
}

void the::PMedia::operator=(string val)
{
    ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

    setValue(val);
}

the::string the::PMedia::getValue() const
{
    return _the_globalResource->getPath(PString::getValue());
}

/////////////////////////////////////////////////////////////////////////////////////////////

the::PRect::PRect()
{
    REGISTER(leftLower);
    REGISTER(rightUpper);
}
the::PRect::~PRect()
{
}
the::ParameterTypes the::PRect::getType() const
{
    return RECTANGLE_PARAM;
}

void the::PRect::set(const Vec2& leftLowerP, const Vec2& rightUpperP)
{
    leftLower.set(leftLowerP.x, leftLowerP.y);
    rightUpper.set(rightUpperP.x, rightUpperP.y);
}

float the::PRect::left() const
{
    return leftLower.x;
}
float the::PRect::right() const
{
    return rightUpper.x;
}
float the::PRect::top() const
{
    return rightUpper.y;
}
float the::PRect::bottom() const
{
    return leftLower.y;
}
void the::PRect::setLeft(float val)
{
    leftLower.x = val;
}
void the::PRect::setRight(float val)
{
    rightUpper.x = val;
}
void the::PRect::setTop(float val)
{
    rightUpper.y = val;
}
void the::PRect::setBottom(float val)
{
    leftLower.y = val;
}

/////////////////////////////////////////////////////////////////////////////////////////////
the::PPolygon::PPolygon(string pointTagName): PParamList(pointTagName)
{
}

the::PPolygon::PPolygon(): PParamList("Vertex")
{
}
the::PPolygon::~PPolygon()
{
}
the::ParameterTypes the::PPolygon::getType() const
{
    return POLYGON_PARAM;
}

void the::PPolygon::addPoint(float x, float y)
{
    std::shared_ptr<PPoint> point(new PPoint());
    point->x = x;
    point->y = y;
    addItem(point);
}

std::vector<the::PPoint*> the::PPolygon::getPoints() const
{
    return getTypedItems();
}

/////////////////////////////////////////////////////////////////////////////////////////////

the::PBitMask::PBitMask()
{
    REGISTER(mask).setRequired(false);
}

the::PBitMask::~PBitMask()
{
}

bool the::PBitMask::getBit(int number) const
{
    ASSERT(number >= 0 && number < 32, ":(");

    return (mask & (1<<number)) > 0;
}

void the::PBitMask::setBit(int number)
{
    ASSERT(number >= 0 && number < 32, ":(");

    mask = mask | (1 << number);
}

void the::PBitMask::resetBit(int number)
{
    ASSERT(number >= 0 && number < 32, ":(");

    mask = mask & (~1 << number);
}

std::list<the::ParsingError> the::PBitMask::onParse(TiXmlElement* element)
{
    ASSERT(isMyTag(element), ":(");

    mask = 0; // If user specified tag, then we forgot default value

    std::list<ParsingError> errors;

    // Parse tag-per-bit specification
    for(TiXmlElement* childElem = element->FirstChildElement(); childElem != nullptr;
        childElem = childElem->NextSiblingElement())
    {
        string elemName = childElem->ValueStr();
        std::transform(elemName.begin(), elemName.end(), elemName.begin(), ::tolower);
        if(elemName == "bit")
        {
            childElem->SetUserData(NODE_VIEWED);

            /// \todo Make case unsensetive.
            const char* numberStr = childElem->Attribute("number");
            if(numberStr != nullptr)
            {
                try
                {
                    int number = the::lexical_cast<int>(numberStr);
                    if(number >=0 && number < 32)
                    {
                        bool value = true;
                        const char* valueStr = childElem->Attribute("value");
                        if(valueStr != nullptr)
                        {
                            value = the::lexical_cast<int>(valueStr);
                        }
                        if(value)
                        {
                            setBit(number);
                        }
                        else
                        {
                            resetBit(number);
                        }
                    }
                    else
                    {
                        errors.push_back({childElem->ValueStr(), true,
                            "Invalid bit number '" + string(numberStr) + "'. It must be in range [0, 31].", childElem->Row()});
                    }
                }
                catch(...)
                {
                    errors.push_back({childElem->ValueStr(), true,
                            "Invalid bit number. Number must number be in range [0, 31].", childElem->Row()});
                }
            }
            else
            {
                errors.push_back({childElem->ValueStr(), true,
                        "Missed required attribute `number`.", childElem->Row()});
            }
        }
    }
    auto parentErrs = ParameterGroup::onParse(element);
    errors.splice(errors.begin(), parentErrs);
    return errors;
}

TiXmlElement* the::PBitMask::serialize()
{
    TiXmlElement* elem = new TiXmlElement(mXmlName);
    for(int i =0; i<32; i++)
    {
        if(getBit(i))
        {
            TiXmlElement* bitElem = new TiXmlElement("bit");
            bitElem->SetAttribute("number", the::lexical_cast<int>(i));
            elem->LinkEndChild(bitElem);
        }
    }
    return elem;
}

void the::PBitMask::readDataFrom(const Parameter* src)
{
    ASSERT(dynamic_cast<const PBitMask*>(src) != 0, "Parameter group can't read data from value parameter.");
    const PBitMask* srcParam = static_cast<const PBitMask*>(src);
    mask.readDataFrom(&srcParam->mask);
}

the::ParameterTypes the::PBitMask::getType() const
{
    return BIT_MASK_PARAM;
}

////////////////////////////////////////////////////////////////////////////////

the::PFlagMask::PFlagMask()
{
    REGISTER(flags).setRequired(false);
}

the::PFlagMask::~PFlagMask()
{
}

the::PFlagMask& the::PFlagMask::addFlagName(string name, uint32 mask)
{
    string lowerName = name;
    std::transform(lowerName.begin(), lowerName.end(), lowerName.begin(), ::tolower);
    mFlags[lowerName] = mask;

    return *this;
}
void the::PFlagMask::removeFlagName(uint32 mask)
{
    for(auto it = mFlags.begin(); it != mFlags.end(); it++)
    {
        if(it->second == mask)
        {
            mFlags.erase(it);
            break;
        }
    }
}
const std::map<the::string, the::uint32>& the::PFlagMask::getFlagsNames() const
{
    return mFlags;
}
void the::PFlagMask::appendFlags(uint32 m)
{
    uint32 oldMask = mask;
    uint32 newMask = oldMask | m;
    mask = newMask;
}
void the::PFlagMask::takeAwayFlags(uint32 m)
{
    uint32 oldMask = mask;
    uint32 newMask = oldMask & ~m;
    mask = newMask;
}
void the::PFlagMask::clearFlags()
{
    mask = 0;
}
std::list<the::ParsingError> the::PFlagMask::onParse(TiXmlElement* element)
{
    ASSERT(isMyTag(element), ":(");

    clearFlags();

    std::list<ParsingError> errors;

    bool bitsSet = mask;
    bool flagPerTagSet = false;

    // Parse tag-per-flag style
    for(TiXmlElement* childElem = element->FirstChildElement(); childElem != nullptr;
        childElem = childElem->NextSiblingElement())
    {
        string elemName = childElem->ValueStr();
        std::transform(elemName.begin(), elemName.end(), elemName.begin(), ::tolower);
        if(elemName == "flag")
        {
            childElem->SetUserData(NODE_VIEWED);
            flagPerTagSet = true;
            if(!bitsSet)
            {
                /// \todo Make case unsensetive.
                const char* flagNamePtr = childElem->Attribute("name");
                if(flagNamePtr != nullptr)
                {
                    string flagName(flagNamePtr);
                    std::transform(flagName.begin(), flagName.end(), flagName.begin(), ::tolower);
                    auto flagIt = mFlags.find(flagName);
                    if(flagIt != mFlags.end())
                    {
                        uint32 flagMask = flagIt->second;
                        bool value = true;
                        /// \todo Make case unsensetive.
                        const char* valueStr = childElem->Attribute("value");
                        if(valueStr != nullptr)
                        {
                            try
                            {
                                value = the::lexical_cast<int>(valueStr);
                            }
                            catch(...)
                            {
                                errors.push_back({childElem->ValueStr(), true,
                                        "Invalid flag value. It must be 0 or 1.", childElem->Row()});
                            }
                        }
                        if(value) appendFlags(flagMask);
                        else takeAwayFlags(flagMask);
                    }
                    else
                    {
                        errors.push_back({childElem->ValueStr(), true,
                            "There is no added flag with name '" + string(flagName) + "'. Use addFlag to add flags.", childElem->Row()});
                    }
                }
                else
                {
                    errors.push_back({childElem->ValueStr(), true,
                            "Missed required attribute `name`.", childElem->Row()});
                }
            }
            else
            {
                errors.push_back({element->ValueStr() + "->flags", true,
                                 "You already specified attribute or element 'mask' or 'bit', there should not be 'flag'. Choose one way to specify value.", element->Row()});
            }
        }
    }

    std::list<ParsingError> parentErrors = PBitMask::onParse(element);
    errors.splice(errors.begin(), parentErrors);

    // Parse flags="flag1|flag2|flagN" style
    if(flags.containsValue())
    {
        if(!flagPerTagSet && !bitsSet)
        {
            string flagsStr = flags;
            std::vector<string> flagsNames = split(flagsStr, '|');
            for(string& flagName : flagsNames)
            {
                the::trim(flagName);
                std::transform(flagName.begin(), flagName.end(), flagName.begin(), ::tolower);
                auto it = mFlags.find(flagName);
                if(it != mFlags.end())
                {
                    appendFlags(it->second);
                }
                else
                {
                    errors.push_back({element->ValueStr(), true,
                                     "There is no added flag with name '" + flagName + "'. Use addFlag to add flags.", element->Row()});
                }
            }
        }
        else
        {
            errors.push_back({element->ValueStr(), true,
                             "You already specified value via attribute 'mask' or 'bit', or by child tag."
                             " Attribute 'flag' should not contains value. Choose one way to specify a value.",
                             element->Row()});
        }
    }


    return errors;
}

TiXmlElement* the::PFlagMask::serialize()
{
    TiXmlElement* elem = new TiXmlElement(mXmlName);
    uint32 maskVal = mask;
    for (auto &pair : mFlags)
    {
        if ((pair.second & maskVal) == pair.second)
        {
            TiXmlElement* flagElem = new TiXmlElement("flag");
            flagElem->SetAttribute("name", pair.first);
            elem->LinkEndChild(flagElem);
        }
    }
    return elem;
}

void the::PFlagMask::readDataFrom(const Parameter* src)
{
    ASSERT(dynamic_cast<const PFlagMask*>(src) != 0, "Parameter group can't read data from value parameter.");
    const PFlagMask* srcParam = static_cast<const PFlagMask*>(src);
    PBitMask::readDataFrom(src);
    flags.readDataFrom(&srcParam->flags);
    mFlags = srcParam->mFlags;
}

the::ParameterTypes the::PFlagMask::getType() const
{
    return FLAG_MASK_PARAM;
}



























