////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>

void* operator new[](size_t size, const char* /* pName */,
                     int /* flags */, unsigned /* debugFlags */,
                     const char* /* file */, int /* line */) {
    return malloc(size);
}

void* operator new[](size_t size, size_t alignment,
                     size_t /* alignmentOffset */, const char* /* pName */,
                     int /* flags */, unsigned /* debugFlags */,
                     const char* /* file */, int /* line */) {
    return malloc(size);
}

void EASTL_DEBUG_BREAK()
{
	
}