////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Bind.hpp>
#include <The2D/Object.hpp>
#include <The2D/GameTree.hpp>

#include <list>




the::BindBase::BindBase(): mBind(nullptr),  mBindIsFound(false), mIsDependency(true), mRequired(true),
    mIgnoreOnDeinit(false)
{
    REGISTER(path).defval("").setRequired(true);

    setInitOrder(BEFORE_PARENT);
}

the::BindBase::~BindBase()
{
    if (isInitialized())
    {
        deinit();
    }
    mBind = nullptr;
}

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

bool the::BindBase::find()
{
    ASSERT(core() != nullptr, "Call setCore first.");

    reset();

    if (!isInitialized())
    {
        onPreInit();
    }
    auto found = onInit();
    if (!found)
    {
        onInitGotStuck();
    }
    onPostInit();

    return containsValue();
}

bool the::BindBase::checkPath(GameEntity* ent)
{
    bool match = false;
    if (mParsedPath.pathItems.size() > 0)
    {
        match = core()->bind()->matchPath(ent, mParsedPath);
    }
    return match;
}

bool the::BindBase::containsValue() const
{
    return mBind != nullptr;
}

void the::BindBase::set(GameEntity* item)
{
    setItem(item, false);
}

void the::BindBase::reset()
{
    setItem(nullptr, false);
}

bool the::BindBase::isMultiBind() const
{
    return false;
}

the::BindBase& the::BindBase::setRequired(bool required)
{
    mRequired = required;
    return *this;
}

bool the::BindBase::getRequired()
{
    return mRequired == true;
}

the::string the::BindBase::getXmlName()
{
    return getXmlTagName();
}

bool the::BindBase::dependency()
{
    return mIsDependency;
}

bool the::BindBase::getParsed()
{
    return getInitData()->getParsed();
}
the::BindBase& the::BindBase::setDependency(Dependency isDependency)
{
    mIsDependency = (isDependency == DEPENDENCY);
    return *this;
}

the::BindBase& the::BindBase::setWeakOnDeinit(bool ignore)
{
    mIgnoreOnDeinit = ignore;
    return *this;
}

bool the::BindBase::setItem(GameEntity* item, bool internal)
{
    removeInitDependency(mBind);
    if (isInitialized())
    {
        if (mBind != nullptr)
        {
            unregisterIn(mBind);
        }
        if (item != nullptr)
        {
            registerIn(item);
        }
    }
    mBind = item;
    mBindIsFound = internal;
    return true;
}

void the::BindBase::onPostInit()
{
    if (mWatcher.valid())
    {
        BindBase::core()->observer()->endWatch(mWatcher);
    }
    if (containsValue() && isInitialized())
    {
        registerIn(mBind);
    }
}

the::InitState the::BindBase::onInit()
{
    if (mParsedPath.pathItems.empty())
    {
        mParsedPath = core()->bind()->parsePath(path(), this);
        if (!mParsedPath.pathItems.empty())
        {
            if (containsValue())
            {
                WARN("Ignoring path '%s' in bind '%s', because bind already set|Bound entity info: %s",
                    getShortInfoString(), getDetailedInfoString(), mBind->getDetailedInfoString());
                return READY;
            }
        }
        else
        {
            if (!containsValue() && getRequired())
            {
                return NOT_READY("Empty path and no value in required bind");
            }
            else if (containsValue())
            {
                return READY("Set by user");
            }
            else
            {
                return READY("Ignoring not required bind with empty path");
            }
        }
    }

    if (!containsValue())
    {
        if (!mWatcher)
        {
            core()->bind()->findAll(mParsedPath,
                [&](GameEntity* item){ return !setItem(item, true); });

            if (!containsValue())
            {
                mWatcher = BindBase::core()->observer()->beginWatch(
                    [&](GameEntity* ent, WatchAction act){
                        if (this->checkPath(ent))
                        {
                            this->setItem(ent, true);
                            core()->observer()->endWatch(mWatcher);
                        }
                    },
                    ENTITY_INITIALIZING);

                return NOT_READY("Required binding path '%s' had not found", path());
            }
        }
        else
        {
            return getInitState();
        }
    }

    if (dependency() && !mBind->isInitialized())
    {
        return NOT_READY("Binded item %s is not initialized|Bind item info:\n%s",
            mBind->getShortInfoString(), mBind->getDetailedInfoString());
    }

    return true;
}

the::InitState the::BindBase::onInitGotStuck()
{
    string debugLog = format("Bind with path='%s' not found.|**Bind location**: %s\n**Path**: '%s'\n**Debug log**: \n\n",
        path(), getPathString(), path());
    debugLog.reserve(5000);
    core()->bind()->findAll(mParsedPath,
                [&](GameEntity* item){ return !setItem(item, true); }, &debugLog);

    if(!containsValue())
    {
        if (!getRequired())
        {
            if (!path().empty())
            {
                WARN(debugLog.c_str());
            }
            return READY("Ignored not required binding");
        }
        else
        {
            ERR(debugLog.c_str());
            return getInitState();
        }
    }
    else
    {
        return READY;
    }
}

void the::BindBase::onDeinit()
{
    if (containsValue())
    {
        unregisterIn(mBind);
    }
    if (mBindIsFound)
    {
        reset();
    }
}

void the::BindBase::registerIn(GameEntity* item)
{
    if (item != nullptr)
    {
        core()->bind()->atIncomingBindings.beginEdit(item).push_back(this);
        core()->bind()->atIncomingBindings.endEdit(item);
        if (dependency())
        {
            addInitDependency(item, "it is a bind", "it is and entity, pointed by bind",  "bind marked as dependency", mIgnoreOnDeinit);
        }
    }
}

void the::BindBase::unregisterIn(GameEntity* item)
{
    if (item != nullptr)
    {
        std::list<eastl::list<BindBase*>::iterator> toRemove;
        eastl::list<BindBase*>& incomingBinding = core()->bind()->atIncomingBindings.beginEdit(item);
        for (auto it = incomingBinding.begin(); it != incomingBinding.end(); ++it)
        {
            if (*it == this)
            {
                toRemove.push_back(it);
            }
        }
        for (auto it : toRemove)
        {
            incomingBinding.erase(it);
        }
        if (dependency())
        {
            removeInitDependency(item);
        }

        core()->bind()->atIncomingBindings.endEdit(item);
    }
}



the::MultiBindBase::MultiBindBase()
{
}
the::MultiBindBase::~MultiBindBase()
{
}
void the::MultiBindBase::addItem(GameEntity* item)
{
    setItem(item, false);
}
void the::MultiBindBase::removeItem(GameEntity* item)
{
    if (isInitialized())
    {
        unregisterIn(item);
    }
    auto it = std::find(mSettedBindings.begin(), mSettedBindings.end(), item);
    if (it != mSettedBindings.end())
    {
        mSettedBindings.erase(it);
    }
    it = std::find(mFoundBindings.begin(), mFoundBindings.end(), item);
    if (it != mFoundBindings.end())
    {
        mFoundBindings.erase(it);
    }
}
int the::MultiBindBase::getCount() const
{
    return mFoundBindings.size() + mSettedBindings.size();
}
std::list<the::GameEntity*> the::MultiBindBase::getBinds() const
{
    std::list<GameEntity*> result;
    for (auto bind : mSettedBindings) result.push_back(bind);
    for (auto bind : mFoundBindings) result.push_back(bind);

#ifdef DEBUG
    for (auto item : result)
    {
        if (!item->isInitialized())
        {
            WARN("Accessing uninitialized bind %s", item->getShortInfoString());
        }
    }
#endif

    return result;
}
bool the::MultiBindBase::containsValue() const
{
    return getCount() > 0;
}
the::GameEntity* the::MultiBindBase::operator->() const
{
    TH_FAIL("Calling operator-> of the multi bind. Call getBinds instead.");
    return nullptr;
}
void the::MultiBindBase::set(GameEntity* item)
{
    reset();
    addItem(item);
}
void the::MultiBindBase::reset()
{
    if (isInitialized())
    {
        for (auto bind : mSettedBindings)
        {
            unregisterIn(bind);
        }
    }
    mSettedBindings.clear();

    resetInternal();
}
bool the::MultiBindBase::isMultiBind() const
{
    return true;
}

the::InitState the::MultiBindBase::onInit()
{
    if (mParsedPath.pathItems.empty())
    {
        mParsedPath = core()->bind()->parsePath(path(), this);
        if (mParsedPath.pathItems.empty())
        {
            if (!containsValue() && getRequired())
            {
                return NOT_READY("Empty path and no value in required bind");
            }
            else if (containsValue())
            {
                return READY("Set by user");
            }
            else
            {
                return READY("Ignoring not required bind with empty path");
            }
        }
    }


    if (!mWatcher)
    {
        core()->bind()->findAll(mParsedPath, [&](GameEntity* item){ return !setItem(item, true); });

        mWatcher = BindBase::core()->observer()->beginWatch(
            [&](GameEntity* ent, WatchAction act){
                if (this->checkPath(ent))
                {
                    this->setItem(ent, true);
                }
            },
            ENTITY_INITIALIZING);
    }

    return NOT_READY("Multibind continue watching for new entities.");
}

the::InitState the::MultiBindBase::onInitGotStuck()
{
    string debugLog;
    if(!containsValue())
    {
        debugLog = format("MultiBind with path='%s' not found any item.|**Bind location**: %s\n**Path**: '%s'\n**Debug log**:\n\n",
            path(), getPathString(), path());
        debugLog.reserve(5000);
        core()->bind()->findAll(mParsedPath, [&](GameEntity* item){ return !setItem(item, true); }, &debugLog);
    }

    if (containsValue())
    {
        string msg;
        if (checkDependencies(getInitDependencies(), true, msg))
        {
            return READY;
        }
        else
        {
            return NOT_READY(msg);
        }
    }
    else
    {
        if (!getRequired())
        {
            if (!path().empty())
            {
                WARN(debugLog.c_str());
            }
            return READY("Ignored not required binding");
        }
        else
        {
            ERR(debugLog.c_str());
            return NOT_READY("Required mulibind with path '%s' had not found any item",
            path.containsValue()? path() : "");
        }
    }
}

void the::MultiBindBase::onPostInit()
{
    if (isInitialized())
    {
        for (auto bind : getBinds())
        {
            registerIn(bind);
        }
    }
}

void the::MultiBindBase::onDeinit()
{
    for (auto bind : getBinds())
    {
        unregisterIn(bind);
    }
    resetInternal();
}

void the::MultiBindBase::resetInternal()
{
    if (isInitialized())
    {
        for (auto bind : mFoundBindings)
        {
            unregisterIn(bind);
        }
    }
    mFoundBindings.clear();
}

bool the::MultiBindBase::setItem(GameEntity* item, bool internal)
{
    if (item != nullptr)
    {
        if (internal)
        {
            if (std::find(mFoundBindings.begin(), mFoundBindings.end(), item) == mFoundBindings.end())
            {
                mFoundBindings.push_back(item);
            }
        }
        else
        {
            if (std::find(mSettedBindings.begin(), mSettedBindings.end(), item) == mSettedBindings.end())
            {
                mSettedBindings.push_back(item);
            }
        }
        if (isInitialized())
        {
            registerIn(item);
        }
    }
    return false;
}
