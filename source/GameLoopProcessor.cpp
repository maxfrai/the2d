////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/GameLoopProcessor.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/InitService.hpp>
#include <The2D/LogService.hpp>
#include <The2D/StateMachine.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/GameTree.hpp>



the::LoopSubscriber::LoopSubscriber(): mPosInUpdateList(nullptr), mPosInDrawList(nullptr), mActive(true)
{
}

the::LoopSubscriber::~LoopSubscriber()
{
}

void the::LoopSubscriber::onActiveChanged()
{
}

bool the::LoopSubscriber::subscribedToUpdate() const
{
    return mPosInUpdateList != nullptr;
}

bool the::LoopSubscriber::subscribedToDraw() const
{
    return mPosInDrawList != nullptr;
}

bool the::LoopSubscriber::active() const
{
    return mActive;
}




#define WAS_SUBSCRIBED ((ChunkItem<LoopSubscriber*>*)42)

the::GameLoopProcessor::GameLoopProcessor(InfrastructureCore* core): mInitializing(false)
{
    prmTimeStep = 1/60.f;
    prmAdaptiveTimeStep = true;
    prmTimeStepCorrectionInterval = 1;
    prmTimeStepMaxDifferencePercent = 10.0f;
    prmMaxTimeStep = 1/30.0f;
    prmSkipFirstIntervalsCount = 2;

    mDrawList.SigCellMoved.add(std::bind(&GameLoopProcessor::processCellMoveDraw, this, std::placeholders::_1, std::placeholders::_2));
    mUpdateList.SigCellMoved.add(std::bind(&GameLoopProcessor::processCellMoveUpdate, this, std::placeholders::_1, std::placeholders::_2));
    mDrawList.zero()->data = nullptr;
    mUpdateList.zero()->data = nullptr;

    mRunning = false;
    mCore = core;

    mCore->initService()->SigTreeInitBegin.add(std::bind(&GameLoopProcessor::onTreeInitBegin, this));
}

the::GameLoopProcessor::~GameLoopProcessor()
{
}

void the::GameLoopProcessor::start()
{
    mRunning = true;

    //while (!the::__THE_RUN_SIMULATION)
    {
    //    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    while(mRunning)
    {
        step(1.f/60.f);
    }
}

void the::GameLoopProcessor::stop()
{
    ASSERT(mRunning, "Method 'start' didnt called, so 'stop' will no effect");

    _the_globalLog->stopService();
    mRunning = false;
}

void the::GameLoopProcessor::step(float timeStep)
{
    mTimeStep = 1.f/60.f;
    mInvTimeStep = 60.0f;

    // mMilisecFromLastSync += elapsed;

    update(1.f/60.f);
    draw(1.f/60.f);

    if (mInitializing)
    {
        mInitializing = false;
        shrinkAndOrderLists();
    }

    SigPostStep();

    // mAccomulatedTimeStep += elapsed;
    // mStepCount++;

    // if (prmAdaptiveTimeStep && mMilisecFromLastSync >= prmTimeStepCorrectionInterval)
    // {
    //     mMilisecFromLastSync = 0;

    //     double mediumElapsed = mAccomulatedTimeStep / double(mStepCount);
    //     double diffPercent = fabs(1 - (mediumElapsed / mTimeStep))*100.0;

    //     if ( diffPercent > prmTimeStepMaxDifferencePercent && mIntervalNum > prmSkipFirstIntervalsCount)
    //     {
    //         mTimeStep = mediumElapsed;
    //         if (mTimeStep > prmMaxTimeStep)
    //         {
    //             mTimeStep = prmMaxTimeStep;
    //         }
    //     }

    //     mCore->log()->INFO("diffpercent= %.5f, medts = %.5f, medfps=%.5f, mTimeStep -> %.5f",
    //                         diffPercent,  mediumElapsed, 1.0/mediumElapsed, mTimeStep);

    //     mAccomulatedDiff = 0;
    //     mAccomulatedTimeStep = 0;
    //     mStepCount = 0;
    //     mIntervalNum++;
    // }
}


bool the::GameLoopProcessor::init()
{
    mAccomulatedDiff = 0.0;
    mAccomulatedTimeStep = 0.0;
    mStepCount = 0;
    mMilisecFromLastSync = 0;
    mTimeStep = prmTimeStep;
    mIntervalNum = 1;

    mObjectsInitalized = true;

    //mCurrentDraw = 0;

    return true;
}


void the::GameLoopProcessor::deinit()
{
    mUpdateList.clear();
    mDrawList.clear();
};

void the::GameLoopProcessor::update(float elapsed)
{
    #ifdef DEBUG
        mCore->stats()->beginEntry(mCore->tree(), "preupdate");
    #endif
    SigPreUpdateLogic(elapsed);
    #ifdef DEBUG
        mCore->stats()->endEntry(mCore->tree(), "preupdate");
    #endif

    mCore->state()->update(elapsed);
    for (mCurrentUpdateItem = mUpdateList.begin();
         mCurrentUpdateItem != mUpdateList.end();
         mCurrentUpdateItem = mCurrentUpdateItem->next())
    {
        #ifdef DEBUG
            auto statsTarget = mCurrentUpdateItem->data;
            mCore->stats()->beginEntry(statsTarget, "update");
        #endif

        mCurrentUpdateItem->data->update();

        #ifdef DEBUG

            mCore->stats()->endEntry(statsTarget, "update");
        #endif
    }
    #ifdef DEBUG
        mCore->stats()->beginEntry(mCore->tree(), "postupdate");
    #endif
    SigPostUpdateLogic(elapsed);
    #ifdef DEBUG
        mCore->stats()->endEntry(mCore->tree(), "postupdate");
    #endif
}

void the::GameLoopProcessor::draw(float elapsed)
{
    SigPreUpdateVisual(elapsed);

    for (mCurrentDrawItem = mDrawList.begin();
         mCurrentDrawItem != mDrawList.end();
         mCurrentDrawItem = mCurrentDrawItem->next())
    {
        #ifdef DEBUG
            auto statsTarget = mCurrentDrawItem->data;
            mCore->stats()->beginEntry(statsTarget, "tdraw");
        #endif

        mCurrentDrawItem->data->draw();

        #ifdef DEBUG
            mCore->stats()->endEntry(statsTarget, "tdraw");
        #endif
    }

    SigPostUpdateVisual(elapsed);
}

void the::GameLoopProcessor::subscribeToUpdate(LoopSubscriber* subscriber)
{
    if (subscriber->mPosInUpdateList == nullptr)
    {
        if (subscriber->mActive)
        {
            // Push front, to not break update process see (GameLoopProcessor::update)
            subscriber->mPosInUpdateList = mUpdateList.push_front(subscriber);
        }
        else
        {
            subscriber->mPosInUpdateList = WAS_SUBSCRIBED;
        }
    }
}
void the::GameLoopProcessor::unsubscribeFromUpdate(LoopSubscriber* subscriber)
{
    if (subscriber->mPosInUpdateList != nullptr)
    {
        if (subscriber->mActive)
        {
            if (subscriber->mPosInUpdateList == mCurrentUpdateItem)
            {
                mCurrentUpdateItem = mCurrentUpdateItem->prev();
            }

            mUpdateList.erase(subscriber->mPosInUpdateList);
            subscriber->mPosInUpdateList = nullptr;
            if (!mInitializing)
            {
                if (mUpdateList.getDefragNeed() >= 2)
                {
                    mUpdateList.defragAndShrink();
                }
            }
        }
        else
        {
            subscriber->mPosInUpdateList = nullptr;
        }
    }
}

void the::GameLoopProcessor::subscribeToDraw(LoopSubscriber* subscriber)
{
    if (subscriber->mPosInDrawList == nullptr)
    {
        if (subscriber->mActive)
        {
            subscriber->mPosInDrawList = mDrawList.push_front(subscriber);
        }
        else
        {
            subscriber->mPosInDrawList = WAS_SUBSCRIBED;
        }
    }
}
void the::GameLoopProcessor::unsubscribeFromDraw(LoopSubscriber* subscriber)
{
    if (subscriber->mPosInDrawList != nullptr)
    {
        if (subscriber->mActive)
        {
            if (subscriber->mPosInDrawList == mCurrentDrawItem)
            {
                mCurrentDrawItem = mCurrentDrawItem->prev();
            }

            mDrawList.erase(subscriber->mPosInDrawList);
            subscriber->mPosInDrawList = nullptr;
            if (!mInitializing)
            {
                // If two last chunks are not needed, make defrag.
                // Two chunks (not simple last one) is to prevent too frequent defrag
                if (mDrawList.getDefragNeed() >= 2)
                {
                     mDrawList.defragAndShrink();
                }
            }
        }
        else
        {
            subscriber->mPosInDrawList = nullptr;
        }
    }
}

void the::GameLoopProcessor::setActive(LoopSubscriber* subscriber, bool active)
{
    if (subscriber->mActive != active)
    {
        if (active)
        {
            subscriber->mActive = true;
            if (subscriber->mPosInDrawList == WAS_SUBSCRIBED)
            {
                subscriber->mPosInDrawList = nullptr;
                subscribeToDraw(subscriber);
            }
            if (subscriber->mPosInUpdateList == WAS_SUBSCRIBED)
            {
                subscriber->mPosInUpdateList = nullptr;
                subscribeToUpdate(subscriber);
            }
        }
        else
        {
            if (subscriber->mPosInDrawList != nullptr)
            {
                unsubscribeFromDraw(subscriber);
                subscriber->mPosInDrawList = WAS_SUBSCRIBED;
            }
            if (subscriber->mPosInUpdateList != nullptr)
            {
                unsubscribeFromUpdate(subscriber);
                subscriber->mPosInUpdateList = WAS_SUBSCRIBED;
            }
            subscriber->mActive = false;
        }
        subscriber->onActiveChanged();
    }
}

bool the::GameLoopProcessor::active(LoopSubscriber* sub) const
{
    return sub->active();
}

bool the::GameLoopProcessor::subscribedToDraw(LoopSubscriber* sub) const
{
    return sub->subscribedToDraw();
}

bool the::GameLoopProcessor::subscribedToUpdate(LoopSubscriber* sub) const
{
    return sub->subscribedToUpdate();
}

the::ChunkList<the::LoopSubscriber*>& the::GameLoopProcessor::getUpdateList()
{
    return mUpdateList;
}
the::ChunkList<the::LoopSubscriber*>& the::GameLoopProcessor::getDrawList()
{
    return mDrawList;
}

/*
the::LoopStatistics the::GameLoopProcessor::getUpdateStatistics(LoopSubscriber* sub, bool resetCounter)
{
#ifdef DEBUrG
    LoopStatistics stat = sub->mUpdateStatistics;
    if (resetCounter)
    {
        sub->mUpdateStatistics.count = 0;
        sub->mUpdateStatistics.totalNanosecElapsed = 0;
    }
    return stat;
#else
    WARN("Loop statistics is not available in release build.");
    return {0 , 0};
#endif
}
the::LoopStatistics the::GameLoopProcessor::getDrawStatistics(LoopSubscriber* sub, bool resetCounter)
{
#ifdef DEBUrG
    LoopStatistics stat = sub->mDrawStatistics;
    if (resetCounter)
    {
        sub->mDrawStatistics.count = 0;
        sub->mDrawStatistics.totalNanosecElapsed = 0;
    }
    return stat;
#else
    WARN("Loop statistics is not available in release build.");
    return {0 , 0};
#endif
}*/

void the::GameLoopProcessor::onTreeInitBegin()
{
    mInitializing = true;
}

void the::GameLoopProcessor::shrinkAndOrderLists()
{
    mUpdateList.defragAndShrink();
    mDrawList.defragAndShrink();
}

void the::GameLoopProcessor::processCellMoveUpdate(ChunkItem<LoopSubscriber*>* oldCellPos, ChunkItem<LoopSubscriber*>* newCellPos)
{
    newCellPos->data->mPosInUpdateList = newCellPos;
}
void the::GameLoopProcessor::processCellMoveDraw(ChunkItem<LoopSubscriber*>* oldCellPos, ChunkItem<LoopSubscriber*>* newCellPos)
{
    newCellPos->data->mPosInDrawList = newCellPos;
}
