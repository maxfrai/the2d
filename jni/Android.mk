LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_FILENAME := libThe2D
LOCAL_MODULE        := The2D
LOCAL_CPPFLAGS      := -std=c++11 -DHAS_SOCKLEN_T
APP_USE_CPP0X       := true
LOCAL_CPP_EXTENSION := .cpp

NDK_APP_OUT := $(LOCAL_PATH)/../build

LOCAL_SRC_FILES :=  \
    ../source/AttachedBase.cpp \
    ../source/AttachedService.cpp \
    ../source/Attached.cpp \
    ../source/Bind.cpp  \
    ../source/BindService.cpp  \
    ../source/Service.cpp   \
    ../source/Component.cpp \
    ../source/Connectors.cpp \
    ../source/Config.cpp \
    ../source/EntityGroup.cpp \
    ../source/GameEntity.cpp \
    ../source/GameLoopProcessor.cpp  \
    ../source/GameTree.cpp  \
    ../source/InfrastructureCore.cpp  \
    ../source/InitService.cpp  \
    ../source/Manager.cpp  \
    ../source/MessageCore.cpp  \
    ../source/Object.cpp  \
    ../source/ObserverService.cpp \
    ../source/Parameters.cpp  \
    ../source/ParametersBase.cpp  \
    ../source/ResourceService.cpp \
    ../source/Rtti.cpp  \
    ../source/RttiSupport.cpp  \
    ../source/StateMachine.cpp  \
    ../source/XmlLoader.cpp  \
    ../source/Utils/ConfigManager.cpp  \
    ../source/LogService.cpp  \
    ../source/Utils/XmlPreprocessor.cpp \
    ../source/StatisticService.cpp\
    ../source/StateMachine/Action.cpp\
    ../source/StateMachine/Exec.cpp\
    ../source/StateMachine/FiniteStateMachine.cpp\
    ../source/StateMachine/SetBool.cpp\
    ../source/StateMachine/State.cpp\
    ../source/StateMachine/Trans.cpp\
    ../source/StateMachine/TransGroup.cpp\
    ../source/StateMachine/UnitedState.cpp\
    ../source/Utils/Tinyxml/tinystr.cpp \
    ../source/Utils/Tinyxml/tinyxml.cpp \
    ../source/Utils/Tinyxml/tinyxmlerror.cpp \
    ../source/Utils/Tinyxml/tinyxmlparser.cpp \
    ../source/Utils/Enet/callbacks.c \
    ../source/Utils/Enet/compress.c \
    ../source/Utils/Enet/host.c \
    ../source/Utils/Enet/list.c \
    ../source/Utils/Enet/packet.c \
    ../source/Utils/Enet/peer.c \
    ../source/Utils/Enet/protocol.c \
    ../source/Utils/Enet/unix.c \
    ../source/Utils/EASTL/allocator.cpp \
    ../source/Utils/EASTL/assert.cpp \
    ../source/Utils/EASTL/fixed_pool.cpp \
    ../source/Utils/EASTL/hashtable.cpp \
    ../source/Utils/EASTL/red_black_tree.cpp \
    ../source/Utils/EASTL/string.cpp

GLOBAL_SRC_FILES := $(LOCAL_SRC_FILES)

GLOBAL_C_INCLUDES :=                               \
    $(LOCAL_PATH)/../include                       \
    $(LOCAL_PATH)/../include/The2D                 \
    $(LOCAL_PATH)/../include/The2D/Utils           \
    $(LOCAL_PATH)/../include/The2D/Utils/Tinyxml   \
    $(LOCAL_PATH)/../include/The2D/Utils/EASTL     \
    "${Cocos2d_root}/cocos2dx" \
    "${Cocos2d_root}/cocos2dx/platform/linux" \
    "${Cocos2d_root}/cocos2dx/include" \
    "${Cocos2d_root}/cocos2dx/kazmath/include" \
    "${Cocos2d_root}/CocosDenshion/include" \
    "${Cocos2d_root}/cocos2dx/cocoa" 

LOCAL_SHARED_LIBRARIES += libcutils libutils
LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog

LOCAL_C_INCLUDES := $(GLOBAL_C_INCLUDES)

include $(BUILD_SHARED_LIBRARY)
