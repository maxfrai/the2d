Сервис информации о типах времени исполнения
------------------------------------------------

В данном отчете описана реализация RTTI, необходимая для множества внутренних механизмов Box2D. Например, уже сейчас могу сказать, что ее будут использовать Bind, система сообщений и XmlLoader. Собственно, к моменту написания отчета, XmlLoader уже создает объекты с ее помощью в SimpleApplication (что позволило сущенственно упростить код приложения).

<!--more-->

### Реализация RTTI

#### Структура The2DTypeInfo

Данная структура хранит информацию о классе. Она включает в себя следующие поля:

		/// \brief Clear name, without namespace and template parameters.
        const the::string name;
        /// \brief Return value of type_info::name().
        const the::string fullName;
        /// \brief Internal id of the type inside the The2D infrastructure.
        mutable uint16 engineID;
        /// \brief Return value of type_info::hash_code().
        const size_t uniqueID;
        /// \brief Size of type instance in bytes.
        /// \note Result of applying `sizeof` operator to class name.
        const size_t size;
        /// \brief Tool for dynamicly creating instances of the type.
        the::IEntityConstructor* const constructor;
        /// \brief List of the immediate base types.
        const std::vector<const The2DTypeInfo*> baseTypes;

Поле `name` используется сейчас парсером как имя тэга компонента. Например, для `the::Bind<the::PhysicComponent>` xml разметка будет выглядеть следующим образом:

	<Bind path="someBody" />
	
То есть, из имени класса исключено пространство имен `the::` и спецификация параметров шаблона `<the::PhysicComponent>`. Это было сделано потому, что в xml символы `<`, `>`, `::` имеют особое значение.

Поле fullName было добавлено для получения возможности однозначной идентификации типа, который представляет объект The2DTypeInfo во время отладки. В данном поле тип содержит спецификаторы пространства имен и параметры шаблона. Именно это значение возвращает оператор << применительно к экзмепляру The2DTypeInfo:

	const The2DTypeInfo& type = type(someEntityPtr);
	std::cout << type; // Prints type.fullName

Поле engineID будет использоваться в качестве индекса различных массивов и матриц, целью которых будет ускорение различных операций за счет потребления дополнительной памяти. Оно было сделано mutable по той причине, что его значение определяется при регистрации типа в сервисе RTTI, в то время как все остальные поля известы compile-time, и не могут измениться. 

Поле uniqueID используется операторами `==` и `!=` при сравнении двух объектов The2DInfo.

Поле size было добавлено просто так, потому что легко было его реализовать. Особых применений ему я пока не вижу, но мне оно кажется полезным.

Поле constructor хранить объект, позволяющий создавать экземпляры класса имя только его The2DTypeInfo структуру. Эта возможность сейчас используется в парсере.

И наконец последнее поле, список базовых классов, используется для реализации проверки совместимости типов. Типы являются совместимыми если они эквивалентны, или один из них является прямым или косвенным потомком другого. Другими словами, совместимые типы это такие типы, для которых dynamic_cast не выдаст исключения.

#### Добавление поддержки RTTI в класс

Для полной поддержки всех механизмов RTTI необходимо определить в классе три вещи:

1. Статическое поле `__the2DTypeInfo` типа `The2DTypeInfo`:

		static const the::The2DTypeInfo __the2DTypeInfo;
		
2. Виртуальный метод `getTypeInfo`, предоставляющий доступ к этому полю:

        virtual const the::The2DTypeInfo& getTypeInfo()
		{
			return __the2DTypeInfo;
		}

3. Класс, реализующий интерфейс IEntityConstructor:
        /// \brief Interface for creating instances of classes derived from `the::GameEntity`.
        /// \ingroup Engine
        /// For internal use in RTTI realisation.
        class IEntityConstructor
        {
            public:
                /// \brief Create instance.
                /// \note Memory managment lays on the caller.
                virtual the::GameEntity* create() = 0;

                virtual ~IEntityConstructor() {}
        };
	
   Экземпляр данного класса сохраняется в поле The2DTypeInfo::constructor.

   Метод create должен создавать экзмепляр класса с помощью оператора new и возвращать указатель на созданный объект. Как видно из сигнатуры метода, данный пункт возможен только для классов игровых сущностей, т.е. классов наследующих от GameEntity. В связи с этим, он является необязательным, и может быть опущен (в принципе, также как и второй). При этом, для поддержки таких классов в XmlLoader придется использовать другие механизмы (см. [отчет по системе парсинга](http://worklog.wincode.org/?p=114)).

Для добавления этих элементов в класс необходимо в объявлении класса добавить макрос THE_RTTI_DECLARATION, а в реализацию THE_RTTI_IMPLEMENTATION:

		--- *.hpp файл -----
		class MyEntity: public the::GameEntity, public ISomeInterface
		{
			THE_RTTI_DECLARATION
		};
		
		--- *.cpp файл -----
		THE_RTTI_IMPLEMENTAION(MyEntity, the::GameEntity, ISomeInterface)
		
Для шаблонного класса:

		template<typename T1, typename T2>
		class MyTemplateClass: public the::GameEntity
		{
			THE_RTTI_DECLARATION
		};
		
		THE_RTTI_TEMPLATE_IMPLEMENTATION(template<typename T1 COMMA typename T2>, MyTemplateClass<T1 COMMA T2>, the::GameEntity)
		
Обратите внимание, что в макросе `THE_RTTI_TEMPLATE_IMPLEMENTATION` параметры шаблона разделяются не запятой, а макросом COMMA. Это необходимо по той причине, что в противном случае препроцессор воспримет запятую как разделитель параметров макроса.

Макрос `THE_RTTI_IMPLEMENTATION` первым параметром принимает имя класса, после которого следует перечисление всех базовых классов через запятую. Аналогично устроен макрос `THE_RTTI_TEMPLATE_IMPLEMENTATION`. Замечу, что перечислять следует только те базовые классы, которые имеют поддержку RTTI (в противном случае возникнет ошибка компиляции). 

В тех случаях, когда необходимо добавить поддержку RTTI в класс не наследующий от GameEntity, можно использовать макросы `THE_RTTI_NOT_ENTITY_DECLARATION`, `THE_RTTI_NOT_ENTITY_IMPLEMENTATION` и `THE_RTTI_NOT_ENTITY_TEMPLATE_IMPLEMENTATION` соответственно. При этом в классе не будет объявлен третий элемент из списка приведенного выше в этом разделе.

### Использование RTTI

#### Сервис Rtti

Представлен серсвис классом Rtti, доступ к экземпляру которого можно получить через метод rtti в InfrastuctureCore. В первую очередь, сервис предназначен для генерации значений The2DTypeInfo::engineID. Все игровые сущности, которые попадают в дерево автоматически регистрируют свой тип в данном сервисе, в ходе которой объект The2DTypeInfo и приобретает свой идентификатор. В сущности, это просто порядковый номер. То есть, максимальное значение engineID равно колличеству типов сущностей, исползуемых в системе.

Помимо описанной задачи, класс Rtti предоставляет интерфейс для проверки совместимости типов. Проверка осуществляется на основе заранее вычисленной матрицы зависимостей, поэтому ее сложность очень низкая (О(1)). 

Матрица зависимостей является квадратной таблицей, в столбцах и строках которой находятся все зарегистрированные типы. На пересечении строки и столбца - число, характеризующее дистанцию в дереве наследования между типами. Число 0 означает, что типы несовместимы, то есть один не является явным или косвенным потомком другого. Значение 1 хранится на пересечении строки и столбца одного и того же типа. Все остальные значения характеризуют, насколько далеко расположены классы в дереве наследования.

Таким образом, для проверки совместимости типов достаточно прочитать соответствующее значение ячейки в матрице:

	virtual inline bool is(const The2DTypeInfo& type, const The2DTypeInfo& base) const
	{
		// Acces type.engineID row and base.engineID column in matrix, stored as flat array.
		return mInheritanceMatrix[type.engineID*mInheritanceMatrixCapacity + base.engineID] > 0;
	}

Метод также имеет шаблонную перегрузку, позволющую выполнять проверку с помощью следующего синтаксиса:

	GameEntity* entityPtr = tamTadam();
	if (core()->rtti()->is<FixtureComponent>(entityPtr))
	{
		FixtureComponent* comp = (FixtureComponent*)entityPtr;
	}
	
Третья функция сервиса - создание экземпляров классов по их имени в строковом виде. Другими словами Rtti реализует интерфейс IEntityCreator, и соответственно может быть зарегистрирован в этой роли в XmlLoader. Собственно, он уже автоматически там регистрируется в конструкторе InfrastructureCore. Поэтому, для добавления поддержки типа в парсер достаточно зарегистрировать его в Rtti:

    mCore->rtti()->registerType<the::PhysicComponent>();
    mCore->rtti()->registerType<the::BreakableFixtureComponent>();
    mCore->rtti()->registerType<the::DisplayManager>();
    mCore->rtti()->registerType<the::EventsManager>();
    mCore->rtti()->registerType<the::PhysicManager>();
    mCore->rtti()->registerType<the::CameraComponent>();
    mCore->rtti()->registerType<the::DebugDrawComponent>();
    mCore->rtti()->registerType<the::MouseDragComponent>();

#### Использование RTTI

Для того чтобы получить объект The2DTypeInfo для класса можно использовать шаблонный метод type:

	const The2DTypeInfo& typeInfo = type<FixtureComponent>();
	
Для получения информации о типе объекта GameEntity существует похожая нешаблонная перегрузка type:

	GameEntity* enitityPtr = tamTadam();
	const The2DTypeInfo& objTypeInfo = type(entityPtr);
	
В действительности вызовы этих методов преобразуются в следующий код:

	const The2DTypeInfo& typeInfo = FixtureComponent::__the2DTypeInfo; // static field
	GameEntity* enitityPtr = tamTadam();
	const The2DTypeInfo& objTypeInfo = entityPtr->getTypeInfo(); // virtual call
	
Замечу также, что для структуры The2DTypeInfo перегружены операторы `==`, `!=` и `<<`. Сравнение структур производится по полям uniqueID, которые хранят значение typeid(TypeName).hash_code(). Оператор `<<` выводит в поток поле fullName. Для более читабельного вывода можно использовать поле name.

В качестве их иллюстрации описанных функций, приведу код функции, которая выводит на экран все игровое дерево и сохраняет в вектор все найденные FixtureComponent:

	// Использование функции
    std::vector<the::FixtureComponent*> fixtures;
    FindFixturesRecursively(core->tree(), fixtures);

    for(auto &fxt: fixtures)
    {
        std::cout << "*" << fxt->getName() << std::endl;
    }

    void FindFixturesRecursively(the::GameEntity* current, std::vector<the::FixtureComponent*>& fixtures, int dbgLevel = 0)
    {
        std::cout << std::string(dbgLevel, '.') << type(current) << std::endl;
        if (the::type(current) == the::type<the::FixtureComponent>())
        {
            fixtures.push_back(static_cast<the::FixtureComponent*>(current));
        }
        for (auto &child : current->getChildren())
        {
            FindFixturesRecursively(child, fixtures, dbgLevel+1);
        }
    }

Вместо оператора `==` в приведенной функции лучше использовать метод is класса Rtti:

		if (core()->rtti()->is<the::FixtureComponent>(current))
		
Это позволит находить не только объекты класса FixtureComponent, но и объекты всех производных от него классов. Замечу, что сложность этой проверки почти равна сложности оператора `==`.
	
[Задача в мегаплане](http://wincode.megaplan.ru/task/1000137/card/?fromList=1000137)
	
Выполнил: Богатырев Павел
