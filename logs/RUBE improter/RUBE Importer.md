Необходимо написать консольное кросплатформенное (linux[x86,x64]-windows[x86,x64]) приложение, выполняющее конвертирование json файла особого формата в соответствующий ему xml файл другого формата. Программа принимает два аргумента командной строки: путь к исходному json-файлу и путь, по которому нужно сохранить результирующий xml-файл. То есть, синтаксис запуска следующий (на linux):

    ./RubeToThe2D SomeSourceFile.json SomeOutputFile.xml

Если SomeOutputFile.xml уже существует, программа должна перезаписать его. Если это невозможно, то вывести в консоль сообщение об ошибке, и ждать нажатия клавиши. Аналогично, если SomeSourceFile.json не существует или недоступен, вывести в консоль сообщение об ошибке и ждать нажатия клавиши. Считать, что входной фал всегда содержит правильную json разметку. По завершении конвертации, вывести в консоль сообщение и завершить выполнение программы (без ожидания нажатия клавиши).

Формат исходного json-файла описывается [здесь](https://www.iforce2d.net/rube/json-structure).

Формат соответствующего выходного файла описывается в прилагаемом документе. Данный документ построчно соответствует описанию исходного json файла по приведенному адресу (можно сравнить их при помощи kdiff3 или аналогичной программы). Комментарии в документе (начинаются с `//`) приведены для соответствия описанию json формата и в результирующем файле их быть не должно.

Особенности, на которые нужно обратить внимание:

Организационное:

- Возможно, удобнее будет предварительно конвертировать json в xml с помощью существующих библиотек. Однако, использование данных библиотек следует согласовать с заказчиком.
- Язык и платформу разработки необходимо предварительно согласовать с заказчиком.
- По всем вопросам относительно подробностей задания обращаться по любому из следующих контактов:

    * email: PFight77@gmail.com
    * jabber: PFight77@gmail.com
    * vk.com: http://vk.com/pfight

По формату файлов:

- Регистр в результирующем файле не имеет значения (<Name> равносильно <name>), в исходном файле имеет;
- В результирующем файле должны быть только те элементы, которые есть в исходном (не нужно добавлять другие);
- Конструкции вида `<bodyA>1</bodyA>` заменяются на `<bodyABind path="../name1" />`. При этом, name1 это значение "name" того элемента BodyComponent, который в файле встречается (в данном случае) 2-тым по счету (1 - zero-based индекс). Например, следующий входной файл:

        {
           "body" : 
           [
              {
                 "name" : "someName1" 

              }      
              {
                 "name" : "someName2" 
              }
           ],
           
           "joint" : 
           [
              {         
                 "type" : "revolute"
                 "bodyA" : 0,
                 "bodyB" : 1
              }
              
          ]
        }

    Должен быть отконвертирован в следующий результирующий файл:

        <Level>
            <BodyComponent>
                <name>someName1</name>
            </BodyComponent>
            <BodyComponent>
                <name>someName2</name>
            </BodyComponent>
            <JointContainerComponent>
                <RevoluteJoint>
                    <bodyABind path="../someName1" />
                    <bodyBBind path="../someName2" />
                </RevoluteJoint>
            </JointContainerComponent>
        </Level>

- Помимо упомянутого в предыдущем пункте порядка определения "body" порядок элементов в json не гарантируется.

- Вектора в результирующем файле могут быть представлены в как `<localAnchorA x="0" y="0" />`, так и в следующей форме:
    
        <localAnchorA>
            <x>0</x>
            <y>0</y>
        </localAnchorA>
      









