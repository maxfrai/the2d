LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE   := The2D
LOCAL_MODULENAME := The2D
LOCAL_CPPFLAGS := -std=c++11
APP_USE_CPP0X := true

LOCAL_SRC_FILES := libs/armeabi/libThe2D.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include $(LOCAL_PATH)/include/The2D/Utils/EASTL \

include $(PREBUILT_SHARED_LIBRARY)
