////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/RttiSupport.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>
#include <The2D/InfrastructureCore.hpp>

#include <vector>
#include <map>
#include <list>
#include <stack>
#include <iostream>
#include <string>
#include <sstream>

////////////////////////////////////////////////////////////
namespace the
{
    typedef std::shared_ptr<TiXmlDocument> DocumentPtr;

    class InvalidSceneFormatException
    {
        public:
            std::string Message;

            InvalidSceneFormatException(std::string msg)
            {
                Message = msg;
            }
    };

    /// \class XmlPreprocessor XmlPreprocessor <The2D/Utils/XmlPreprocessor.hpp>
    /// \ingroup Utils
    /// \brief Black magic, that makes possible templates, conditional xml, params.
    class XmlPreprocessor
    {

        public:

            XmlPreprocessor(InfrastructureCore* core);
            XmlPreprocessor();
            /// \brief Applies all templates in xml-file.
            DocumentPtr process(const std::string &sceneFile);
            /// \brief Applies all templates in xml-file.
            TiXmlElement* process(TiXmlElement* tag, DocumentPtr baseDocument, const std::string& documentFile);
            /// \brief Applies all templates in xml-markup.
            DocumentPtr processXml(const std::string &xml);

            TiXmlElement* processParams(TiXmlElement* tag);
            TiXmlElement* processTemplates(TiXmlElement* tag, DocumentPtr baseDocument, const std::string& documentFile);

            void define(std::string var);
            void undefine(std::string var);
            bool defined(std::string var);
            void clearDefines();
            const std::vector<std::string>& getDefined() const;

        private:
            InfrastructureCore* mCore;
            std::string commonTemplateFile, templateFolder, paramTagName, paramPlaceholderTagName,
                paramPlaceholderParamAttributeName, debugLogAttributeName, universalTagName, notSetParamValue;
            std::map<std::string, DocumentPtr> mFileToDocument;
            std::list<std::pair<std::string, std::string>> mIncludeChain;
            int mDebugLogIndent;
            std::stringstream mLogMessage;
            std::stack<TiXmlElement*> mDebugLogElements;
            std::vector<std::string> mDefinedVariables;

            void beginDebugLogBlock(std::string msg);
            void endDebugLogBlock(std::string msg);
            void debugLog(std::string msg, bool newLine=true);

            void resetState();
            void printLog(std::string header, TiXmlElement* result);

            TiXmlElement getTemplate(const std::string& spec,
                                      DocumentPtr baseDocument,
                                      const std::string& baseFile);

            TiXmlElement* getSingleTemplate(const std::string& _spec,
                                            DocumentPtr baseDocument,
                                            const std::string& baseFile);

            TiXmlElement* getTemplatesTag(DocumentPtr baseDocument, const std::string& baseFile);

            std::vector<std::string> getIncludedFiles(DocumentPtr baseDocument, const std::string& baseFile);

            TiXmlElement* getLocalTemplate(const std::string& name,
                                           DocumentPtr baseDocument,
                                           const std::string& baseFile);

            std::string solveFileName(const std::string& file, const std::string& basePath);

            DocumentPtr getFileDocument(const std::string& filePath);

            void processTag(TiXmlElement* tag, DocumentPtr baseDocument, const std::string& baseFile);

            void applyTemplate(TiXmlElement* tag, TiXmlElement* templateTag);
            void applyParent(TiXmlElement* tag, TiXmlElement* parentTag);
            void applyTemplateRecursive(TiXmlElement* tag, TiXmlElement* templateTag);
            void copyAttributes(TiXmlElement* tag, TiXmlElement* templateTag);
            bool tagsEquivalent(const TiXmlElement*  tag1, const TiXmlElement* tag2);
            void clearStuffRec(TiXmlElement* current);
            std::list<TiXmlNode*> getParamValue(TiXmlElement* paramElem);
            std::string getParamValueStr(TiXmlElement* paramElem);
            void processParamsRec(TiXmlElement* elem, std::list<TiXmlElement*>& paramStack);

            bool parseIfdef(std::string def);
            bool parseFlatIfdef(std::string def);
    };
}; // namespace the
////////////////////////////////////////////////////////////
