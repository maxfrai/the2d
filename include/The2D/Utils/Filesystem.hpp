////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#ifdef NIX
    #include <sys/stat.h>
#endif

#include <string>
#include <algorithm>

namespace the
{
    struct Filesystem
    {
        inline static void normalize_path(std::string& _path)
        {
            std::replace(_path.begin(), _path.end(), '\\', '/');
        }

        // /foo/bar -->  /foo
        inline static std::string branch_path(const std::string& _path)
        {
            std::string result = _path;
            normalize_path(result);

            size_t pos = result.find_last_of('/');
            if (pos != string::npos)
                result.erase(result.begin()+pos, result.end());

            return result;
        }

        // /foo/bar/temp.txt --> temp.txt
        inline static std::string leaf_path(const std::string& _path)
        {
            std::string result = _path;
            normalize_path(result);

            size_t pos = result.find_last_of('/');
            if (pos != string::npos)
                result.erase(result.begin(), result.begin()+pos+1);

            return result;
        }

        // /foo/bar/temp.txt --> /foo/bar
        inline static std::string pure_path(const std::string& _path)
        {
            std::string result = _path;
            normalize_path(result);

            size_t pos = result.find_last_of('/');
            if (pos != string::npos)
                result.erase(result.begin()+pos+1, result.end());

            return result;
        }

        inline static bool exists(const std::string& _path)
        {
            //#ifdef NIX
                struct stat st;
                return stat(_path.c_str(), &st) == 0;
            //#endif

            /*#ifdef WIN32
                DWORD attr = GetFileAttributes(_path.c_str());
                if (attr == 0xFFFFFFFF)
                {
                    switch (GetLastError())
                    {
                    case ERROR_FILE_NOT_FOUND:
                    case ERROR_PATH_NOT_FOUND:
                    case ERROR_NOT_READY:
                    case ERROR_INVALID_DRIVE:
                        return false;
                    }
                }
                return true;
            #endif*/
        }
    };
};