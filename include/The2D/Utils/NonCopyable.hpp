////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
namespace the
{
    /// \brief Denies to copy class which inherits it
    /// Use private inheritance
    /// Example usage:
    /// \code
    /// class MyUniqueClass : private the::NonCopyable<MyUniqueClass> { /* ... */ };
    /// \endcode
    template <class T>
    class NonCopyable
    {
      protected:
        NonCopyable () {}
        ~NonCopyable () {}

      private:
        NonCopyable (const NonCopyable &);
        NonCopyable& operator = (const NonCopyable &);
    };
} // namespace the
////////////////////////////////////////////////////////////
