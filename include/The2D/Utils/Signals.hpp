////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#include <The2D/Config.hpp>
#include <EASTL/list.h>
#include <functional>

namespace the
{


    template<typename T>
    class Signal;

    template<typename R, typename ...Args>
    class Signal<R (Args...)>
    {
        public:

          typedef void* Handle;
          typedef eastl::pair<Handle, std::function<R(Args...)>> Subscriber;
          typedef typename eastl::list<Subscriber>::iterator SubscribersIterator;
          typedef Signal<R (Args...)> SignalType;

          Signal()
          {
          }

          Handle add(std::function<R(Args...)> func)
          {
              // Push front, to not break raise process, in case of adding in its middle
              mSubscribers.push_back(eastl::make_pair(nullptr, func));
              mSubscribers.back().first = &mSubscribers.back();

              return mSubscribers.back().first;
          }

          Handle add(Handle subscriberHandle, std::function<R(Args...)> func)
          {
              assert(subscriberHandle != nullptr && "nullptr is used for removal marking");

              // Push front, to not break raise process, in case of adding in its middle
              mSubscribers.push_back(eastl::make_pair(subscriberHandle, func));

              return subscriberHandle;
          }


          void remove(Handle handle)
          {
              for (SubscribersIterator it = mSubscribers.begin(); it != mSubscribers.end(); it++)
              {
                  if (it->first == handle)
                  {
                      it->first = nullptr;
                      it->second = std::function<R(Args...)>(); // remove old functor,
                                                                //to prevent lazy removal problems
                  }
              }
          }

          void removeAll()
          {
              for (auto& sub : mSubscribers)
              {
                  sub.first = nullptr;
                  sub.second = std::function<R(Args...)>(); // remove old functor,
                                                            //to prevent lazy removal problems
              }
          }

          bool subscribed(Handle handle)
          {
              return find(handle) != mSubscribers.end();
          }

          bool hasSubscribers() const
          {
              for (auto sub : mSubscribers)
              {
                  if (sub.first != nullptr) return true;
              }
              return false;
          }

          inline void raise(Args ...args)
          {
              operator()(args...);
          }

          inline void operator()(Args ...args)
          {
              for (SubscribersIterator it = mSubscribers.begin();
                  it != mSubscribers.end(); it++)
              {
                  // nullptr handle is removal mark
                  while (it->first == nullptr)
                  {
                      auto toRemove = it;
                      it++;
                      mSubscribers.erase(toRemove);
                      if (it == mSubscribers.end())
                      {
                          return;
                      }
                  }

                  it->second(args...);
              }
          }


        private:
          eastl::list<Subscriber> mSubscribers;

          SubscribersIterator find(Handle handle)
          {
              SubscribersIterator it = mSubscribers.begin();
              while(it != mSubscribers.end())
              {
                  if (it->first == handle) break;
                  else it++;
              }
              return it;
          }

          Signal(const SignalType& other)
          {
              // Not copyable
          }

          SignalType& operator=(const SignalType& other)
          {
              // Not copyable
          }

    };
}