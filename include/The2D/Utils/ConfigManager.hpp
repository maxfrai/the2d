////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>

#include <vector>
#include <unordered_map>
#include <string>

////////////////////////////////////////////////////////////
namespace the
{
    /**
     * \class ConfigManager ConfigManager.hpp "Utils/ConfigManager.hpp"
     * \ingroup Utils
     * \brief Storing and reading world associated params (key=>value)
     *
     * Example usage:
     * \code
     * #include "Utils/ConfigManager.hpp"
     * ConfigManager *configHandle = core()->config();
     * // Read from config file value of "window.width" key. The default return value is 1024
     * configHandle->GetValue<float>("window.width", 1024);
     *
     * // Read and write params into/from memory
     * configHandle->SetFloat("Pi", 3.1415f);
     * configHandle->GetFloat("Pi");
     * \endcode
     */
    class ConfigManager
    {
        public:

            /**
             * \brief Constructor. Here the config file is loaded into memory
             */
            ConfigManager();
            virtual ~ConfigManager(){}

            /**
             * \brief Get list values of single key from config file
             */
            std::vector<std::string> getListValue(const std::string &key) const;

            /**
             * \brief Returns unique string (Used for naming of diffrent objects
             */
            std::string randomUUID();

            /**
             * \brief Get value associated to passed key
             * \param key          The key we need value for
             * \param defaultValue The default returned value in case of fail
             */
            template<typename T>
            T getValue(const std::string &key, const T defaultValue = T()) const
            {
                // return mTreeHandle.get<T>(key, defaultValue);
                return T();
            }

            ////////////////////////////////////////////////////////////
            // Getters
            ////////////////////////////////////////////////////////////

            /**
             * \brief Get integer value by passed key from memory
             * \param key The key we need value for
             */
            int32* Int(const std::string &key);

            /**
             * \brief Get float value by passed key from memory
             * \param key The key we need value for
             */
            float* Float(const std::string &key);

            /**
             * \brief Get string value by passed key from memory
             * \param key The key we need value for
             */
            std::string* String(const std::string &key);

            ////////////////////////////////////////////////////////////
            // Setters
            ////////////////////////////////////////////////////////////

            /**
             * \brief Set int value for passed key in memory
             * \param key   The key we set value for
             * \param value The value of key
             * \return Pointer in memory for inserted key
             */
            int32* setInt(const std::string &key, int32 value);

            /**
             * \brief Set float value for passed key in memory
             * \param key   The key we set value for
             * \param value The value of key
             * \return Pointer in memory for inserted key
             */
            float* setFloat(const std::string &key, float value);

            /**
             * \brief Set string value for passed key in memory
             * \param key   The key we set value for
             * \param value The value of key
             * \return Pointer in memory for inserted key
             */
            std::string* setString(const std::string &key, std::string value);

        private:
            string mTreePath;                                        ///< Name of the config file

            std::unordered_map<std::string, int32> mIntegerStorage;       ///< Associative map for storing int values
            std::unordered_map<std::string, float> mFloatStorage;         ///< Associative map for storing float values
            std::unordered_map<std::string, std::string> mStringStorage;  ///< Associative map for storing string values

            // For uinuque ids
            uint32 mUUIDs;
    };
}; // namespace the
////////////////////////////////////////////////////////////
