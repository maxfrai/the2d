////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#include <sstream>
#include <algorithm>

namespace the
{
    template<typename T2, typename T1>
    inline T2 lexical_cast(const T1 &in) {
        T2 out;
        std::stringstream ss;
        ss << in;
        ss >> out;
        return out;
    }

    template<>
    inline bool lexical_cast(const string &in) {
        if (in == "true" || in == "1") return true;
        else return false;
    }

    inline void trim(std::string& data)
    {
        std::stringstream trimmer;
        trimmer << data;
        data.clear();
        trimmer >> data;
    }
    inline std::string trimCopy(std::string data)
    {
        std::string result = data;
        trim(result);
        return result;
    }

    inline bool compareCaseInsensitive(std::string first, std::string second)
    {
        string firstLower = first;
        std::transform(firstLower.begin(), firstLower.end(), firstLower.begin(), ::tolower);
        string secondLower = second;
        std::transform(secondLower.begin(), secondLower.end(), secondLower.begin(), ::tolower);
        return firstLower == secondLower;
    }

    typedef std::size_t HashCode;

    constexpr HashCode INVALID_HASH = 0;

    template<typename T>
    inline HashCode getHashCode(const T& val)
    {
        std::hash<T> h;
        HashCode result = h(val);
        return (result == INVALID_HASH)? 1 : result;
    }

    template<typename ParentT, typename ChildT>
    ChildT& fluentRegisterChild(ParentT* const parent, ChildT& child, const char* childName)
    {
        parent->registerChild(&child, childName);
        return child;
    }

    template<typename ParentT, typename ChildT>
    ChildT& fluentRegisterChild(ParentT* const parent, ChildT* child, const char* childName)
    {
        parent->registerChild(child, childName);
        return *child;
    }

    template<typename ParentT, typename ChildT>
    ChildT& fluentRegisterChild(ParentT* const parent, std::shared_ptr<ChildT> child, const char* childName)
    {
        parent->registerChild(child.get(), childName);
        return *(child.get());
    }

    #define REGISTER_IN(parent, child) fluentRegisterChild(parent, child, #child)
    #define REGISTER(p) REGISTER_IN(this, p)

    template<typename ParentT, typename ChildT>
    ChildT& fluentUnregisterChild(ParentT* const parent, ChildT& child)
    {
        parent->unregisterChild(&child);
        return child;
    }

    template<typename ParentT, typename ChildT>
    ChildT& fluentUnregisterChild(ParentT* const parent, ChildT* child)
    {
        parent->unregisterChild(child);
        return *child;
    }

    template<typename ParentT, typename ChildT>
    ChildT& fluentUnregisterChild(ParentT* const parent, std::shared_ptr<ChildT> child)
    {
        parent->unregisterChild(child.get());
        return *(child.get());
    }

    #define UNREGISTER_IN(parent, child) fluentUnregisterChild(parent, child)
    #define UNREGISTER(p) UNREGISTER_IN(this, p)
};
