// stacktrace.h (c) 2008, Timo Bingmann from http://idlebox.net/
// published under the WTFPL v2.0

#ifndef _STACKTRACE_H_
#define _STACKTRACE_H_

#if defined(PC_PLATFORM_BUILD) && defined(NIX)

#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <The2D/Utils/TinyFormat.hpp>
#include <The2D/Utils/Filesystem.hpp>
#include <unordered_map>

namespace
{
    std::unordered_map<int, std::string> __the__callstack_data;
}

/** Print a demangled stack backtrace of the caller function to FILE* out. */
static inline std::string getStacktrace(unsigned int max_frames = 63)
{
    std::string result;
    result.reserve(200);
    result.append("[Stack Trace]:\n");

    // Storage array for stack trace address data
    void* addrlist[max_frames+1];

    // Retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0)
    {
        result.append("  <empty, possibly corrupt>\n");
        return result;
    }

    auto it = __the__callstack_data.find(addrlen);
    if (it != __the__callstack_data.end())
        return (*it).second;

    // Resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // Allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // Iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 4; i < addrlen; i++)
    {
        char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

        // Find parentheses and +address offset surrounding the mangled name:
        // ./module(function+0x15c) [0x8048a6d]
        for (char *p = symbollist[i]; *p; ++p)
        {
            if (*p == '(')
                begin_name = p;
            else if (*p == '+')
                begin_offset = p;
            else if (*p == ')' && begin_offset)
            {
                end_offset = p;
                break;
            }
        }

        if (begin_name && begin_offset && end_offset
            && begin_name < begin_offset)
        {
            *begin_name++ = '\0';
            *begin_offset++ = '\0';
            *end_offset = '\0';

            // Mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():

            std::string libName = the::Filesystem::leaf_path(symbollist[i]);

            int status;
            char* ret = abi::__cxa_demangle(begin_name, funcname, &funcnamesize, &status);
            if (status == 0)
            {
                funcname = ret; // use possibly realloc()-ed string
                result.append(the::format("  %s : %s+%s\n", libName.c_str(), funcname, begin_offset));
            }
            else
            {
                // Demangling failed. Output function name as a C function with
                // no arguments.
                result.append(the::format("  %s : %s()+%s\n", libName.c_str(), begin_name, begin_offset));
            }
        }
        else
        {
            // Couldn't parse the line? print the whole line.
            result.append(the::format("  %s\n", symbollist[i]));
        }
    }

    free(funcname);
    free(symbollist);

    __the__callstack_data[addrlen] = result;
    return result;
}

#else // PC_PLATFORM_BUILD
static inline std::string getStacktrace(unsigned int max_frames = 63)
{
    return "";
}
#endif

#endif // _STACKTRACE_H_
