////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/AttachedBase.hpp>
#include <The2D/ChunkVector.hpp>
#include <EASTL/vector.h>

#include <The2D/Utils/Utils.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \class Attached Attached "Attached.hpp
    /// \brief Adds support for attached properties and logic.
    /// There are two ways to attach some info: attach whole object
    /// or simply add string pair (key, value). When parsing xml,
    /// Attached trying to create object with GameTree::CreateObject,
    /// and if it failed, adds to internal map pair (<element_name>, <element_text>)
    /// To access attached objects use GetChildren, and to access attached
    /// string properties, use HasProperty and GetProperty.
    class Attached: public GameEntity, public IXmlParser, private NonCopyable<Attached>
    {
        public:
            THE_ENTITY(the::Attached, the::GameEntity);

            Attached();
            virtual ~Attached();

            std::list<ParsingError> parse(TiXmlElement* element);
            IXmlParser* getParser();
            bool isMyTag(TiXmlElement *element);
            TiXmlElement* serialize();

            /// \brief Manual adding of attached nodes.
            /// \param ent Node to attach.
            virtual void attach(std::shared_ptr<GameEntity> ent, InitOrder initOrder = AUTO);

            /// \brief Manual removing of attached nodes.
            /// \param ent Node to remove.
            virtual void detach(GameEntity* ent);

            /// \brief Manual removing of attached nodes.
            /// \param ent Node to remove.
            virtual void detach(std::shared_ptr<GameEntity> ent);

            /// \brief Return true, if there is property with name 'name'.
            virtual bool hasProperty(string name);

            /// \brief Parses property, and writes result into valueVariable.
            /// \param name Name of the attached property.
            /// \param valueVariable Refernce to place, where to write result value.
            /// \param T Excepted type of  property (awailable bool, int, float, double).
            /// \return Is there property with name 'name' and is value parsed successfully.
            template<typename T>
            bool getProperty(string name, T& valueVariable)
            {
                auto it = mAttachedProperties.find(name);
                if (it != mAttachedProperties.end())
                {
                    auto parser = createDefaultParameter<T>();
                    parser->setNoParent();
                    string strVal = getText(it->second);
                    if (parser->parse(strVal))
                    {
                        valueVariable =  parser->getValue();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            /// \brief Parses property, and writes result into xVariable and yVariable.
            /// \param name Name of the attached property.
            /// \param xVariable Refernce to place, where to write x-component of result value.
            /// \param yVariable Refernce to place, where to write y-component of result value.
            /// \return Is there property with name 'name' or not.
            bool getVector(string name, float& xVariable, float& yVariable)
            {
                TiXmlElement* propertyElement = getProperty(name);
                if (propertyElement != nullptr)
                {
                    PVector parser;
                    parser.setNoParent();
                    parser.setXmlName(name);
                    parser.parse(propertyElement);
                    xVariable = parser.x;
                    yVariable = parser.y;
                }
                else
                {
                    return false;
                }
            }

            /// \brief Returns attached property as it was in level file.
            /// \param name Name of the attached string property.
            /// \returns Clone of the TiXmlElement of attached property.
            virtual TiXmlElement* getProperty(string name);

            InitState onInit();
            void onDeinit();

        protected:
            // Store shared_ptr to prevent object removal
            std::vector<std::shared_ptr<GameEntity>> mAttached;
            std::map<string, TiXmlElement*> mAttachedProperties;
            string getText(TiXmlElement* el); // To avoid include tinyxml.h in header
    };


    template<typename TargetT, typename T>
    class AttachedProperty: public AttachedBase
    {
        protected:
            template<typename TT>
            struct AttachedValueContainer
            {
                TT value;
                ISupportAttachedInternal* owner;
            };

        public:
            THE_ENTITY(AttachedProperty<TargetT COMMA T>, AttachedBase)

            typedef AttachedValueContainer<T> ValueContainer;

            Signal<void(TargetT*)> SigValueChanged;

            AttachedProperty(): AttachedBase(type<TargetT>().name, false),
                mFreeCellsCount(0), mFreeCellsCountToStartDefrag(257),
                mDefaultValue(new T()), mOwnDefaultValueObj(true), 
                mHasAccessors(false), mGetterIntermediateValue(nullptr)
            {
            }

            AttachedProperty(const T* defaultValue): AttachedBase(type<TargetT>().name, false),
                mFreeCellsCount(0), mFreeCellsCountToStartDefrag(257),
                mDefaultValue(defaultValue), mOwnDefaultValueObj(false), 
                mHasAccessors(false), mGetterIntermediateValue(nullptr)
            {
            }

            virtual ~AttachedProperty()
            {
                if (mOwnDefaultValueObj)
                {
                    delete mDefaultValue;
                }
                delete mGetterIntermediateValue;
            }

            inline bool hasAccessors() const
            {
                return mGetterIntermediateValue;
            }

            inline const T& operator[](TargetT* ent) const
            {
                return getValue(ent);
            }

            inline bool hasValue(ISupportAttachedInternal* ent) const
            {
                if (hasAccessors())
                {
                    return true;
                }
                else
                {
                    return ent->getAttachedIndex(getID(), getClassID()) != INVALID_INDEX;
                }
            }

            /// \returns Value, attached to `ent`. Reference is const, because value modification allowed only with setValue function.
            inline const T& getValue(TargetT* ent) const
            {
                if (hasAccessors())
                {
                    mGetter(ent, *mGetterIntermediateValue);
                    return *mGetterIntermediateValue;
                }
                else
                {
                    ChunkVectorIndex index = ent->getAttachedIndex(getID(), getClassID());
                    //INFO("Get value of %i for %s at index %i %i", getID(), ent->getPathString(), index.chunkIndex, index.itemIndex);
                    if (index != INVALID_INDEX)
                    {
                        return mValues[index].value;
                    }
                    else
                    {
                        return *mDefaultValue;
                    }
                }
            }

            inline T& beginEdit(TargetT* ent)
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");
                
                ChunkVectorIndex index = ent->getAttachedIndex(getID(), getClassID());
                //INFO("Get value of %i for %s at index %i %i", getID(), ent->getPathString(), index.chunkIndex, index.itemIndex);
                if (index == INVALID_INDEX)
                {
                    index = mValues.push_back({T(), ent});
                    ent->setAttachedIndex(getID(), getClassID(), index);
                }
                return mValues[index].value;
            }

            inline void endEdit(TargetT* ent)
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                SigValueChanged(ent);
            }

            inline void reset(ISupportAttachedInternal* ent)
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                ChunkVectorIndex index = ent->getAttachedIndex(getID(), getClassID());
                if (index != INVALID_INDEX)
                {
                    ASSERT(mValues[index].owner == ent, "There is bug in attached system");
                    mValues[index].owner->setAttachedIndex(getID(), getClassID(), INVALID_INDEX);
                    mValues[index].owner = nullptr;
                    mFreeCellsCount++;
                    if (mFreeCellsCount >= mFreeCellsCountToStartDefrag)
                    {
                        defragAndShrink();
                    }
                    SigValueChanged(static_cast<TargetT*>(ent));
                }                
            }

            inline void setValue(TargetT* ent, const T& val)
            {
                //ASSERT(ent->getTypeInfo().is(type<TargetT>()), "This property setup to use only with TargetT objects (and derived from it)");

                if (hasAccessors())
                {
                    mSetter(ent, val);
                }
                else
                {
                    ChunkVectorIndex index = ent->getAttachedIndex(getID(), getClassID());
                    if (index != INVALID_INDEX)
                    {
                        mValues[index].value = val;
                        // INFO("Set value %i at index %i %i for %s", val, index.chunkIndex, index.itemIndex, ent->getPathString());
                    }
                    else
                    {
                        // There is no cell in mValues for `ent`, so allocate it
                        index = mValues.push_back({val, ent});
                        ent->setAttachedIndex(getID(), getClassID(), index);
                        // INFO("Set value %i at index %i %i for %s",mValues[index].value, index.chunkIndex, index.itemIndex, mValues[index].owner->getPathString());
                    }
                }
                SigValueChanged(ent);
            }

            /// \brief Get all objects, where that property has a value, different from default.
            sptr<eastl::vector<TargetT*>> getAttachiersT() const
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                eastl::vector<TargetT*>* result = new eastl::vector<TargetT*>();

                result->reserve(mValues.size() - mFreeCellsCount + 1);
                for (auto it = mValues.begin(); it != mValues.end(); it = mValues.inc(it))
                {
                    if (mValues[it].owner != nullptr)
                    {
                        result->push_back(static_cast<TargetT*>(mValues[it].owner));
                    }
                }

                return  sptr<eastl::vector<TargetT*>>(result);             
            }
            /// \brief Get all objects, where that property has a value, different from default.
            sptr<eastl::vector<ISupportAttachedInternal*>> getAttachiers() const override
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");
                
                eastl::vector<ISupportAttachedInternal*>* result = new eastl::vector<ISupportAttachedInternal*>();
                if (!hasAccessors())
                {
                    result->reserve(mValues.size() - mFreeCellsCount + 1);
                    for (auto it = mValues.begin(); it != mValues.end(); it = mValues.inc(it))
                    {
                        if (mValues[it].owner != nullptr)
                        {
                            result->push_back(mValues[it].owner);
                        }
                    }
                }
                return  sptr<eastl::vector<ISupportAttachedInternal*>>(result);
            }

            /// \brief Get all values of properties.
            sptr<eastl::vector<T>> getValues() const
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");
                
                eastl::vector<T>* result = new eastl::vector<T>();
                result->reserve(mValues.size() - mFreeCellsCount + 1);
                for (auto it = mValues.begin(); it != mValues.end(); it = mValues.inc(it))
                {
                    if (mValues[it].owner != nullptr)
                    {
                        result->push_back(mValues[it].value);
                    }
                }
                return  sptr<eastl::vector<T>>(result);
            }

            void resetAll(bool shrinkAttachiers = false) override
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");
                
                auto attachiers = getAttachiers();
                for (auto at : *attachiers.get())
                {
                    reset(at);
                    if (shrinkAttachiers)
                    {
                        at->shrinkAttachedIndexes();
                    }
                }
                mValues.clear();
                mFreeCellsCount = 0;
            }

            /// \brief Free memory for not used cells.
            /// Complexity is O(getValuesCount() + getFreeCellsCount()) in the worst case
            /// and O(getFreeCellsCount()) in the best case.
            void defragAndShrink() override
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");
                
                ChunkVectorIndex it1 = mValues.begin();
                ChunkVectorIndex it2 = mValues.rbegin();
                while (it1 != it2 && mFreeCellsCount > 0)
                {
                    if (mValues[it2].owner == nullptr)
                    {
                        while(mValues[it1].owner != nullptr)
                        {
                            it1 = mValues.inc(it1);
                        }
                        mValues[it1] = mValues[it2];
                        mValues[it1].owner->setAttachedIndex(getID(), getClassID(), it1);
                        mValues.pop_back(); // it2 is back
                        mFreeCellsCount--;
                    }
                    it2 = mValues.rinc(it2);
                }
            }


            int getFeeCellsCountToStartDefrag() const
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                return mFreeCellsCountToStartDefrag;
            }

            int getFreeCellsCount() const
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                return mFreeCellsCount;
            }

            /// \brief Get count of attachiers, that has not default values of this property.
            /// Complexity is O(1).
            /// \see getAttachiers
            int getValuesCount() const override
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                return mValues.size() - mFreeCellsCount;
            }

            size_t getUsedMemory() const override
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                return mValues.used_memory() + sizeof(AttachedProperty<TargetT, T>);
            }

            size_t getWastedMemory() const override
            {
                ASSERT(!hasAccessors(), "Function do not supported when accessors are specified");

                return mValues.wasted_memory() + mFreeCellsCount * sizeof(ValueContainer);
            }

             // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline AttachedProperty<TargetT, T>& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline AttachedProperty<TargetT, T>& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline AttachedProperty<TargetT, T>& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

            AttachedProperty<TargetT, T>& defval(const T& val)
            {
                setDefaultValue(val);
                return *this;
            }

            AttachedProperty<TargetT, T>& setDefaultValue(const T& val)
            {
                if (mOwnDefaultValueObj)
                {
                    delete mDefaultValue;
                }
                mDefaultValue = new T(val);
                return *this;
            }

            AttachedProperty<TargetT, T>& setDefaultValue(const T* val)
            {
                if (mOwnDefaultValueObj)
                {
                    delete mDefaultValue;
                    mOwnDefaultValueObj = false;
                }
                mDefaultValue = val;
                return *this;
            }

            /// \brief If count of free cells will become more the `count`, `defragAndShrink` will be called.
            AttachedProperty<TargetT, T>& setFeeCellsCountToStartDefrag(int count)
            {
                mFreeCellsCountToStartDefrag = count;
                return *this;
            }

            AttachedProperty<TargetT, T>& setGetter(std::function<void(TargetT*, T&)> getter)
            {
                setAccessors(getter, nullptr);
            }
            AttachedProperty<TargetT, T>& setAccessors(std::function<void(TargetT*, T&)> getter,
                std::function<void(TargetT*, const T&)> setter)
            {
                mGetter = getter;
                mSetter = setter;
                mGetterIntermediateValue = new T();
                mHasAccessors = true;
            }

            //------------------------------------------------------------------

            void removeAccessors()
            {
                mHasAccessors = false;
                mGetter = nullptr;
                mSetter = nullptr;
                delete mGetterIntermediateValue;
                mGetterIntermediateValue = nullptr;
            }

            const T& getDefaultValue() const
            {
                return *mDefaultValue;
            }

            virtual void onSetCore(InfrastructureCore* oldVal, InfrastructureCore* newVal) override
            {
                if (newVal == nullptr)
                {
                    resetAll();
                }
                AttachedBase::onSetCore(oldVal, newVal);
            }

            virtual string getValueType() override
            {
                return typeid(T).name();
            }

        protected:
            the::ChunkVector<ValueContainer> mValues;
            int mFreeCellsCount;
            int mFreeCellsCountToStartDefrag;
            const T* mDefaultValue;
            bool mOwnDefaultValueObj;
            bool mHasAccessors;
            T* mGetterIntermediateValue;
            std::function<void(TargetT*, T&)> mGetter;
            std::function<void(TargetT*, const T&)> mSetter;

    };

} // namespace the
////////////////////////////////////////////////////////////
