////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>

#include <deque>
#include <memory>

////////////////////////////////////////////////////////////
namespace the
{
    class InfrastructureCore;

    /**
     * \class IState IState
     * \ingroup Scene
     * \brief Abstract base class for defining of states in world
     */
    class IState : private the::NonCopyable<IState>
    {
        public:

            virtual ~IState() { }
            /**
             * \brief Initialization code of the state
             *
             * Here you can initialize objects, load level, configure everything, etc.
             */
            virtual void init(InfrastructureCore* core) = 0;

            /**
             * \brief Is called each global game cycle update
             *
             * Here you can affect on your objects during runtime
             * \warning You shouldn't call graphic and physic update here manually!
             * \param dSeconds delta time from last frame rendering
             */
            virtual void update(float dSeconds) = 0;

            /**
             * \brief Is called before changing or shutting down active state
             */
            virtual void shutdown() = 0;

            /**
             * \brief Returs string identifier of current state
             */
            virtual string type() const = 0;
    };

    typedef std::deque<std::shared_ptr<IState>> StateStorage;  ///< Deque whish stores instances of states

    /**
     * \class StateMachine StateMachine.hpp "StateMachine.hpp"
     * \ingroup Scene
     * \brief Stores and manipulates game states
     */
    class StateMachine : private the::NonCopyable<StateMachine>
    {
        public:

            StateMachine(InfrastructureCore* core);
            virtual ~StateMachine();

            void init();
            void deinit();

            /// \brief Push pointer to state from C++
            virtual void push(std::shared_ptr<IState> state); // From C++
            /// \brief Push pointer to state from Python
            //void push(bp::object state);   // From Py
            /// \brief In this function state's update is called
            virtual void update(float dSeconds);

            /// \brief Returns pointer to current active state
            virtual std::shared_ptr<IState> getActiveState() const;
            virtual void pop();

            /// \brief This function is called when you want to change active state
            static void readyToAdvance();

        protected:
            ///< Shows whether from last call the state was changed
            bool mStateChanged;

            ///< List of pushed states
            StateStorage mStates;
            ///< Pointer to current active state
            std::shared_ptr<IState> mActiveState;

            ///< Set to true if you want to switch to next state
            static bool mAdvanceState;

            InfrastructureCore* mCore;
    };
}; // namespace the
////////////////////////////////////////////////////////////
