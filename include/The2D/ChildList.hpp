////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>
#include <The2D/Utils/Signals.hpp>

#include "XmlLoader.hpp"

////////////////////////////////////////////////////////////
namespace the
{
    /// \class ChildList ChildList "The2D/ChildList.hpp"
    template<typename ChildT>
    class ChildList: public GameEntity, public IXmlParser
    {
        public:
            THE_ENTITY(the::ChildList<ChildT>, the::GameEntity);

            ChildList(): mChildInitOrder(NOT_SET)
            {
                setChildTagName(type<ChildT>().name);
                setConstructor([&](){ return static_cast<GameEntity*>(type<ChildT>().constructor->create()); });
            }

            virtual ~ChildList() {}

            virtual  std::list<the::ParsingError> parse(TiXmlElement* element)
            {
                ASSERT(isMyTag(element), ":(");

                std::list<ParsingError> errors;

                string childTagLower = getChildTagName();
                std::transform(childTagLower.begin(), childTagLower.end(), childTagLower.begin(), ::tolower);

                for (auto childElem = element->FirstChildElement(); childElem != nullptr;
                    childElem = childElem->NextSiblingElement())
                {
                    if (childElem->GetUserData() == NODE_VIEWED) continue;

                    string elemNameLower= childElem->ValueStr();
                    std::transform(elemNameLower.begin(), elemNameLower.end(), elemNameLower.begin(), ::tolower);

                    std::shared_ptr<ChildT> child;
                    if (elemNameLower == childTagLower)
                    {
                        child = std::shared_ptr<ChildT>(static_cast<ChildT*>(getConstructor()()));
                    }
                    else
                    {
                        std::shared_ptr<GameEntity> childEnt = core()->xml()->createEntity(childElem->ValueStr());
                        if (childEnt.get() != nullptr)
                        {
                            if (core()->rtti()->is(type(childEnt.get()), type<ChildT>()))
                            {
                                child = std::static_pointer_cast<ChildT>(childEnt);
                            }
                            else
                            {
                                childEnt->setNoParent();
                            }
                        }
                    }
                    if (child.get() != nullptr)
                    {
                        add(child);
                        child->setXmlTagName(childElem->Value());
                        auto childErrors = child->getParser()->parse(childElem);
                        childElem->SetUserData(NODE_VIEWED);
                        errors.splice(errors.begin(), childErrors);
                    }
                }

                auto parentErrors = GameEntity::getParser()->parse(element);
                errors.splice(errors.begin(), parentErrors);

                return errors;
            }

            virtual bool isMyTag(TiXmlElement *element)
            {
                return GameEntity::getParser()->isMyTag(element);
            }

            virtual TiXmlElement* serialize()
            {
                TiXmlElement* root = new TiXmlElement(getXmlTagName());
                for (auto it = mAllChildren.begin(); it != mAllChildren.end(); it++)
                {
                    TiXmlElement* childElem = (*it)->getParser()->serialize();
                    if (childElem != nullptr)
                    {
                        root->LinkEndChild(childElem);
                    }
                }
                if (root->FirstChildElement() != nullptr || root->FirstAttribute() != nullptr)
                {
                    return root;
                }
                else
                {
                    return nullptr;
                }
            }

            virtual the::IXmlParser* getParser()
            {
                return this;
            }


            virtual void add(std::shared_ptr<ChildT> ent)
            {
                InitOrder order = mChildInitOrder;
                if (order == NOT_SET)
                {
                    order = ent->getInitOrder();
                }
                add(ent, order);
            }

            virtual void add(std::shared_ptr<ChildT> ent, InitOrder initOrder)
            {
                add(ent.get(), initOrder);
                mStoredChildren.push_back(ent);
            }

            virtual void add(ChildT* ent)
            {
                InitOrder order = mChildInitOrder;
                if (order == NOT_SET)
                {
                    order = ent->getInitOrder();
                }
                add(ent, order);
            }

            virtual void add(ChildT* ent, InitOrder initOrder)
            {
                ent->setParent(this, mChildTagName, initOrder);
                // Rest of the work will do onRegisterChild
            }

            virtual void remove(ChildT* ent)
            {
                if (ent->isInitialized())
                {
                    ent->deinit();
                }
                ent->resetParent();

                // Rest of the work will do onUnregisterChild
            }
            virtual void remove(std::shared_ptr<ChildT> ent)
            {
                remove(ent.get());
            }

            virtual the::string getChildTagName()
            {
                return mChildTagName;
            }

            virtual std::function<GameEntity*()> getConstructor()
            {
                return mConstructor;
            }

            virtual int size()
            {
                return mAllChildren.size();
            }

            virtual ChildT* operator[](int i)
            {
                return static_cast<ChildT*>(mAllChildren[i]);
            }

            virtual void clear()
            {
                for (int i = mAllChildren.size()-1; i>=0; i--)
                {
                    remove(mAllChildren[i]);
                }
            }

            virtual InitOrder getChildInitOrder()
            {
                return mChildInitOrder;
            }

            virtual const std::vector<ChildT*>& get()
            {
                return mAllChildren;
            }

            virtual const std::vector<ChildT*>& operator()()
            {
                return get();
            }

            // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline ChildList& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline ChildList& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline ChildList& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

            virtual ChildList& setConstructor(std::function<GameEntity*()> ctor)
            {
                mConstructor = ctor;
                return *this;
            }

            virtual ChildList& setChildInitOrder(InitOrder order)
            {
                mChildInitOrder = order;
                return *this;
            }

            virtual ChildList& setChildTagName(string name)
            {
                mChildTagName = name;
                return *this;
            }
            // ---------------------------------------------------------------

        protected:
            string mChildTagName;
            std::vector<ChildT*> mAllChildren;
            std::vector<std::shared_ptr<ChildT>> mStoredChildren;
            std::function<GameEntity*()> mConstructor;
            InitOrder mChildInitOrder;

            virtual void onRegisterChild(GameEntity* child) override
            {
                if (type(child).is(type<ChildT>()))
                {
                    mAllChildren.push_back(static_cast<ChildT*>(child));

                    if (getInitOrder() == AUTO)
                    {
                        setInitOrder(child->getInitOrder());
                    }
                    if (mChildInitOrder == NOT_SET)
                    {
                        mChildInitOrder = child->getInitOrder();
                    }
                }
            }

            virtual void onUnregisterChild(GameEntity* child) override
            {
                auto it = std::find(mAllChildren.begin(),mAllChildren.end(), child);
                if (it != mAllChildren.end())
                {
                    mAllChildren.erase(it);
                }
                for (auto it = mStoredChildren.begin(); it != mStoredChildren.end(); it++)
                {
                    if (it->get() == child)
                    {
                        mStoredChildren.erase(it);
                        break;
                    }
                }
            }
    };

} // namespace the
////////////////////////////////////////////////////////////
