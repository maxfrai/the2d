////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#include <The2D/GameTree.hpp>
#include <The2D/IEntityCreator.hpp>
#include <The2D/LogService.hpp>
#include <The2D/Utils/XmlPreprocessor.hpp>
#include <The2D/InfrastructureCore.hpp>


namespace the
{

    /// \brief Automatizes loading of game tree from xml-file.
    class XmlLoader
    {
    public:

            XmlLoader(InfrastructureCore* core);
            virtual ~XmlLoader();

            XmlPreprocessor* getPreprocessor();

            /// \brief Involve specified realization of IEntityCreator into process of creation objects.
            /// \param realization Realization of IEntityCreator interface.
            virtual void registerCreator(std::shared_ptr<IEntityCreator> realization);

            /// \brief Involve specified realization of IEntityCreator into process of creation objects.
            /// \param realization Realization of IEntityCreator interface.
            virtual void registerCreator(IEntityCreator* realization);

            /// \brief Do not use specified realization of IEntityCreator in process of creation objects.
            /// \param realization Realization of IEntityCreator interface registered before by call of `registerCreator`.
            virtual void unregisterCreator(const IEntityCreator* realization);

            /// \brief Create instance of specified type.
            /// \param typeName Name of the type.
            /// \return Shared pointer to created instance of specified type.
            /// Method trying to create instance with all registered creators sequentially. When some
            /// creator returns non-nullptr value, this value returns as result.
            virtual std::shared_ptr<GameEntity> createEntity(const the::string& typeName) const;

            /// \brief Load entities from file and register them in main tree.
            /// \return Pointer to root of the loaded entities.
            /// \param xmlFilePath Path to file, where declared game entities.
            /// \param doInitialization If true, then loaded tree will be initialized (by call of GameTree::init).
            /// Method uses registered creators to create entities by its type names in xml. Then, it add all created
            /// entities as children to specially created GameTree object, that registers in main game tree.
            virtual GameEntity* loadSubTree(const the::string& xmlFilePath, bool doInitialization);

            /// \brief Load entities from xml and register them in main tree.
            /// \return Pointer to root of the loaded entities.
            /// \param xml Xml elements, represents root of the tree.
            /// \param doInitialization If true, then loaded tree will be initialized (by call of GameTree::init).
            /// Method uses registered creators to create entities by its type names in xml. Then, it add all created
            /// entities as children to specially created GameTree object, that registers in main game tree.
            virtual GameEntity* loadSubTree(TiXmlElement* xml, DocumentPtr baseDocument, const string& documentFile, bool doInitialization);

            /// \brief Deinitializes and removes from tree specified subtree.
            /// \param subTree Subtree to unload.
            virtual void unloadSubTree(GameEntity* subTree);

            /// \brief Load entities from file.
            /// \note Method do not register loaded entities in main game tree.
            /// \param xmlFilePath Path to file, where declared game entities.
            /// \return Shared pointer to root of the loaded entities.
            /// Method uses registered creators to create entities by its type names in xml. Then, it add all created
            /// entities as children to specially created GameTree object. Shared pointer to this object is the return value.
            virtual std::shared_ptr<GameEntity> load(const the::string& xmlFilePath, GameEntity* parent);

            virtual void parse(const the::string& xmlFilePath, GameEntity* tree);

            /// \brief Load entities from file.
            /// \note Method do not register loaded entities in main game tree.
            /// \param xml  Xml elements, represents root of the tree.
            /// \return Shared pointer to root of the loaded entities.
            /// Method uses registered creators to create entities by its type names in xml. Then, it add all created
            /// entities as children to specially created GameTree object. Shared pointer to this object is the return value.
            virtual std::shared_ptr<GameEntity> load(TiXmlElement* xml, DocumentPtr baseDocument, const string& documentFile, GameEntity* parent);

            virtual void parse(TiXmlElement* xml, DocumentPtr baseDocument, const string& documentFile, GameEntity* tree);

        protected:
            InfrastructureCore* mCore;
            std::list<IEntityCreator*> mCreators;
            std::list<std::shared_ptr<IEntityCreator>> mStoredCreators;
            XmlPreprocessor mPreprocessor;
    };
}
