/*
    Copyright 2011 Pavel Bogatirev

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <The2D/Config.hpp>
#include <The2D/Utils/Utils.hpp>
#include <list>

namespace the
{

    /// \struct ParsingError ParsingError "DParser/ParsingError.hpp"
    struct ParsingError
    {
        /// \brief Error message.
        std::string Message;
        /// \brief Path in the XML tree to element, where occured error.
        std::list<std::string> TagNamePath;
        /// \brief If this field is true, then XML input was invalid.
        /// Critical are errors of missing node, marked as MUST_BE, or fail during parse some string value.
        /// Not critical - unexcepted node.
        bool Critical;
        int Row;

        /*ParsingError(std::string tagName, bool critical, std::string message)
            :Message(message), TagNamePath(), Critical(critical), Row(-1)
        {
            TagNamePath.push_back(tagName);
        }*/

         ParsingError(std::string tagName, bool critical, std::string message, int row)
            :Message(message), TagNamePath(), Critical(critical), Row(row)
        {
            TagNamePath.push_back(tagName);
        }

        /// \brief Forms summary string for this error.
        /// \return String of the next format: <path to the node>: <Message>
        std::string toString()
        {
            std::string result;
            std::string arrow = "";
            for (auto tag = TagNamePath.rbegin(); tag != TagNamePath.rend(); tag++)
            {
                result += arrow + *tag;
                arrow = "->";
            }

            result += "| row " + the::lexical_cast<std::string>(Row) + ": " + Message;

            return result;
        }
    };
}