////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <iostream>

////////////////////////////////////////////////////////////
namespace the {


    /// \brief 2D vector structure for use in the2d applications`.
    /// Fairly stoled from Box2D (b2Vec2) : ) Thanks to Erin.
    ///
    /// Structure introduced to increase compability of components, but saving syntax clear
    /// and perfomance high.
    struct Vec2
    {
        /// Default constructor does nothing (for performance).
        Vec2() {}
        Vec2(float x, float y) : x(x), y(y) {}
        inline void setZero() { x = 0.0f; y = 0.0f; }
        inline void set(float x_, float y_) { x = x_; y = y_; }
        inline Vec2 operator -() const { Vec2 v; v.set(-x, -y); return v; }
        inline float operator () (int32 i) const
        {
            return (&x)[i];
        }
        inline float& operator () (int32 i)
        {
            return (&x)[i];
        }
        inline void operator += (const Vec2& v)
        {
            x += v.x; y += v.y;
        }
        inline void operator -= (const Vec2& v)
        {
            x -= v.x; y -= v.y;
        }
        inline void operator *= (float a)
        {
            x *= a; y *= a;
        }
        inline float length() const
        {
            return sqrt(x * x + y * y);
        }
        /// Get the length squared. For performance, use this instead of
        /// Vec2::Length (if possible).
        inline float lengthSquared() const
        {
            return x * x + y * y;
        }
        /// Convert this vector into a unit vector. Returns the length.
        inline float normalize()
        {
            float l = length();
            if (l < THE_EPSILON)
            {
                return 0.0f;
            }
            float invLength = 1.0f / l;
            x *= invLength;
            y *= invLength;

            return l;
        }
        /// Get the skew vector such that dot(skew_vec, other) == cross(vec, other)
        inline Vec2 skew() const
        {
            return Vec2(-y, x);
        }

        float x, y;
    };

    template<typename T>
    std::ostream& operator << (std::ostream &o,const Vec2 &a)
    {
        return o << a.x << ", " << a.y;
    }

    template<typename T>
    std::istream& operator >> (std::istream &o, Vec2 &a)
    {
        o >> a.x;
        o.get();
        o.get();
        o >> a.y;
        return o;
    }

    inline Vec2 operator + (const Vec2& a, const Vec2& b)
    {
        return Vec2(a.x + b.x, a.y + b.y);
    }
    inline Vec2 operator - (const Vec2& a, const Vec2& b)
    {
        return Vec2(a.x - b.x, a.y - b.y);
    }
    inline Vec2 operator * (float s, const Vec2& a)
    {
        return Vec2(s * a.x, s * a.y);
    }
    inline bool operator == (const Vec2& a, const Vec2& b)
    {
        return a.x == b.x && a.y == b.y;
    }
    inline bool operator != (const Vec2& a, const Vec2& b)
    {
        return a.x != b.x || a.y != b.y;
    }

}; // namespace the



////////////////////////////////////////////////////////////
