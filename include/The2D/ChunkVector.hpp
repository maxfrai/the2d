/*
    Copyright 2011 Pavel Bogatirev

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <The2D/Config.hpp>
#include <The2D/Utils/Utils.hpp>
#include "Utils/NonCopyable.hpp"
#include <The2D/LogService.hpp>
#include <The2D/ChunkVectorIndex.hpp>

namespace the
{
    template<typename T>
    class ChunkVector: private NonCopyable<ChunkVector<T>>
    {
        public:

            template<typename CT>
            class Chunk: private NonCopyable<Chunk<CT>>
            {
                public:
                    Chunk(int capacity = 10)
                        : mCapacity(capacity)
                    {
                         mData.reserve(capacity);
                    }

                    inline int capacity() const
                    {
                        return mCapacity;
                    }

                private:
                    int mCapacity;
                    eastl::vector<CT> mData;

                    friend class ChunkVector<CT>;
            };

            ChunkVector(int firstChunkCapacity = 8, float sizeGrow = 2.f, int maxChunkSize=256)
                : mSizeGrowKoef(sizeGrow), mSize(0), mCapacity(firstChunkCapacity), mMaxChunkSize(maxChunkSize)
            {
                mChunks.push_back(new Chunk<T>(firstChunkCapacity));
                mCurrentChunk = mChunks.size()-1;
            }

            ~ChunkVector()
            {
                for(auto chunk : mChunks)
                {
                    delete chunk;
                }
            }

            inline ChunkVectorIndex inc(ChunkVectorIndex ind) const
            {
                ASSERT(ind.chunkIndex < mChunks.size(), "");
                if (ind.itemIndex+1 < (int)mChunks[ind.chunkIndex]->mCapacity)
                {
                    return { ind.chunkIndex, (uint8)(ind.itemIndex +1)};
                }
                else
                {
                    return { (uint8)(ind.chunkIndex +1), 0 };
                }
            }
            inline ChunkVectorIndex rinc(ChunkVectorIndex ind) const
            {
                return dec(ind);
            }
            inline ChunkVectorIndex dec(ChunkVectorIndex ind) const
            {
                ASSERT(ind.chunkIndex < mChunks.size(), "");

                if (ind.itemIndex > 0)
                {
                    return { ind.chunkIndex, (uint8)(ind.itemIndex -1)};
                }
                else
                {
                    // If chunkIndex is zero, overflow fill cause, it is ok
                    return { (uint8)(ind.chunkIndex - 1), 0 };
                }
            }
            inline ChunkVectorIndex rdec(ChunkVectorIndex ind) const
            {
                return inc(ind);
            }

            inline ChunkVectorIndex first() const
            {
                ASSERT(size() > 0, "There is no first element, vector is empty");
                        
                return {0, 0};
            }

            inline ChunkVectorIndex last() const
            {
                ASSERT(size() > 0, "There is no last element, vector is empty");

                for (int chunk = mCurrentChunk; chunk >=0; chunk--)
                {
                    if (mChunks[chunk]->mData.size() > 0)
                    {
                        return {(uint8)chunk, (uint8)(mChunks[chunk]->mData.size()-1)};
                    }
                }
                // Impossible, because size() > 0
                return {0, 0};
            }
            inline ChunkVectorIndex begin() const
            {
                if (size() > 0) return first();
                else return {0, 0};
            }
            inline ChunkVectorIndex end() const
            {
                if (size() > 0) return inc(last());
                else return {0, 0};
            }
            inline ChunkVectorIndex rbegin() const
            {
                 if (size() > 0) return last();
                 else return {0,0};
            }
            inline ChunkVectorIndex rend() const
            {
                if (size() > 0) return dec(first());
                else return {0, 0};
            }

            template<typename ...Types>
            inline ChunkVectorIndex push_back(T data)
            {
                Chunk<T>* chunk = mChunks[mCurrentChunk];
                while ((int)chunk->mData.size() >= chunk->mCapacity)
                {
                    mCurrentChunk++;
                    if (mCurrentChunk >= (int)mChunks.size())
                    {
                        chunk = addChunk();
                        mCurrentChunk = mChunks.size()-1;
                    }
                }
                chunk->mData.push_back(data);
                mSize++;
                return { (uint8)mCurrentChunk, (uint8)(chunk->mData.size()-1) };
            }

            inline void pop_back()
            {
                ASSERT(size() > 0, "There is nothing to pop");

                if (last().itemIndex > 0)
                {
                    mChunks[last().chunkIndex]->mData.pop_back();
                }
                else
                {
                    removeLastChunk();
                }
            }

            inline int size() const
            {
                return mSize;
            }

            inline T& at(ChunkVectorIndex ind) const
            {
                ASSERT(ind.chunkIndex < mChunks.size(), "Invalid index");
                ASSERT(ind.itemIndex < mChunks[ind.chunkIndex]->mData.size(), "Invalid index");

                return mChunks[ind.chunkIndex]->mData[ind.itemIndex];
            }

            inline T& operator[](ChunkVectorIndex ind) const
            {
                return at(ind);
            }

            inline T& front() const
            {
                ASSERT(!empty(), "No front element");
                return at(first());
            }
            inline T& back() const
            {
                ASSERT(!empty(), "No back element");
                return at(last());
            }

            inline bool empty() const
            {
                return mSize == 0;
            }

            inline int capacity() const
            {
                return mCapacity;
            }

            inline void erase(ChunkVectorIndex ind)
            {
                ASSERT(ind.chunkIndex < mChunks.size(), "Invalid index");
                ASSERT(ind.itemIndex < mChunks[ind.chunkIndex]->mData.size(), "Invalid index");

                mChunks[ind.chunkIndex]->mData.erase(mChunks[ind.chunkIndex]->mData.begin() + ind.itemIndex);
                while(mChunks.back()->mData.size() == 0 && mChunks.size() > 1)
                {
                    removeLastChunk();
                }
                mSize--;
            }

            inline bool remove(const T& value)
            {
                ChunkVectorIndex found = find(value);
                if (found != end())
                {
                    erase(found);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            inline bool contains(const T& value) const
            {
                return find(value) != end();;
            }

            inline ChunkVectorIndex find(const T& value) const
            {
                for (ChunkVectorIndex ind = begin(); ind != end(); ind = inc(ind))
                {
                    if (at(ind) == value) return ind;
                }
                return end();
            }

            inline void clear()
            {
                while(mChunks.size() > 1)
                {
                    mChunks.pop_back();
                }
                mChunks.back()->mData.clear();
                mCurrentChunk = 0;
                mSize = 0;
            }

            inline size_t used_memory() const
            {
                return capacity() * sizeof(ChunkVectorIndex) + sizeof(Chunk<T>)*mChunks.size() + sizeof(ChunkVector<T>);
            }
            
            inline size_t wasted_memory() const
            {
                return (capacity() - size()) * sizeof(ChunkVectorIndex);
            }

            inline const std::vector<Chunk<T>*>& get_chunks() const
            {
                return mChunks;
            }

            
        private:
            float mSizeGrowKoef;
            std::vector<Chunk<T>*> mChunks;
            int mCurrentChunk;

            int mSize;
            int mCapacity;
            int mMaxChunkSize;


            inline Chunk<T>* addChunk()
            {
                int size = mChunks.back()->capacity() * mSizeGrowKoef + 0.5f;
                if (size > mMaxChunkSize)
                {
                    size = mMaxChunkSize;
                }
                mChunks.push_back(new Chunk<T>(size));
                mCapacity += size;
                return mChunks.back();
            }
            inline void removeLastChunk()
            {
                ASSERT(mChunks.size() > 1, "Removing last chunk will break vector logic");

                mCapacity -= mChunks.back()->capacity();
                auto it = mChunks.end();
                it--;
                if (it == mChunks.begin()+mCurrentChunk)
                {
                    mCurrentChunk--;
                }
                delete *it;
                mChunks.erase(it);
            }
    };
}