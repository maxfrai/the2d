////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>

#include <cstdarg>

////////////////////////////////////////////////////////////
namespace the
{
    class GameEntity;

    /// \class Manager Manager "Manager.hpp"
    /// \brief Base class for managers.
    /// Managers control unique, common for many components resources.
    class Manager: public GameEntity
    {
        public:
            THE_ENTITY(the::Manager, the::GameEntity)

            Manager();
            virtual ~Manager();

            virtual void onParentChanged() override;
            virtual void onParentChanging(GameEntity* ent) override;
    };
} // namespace the
////////////////////////////////////////////////////////////
