////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Connectors.hpp>
#include <The2D/Attached.hpp>

////////////////////////////////////////////////////////////
namespace the {

    /// \defgroup Connectors Connectors
    /// @{
    
    
    template<typename T>
    class TargetFuncImplBase;

    template<typename ReturnT, typename ... ArgsT>
    class TargetFuncImplBase<ReturnT(ArgsT...)>: public ISupportRtti
    {
        public:
            THE_ABSTRACT_ENTITY(TargetFuncImplBase<ReturnT(ArgsT...)>);

            virtual ReturnT execBase(ISupportRtti* target, ArgsT... args) const = 0;
            virtual const The2DTypeInfo& getTargetType() const = 0;
            virtual void addImpl(TargetFuncImplBase* impl) = 0;
            virtual const TargetFuncImplBase* getImplFor(const The2DTypeInfo& type) const = 0;
    };

    template<typename TargetT, typename ArgsT, bool needsSource>
    class TargetFuncBase;


    /// \brief Connector, representing some action, that can perform a component.
    /// Command should be binded via functor to some code, performing an action. Code can be executed
    /// manually by call of CFuncBase::exec, or by subscribing command to some CEvent.
    /// Subscribing can be done from code or from xml.
    ///
    /// Arguments of command passes via children connectors. Access these argumens in convinient way one canal
    /// through CParametrized interface.
    template<typename TargetT, typename ReturnT, typename ... ArgsT, bool needsSource>
    class TargetFuncBase<TargetT, ReturnT(ArgsT...), needsSource>: 
        public CFuncBase<ReturnT(TargetT*, ArgsT...), needsSource>,
        public TargetFuncImplBase<ReturnT(ArgsT...)>
    {
        public:
            THE_ABSTRACT_ENTITY(TargetFuncBase<TargetT COMMA ReturnT(ArgsT...) COMMA needsSource>,
                CFuncBase<ReturnT(TargetT* COMMA ArgsT...) COMMA needsSource>);

            typedef TargetFuncImplBase<ReturnT(ArgsT...)> ImplT;

            AttachedProperty<The2DTypeInfo, ImplT*> atVTable;
            Bind<ImplT> toBaseImplBind;
            PString baseImpl;

            TargetFuncBase()
            {
                REGISTER(atVTable).defval(this);
                REGISTER(toBaseImplBind).setRequired(false);
                REGISTER(baseImpl).defval("");
            }

            inline std::function<ReturnT(ArgsT...)> forTarget(TargetT* target) const
            {
                return operator[](target);
            }

            inline std::function<ReturnT(ArgsT...)> operator[](TargetT* target) const
            {
                ASSERT(target != nullptr, "Invalid argument");

                const The2DTypeInfo& targetType = type(target);

                if (!this->core()->rtti()->typeRegistered(targetType))
                {
                    this->core()->rtti()->registerType(targetType);
                }
                
                const ImplT* impl = atVTable[const_cast<The2DTypeInfo*>(&targetType)];
                if (impl == this)
                {
                    return [target, this](ArgsT... args){ return this->exec(target, args...); };
                }
                else
                {
                    return [target, impl](ArgsT... args){ return impl->execBase(target, args...); };
                }
            }

            virtual ReturnT execBase(ISupportRtti* target, ArgsT... args) const override
            {
                return this->forTarget(static_cast<TargetT*>(target))(args...);
            }

            virtual const The2DTypeInfo& getTargetType() const override
            {
                return type<TargetT>();
            }

            virtual void addImpl(ImplT* impl) override
            {
                ASSERT(impl != nullptr, "Invalid argument");

                // Implementation of function in base class should be accessible for all derived
                spreadPropertyDownToHierarchyRec(&impl->getTargetType(), impl);
            }

            virtual const ImplT* getImplFor(const The2DTypeInfo& type) const override
            {
                return atVTable[const_cast<The2DTypeInfo*>(&type)];
            }

        protected:

            virtual void onPreInit() override
            {
                if (baseImpl.containsValue())
                {
                    if (!toBaseImplBind.path().empty())
                    {
                        ERR("Set both, `baseImpl` and toBaseImplBind.path. Value of `baseImpl` ignored.");
                    }
                    else
                    {
                        toBaseImplBind.path = baseImpl();
                    }
                }
            }

            InitState onInit() override
            {
                if (toBaseImplBind.containsValue())
                {
                    toBaseImplBind->addImpl(this);
                }
                return READY;
            }

            void spreadPropertyDownToHierarchyRec(const The2DTypeInfo* type, ImplT* impl)
            {
                atVTable.setValue(const_cast<The2DTypeInfo*>(type), impl);

                for (auto& derived : type->derivedTypes)
                {
                    spreadPropertyDownToHierarchyRec(derived, impl);
                }
            }
    };

    template<typename TargetT, typename ArgsT>
    using ToTargetFunc = TargetFuncBase<TargetT, ArgsT, true>;

    template<typename TargetT, typename ArgsT>
    using CTargetFunc = TargetFuncBase<TargetT, ArgsT, false>;


    /// @}

}; // namespace the



////////////////////////////////////////////////////////////