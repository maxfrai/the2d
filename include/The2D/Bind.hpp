////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>
#include <The2D/Utils/Signals.hpp>
#include <The2D/ObserverService.hpp>
#include <list>
#include <cstring>
#include "Rtti.hpp"
#include "BindService.hpp"
namespace the
{
    class InfrastructureCore;
    class GameTree;
}

////////////////////////////////////////////////////////////
namespace the
{
    enum Dependency
    {
        DEPENDENCY,
        NOT_A_DEPENDENCY
    };

    /// \class Bind<T> Bind<T> "Components/Basic/Bind.hpp"
    /// \brief Tool for static xml binding.
    ///
    /// Class may be useful, if you want point from one part of scene to another. <br/>
    /// For example, to create joint, you need point two bodies, wich you want to connect.<br/>
    /// To make possible bind something with the::Bind, you must realize there the::GameEntity.<br/>
    /// For binding you can use name, type and group of item.<br/>
    /// There are examples:<br/>
    /// Bind to component with name myBodyComponent of object with name Stone1:<br/>
    /// <Bind path="Stone1/myBodyComponent" />
    /// Bind to all BodyComponent in all RopeComponent contains in object with name SomeRopeObject:<br/>
    /// <Bind path="SomeRopeObject/RopeComponent/BodyComponent" />
    /// Bind to BodyComponent with name start of all RopeComponent contains in object with name SomeRopeObject:<br/>
    /// <Bind path="SomeRopeObject/RopeComponent/BodyComponent/start" />
    /// Bind to item with name start of all RopeComponent contains in object with name SomeRopeObject:<br/>
    /// <Bind path="SomeRopeObject/RopeComponent/start" />
    /// Bind to all items of group "car1", with no differece, where they lay:<br/>
    /// <Bind path="../car1" />
    /// Bind to all existing on the scene BodyComponent:<br/>
    /// <Bind path="../BodyComponent" />
    /// Bind to all BodyComponent that belong to group vehicles in MyCarObject:<br/>
    /// Bind path="MyCarObject/vehicles/BodyComponent"
    /// Bind to PhysicCompoennt of item with name Stone1, laying somewhere:<br/>
    /// <Bind path="../Stone1/BodyComponent" />
    /// Bind to all BodyComponent of group car1 <br/>
    /// <Bind path="../car1/../BodyComponent" />
    /// \warning Bind keeps shared_ptr to object (GameEntity::GetObject). Cyrcle binding will raise cyrcle
    /// reference, and object will never be deleted.
    class BindBase: public GameEntity
    {
        public:
            THE_ENTITY(the::BindBase, the::Component);

            PString path;

            BindBase();
            virtual ~BindBase();

            /// \brief Makes search for item to bind at path.
            virtual bool find();

            virtual bool checkPath(GameEntity* ent);

            virtual bool containsValue() const;

            inline GameEntity* getBind() const
            {
                ASSERT(!isMultiBind(), "Calling getBind of the multi bind. Call getBinds instead.");

#ifdef DEBUG
                if (mBind != nullptr && !mBind->isInitialized())
                {
                    WARN("Accessing uninitialized entity %s via bind %s", mBind->getShortInfoString(), getPathString());
                }
#endif

                return mBind;
            }

            /// \brief Removes all binded items from cash, and adds 'item'.
            virtual void set(GameEntity* item);

            /// \brief Cleans cash of binded items.
            virtual void reset();

            virtual bool isMultiBind() const;

            // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline BindBase& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline BindBase& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline BindBase& setXmlTagName(std::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

            BindBase& setRequired(bool required);
            BindBase& setDependency(Dependency dependency);
            BindBase& setWeakOnDeinit(bool weak);
            // ----------------------------------------------------

            bool getRequired();

            the::string getXmlName();

            bool dependency();

            bool getParsed();

        protected:
            GameEntity* mBind;
            bool mBindIsFound;
            bool mIsDependency;
            bool mRequired;

            ParsedPath mParsedPath;
            WatcherHandle mWatcher;
            bool mIgnoreOnDeinit;

            virtual bool setItem(GameEntity* item, bool internal);

            /// \brief Calls the::Bind::FindItems.
            /// \return true, if found at least one item, otherwise false.
            virtual InitState onInit() override;
            virtual InitState onInitGotStuck() override;
            virtual void onPostInit() override;
            virtual void onDeinit() override;
            void registerIn(GameEntity* item);
            void unregisterIn(GameEntity* item);
    };

    template<typename T>
    class Bind: public BindBase
    {
        public:
            THE_ENTITY(Bind<T>, BindBase)

            Bind()
            {
            }

            virtual ~Bind()
            {
            }

            inline T* Get() const
            {
                GameEntity* item = BindBase::getBind();
                ASSERT(item != nullptr, ":(");

                if (this->isInGameTree() && !core()->rtti()->is(type(item), type<T>()))
                {
                    CRITICAL("Invalid binded item type. Needs '%s', got '%s'",
                        type<T>().fullName, type(item).fullName);
                }

                /// \todo Override setItem, and make cast once
                return dynamic_cast<T*>(item);
            }

            virtual bool find()
            {
                core()->rtti()->registerType(type<T>());

                return BindBase::find();
            }

            /// \brief Overloading of access operator instead of Bind::Get function
            virtual T* operator->() const
            {
                return Get();
            }

            virtual T* operator()() const
            {
                return Get();
            }

        protected:

            virtual bool setItem(GameEntity* item, bool internal) override
            {
                if (!isInGameTree() || item == nullptr || core()->rtti()->is(type(item), type<T>())) return BindBase::setItem(item, internal);
                else
                {
                    WARN("For path '%s' found item, but there is types mismatch: found %s, needed %s (or derived from it).| Item info:\n %s",
                         path(), type(item).fullName, type<T>().fullName, item->getDetailedInfoString());
                    return false;
                }
            }
    };

    template<typename TargetT, typename ValueT>
    using ToAttachedProperty = Bind<AttachedProperty<TargetT, ValueT>>;


    template<typename ManagerT>
    class ManagerBind: public Bind<ManagerT>
    {
        public:
            THE_ENTITY(ManagerBind<ManagerT>, Bind<ManagerT>)

            ManagerBind()
            {
                this->path.setRequired(false);
                this->setRequired(false);
            }

            virtual ~ManagerBind()
            {
            }

            virtual InitState onInit() override
            {
                if (!this->containsValue())
                {
                    for (auto man : this->core()->observer()->getManagerList())
                    {
                        if (this->core()->rtti()->is(type(man), type<ManagerT>()))
                        {
                            this->setItem(man, true);

                        }
                    }
                }
                // If previous loop found item, or if it is not first call of onInit (mBind was not initialized)
                if (this->containsValue())
                {
                    if (this->dependency() && !this->mBind->isInitialized())
                    {
                        return NOT_READY("Binded item %s is not initialized|Bind item info:\n%s",
                            this->mBind->getShortInfoString(), this->mBind->getDetailedInfoString());
                    }
                    else
                    {
                        return READY;
                    }
                }
                else
                {
                    return NOT_READY("Manager not found");
                }
            }
            virtual InitState onInitGotStuck() override
            {
                return this->getInitState();
            }

            virtual ManagerT* operator()() const
            {
                return this->Get();
            }

            virtual ManagerT* operator->() const
            {
                return this->Get();
            }
    };


    class MultiBindBase: public BindBase
    {
        public:
            THE_ENTITY(the::MultiBindBase, the::BindBase);

            MultiBindBase();
            virtual ~MultiBindBase();

            /// \brief Manual adding binded items.
            virtual void addItem(GameEntity* item);

            virtual void removeItem(GameEntity* item);

            /// \return Count of binded items.
            virtual int getCount() const;

            virtual std::list<GameEntity*> getBinds() const;

            virtual bool containsValue() const;

            /// \brief Overloading of access operator instead of BindBase::Get function
            virtual GameEntity* operator->() const;

            /// \brief Removes all binded items from cash, and adds 'item'.
            virtual void set(GameEntity* item);

            /// \brief Cleans cash of binded items.
            virtual void reset();

            virtual bool isMultiBind() const;

        protected:

            virtual void resetInternal();
            virtual bool setItem(GameEntity* item, bool internal);

            virtual InitState onInitGotStuck() override;
            virtual InitState onInit() override;
            virtual void onPostInit() override;
            virtual void onDeinit() override;

            std::list<GameEntity*> mFoundBindings;
            std::list<GameEntity*> mSettedBindings;
    };

    template<typename T>
    class MultiBind: public MultiBindBase
    {
        public:
            THE_ENTITY(MultiBind<T>, MultiBindBase)

            MultiBind()
            {
            }

            virtual ~MultiBind()
            {
            }

            std::list<T*> getBindsT() const
            {
                std::list<GameEntity*> items = MultiBindBase::getBinds();
                std::list<T*> result;
                for (auto item : items)
                {
                    if (core()->rtti()->is(type(item), type<T>()))
                    {
                        /// \todo Override setItem, and make cast once
                        result.push_back(dynamic_cast<T*>(item));
                    }
                    else
                    {
                        CRITICAL("Invalid binded item type. Needs '%s', got '%s'",
                            type<T>().fullName, type(item).fullName);
                    }
                }
                return result;
            }

            virtual bool find()
            {
                core()->rtti()->registerType(type<T>());

                return MultiBindBase::find();
            }

        protected:

            virtual bool setItem(GameEntity* item, bool internal) override
            {
                if (!isInGameTree() || item == nullptr || core()->rtti()->is(type(item), type<T>())) return MultiBindBase::setItem(item, internal);
                else return false;
            }
    };

   template<typename T>
   class ObservingMultiBind: public MultiBind<T>
   {
       public:
           THE_ENTITY(ObservingMultiBind<T>, MultiBind<T>)

           Signal<void(T*)> SigItemAdded;

           ObservingMultiBind()
           {
           }

           virtual ~ObservingMultiBind()
           {
               if (mWatcherInit) BindBase::core()->observer()->endWatch(mWatcherInit);
               if (mWatcherDeinit) BindBase::core()->observer()->endWatch(mWatcherDeinit);
           }

           virtual void onPreInit() override
           {
               BindBase::core()->rtti()->registerType(type<T>());
               MultiBindBase::onPreInit();
           }

           virtual void onPostInit() override
           {
               mWatcherInit = BindBase::core()->observer()->beginWatch(
                    [&](GameEntity* ent, WatchAction act){
                        if (this->checkPath(ent)) this->setItem(ent, true); },
                    ENTITY_INITIALIZING);
               mWatcherDeinit = BindBase::core()->observer()->beginWatch(
                    [&](GameEntity* ent, WatchAction act){
                        if (CONTAINS(this->mSettedBindings, ent) || CONTAINS(this->mFoundBindings, ent))
                            this->removeItem(ent);
                    },
                    ENTITY_INITIALIZING);
               MultiBindBase::onPostInit();
           }
       protected:
           WatcherHandle mWatcherInit;
           WatcherHandle mWatcherDeinit;

           virtual bool setItem(GameEntity* item, bool internal) override
           {
               int was = this->mFoundBindings.size();
               bool ret = MultiBind<T>::setItem(item, internal);
               if ((int)this->mFoundBindings.size() > was)
               {
                  if (!this->isInGameTree() || item == nullptr || this->core()->rtti()->is(type(item), type<T>()))
                  {
                      SigItemAdded(static_cast<T*>(item));
                  }
               }
               return ret;
           }
   };

} // namespace the
////////////////////////////////////////////////////////////
