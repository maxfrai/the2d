////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
// #include "Config.hpp"
#include <The2D/AttachedBase.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/Attached.hpp>
#include <The2D/ChunkVector.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    template<typename ParentT, typename TargetT, typename T>
    class StaticAttached: public AttachedBase
    {
        public:
            StaticAttached(): AttachedBase(type<TargetT>().fullName, true),
                mDefaultValue(new T()), mOwnDefaultValueObj(true)
            {
            }
            StaticAttached(const T* defaultValue): AttachedBase(type<TargetT>().fullName, true),
                mDefaultValue(defaultValue), mOwnDefaultValueObj(false)
            {
            }
            virtual ~StaticAttached()
            {
                if (mSource.get() != nullptr && mSource.use_count() == 1)
                {
                    mSource->resetParent();
                }
                if (mOwnDefaultValueObj)
                {
                    delete mDefaultValue;
                }
            }

            inline const T& operator[](TargetT* ent) const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                return mSource->operator[](ent);
            }

            inline const T& getValue(TargetT* ent) const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                return mSource->getValue(ent);
            }

            inline bool hasValue(TargetT* ent) const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                return mSource->hasValue(ent);
            }

            inline void setValue(TargetT* ent, const T& val)
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                mSource->setValue(ent, val);
            }

            inline T& beginEdit(TargetT* ent)
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                return mSource->beginEdit(ent);
            }

            inline void endEdit(TargetT* ent)
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                mSource->endEdit(ent);
            }
            
            inline void reset(ISupportAttachedInternal* ent)
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");
                
                mSource->reset(ent);                
            }

            /// \brief Get all objects, where that property has a value, different from default.
            sptr<eastl::vector<ISupportAttachedInternal*>> getAttachiers() const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                return mSource->getAttachiers();
            }

            sptr<eastl::vector<T>> getValues() const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                return mSource->getValues();
            }

            /// \brief Free memory for not used cells.
            /// Complexity is O(getValuesCount() + getFreeCellsCount()) in the worst case
            /// and O(getFreeCellsCount()) in the best case.
            void defragAndShrink()
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                mSource->defragAndShrink();
            }

            
            void getFeeCellsCountToStartDefrag() const
            {
                if (mSource.get() != nullptr)
                {
                    return mSource->getFeeCellsCountToStartDefrag();
                }
                else
                {
                    return mFreeCellsCountToStartDefrag;
                }
            }

            int getFreeCellsCount() const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                return mSource->getFreeCellsCount();
            }

            /// \brief Get count of attachiers, that has not default values of this property.
            /// Complexity is O(1).
            /// \see getAttachiers
            int getValuesCount() const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                return mSource->getValuesCount();                
            }

            size_t getUsedMemory() const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                return mSource->getUsedMemory() + sizeof(StaticAttached<ParentT, TargetT, T>);
            }

            size_t getWastedMemory() const
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                return mSource->getWastedMemory();
            }

            const T& getDefaultValue() const
            {
                if (mSource.get() != nullptr)
                {
                    return mSource->getDefaultValue();
                }
                else
                {
                    return *mDefaultValue;
                }
            }


            void resetAll(bool shrinkAttachiers = false) override
            {
                ASSERT(mSource.get() != nullptr, "Attached property must be initialized before first access");

                mSource->resetAll(shrinkAttachiers);
            }

             // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline StaticAttached<ParentT, TargetT, T>& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline StaticAttached<ParentT, TargetT, T>& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline StaticAttached<ParentT, TargetT, T>& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

            /// \brief If count of free cells will become more the `count`, `defragAndShrink` will be called.
            StaticAttached<ParentT, TargetT, T>& setFeeCellsCountToStartDefrag(int count)
            {
                mFreeCellsCountToStartDefrag = count;
                if (mSource.get() != nullptr)
                {
                    mSource->setFeeCellsCountToStartDefrag(count);
                }
                return *this;
            }

            StaticAttached<ParentT, TargetT, T>& defval(const T& val)
            {
                setDefaultValue(val);
                return *this;
            }

            StaticAttached<ParentT, TargetT, T>& setDefaultValue(const T& val)
            {
                if (mOwnDefaultValueObj)
                {
                    delete mDefaultValue;
                }
                mDefaultValue = new T(val);
                if (mSource.get() != nullptr)
                {
                    mSource->setDefaultValue(val);
                }
                return *this;
            }
            StaticAttached<ParentT, TargetT, T>& setDefaultValue(const T* val)
            {
                if (mOwnDefaultValueObj)
                {
                    delete mDefaultValue;
                    mOwnDefaultValueObj = false;
                }
                mDefaultValue = val;
                if (mSource.get() != nullptr)
                {
                    mSource->setDefaultValue(val);
                }
                return *this;
            }
            // ------------------------------------------------------------

            virtual sptr<AttachedBase> getSource() override
            {
                return mSource;
            }
            virtual void setSource(sptr<AttachedBase> source) override
            {
                mSource = std::static_pointer_cast<AttachedProperty<TargetT, T>>(source);
                setID(mSource->getID(), mSource->getClassID());
            }

            virtual void createSource() override
            {
                mSource = sptr<AttachedProperty<TargetT, T>>(
                            new AttachedProperty<TargetT, T>());
                mSource->setDefaultValue(mDefaultValue);
                mSource->setFeeCellsCountToStartDefrag(mFreeCellsCountToStartDefrag);                
                core()->tree()->registerChild(mSource.get(), "StaticAttachedSource");
                setID(mSource->getID(), mSource->getClassID());
            }

            virtual string getStaticAttachedParentType() override
            {
                return type<ParentT>().fullName;
            }

            virtual string getValueType() override
            {
                return typeid(T).name();
            }

        protected:
            sptr<AttachedProperty<TargetT, T>> mSource;
            int mFreeCellsCountToStartDefrag;
            const T* mDefaultValue;
            bool mOwnDefaultValueObj;

    };
} // namespace the
////////////////////////////////////////////////////////////
