////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/RttiSupport.hpp>

#include <functional>
#include <map>
#include <list>

////////////////////////////////////////////////////////////
namespace the
{
    class Message;
    class GameEntity;

    typedef std::pair<GameEntity*, std::function<void (Message*)>> Subscriber;

    /// \class MessageCore MessageCore "MessageCore.hpp"
    /// \ingroup Core
    /// \brief Tool for global message exchange.
    class MessageCore : private the::NonCopyable<MessageCore>
    {
        public:

            static const the::uint8 ID = MessageCoreID;

            /// \brief Data for MessageCore::MSG_SUBSCRIBED and MessageCore::MSG_UNSUBSCRIBED.
            struct SubscriberInfo
            {
                uint32 msgType;
                GameEntity* subscriber;
                std::function<void (Message*)> callback;
                SubscriberInfo(uint32 msg, GameEntity* subscr, std::function<void (Message*)> callb)
                        : msgType(msg), subscriber(subscr), callback(callb)
                {
                }
            };

            /// \brief Broadcasts whenever someone subscribed to some message type.
            /// Type: MessageSendData<SubscriberInfo>
            ENGINE_MSG(MSG_SUBSCRIBED);

            /// \brief Broadcasts whenever someone unsubscribed from some message type.
            /// Type: MessageSendData<SubscriberInfo>
            ENGINE_MSG(MSG_UNSUBSCRIBED);

            MessageCore();
            virtual ~MessageCore();

            /// \brief Register callback for all broadcsts of specified message type.
            /// \param msgType Type of message, what you want to handle.
            /// \param callback Function or method which process message.
            /// \see http://en.cppreference.com/w/cpp/utility/functional/function
            /// \param subscriber Game entity, which doing subscription.
            void subscribe(uint32 msgType, GameEntity* subscriber, std::function<void (Message*)> callback);
            /// \brief Cancel subscription to message.
            /// \param msgType Type of message, to what you had subscribed earlier.
            /// \param subscriber Game entity, that had done subscription.
            void unsubscribe(GameEntity* subscriber, uint32 msgType);
            /// \brief Send message to all subscribers.
            /// \param msg Message to send.
            /// \return Count of subscribers to this type of message.
            uint16 broadcast(Message* msg) ;
            /// \brief Init internal structures.
            void init();
            /// \bnrief Get list of GameEntity, which are listen specified type of message.
            /// \param msgType Type of message, to get subscribers for.
            /// \return Subscribers for specified type of message.
            std::list<Subscriber> getSubscribers(uint32 msgType) ;

            /// \brief Get first 16 bits of msgType.
            static inline uint16 getLibraryId(uint32 msgType)
            {
                return static_cast<uint16>(msgType >> 16);
            }
            /// \brief Get first 16-24 bits of msgType.
            static inline uint8 getOwnerId(uint32 msgType)
            {
                return static_cast<uint8>((msgType & 0x0000FF00) >> 8);
            }
            /// \brief Get first 24-32 bits of msgType.
            static inline uint8 getMessageId(uint32 msgType)
            {
                return static_cast<uint8>(msgType & 0x000000FF);
            }

        private:
            typedef std::map<uint8, std::list<Subscriber>> MessagesToCallbacks;
            typedef std::map<uint8, MessagesToCallbacks> ComponentsToMessages;
            typedef std::map<uint16, ComponentsToMessages> LibsToComponents;

            LibsToComponents mMap;
    };
}; // namespace the
////////////////////////////////////////////////////////////
