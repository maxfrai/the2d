////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/Utils.hpp>
#include <The2D/ISupportAttachedInternal.hpp>
#include <vector>
#include <iostream>

////////////////////////////////////////////////////////////
namespace the
{
    class IEntityConstructor;

    /// \brief Runtime information about type.
    /// \ingroup Engine
    /// \note To get type info of the type use macro THE_TYPE_OF(<type_name>) or <type_name>::THE_TYPE_INFO.
    /// \note To get type info of the ISupportRtti instance call getTypeInfo().
    /// \note To add RTTI support in your class add macro THE_ENTITY in class declaration and TH_RTTI_IMPLEMENTATION(<type_name>, <base_type_name>) in class implementation.
    /// \note To add RTTI support in template class use macro TH_RTTI_TEMPLATE_IMPLEMENTATION instead of TH_RTTI_IMPLEMENTATION.
    /// \note To add RTTI support for class with multiply inheritance use macro TH_RTTI_IMPLEMENTATION_2 or TH_RTTI_IMPLEMENTATION_3 intead of TH_RTTI_IMPLEMENTATION.
    /// \note To add RTTI support for class that do not inherits from ISupportRtti, use macro THE_ENTITY_NOT_ENTITY and TH_RTTI_IMPLEMENTATION_NOT_ENTITY, TH_RTTI_IMPLEMENTATION_NOT_ENTITY_NO_BASE, TH_RTTI_TEMPLATE_IMPLEMENTATION_NOT_ENTITY or TH_RTTI_TEMPLATE_IMPLEMENTATION_NOT_ENTITY_NO_BASE instead of THE_ENTITY and TH_RTTI_IMPLEMENTATION.
    struct The2DTypeInfo: public ISupportAttachedInternal
    {
        /// \brief Clear name, without namespace and template parameters.
        const the::string name;
        /// \brief Return value the::getHashCode(name).
        const HashCode nameHash;
        /// \brief Return value of type_info::name().
        const the::string fullName;
        /// \brief Internal id of the type inside the The2D infrastructure.
        mutable uint16 engineID;
        /// \brief Return value the::getHashCode(fullName).
        const HashCode fullNameHash;
        /// \brief Size of type instance in bytes.
        /// \note Result of applying `sizeof` operator.
        const size_t size;
        /// \brief Tool for dynamicly creating instances of the type.
        the::IEntityConstructor* const constructor;
        /// \brief List of the immediate base types.
        const std::vector<const The2DTypeInfo*> baseTypes;
        /// \brief List of derived types.
        /// Field fills during registration of type in RttiService.
        mutable std::vector<const The2DTypeInfo*> derivedTypes;

        /// \brief Uses fullNameHash and fullName to compare types.
        bool equalTo(const The2DTypeInfo& type) const;

        /// \brief Uses equalTo method.
        bool operator==(const The2DTypeInfo& type) const;

        /// \brief Uses equalTo method.
        bool operator!=(const The2DTypeInfo& type) const;

        /// \brief Determine, do this type inherits from specified type.
        /// \param type Possibly parent of the type.
        /// \return Do this type inherits from specified type.
        /// \note If *this == type, then method returns true.
        /// \note Method uses `operator==`.
        /// Method iterates through all parents in inheritance tree and looks for specified type.
        bool is(const The2DTypeInfo& type) const;

        template<typename T>
        bool is() const
        {
            return is(T::EntityType::mTypeInfo);
        }

        The2DTypeInfo(the::string Name,
            the::string FullName,
            uint16 EngineID,
            size_t Size,
            the::IEntityConstructor* Constructor,
            std::vector<const The2DTypeInfo*>  BaseTypes);

        virtual ~The2DTypeInfo();
    };

    extern const The2DTypeInfo The2DTypeInfoTypeInfo;



    /// \brief Returns The2DTypeInfo object for specified type.
    /// \note Type must contains macro THE_ENTITY at declaration and macro  in implementation.
    /// \example
    ///     ISupportRtti* entity = core()->rtti()->create(strName);
    ///     if (type(entity) == type<the::FixtureComponent>())
    ///     {
    ///         the::FixtureComponent* fixture = static_cast<the::FixtureComponent*>(entity);
    ///     }
    template<typename T>
    inline const The2DTypeInfo& type()
    {
        return T::EntityType::mTypeInfo;
    }

    template<>
    inline const The2DTypeInfo& type<The2DTypeInfo>()
    {
        return The2DTypeInfoTypeInfo;
    }

    template<typename T>
    inline const The2DTypeInfo& type(T* ent)
    {
        return ent->getTypeInfo();
    }

    const the::string RemoveNamespaceAndTemplateSpec(the::string str);

     class EntityInfoBase
    {
      public:
        virtual ~EntityInfoBase() {}
        virtual const the::The2DTypeInfo& getTypeInfo() = 0;
    };


    class ISupportRtti
    {
        public:
            virtual ~ISupportRtti() {}
            /// To implement declare THE_ENTITY(type, basetype) macro
            virtual EntityInfoBase* getEntity() = 0;
            /// To implement declare THE_ENTITY(type, basetype) macro
            virtual const the::The2DTypeInfo& getTypeInfo() const = 0;
    };

    /// \brief Interface for creating instances of classes derived from `the::GameEntity`.
    /// \ingroup Engine
    /// For internal use in RTTI realisation.
    class IEntityConstructor
    {
        public:
            /// \brief Create instance.
            /// \note Memory managment lays on the caller.
            virtual the::ISupportRtti* create() { return nullptr; }

            virtual ~IEntityConstructor() {}
    };

    template<typename T>
    class EntityConstructor: public the::IEntityConstructor
    {
        public:
          virtual the::ISupportRtti* create() { return new T(); }
    };

    /////////////////////////////////////////////////////////////////////////////////
    //// Macro
    ////////////////////////////////////////////////////////////////////////////////


    /// \brief Replacement for symbol ',', to use in macro.
    /// Macro interpretates comma as splitter of its parameters. So, if you want pass expression like MyTemplateClass<T1,T2> as one parameter,
    /// you should use COMMA instead of ',' between T1 and T2.
    /// \example
    ///
    ///     THE_RTTI_TEMPLATE_IMPLEMENTATION(template<typename T1 COMMA typename T2>, MyTemplateClass<T1 COMMA T2>, ItsBaseClass<T1 COMMA T2>)
    #define COMMA ,

    
    template<typename ...Stub>
    class EntityInfo
    {
    };

    template<typename ...T>
    class BaseList
    {
    };

    template<typename T, typename ...Bases>
    class EntityInfo<T, BaseList<Bases...>>: public EntityInfoBase
    {
      public:
        typedef T Type;

        static const the::The2DTypeInfo mTypeInfo;

        virtual const the::The2DTypeInfo& getTypeInfo()
        {
            return mTypeInfo;
        }
    };

    // Building type vector via variadic templateSpec

    class __MakeTypeInfoVectorStub
    {
        public:
            static constexpr const char*  getNativeTypeName() { return "__MakeTypeInfoVectorStub"; }
            typedef EntityInfo<__MakeTypeInfoVectorStub, BaseList<>> EntityType;
            typedef IEntityConstructor Constructor;
            EntityType mEntity; 
            virtual EntityInfoBase* getEntity(){ return &mEntity; }
            virtual const the::The2DTypeInfo& getTypeInfo(){ return EntityType::mTypeInfo; }
    };

    /// \brief Used in theStartMakeTypeInfoVector function.
    template <typename T>
    std::vector<const the::The2DTypeInfo*> theMakeTypeInfoVector(std::vector<const the::The2DTypeInfo*> vect)
    {
        // Never will be there
        return vect;
    }
    /// \brief Used in theStartMakeTypeInfoVector function.
    template <typename T, typename R, typename ...P>
    std::vector<const the::The2DTypeInfo*> theMakeTypeInfoVector(std::vector<const the::The2DTypeInfo*> vect)
    {
        if(sizeof...(P))
        {
            vect.push_back(&the::type<T>());
            return theMakeTypeInfoVector<R, P...>(vect);
        }
        else
        {
            if(vect.size() == 0)
            {
                // Empty types list case
                return vect;
            }
            else if(vect.size() == 1)
            {
                // One item in type list case
                vect.clear();
                vect.push_back(&the::type<R>());
                return vect;
            }
            else
            {
                // Two and more items in type list case
                vect.erase(vect.begin());
                vect.erase(vect.begin());

                vect.push_back(&the::type<T>());
                vect.push_back(&the::type<R>());
                return vect;
            }
        }
    }
    /// \brief For internal use. Builds std::vector<const the::The2DTypeInfo*> from list of template arguments.
    template<typename ...P>
    std::vector<const the::The2DTypeInfo*> theStartMakeTypeInfoVector()
    {
        return theMakeTypeInfoVector<__MakeTypeInfoVectorStub, __MakeTypeInfoVectorStub, P...>(std::vector<const the::The2DTypeInfo*>());
    }

    template<typename T, typename ...Bases>
    const the::The2DTypeInfo EntityInfo<T, BaseList<Bases...>>::mTypeInfo(the::RemoveNamespaceAndTemplateSpec(T::getNativeTypeName()), typeid(T).name(), 0,
        sizeof(T), new typename T::Constructor(), theStartMakeTypeInfoVector<Bases...>());


    #define THE_ENTITY(T, Bases...) \
        static constexpr const char*  getNativeTypeName() { return #T; } \
        typedef EntityInfo<T, BaseList<Bases>> EntityType; \
        typedef EntityConstructor<T> Constructor; \
        EntityType mEntity; \
        virtual EntityInfoBase* getEntity() { return &mEntity; } \
        virtual const the::The2DTypeInfo& getTypeInfo() const { return EntityType::mTypeInfo; }

     #define THE_ABSTRACT_ENTITY(T, Bases...) \
        static constexpr const char*  getNativeTypeName() { return #T; } \
        typedef EntityInfo<T, BaseList<Bases>> EntityType; \
        typedef IEntityConstructor Constructor; \
        EntityType mEntity; \
        virtual EntityInfoBase* getEntity(){ return &mEntity; } \
        virtual const the::The2DTypeInfo& getTypeInfo() const{ return EntityType::mTypeInfo; }

}// namespace the

/// \brief Prints the::The2DTypeInfo::fullName.
std::ostream& operator << (std::ostream& s, const the::The2DTypeInfo& d);