////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2012 Max Tyslenko, Pavel Bogatirev (Wincode team), max.tyslenko@gmail.com, pfight77@gmail.com
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#pragma once

#include <The2D/ParsingError.hpp>

class TiXmlElement;

namespace the
{
    /// \class IXmlParser IXmlParser "DParser/IXmlParser.hpp"
    /// \brief Abstract interface, representing some XML-parser.
    /// \note Realized by XmlParser.
    class IXmlParser
    {
        public:
            /// \brief Runs parsing, and returns occurred errors.
            virtual std::list<ParsingError> parse(TiXmlElement* element) = 0;
            /// \brief Check, if `element` is that element, which parser can process.
            virtual bool isMyTag(TiXmlElement *element) = 0;
            /// \brief Read data, and generate XML equivalent.
            virtual TiXmlElement* serialize() = 0;
            /// \brief Prepare internal structures for new parsing.
            virtual void initParser() {};

            virtual bool required() { return false; }
    };
}
