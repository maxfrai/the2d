////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>


////////////////////////////////////////////////////////////

namespace the
{
    class LogService;
    class ConfigManager;
    class GameTree;
    class StateMachine;
    class ScriptManager;
    class MessageCore;
    class GameLoopProcessor;
    class XmlLoader;
    class Rtti;
    class InitService;
    class ObserverService;
    class ResourceService;
    class StatisticService;
    class BindService;
    class AttachedService;

    /**
     * \class Root InfrastructureCore.hpp "InfrastructureCore.hpp"
     * \brief Main class of the engine
     * \ingroup Core
     *
     * This class is a singleton, it stores all the engine managers.
     * It should create them in predefined order, initialize of needed
     * and deinitialize.
     */
    class InfrastructureCore : private NonCopyable<InfrastructureCore>
    {
        public:

            InfrastructureCore();
            ~InfrastructureCore();

            int runGame();
            void exitGame();

            /// \brief Init need managers
            void init();
            /// \brief Deinit need managers
            void deinit();

            ////////////////////////////////////////////////////////////
            // Getter section
            ////////////////////////////////////////////////////////////

            /// \brief Returns LogService instance
            inline LogService* log() { return mLog; }

            void setLog(LogService* log);

            /// \brief Returns ConfigManager instance
            inline ConfigManager* config() { return mConfig; }

            /// \brief Returns GameTree instance
            inline GameTree* tree() { return mTree; }

            /// \brief Returns StateMachine instance
            inline StateMachine* state() { return mState; }

            /// \brief Returns ScriptManager instance
            inline ScriptManager* script() { return mScript; }

            /// \brief Returns MessageCore instance
            inline the::MessageCore* msg() { return mMessages; }

            /// \brief Returns instance of GameLoopProcessor, that manage game loop.
            inline GameLoopProcessor* loop() { return mLoopProcessor; }

            inline XmlLoader* xml() { return mXmlLoader; }

            inline Rtti* rtti() { return mRtti; }

            inline InitService* initService() { return mInitService; }

            inline ObserverService* observer() { return mObserver; }

            inline ResourceService* resource() { return mResource; }

            inline StatisticService* stats() { return mStatistics; }

            inline BindService* bind() { return mBind; }

            inline AttachedService* attached() { return mAttached; }


        private:

            bool mInitialized;                    ///< Indicates whether managers are already initialized

            LogService         *mLog;
            ConfigManager      *mConfig;
            GameTree           *mTree;
            StateMachine       *mState;
            ScriptManager      *mScript;
            MessageCore        *mMessages;
            GameLoopProcessor  *mLoopProcessor;
            XmlLoader          *mXmlLoader;
            Rtti               *mRtti;
            InitService        *mInitService;
            ObserverService    *mObserver;
            ResourceService    *mResource;
            StatisticService   *mStatistics;
            BindService        *mBind;
            AttachedService    *mAttached;
    };
}; // namespace the
////////////////////////////////////////////////////////////
