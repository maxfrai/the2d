////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class GameEntity;

    /// \class Message Message "Message.hpp"
    /// \brief Base class for messages. Can be used itself.
    /// \ingroup Core
    class Message : private the::NonCopyable<Message>
    {
        public:

            /// \brief Method of distribution of the message.
            enum TrasferDirrection
            {
                /// \brief Every node will send it to parent.
                UP,
                /// \brief Every node will send it to the all children.
                DOWN,
                /// \brief Every node in the tree, who subscribed to it. For sending use MessageManager.
                MESSAGE_CORE
            };

            /// \brief Message ID.
            const uint32 mType;
            /// \brief Method of distribution of the message.
            const TrasferDirrection mDirrection;
            /// \brief Node, that have send the message.
            const GameEntity* mSender;
            bool mProcessed; ///< \brief Indicates, do transfer message further along the tree. True means stop transfer.

            /// \param type Message ID (for example GameEntity::MSG_ITEM_ADDED_TO_TREE).
            /// \param dir Method of distribution of the message.
            /// \param sender Node, that have send the message.
            Message(const uint32 type, const TrasferDirrection dir, const GameEntity *sender)
                : mType(type), mDirrection(dir), mSender(sender)
            { mProcessed = false; }
            virtual ~Message()
            {}
    };

    /// \class MessageReceiveData MessageReceiveData "Message.hpp"
    /// \brief Message, destinate to request data from another node of the tree.
    template<typename T>
    class MessageReceiveData: public Message
    {
        public:
            /// \brief Reference to variable, where to place requested value.
            T& mVariable;

            /// \param type Message ID (for example GameEntity::MSG_ITEM_ADDED_TO_TREE).
            /// \param dir Method of distribution of the message.
            /// \param sender Node, that have send the message.
            /// \param var Reference to variable, where to place requested value.
            MessageReceiveData(T& var, const uint32 type, const TrasferDirrection dir, const GameEntity *sender)
                : Message(type, dir, sender), mVariable(var)
            { }
    };


    /// \class MessageSendData MessageSendData "Message.hpp"
    /// \brief Message with attached data.
    template<typename T>
    class MessageSendData: public Message
    {
        public:
            /// \brief Attached data.
            T mData;

            /// \param type Message ID (for example GameEntity::MSG_ITEM_ADDED_TO_TREE).
            /// \param dir Method of distribution of the message.
            /// \param sender Node, that have send the message.
            /// \param var Attached data.
            MessageSendData(T data, const uint32 type, const TrasferDirrection dir, const GameEntity *sender)
                : Message(type, dir, sender), mData(data)
            {  }

    };

    /// \class MessageSendReceiveData MessageSendReceiveData "Message.hpp"
    /// \brief Message with attached data, destinate to request some value from another node.
    template<typename S, typename R>
    class MessageSendReceiveData: public Message
    {
        public:
            /// \brief Reference to variable, where to place requested value.
            R& mReceiveVariable;
            /// \brief Attached data.
            S mSendData;

            /// \param type Message ID (for example GameEntity::MSG_ITEM_ADDED_TO_TREE).
            /// \param dir Method of distribution of the message.
            /// \param sender Node, that have send the message.
            /// \param send Attached data.
            /// \param receive Reference to variable, where to place requested value.
            MessageSendReceiveData(S send, R& receive, const uint32 type, const TrasferDirrection dir, const GameEntity *sender)
                : Message(type, dir, sender), mReceiveVariable(receive), mSendData(send)
            {  }

    };

} // namespace the
////////////////////////////////////////////////////////////
