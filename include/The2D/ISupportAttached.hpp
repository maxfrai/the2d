////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/ISupportAttachedInternal.hpp>
#include <The2D/RttiSupport.hpp>

////////////////////////////////////////////////////////////
namespace the {

    /// \brief Objects of classes, derived from this class can have attached properties.
    class ISupportAttached: public ISupportAttachedInternal, public ISupportRtti
    {
        public:
            THE_ENTITY(ISupportAttached);
    };

}; // namespace the



////////////////////////////////////////////////////////////
