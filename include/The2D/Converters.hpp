////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Connectors.hpp>
#include <The2D/Attached.hpp>

////////////////////////////////////////////////////////////
namespace the {

    template<typename SourceT, typename TargetT>
    class ValueConversion: public IValueConversion<TargetT>
    {
        public:
            THE_ENTITY(ValueConversion<SourceT COMMA TargetT>, IValueConversion<TargetT>);

            typedef ValueConversion<SourceT, TargetT> ThisType;
            typedef DataConnector<SourceT> SourceConnectorT;
            typedef DataConnector<TargetT> TargetConnectorT;

            bool canConvert(Connector* from, Connector* to, string* errMsg) override
            {
                bool fwd_sourceTypeOk = this->core()->rtti()->is(type(from), type<SourceConnectorT>());
                bool fwd_targetTypeOk = this->core()->rtti()->is(type(to), type<TargetConnectorT>());
                bool fwd_convOk = mGetConversion;
                bool bk_sourceTypeOk = this->core()->rtti()->is(type(from), type<TargetConnectorT>());
                bool bk_targetTypeOk = this->core()->rtti()->is(type(to), type<SourceConnectorT>());
                bool bk_convOk = mSetConversion;
                if (fwd_sourceTypeOk && fwd_targetTypeOk && fwd_convOk) return true;
                else if (bk_sourceTypeOk && bk_targetTypeOk && bk_convOk) return true;
                else if (errMsg != nullptr)
                {
                    // If suppose, that caller meant forward conversion, because some types math
                    if (fwd_sourceTypeOk || fwd_targetTypeOk)
                    {
                        if (!fwd_sourceTypeOk)
                        {
                            *errMsg += format("wrong source type for forward conversion (excepted '%s', got '%s'); ",
                                        type<SourceConnectorT>().fullName, type(from).fullName);
                        }
                        if (!fwd_targetTypeOk)
                        {
                            *errMsg += format("wrong target type for forward conversion (excepted '%s', got '%s'); ",
                                        type<TargetConnectorT>().fullName, type(to).fullName);
                        }
                        if (!fwd_convOk)
                        {
                            *errMsg += format("forward conversion not configured (use setForwardConv method);");
                        }
                    }
                    if (bk_sourceTypeOk || bk_targetTypeOk)
                    {
                        if (!bk_sourceTypeOk)
                        {
                            *errMsg += format("wrong source type for backward conversion (excepted '%s', got '%s'); ",
                                        type<SourceConnectorT>().fullName, type(from).fullName);
                        }
                        if (!bk_targetTypeOk)
                        {
                            *errMsg += format("wrong target type for backward conversion (excepted '%s', got '%s'); ",
                                        type<TargetConnectorT>().fullName, type(to).fullName);
                        }
                        if (!bk_convOk)
                        {
                            *errMsg += format("backward conversion not configured (use setBackwardConv method);");
                        }
                    }
                    if (!fwd_sourceTypeOk && !fwd_targetTypeOk && !bk_sourceTypeOk && !bk_targetTypeOk)
                    {
                        *errMsg = format("wrong source and target types for both, forward and backward"
                            " conversions (excepted source type '%s' and target type '%s', got '%s' and '%s')",
                            type<SourceConnectorT>().fullName, type<TargetConnectorT>().fullName,
                            type(from).fullName, type(to).fullName);
                    }                    
                }
                return false;
            }

            void getFromSource(IConversionConnection* con, TargetT& result) override
            {
                result = mGetConversion(static_cast<SourceConnectorT*>(con->getSource())->get());
            }

            void setToSource(IConversionConnection* con, const TargetT& value) override
            {
                static_cast<SourceConnectorT*>(con->getSource())->set(mSetConversion(value));
            }

            // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline ThisType& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline ThisType& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline ThisType& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }


            /// \brief Set function, that realize conversion logic.
            ThisType& setForwardConv(std::function<TargetT(const SourceT&)> conversion)
            {
                mGetConversion = conversion;
                return *this;
            }
            /// \brief Set function, that realize conversion logic.
            ThisType& setBackwardConv(std::function<SourceT(const TargetT&)> conversion)
            {
                mSetConversion = conversion;
                return *this;
            }
            // --------------------------------------------------------------

        protected:
            std::function<TargetT(const SourceT&)> mGetConversion;
            std::function<SourceT(const TargetT&)> mSetConversion;
    };

    template<typename SourceConnectorT, typename TargetConnectorT>
    class LargeValueConversion: public IValueConversion<typename TargetConnectorT::ValueType>
    {
        class LargeValueConversionConnection: public IConversionConnection
        {
            public:
                LargeValueConversionConnection(IConversion* conv, Connector* from, Connector* to)
                    : IConversionConnection(conv, from, to), mSourceValueChanged(true)
                {
                }

                bool mSourceValueChanged;
        };

        public:

            typedef LargeValueConversion<SourceConnectorT, TargetConnectorT> ThisType;
            typedef typename SourceConnectorT::ValueType SourceT;
            typedef typename TargetConnectorT::ValueType TargetT;
            typedef IValueConversion<TargetT> BaseType;

            THE_ENTITY(LargeValueConversion<SourceConnectorT COMMA TargetConnectorT>, IValueConversion<TargetT>);


            bool canConvert(Connector* from, Connector* to, string* errMsg = nullptr) override
            {
                bool fwd_sourceTypeOk = this->core()->rtti()->is(type(from), type<SourceConnectorT>());
                bool fwd_targetTypeOk = this->core()->rtti()->is(type(to), type<TargetConnectorT>());
                bool fwd_convOk = mGetConversion;
                bool bk_sourceTypeOk = this->core()->rtti()->is(type(from), type<TargetConnectorT>());
                bool bk_targetTypeOk = this->core()->rtti()->is(type(to), type<SourceConnectorT>());
                bool bk_convOk = mSetConversion;
                if (fwd_sourceTypeOk && fwd_targetTypeOk && fwd_convOk) return true;
                else if (bk_sourceTypeOk && bk_targetTypeOk && bk_convOk) return true;
                else if (errMsg != nullptr)
                {
                    // If suppose, that caller meant forward conversion, because some types math
                    if (fwd_sourceTypeOk || fwd_targetTypeOk)
                    {
                        if (!fwd_sourceTypeOk)
                        {
                            *errMsg += format("wrong source type for forward conversion (excepted '%s', got '%s'); ",
                                        type<SourceConnectorT>().fullName, type(from).fullName);
                        }
                        if (!fwd_targetTypeOk)
                        {
                            *errMsg += format("wrong target type for forward conversion (excepted '%s', got '%s'); ",
                                        type<TargetConnectorT>().fullName, type(to).fullName);
                        }
                        if (!fwd_convOk)
                        {
                            *errMsg += format("forward conversion not configured (use setForwardConv method);");
                        }
                    }
                    if (bk_sourceTypeOk || bk_targetTypeOk)
                    {
                        if (!bk_sourceTypeOk)
                        {
                            *errMsg += format("wrong source type for backward conversion (excepted '%s', got '%s'); ",
                                        type<SourceConnectorT>().fullName, type(from).fullName);
                        }
                        if (!bk_targetTypeOk)
                        {
                            *errMsg += format("wrong target type for backward conversion (excepted '%s', got '%s'); ",
                                        type<TargetConnectorT>().fullName, type(to).fullName);
                        }
                        if (!bk_convOk)
                        {
                            *errMsg += format("backward conversion not configured (use setBackwardConv method);");
                        }
                    }
                    if (!fwd_sourceTypeOk && !fwd_targetTypeOk && !bk_sourceTypeOk && !bk_targetTypeOk)
                    {
                        *errMsg = format("wrong source and target types for both, forward and backward"
                            " conversions (excepted source type '%s' and target type '%s', got '%s' and '%s')",
                            type<SourceConnectorT>().fullName, type<TargetConnectorT>().fullName,
                            type(from).fullName, type(to).fullName);
                    }                    
                }
                return false;
            }

            virtual IConversionConnection* connect(Connector* from, Connector* to)
            {
                ASSERT(canConvert(from, to), "Unsupported source or target");

                auto con = new LargeValueConversionConnection(this, from, to);
                SourceConnectorT* source = static_cast<SourceConnectorT*>(from);
                source->SigValueChanged.add(con, [this, con]{
                    con->mSourceValueChanged = true;
                });
                return con;
            }
            virtual void disconnect(IConversionConnection* con)
            {
                SourceConnectorT* source = static_cast<SourceConnectorT*>(con->getSource());
                source->SigValueChanged.remove(con);
                delete con;
            }

            void getFromSource(IConversionConnection* pcon, TargetT& result) override
            {
                ASSERT((bool)mGetConversion, "Setter conversion is not supported.");

                // Target will request value only via converter, so we convert only when needed
                LargeValueConversionConnection* con = static_cast<LargeValueConversionConnection*>(pcon);
                SourceConnectorT* source = static_cast<SourceConnectorT*>(con->getSource());
                if (con->mSourceValueChanged)
                {
                    mGetConversion(source, result);
                    con->mSourceValueChanged = false;
                }
                else
                {
                    // Already converted earlier
                }
            }

            void setToSource(IConversionConnection* pcon, const TargetT& value) override
            {
                ASSERT((bool)mSetConversion, "Setter conversion is not supported.");

                // Access to source we cant manage, so convert at every set
                LargeValueConversionConnection* con = static_cast<LargeValueConversionConnection*>(pcon);
                SourceConnectorT* source = static_cast<SourceConnectorT*>(con->getSource());
                mSetConversion(source, value);
                con->mSourceValueChanged = false;
            }

            // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline ThisType& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline ThisType& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline ThisType& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }


            /// \brief Set function, that realize conversion logic.
            ThisType& setForwardConv(std::function<void(SourceConnectorT* getFrom, TargetT& setTo)> conversion)
            {
                mGetConversion = conversion;
                return *this;
            }
            /// \brief Set function, that realize conversion logic.
            ThisType& setBackwardConv(std::function<void(SourceConnectorT* setTo, const TargetT& value)> conversion)
            {
                mSetConversion = conversion;
                return *this;
            }
            // --------------------------------------------------------------

        protected:
            std::function<void(SourceConnectorT*, TargetT&)> mGetConversion;
            std::function<void(SourceConnectorT*, const TargetT&)> mSetConversion;
    };


    template<typename TargetT, typename SourceT>
    class FuncConversion;

    template<typename R, typename ...TargetArgs, typename ...SourceArgs>
    class FuncConversion<R(TargetArgs...), R(SourceArgs...)> : public IFuncConversion<R(TargetArgs...)>
    {
      public:
        THE_ENTITY(FuncConversion<R(TargetArgs...) COMMA R(SourceArgs...)>, IFuncConversion<R(TargetArgs...)>);

        typedef FuncConversion<R(TargetArgs...), R(SourceArgs...)> ThisType;
        typedef CFuncBase<R(SourceArgs...)> SourceConnectorT;
        typedef CFuncBase<R(TargetArgs...)> TargetConnectorT;

        bool canConvert(Connector* from, Connector* to, string* errMsg = nullptr) override
        {
            bool sourceTypeOk = this->core()->rtti()->is(type(from), type<SourceConnectorT>());
            bool targetTypeOk = this->core()->rtti()->is(type(to), type<TargetConnectorT>());
            bool convOk = mConversion;
            if (sourceTypeOk && targetTypeOk && convOk) return true;
            else if (errMsg != nullptr)
            {
                if (!sourceTypeOk)
                {
                    *errMsg += format("wrong source type (excepted '%s', got '%s'); ",
                                type<SourceConnectorT>().fullName, type(from).fullName);
                }
                if (!targetTypeOk)
                {
                    *errMsg += format("wrong target type (excepted '%s', got '%s'); ",
                                type<TargetConnectorT>().fullName, type(to).fullName);
                }
                if (!convOk)
                {
                    *errMsg += format("conversion not configured (use setConv method);");
                }
                return false;
            }
        }

        R callSource(IConversionConnection* con, TargetArgs ... args) override
        {
            return mConversion(static_cast<SourceConnectorT*>(con->getSource()), args...);
        }

        // Fluent support -----------------------------------------------
        /// \brief Fluent overload of GameEntity::setInitOrder
        inline ThisType& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
        /// \brief Fluent overload of GameEntity::setVisibility
        inline ThisType& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
        /// \brief Fluent overload of GameEntity::setXmlTagName
        inline ThisType& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

        /// \brief Set function, that realize conversion logic.
        ThisType& setConv(std::function<R(SourceConnectorT*, TargetArgs...)> conversion)
        {
            mConversion = conversion;
            return *this;
        }
        // --------------------------------------------------------------

        protected:
            std::function<R(SourceConnectorT*, TargetArgs...)> mConversion;
    };

}; // namespace the



////////////////////////////////////////////////////////////