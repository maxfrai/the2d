////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/Utils/ConfigManager.hpp>
#include <The2D/LogService.hpp>
#include <The2D/IXmlParser.hpp>
#include <The2D/RttiSupport.hpp>
#include <The2D/Parameters.hpp>
#include <The2D/InitService.hpp>
#include <The2D/GameLoopProcessor.hpp>
#include <The2D/ISupportAttached.hpp>

#include <functional>
#include <vector>
#include <EASTL/vector.h>



////////////////////////////////////////////////////////////
namespace the
{
    class BindBase;
    class Message;

    enum InitOrder
    {
        NOT_SET=0,
        BEFORE_PARENT,
        AFTER_PARENT,
        BY_PARENT,
        AUTO,
        NEVER
    };

    enum Visibility
    {
        PUBLIC,
        PRIVATE
    };

    enum VistorBehaviour
    {
        /// \brief Go to the children of the current entity.
        CONTINUE,
        /// \brief Skip children of the current entity, and move to the next sibling.
        SKIP_CURRENT,
        /// \brief Stop visit, but call leave functions for all entered entities.
        STOP_VISIT,
        /// \brief Stop visit, without calling leave functions.
        STOP_VISIT_NO_LEAVE
    };

    struct InitState
    {
        bool initialized;
        the::string comment;

        InitState(bool _success, string _comment = "");

        /// \brief Returns copy of this object, with specified comment.
        /// \note Needed for nice syntax of using global object NOT_READY.
        ///       For example return NOT_READY("Invalid value %i of prmMode", val);
        template<typename... Args>
        InitState operator()(const char* commentFmt, const Args&... args)
        {
            this->comment = format(commentFmt, args...);
            return *this;
        }
        template<typename... Args>
        InitState operator()(string commentFmt, const Args&... args)
        {
            this->comment = format(commentFmt.c_str(), args...);
            return *this;
        }
        /// \brief Compares only 'initialized' fields.
        /// \note Needed to make possible comparision with READY and NOT_READY.
        inline bool operator==(const InitState& cmp)
        {
            return initialized == cmp.initialized;
        }

        inline operator bool() const
        {
            return initialized;
        }
    };
    extern InitState READY;
    extern InitState NOT_READY;

    typedef int GameEntityID;

    /// \class GameEntity GameEntity "GameEntity.hpp"
	/// \ingroup Core
    /// \brief Abstract interface, represents node in dynamic game tree.
    /// Declares two-way binded game tree, naming, grouping, parsing support,
    /// initialize, RTTI, messaging and game-loop-update interfaces.
    class GameEntity : public LogInterface, private NonCopyable<GameEntity>,
        public LoopSubscriber, public ISupportAttached
    {

        public:
            THE_ENTITY(the::GameEntity, the::LoopSubscriber)


                /// \brief Name of the node. Name should be unique inside the parent.
            PString prmName;
            /// \brief List of groups of the node, separated by semicolon.
            /// Groups may not be unique at all. For example, car may mark gears as group "gear".
            PString prmGroup;
            //PInt prmZIndex;

            Signal<void (GameEntity* child)> SigChildRegistered;
            Signal<void (GameEntity* child)> SigChildUnregistered;
            Signal<void (GameEntity* child)> SigChildInitialized;
            Signal<void (GameEntity* child)> SigChildDeinitialized;
            Signal<void ()> SigParentChanged;
            Signal<void (GameEntity* newParent)> SigParentChanging;


            GameEntity();

            /// \note Destructor GameEntity calls Deinit.
            virtual ~GameEntity();

            /// \brief This is overloaded method from ILogInterface which contains the logic of the logging
            virtual void logLogic(const the::string& formattedLine, LogType type,
                const char* func, const char* file, const char* cmdate, const char* cmtime, int line);


            // Fluent support----------------------------------------------
            /// \brief Describes, how to initialize game tree.
            GameEntity& setInitOrder(InitOrder order);
            GameEntity& setVisibility(Visibility vis);
            GameEntity& setXmlTagName(the::string xmlTag);
            // ------------------------------------------------------------


            /////////////////////////////////////////////////////////////////////////////////////////////
            //// Initialization /////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////

            const GameEntityInitData* getInitData() const;
            GameEntityInitData* getInitData();
            void readInitData(const GameEntityInitData* initData);
            void readInitDataFrom(const GameEntity* src);

            bool isParametersLocked();

            /// \brief Query to save state to parameters (if component supports).
            /// To add support of saving state override onSaveState. In onSaveState parameters
            /// are unlocked.
            void saveState();

            /// \brief Describes, how to initialize game tree.
            InitOrder getInitOrder() const;

            /// \brief  Calls onInit and initializes all children in specified order.
            /// \note In derived class override onInit to specify initialization logic.
            /// This method is not virtual, because each entity can has unexpected
            /// children (attached for expample). Init makes correct initialization of them.
            InitState init();
            /// \brief Valid free all resources by calling OnDeinit.
            /// \note For your deinitialization logic use onDeinit()!
            /// \note Deinitialization going in reverse order of initialization.
            /// \note Destructor GameEntity calls Deinit.
            void deinit();
            /// \brief Indicates, if init() was called, and had return true.
            inline bool isInitialized() const
            {
                return mInitState;
            }
            /// \brief Manually set entity as initialized or not initialized.
            void setInitialized(bool initialized);

            virtual const InitState& getInitState() const;


            /////////////////////////////////////////////////////////////////////////////////////////////
            //// Loop      //////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////


            virtual void update() override;
            virtual void draw() override;
            virtual void setActive(bool active);

            /////////////////////////////////////////////////////////////////////////////////////////////
            //// Messaging //////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////

            static const uint8 ID = 1;

            /// \brief When implemented in derived class, should contains logic, for process supported messages.
            /// \note Transfering messages up and down tree makes in ProcessMessage.
            /// \remarks This realizatin doing nothing.
            virtual void onIncomingMessage(Message* msg);
            /// \brief Support for message travelling up and down the game tree.
            /// \remarks Logic gor processing message contains in OnIncomingMessage.
            /// \note For derived classed, in common case, no need to implement this method.
            virtual void processMessage(Message* msg);

            ////////////////////////////////////////////////////////////////////////////////////////////
            //// Game tree /////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////

            /// \brief Support for constructing game tree.
            virtual void registerChild(GameEntity* p, the::string xmlTagName);
            virtual void registerChild(GameEntity* p);
            virtual void unregisterChild(GameEntity* p, bool deinitialize = true);
            /// \brief Support for constructing game tree.
            virtual void setParent(GameEntity* parent, the::string name, InitOrder order = AUTO);
            virtual void setParent(GameEntity* parent, InitOrder order = AUTO);
            virtual void resetParent(bool deinitialize = true);
            /// \brief Returns objects, components or something another, that owned by this.
            /// \remarks Owned meants, that parent creates, uses and destroys its children.
            const std::list<GameEntity*>& getChildren() const;
            /// \brief Returns owner entity.
            /// \remarks Owned means, that parent creates, uses and destroys its children.
            /// \remarks For Object return value is nullptr (Objects are the topmost in tree).
            GameEntity* getParent() const;
            bool forefatherOf(GameEntity* ent);

            /// \brief Register child, that created init-time or run-time.
            /// Method deinits and removes a child after onDeinit call.
            /// \param init Do initialize child.
            /// \param name Name of the child. It is usefull to set name for debug purposes.
            virtual void manageStateChild(std::shared_ptr<GameEntity> child, the::string group="", the::string name="");

            /// \brief Create entity with default ctor, and calls manageStateChild.
            /// \return Created entity.
            template<typename T>
            std::shared_ptr<T> createStateChild(the::string group="", the::string name="", InitOrder ord = BY_PARENT)
            {
                std::shared_ptr<T> childPtr(new T());
                childPtr->setInitOrder(ord);
                manageStateChild(childPtr, group, name);
                return childPtr;
            }

            /// \brief Support for REGISTER macro.
            virtual void registerChild(Parameter* p, the::string xmlName);
            virtual void unregisterChild(Parameter* p);

            void setNoParent();

            inline bool isInGameTree() const
            {
                return mCore != nullptr;
            }

            inline InfrastructureCore* core() const
            {
                ASSERT(mCore != nullptr, "Component doesn't added to game tree, so infrastructure services are not available.");
                return mCore;
            }

            virtual VistorBehaviour accept(std::function<VistorBehaviour (GameEntity*)> enter, std::function<void(GameEntity*)> leave);
            virtual VistorBehaviour accept(std::function<VistorBehaviour (GameEntity*)> enter);
            virtual void acceptAll(std::function<void (GameEntity*)> visit);

            /////////////////////////////////////////////////////////////////////////////////////////////
            //// Names, groups, parser, zindex //////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////

            /// \brief Returns unical in it's context name. For example, name
            /// of component should be unical inside parent object or component.
            inline the::string getName() const
            {
                return mName;
            }
            inline HashCode getNameHash() const
            {
                return mNameHash;
            }
            /// \brief Sets name. Name must be unical in it's context. For example,
            /// name of component should be unical inside parent object or component.
            virtual void setName(const the::string& name);
            /// \brief Returns name of the item group.
            /// \remarks Groups are useful, when one need to bind to several items. Return string can contains several
            /// group names, splitted by comma ( group="group1, group2" ). Groups can be nested ( group="car1/vehicles" ).
            inline the::string getGroup() const
            {
                return mGroup;
            }
            inline const eastl::vector<HashCode>& getGroupsHashes() const
            {
                return mGroupsHashes;
            }
            /// \brief Returns true, if component is in specified group.
            /// Method checks, if GetGroup return value contains `group`.
            inline bool inGroup(const the::string& group) const
            {
                HashCode hash = getHashCode<string>(group);
                return eastl::contains(mGroupsHashes, hash);
            }
            inline bool inGroup(HashCode groupHash) const
            {
                return eastl::contains(mGroupsHashes, groupHash);
            }
            /// \brief Sets name of the item group, by removing all previous groups.
            /// \remarks Groups are useful, when one need to bind to several items. Return string can contains several
            /// group names, splitted be comma ( group="group1, group2" ). Groups can be nested ( group="car1/vehicles" ).
            virtual void setGroup(const the::string& group);
            /// \brief Addes group for item. This method do not deletes previous groups. To remove group use SetGroup.
            /// \remarks Groups are useful, when one need to bind to several items. Return string can contains several
            /// group names, splitted be comma ( group="group1, group2" ). Groups can be nested ( group="car1/vehicles" ).
            virtual void addGroup(const the::string& group);
            /// \brief Return name of the class, wich should be equal to name of the its xml-tag.
            inline the::string getType() const
            {
                return this->getTypeInfo().name;
            }
            /// \brief Returns automatic parser for this object.
            /// \remarks Current realization returns parser,
            /// that has root element with name mType, and that
            /// can parse attributes name, group and zindex.
            virtual IXmlParser* getParser();
            /// \brief When implemented in derived class, should return bounding box of full entity.
            /// \remarks Current realization returns bounding rectangle around of all children.
            // virtual sf::FloatRect getRectangle() const;
            /*/// \brief Returns z-index of entity.
            virtual int8 getZIndex() const;
            /// \brief Sets z-index of entity.
            virtual void setZIndex(int8 _ZIndex);*/
            /// \brief Returns position of the node in game tree.
            /// \return String, that contains name and type of the node, and of the all its parents.
            /// Function is useful for debug purposes.
            virtual the::string getPathString() const;
            virtual the::string getShortInfoString() const override;
            virtual the::string getDetailedInfoString() const override;

            the::string getXmlTagName() const;


            ParameterGroup* getParameterGroup();

            virtual std::shared_ptr<GameEntity> clone() const;

            template <typename T>
            std::shared_ptr<T> cloneAs() const
            {
                return std::static_pointer_cast<T>(clone());
            }

            GameEntityID id();

            struct InitDependency
            {
                GameEntity* entity;
                the::string entityRole;
                the::string dependencyReason;
                bool weakOnDeinit;
            };

            const std::vector<InitDependency>& getInitDependencies();
            const std::vector<InitDependency>& getIncomingInitDependencies();
            bool initDependsOn(GameEntity* ent);
            void addInitDependency(GameEntity* ent, the::string myRole,
                the::string entRole, the::string dependencyReason, bool weakOnDeinit = false);
            void removeInitDependency(GameEntity* ent);
            void clearDependencies();

            bool getBool();
            void setBool(bool s);
            bool b;

        protected:
            //int mZIndex;
            the::string mName;
            HashCode mNameHash;
            the::string mGroup;
            eastl::vector<HashCode> mGroupsHashes;
            bool mHadParent;
            bool mNoParent;
            the::string mXmlTag;
            GameEntityInitData mParameters;
            Visibility mVisibility;
            GameEntityID mEngineID;
            std::vector<std::shared_ptr<GameEntity>> mStateChildren;
            InitState mInitState;
            std::vector<InitDependency> mDependencies;
            std::vector<InitDependency> mIncomingDependencies;
            bool mAutoSubscribeToLoop;
            bool mForceParamsUnlock;

            /// \brief Calls, after child being registered.
            virtual void onRegisterChild(GameEntity* child);
            virtual void onUnregisterChild(GameEntity* child);
            virtual void onChildInit(GameEntity* child);
            virtual void onChildDeinit(GameEntity* child);
            virtual void onParentChanged();
            virtual void onParentChanging(GameEntity* newParent);

            inline void setInitState(InitState result)
            {
                mInitState = result;
            }

            void updateInitData();

            InitState initInternal();

            /// \brief When implemented in derived class, should perform operations,
            /// that must be done after parsing, but before using of entity.
            /// \remarks This realization not do anything. For derived classed no reason to call base realization.
            /// \returns Do initialization completed.
            virtual InitState onInit();

            InitState deinitInternal();

            /// \brief When implemented in derived class, should free all occupied resource.
            /// \remarks This realization not do anything. For derived classed no reason to call base realization.
            /// \note Destructor GameEntity calls Deinit.
            virtual void onDeinit();


            void preInit();
            virtual void onPreInit();
            void postInit();
            virtual void onPostInit();
            InitState initGotStuck();
            virtual InitState onInitGotStuck();

            void preDeinit();
            virtual void onPreDeinit();
            void postDeinit();
            virtual void onPostDeinit();
            InitState deinitGotStuck();
            virtual InitState onDeinitGotStuck();
            the::string forceDeinit();

            virtual bool checkDependencies(const std::vector<InitDependency>& list,
                bool checkInitialzed, the::string& msg, bool ignoreWeak = false);

            void updateChildInitDependencies(GameEntity* child);

            void findUnsetRequiredParams(ParameterGroup* current, std::list<ValueParameter*>& unsetRequired);

            void setAutoSubscribeToLoop(bool subscribe);

            void setCore(InfrastructureCore* core);
            virtual void onSetCore(InfrastructureCore* oldValue, InfrastructureCore* newValue);

            void clearStateChildren();

            virtual void onSaveState()
            {
            }

        private:
            // These fields are private, because RegisterChild, UnregisterChild, EnabeUpdate and EnableUpdateVisual
            // supports notify for GameTree.
            GameEntity* mParentEntity;
            InfrastructureCore* mCore;
            std::list<GameEntity*> mChildren;
            const The2DTypeInfo* mEntityTypeToLookInDebugger;
            InitOrder mOrderOfInit;


            friend class InitService;
            friend class BindBase;
            friend class ObserverService;
    };
} // namespace the
////////////////////////////////////////////////////////////
