////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \class Component Component "Component.hpp"
    /// \brief Base class for services.
    /// Services implements some common functions, to support infrastructure.
    class Service: public GameEntity
    {
        public:
            THE_ENTITY(the::Service, the::GameEntity)

            virtual ~Service();
    };
} // namespace the
////////////////////////////////////////////////////////////
