////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/ChildList.hpp>
#include <The2D/StateMachine/Trans.hpp>
#include <vector>

////////////////////////////////////////////////////////////
namespace the
{
    class FiniteStateMachine: public GameEntity
    {
        public:
            THE_ENTITY(FiniteStateMachine, GameEntity)

            Bind<State> toStartState;
            Bind<State> toFinalState;
            ChildList<Trans> mTransitionTable;

            /// \brief Start on init.
            /// Default value is true.
            PBool prmAutoStart;

            CBool CWorking;
            
            CCommand DoStart;
            CCommand DoForceFinish;
            CEvent EvStarted;
            CEvent EvFinished;
            
            FiniteStateMachine();
            virtual ~FiniteStateMachine();

            const std::vector<State*> getStates();

        protected:
            std::vector<State*> mStates;

            virtual InitState onInit() override;
            virtual void onPostInit() override;
            virtual void onDeinit() override;

            virtual void start();
            virtual void forceFinish();

            virtual void onFinished();

    };
} // namespace the
////////////////////////////////////////////////////////////
