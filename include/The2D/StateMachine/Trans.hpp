////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/StateMachine/State.hpp>
#include <The2D/StateMachine/Exec.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Parameters.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \brief Entry for transition table for FSM.
    class Trans: public GameEntity
    {
        public:
            THE_ENTITY(Trans, GameEntity)

            enum StatesUnion
            {
                AND,
                OR
            };
            
            /// \brief These states must be active, to make transition perform.
            ChildList<Bind<State>> toStates;
            /// \brief Another way to specify single state in toStates.
            PString prmState;
            /// \brief Specifies, how to combine states in toStates.
            PMapped<StatesUnion> prmStatesUnion;
            /// \brief Event, that activates the transition.
            ChildList<Bind<CEvent>> toEvents;
            /// \brief Another way to specify single bind in toEvents.
            PString prmEvent;
            /// \brief This action will be executed, when transition performed.
            Action mAction;
            /// \brief Another way to specify path to CCommand for single Exec command in mActions;
            PString prmCommand;
            /// \brief States to activate after transition performed.
            /// If this will be specified, then all toStates will be deactivated, and toNextStates activated.
            /// Otherwise, nothing will be done with current states.
            ChildList<Bind<State>> toNextStates;
            /// \brief Another way to specify single state in toNextStates.
            PString prmNextState;


            CEvent EvPrePerform;
            CEvent EvPostPerform;

            Trans();
            virtual ~Trans();

        protected:
            std::shared_ptr<Bind<State>> mCreatedStateBind;
            std::shared_ptr<Bind<CEvent>> mCreatedEventBind;
            std::shared_ptr<Bind<State>> mCreatedNextStateBind;
            std::shared_ptr<Exec> mCreatedAction;

            virtual void onPreInit() override;
            virtual InitState onInit() override;
            virtual void onDeinit() override;
            virtual void onPostDeinit() override;

            virtual void onEvent(CEvent* ev);

            virtual void perform();
            
    };
} // namespace the
////////////////////////////////////////////////////////////
