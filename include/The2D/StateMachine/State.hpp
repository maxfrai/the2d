////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/EntityGroup.hpp>
#include <The2D/StateMachine/Action.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class State: public ChildList<State>
    {
        public:
            THE_ENTITY(State, ChildList<State>)

            Action mActivateAction;
            Action mDeactivateAction;

            /// \brief Just for self documentation purposes.
            PString prmDescription;

            CBool CActive;

            CCommand DoActivate;
            CCommand DoDeactivate;
            CEvent EvActivated;
            CEvent EvDeactivated;
            
            State();
            virtual ~State();

        protected:
            bool mActive;

            virtual void setActive(bool active);
            /// \brief Methods to override in derived classes.
            virtual void onActivate();
            /// \brief Methods to override in derived classes.
            virtual void onDeactivate();

    };
} // namespace the
////////////////////////////////////////////////////////////
