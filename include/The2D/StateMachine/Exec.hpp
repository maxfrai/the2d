////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/StateMachine/Action.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \brief Action, that executes some CCommand.
    class Exec: public Action
    {
        public:
            THE_ENTITY(Exec, Action)

            Bind<CCommand> toCommand;

            /// \brief Another way to specify toCommand.path.
            PString prmCommandPath;

            Exec();
            virtual ~Exec();

        protected:
            
            virtual void onExec() override;
            virtual void onPreInit();
    };
} // namespace the
////////////////////////////////////////////////////////////
