////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/ChildList.hpp>
#include <The2D/Connectors.hpp>
#include <The2D/Parameters.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class Action: public ChildList<Action>
    {
        public:
            THE_ENTITY(Action, ChildList<Action>)

            PInt prmOrder;

            CCommand DoExec;
            CEvent EvPreExec;
            CEvent EvPostExec;

            Action();
            virtual ~Action();

            void exec();

        protected:
            virtual void onRegisterChild(GameEntity* child) override;

            /// \brief Calls before calling of the children actions, but after EvPreExec.
            /// Base implementation doesn't contains any logic.
            virtual void onExec();
    };
} // namespace the
////////////////////////////////////////////////////////////
