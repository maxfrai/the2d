////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/StateMachine/Trans.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \brief Group of Trans, that allow specify parameters for all items at once.
    class TransGroup: public Trans
    {
        public:
            THE_ENTITY(TransGroup, Trans)

            ChildList<Trans> mTransitions;
           

            TransGroup();
            virtual ~TransGroup();

        protected:
            
            virtual InitState onInit() override;
            virtual void onDeinit() override;
           
            
    };
} // namespace the
////////////////////////////////////////////////////////////
