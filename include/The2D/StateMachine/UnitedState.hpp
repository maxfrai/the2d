////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/StateMachine/State.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class UnitedState: public State
    {
        public:
            THE_ENTITY(UnitedState, State)

            ChildList<Bind<State>> toStates;
 
            UnitedState();
            virtual ~UnitedState();

        protected:

            virtual void setActive(bool active) override;

    };
} // namespace the
////////////////////////////////////////////////////////////
