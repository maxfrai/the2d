////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/Utils/Signals.hpp>
#include <The2D/RttiSupport.hpp>
#include <The2D/ChunkList.hpp>
#include <chrono>
#include <EASTL/map.h>
#include <EASTL/list.h>

#include <iostream>


////////////////////////////////////////////////////////////
namespace the
{
    class InfrastructureCore;
    class GameLoopProcessor;

    constexpr double nanoToMili(double val)
    {
        return (val / 1000000.0);
    }

    class StatisticTarget
    {
        public:
            THE_ABSTRACT_ENTITY(the::StatisticTarget)

            virtual ~StatisticTarget();

            virtual the::string getShortInfoString() const;
            virtual the::string getDetailedInfoString() const;
    };

    struct StatisticEntry
    {
        uint64 elapsedNanosec;
        StatisticEntry* parentEntry;
        StatisticTarget* target;
        const char* tag;

        inline double elapsedMilisec()
        {
            return nanoToMili(elapsedNanosec);
        }
    };

#ifdef DEBUG
    #define BEGIN_STATS_FOR(ent) ent->core()->stats()->beginEntry(ent, THE_CURRENT_FUNCTION); \
        auto __currentStatTarget = ent; \
        {
    #define BEGIN_STATS BEGIN_STATS_FOR(this)
    #define END_STATS  } __currentStatTarget->core()->stats()->endEntry(__currentStatTarget, THE_CURRENT_FUNCTION);
#else
    #define BEGIN_STATS void;
    #define BEGIN_STATS_FOR(ent) void;
    #define END_STATS void;
#endif

    /// \brief Class which manages statistic calculations, such as perfomance measurement.
    /// \ingroup Core
    class StatisticService : private the::NonCopyable<StatisticService>
    {
        public:

            StatisticService(InfrastructureCore* core);
            virtual ~StatisticService();

            inline const StatisticEntry* beginEntry(StatisticTarget* target, const char* tag)
            {
                if (mStatisticEnabled)
                {
                    auto item = mStatistic.push_front();
                    item->data.elapsedNanosec = std::chrono::duration_cast<std::chrono::nanoseconds>(
                        std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                    item->data.tag = tag;
                    item->data.target = target;
                    item->data.parentEntry = &(item->next()->data);
                    return &item->data;
                }
                else
                {
                    return nullptr;
                }
            }

            inline const StatisticEntry* getCurrentEntry()
            {
                if (!mStatistic.empty()) return &mStatistic.front();
                else return nullptr;
            }

            inline double getCurrentElapsedMilisec(const StatisticEntry* entry)
            {
                if (entry == nullptr) return 0;
                else
                {
                    int64 cur = std::chrono::duration_cast<std::chrono::nanoseconds>(
                        std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                    int64 elapsed = cur - entry->elapsedNanosec;
                    return nanoToMili(elapsed);
                }
            }

            inline const StatisticEntry* endEntry(StatisticTarget* target, const char* tag)
            {
                if (mStatisticEnabled && !mStatistic.empty())
                {
                    ASSERT(mStatistic.front().target == target && mStatistic.front().tag == tag,
                           "Mismatched beginEntry/endEntry calls.");
                    int64 cur = std::chrono::duration_cast<std::chrono::nanoseconds>(
                        std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                    int64 elapsed = cur - mStatistic.front().elapsedNanosec;
                    mStatistic.front().elapsedNanosec = elapsed;
                    mStatistic.move_back(mStatistic.first());
                    return &(mStatistic.back());
                }
                else
                {
                    return nullptr;
                }
            }
            void resetData();
            bool statisticsIsGathering();
            void beginStatisticsGathering();
            void endStatisticsGathering();
            ChunkList<StatisticEntry>& getStatistics();

            struct ResultTree
            {
                double averageElapsedMilisec;
                double minElapsedMilisec;
                double maxElapsedMilisec;
                int count;
                StatisticTarget* target;
                string tag;
                ResultTree* parent;

                eastl::list<std::shared_ptr<ResultTree>> children;

                ResultTree();

                void accept(std::function<void (ResultTree* cur, int level)> visitor, int level = 0);

                // For calculation purposes
                StatisticTarget* parentTarget;
                string parentTag;
            };

            std::shared_ptr<ResultTree> calculateResultTree(StatisticTarget* target = nullptr);

        protected:
            InfrastructureCore* mCore;
            ChunkList<StatisticEntry> mStatistic;
            bool mStatisticEnabled;

    };
}; // namespace the
////////////////////////////////////////////////////////////
