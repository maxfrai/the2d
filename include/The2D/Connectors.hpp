////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/Utils/Signals.hpp>
#include <The2D/Rtti.hpp>
#include <The2D/Bind.hpp>
#include <The2D/GameLoopProcessor.hpp>
#include <The2D/ParametersBase.hpp>
#include <The2D/ChildList.hpp>
#include <The2D/Vec2.hpp>

////////////////////////////////////////////////////////////
namespace the {

    /// \defgroup Connectors Connectors
    /// @{

    /// \brief Base class for all connectors.
    /// Connectors are entities, that provide access to some value, value group,
    /// command or event of the component. They allow connect between themselves
    /// from xml, reducing dependencies of components.
    class Connector : public the::GameEntity
    {
        public:

            /// \skipline THE_RTTI_NOT_ENTITY_DECLARATION
            THE_ENTITY(Connector, the::GameEntity)

            Connector();
            /// Destructor.
            virtual ~Connector();

            std::list<Connector*> getChildConnectors() const;
    };

    template<typename ...ArgsT>
    class CArgEvent;

    /// \brief Connector representing some event in component.
    /// This connector allows to subscribe on the some event in component
    /// from code (using SigRaised field) or from xml (via CFunc on antoher side).
    template<typename ReturnT, typename ...ArgsT>
    class CArgEvent<ReturnT (ArgsT...)>: public Connector
    {
        public:
            THE_ENTITY(CArgEvent<ReturnT(ArgsT...)>, Connector);

            /// \brief Callback list for subcribers to event.
            the::Signal<ReturnT (ArgsT...)> SigRaised;

            virtual ~CArgEvent(){}

            /// \brief Notify all subscibers about the event.
            /// \note To pass arguments for an event set values to
            ///       corresponding child connectors (see CParametrized).
            virtual void raise(ArgsT ... args)
            {
                SigRaised(args...);
            }

            void operator()(ArgsT ... args)
            {
                SigRaised(args...);
            }
    };

    typedef CArgEvent<void()> CEvent;


    class DataConnectorBase: public Connector
    {
        public:
            THE_ENTITY(DataConnectorBase, Connector)

            Signal<void()> SigValueChanged;

            PString source;
            /// \brief Bind to source connector, from which should copy value on occurance
            ///        of event, specified in prmTransferCause.
            /// \note There uses Bind, so converters are awailable. But using them harm productivity.
            /// \sa prmTransferCause.
            Bind<DataConnectorBase> toSourceBind;

            DataConnectorBase(): mSource(nullptr)
            {
                REGISTER(source).setRequired(false);
                REGISTER(toSourceBind).setRequired(false);
            }

            inline void onValueChanged(bool notifySource = true)
            {
                if (notifySource && mSource != nullptr)
                {
                    mSource->onValueChanged();
                }
                SigValueChanged();
            }

            virtual void connect(DataConnectorBase* source)
            {
                if (mSource != nullptr)
                {
                    mSource->SigValueChanged.remove(this);
                }
                mSource = source;
                source->SigValueChanged.add(this, [&]{ onValueChanged(false); });
                if (!toSourceBind.isInitialized()) toSourceBind.set(source);

                onValueChanged();
            }

            virtual void disconnect()
            {
                if (mSource != nullptr)
                {
                    mSource->SigValueChanged.remove(this);
                    mSource = nullptr;
                }
            }

        protected:
            DataConnectorBase* mSource;

            virtual void onPreInit() override
            {
                if (source.containsValue())
                {
                    if (!toSourceBind.path().empty())
                    {
                        ERR("Set both, `source` and toSource.path. Value of `source` ignored.");
                    }
                    else
                    {
                        toSourceBind.path = source();
                    }
                }
            }

            virtual InitState onInit()
            {
                if (toSourceBind.containsValue())
                {
                    connect(toSourceBind());
                }
                return READY;
            }

            virtual void onDeinit() override
            {
                if (toSourceBind.containsValue())
                {
                    disconnect();
                }
            }

    };

    class IConversion;

    class IConversionConnection
    {
        public:
            IConversionConnection(IConversion* conv, Connector* from, Connector* to);
            virtual ~IConversionConnection();

            inline IConversion* getConv() const { return mConversion; }
            inline Connector* getSource() const { return mSource; }
            inline Connector* getTarget() const { return mTarget; }

        protected:
            IConversion* mConversion;
            Connector* mSource;
            Connector* mTarget;
    };

    class IConversion: public GameEntity
    {
        public:
            THE_ABSTRACT_ENTITY(IConversion, GameEntity);

            virtual bool canConvert(Connector* from, Connector* to, string* errMsg = nullptr);
            virtual IConversionConnection* connect(Connector* from, Connector* to);
            virtual void disconnect(IConversionConnection* connection);
    };

    template<typename T>
    class IValueConversion: public IConversion
    {
        public:
            THE_ABSTRACT_ENTITY(IValueConversion<T>, IConversion);

            virtual void getFromSource(IConversionConnection* con, T& target) = 0;

            virtual void setToSource(IConversionConnection* con, const T& value) = 0;
    };

    enum AccessModes {
        READ_ONLY=1, READ_WRITE=0
    };

    /// \brief Data connector.
    /// CValue is a connector, representing some value in component. It can store value inside (default),
    /// use some external memory or manual get/set functors. In all cases, interface to access values is one.
    /// External getters and setters can be setup via special setXXX methods, or during registration in parent.
    /// For emample:
    ///
    ///         // Setup getter and setter via lambda
    ///         THE_REGISTER_CONNECTOR_EXT(CPosition,
    ///             [&](){ return Vec2(mCamera->getCenter().x, mCamera->getCenter().y); },
    ///             [&](const Vec2& pos){ mCamera->setCenter(sf::Vector2f(pos.x, pos.y)); });
    ///         // Setup getter via external memory and setter via lambda.
    ///         THE_REGISTER_CONNECTOR_EXT(CBodyAngle, &mBody->m_sweep.a,
    ///             [&](float angle){ mBody->SetTransform(mBody->GetPosition(), angle); } );
    ///         // Setup readonly connector, with getter via std::bind
    ///         THE_REGISTER_CONNECTOR_EXT(CCurrentCommand, std::bind(&KeyMoveControll::getCurrentCommand, this));
    ///         // Setup connector with value stored inside
    ///         THE_REGISTER_CONNECTOR(CCurrentState);
    ///
    /// CValue can be bound via Bind or connected with another CValue. This allow to decrease dependencies between
    /// Components (because of binding to connector, not to a whole component). Besides, methods get and set of CValue optimised
    /// for speed. So, accessing connector value almost equal to access a simple field or callback call (if some accessor is functor).
    ///
    /// If CValue binded to another CValue. In that case by the some event (see the::TransferValueModes), value copies from
    /// source to target. Binding performs via Bind, so converters are also awailable there.
    ///
    /// Class declares set of operators for easy access to a underlying value. So, in many cases there can be
    /// used standard syntax as if it was usual field. But sometimes compiler cant apply operators (for exmaple,
    /// it cant implicitly convert CFloat to double). In this cases one can use call-operator, to get a value.
    /// There examples:
    ///
    ///        CFloat floatCon;
    ///        float val = floatCon*2;
    ///        floatCon = val;
    ///        val = fabs(floatCon);
    ///        double dval = floatCon(); // Implicit conversion is not supported, using call operator.
    ///
    /// \attention Connector is not a simple field. Access to it can raise method call with side effects. It is
    ///            specially important in multithread programming.
    /// \note In many cases it is better to use Bind instead of CValue. See Bind for details.
    /// \tparam T Type of underlying value. For example, CValue<float>.
    /// \note It is recomended use predefined set of types, including: \n
    ///       CFloat, CInt, CBool, CString, CVec2, CEnum, CAngle, CColor, CTime \n
    ///       CFloatVect, CIntVect, CBoolVect, CStringVect, CVec2Vect, CEnumVect, CAngleVect, CColorVect, CTimeVect \n
    ///       CFloatList, CIntList, CBoolList, CStringList, CVec2List, CEnumList, CAngleList, CColorList, CTimeList \n
    ///       Using only these connectors instead of native CValue will make greater possiblity of compability of components.
    template<typename T>
    class DataConnector: public DataConnectorBase
    {
        public:
            THE_ENTITY(DataConnector<T>, DataConnectorBase);

            typedef IValueConversion<T> ConvType;

            /// \brief Type of underlying value.
            /// For example, CFloat::ValueType is float.
            typedef T ValueType;


            /// \brief Information about converter.
            /// \see PConverter.
            Bind<ConvType> toConversionBind;
            PString conversion;

            PBool prmTransferEveryStep;

            /// Setup CValue to use internal value.
            DataConnector(): mGetValue(new T()), mOwnGetValue(true), mGetPtrAccess(true),
                mSetValue(mGetValue), mOwnSetValue(true), mSetPtrAccess(true),mAccessMode(READ_WRITE),
                mConvConnection(nullptr)
            {
                REGISTER(toConversionBind).setRequired(false);
                REGISTER(conversion); conversion.defval("");
                REGISTER(prmTransferEveryStep).defval(false);
                setAutoSubscribeToLoop(false);
            }

            virtual ~DataConnector()
            {
                deleteGetValue();
                deleteSetValue();
            }

            /// \brief Set value of a connector.
            /// \param val New value of the connector.
            /// \sa CValue::get, setLocal, getLocal, the::AccessModes, setAccessMode, getAccessMode
            inline void set(const T& val)
            {
                ASSERT(mAccessMode != READ_ONLY, ":(");

                if (mSource != nullptr && !prmTransferEveryStep)
                {
                    if (mConvConnection != nullptr)
                    {
                        getConv()->setToSource(mConvConnection, val);
                    }
                    else
                    {
                        static_cast<DataConnector*>(mSource)->set(val);
                    }
                }
                else
                {
                    if (mSetPtrAccess)
                    {
                        *mSetValue = val;
                    }
                    else
                    {
                        mSetter(val);
                    }
                }

                onValueChanged();
            }

            /// \brief Get a value of a connector.
            /// If getter setup to use internal value or external memory,
            /// then there read value by pointer. If getter setup to use
            /// functor, there performs callback call.
            /// \returns Value of the connector.
            inline const T& get()
            {
                if (mSource != nullptr && !prmTransferEveryStep)
                {
                    if (mConvConnection)
                    {
                        getConv()->getFromSource(mConvConnection, *mGetValue);
                        return *mGetValue;
                    }
                    else
                    {
                        return static_cast<DataConnector*>(mSource)->get();
                    }
                }
                else
                {
                    if (mGetPtrAccess)
                    {
                        return *mGetValue;
                    }
                    else
                    {
                        mGetter(*mGetValue);
                        return *mGetValue;
                    }
                }
            }

            /// \name Operators
            /// Easy syntax access to connector value
            /// @{
            inline const T& operator()()
            {
                return get();
            }
            inline operator const T&()
            {
                return get();
            }
            inline DataConnector& operator=(const T& val)
            {
                set(val);
                return *this;
            }
            inline bool operator==(const T& val)
            {
                return get() == val;
            }
            inline bool operator!=(const T& val)
            {
                return get() != val;
            }
            /// @} Operators-------------------------

             /// \brief Get awailable operations on connector.
            AccessModes getAccessMode()
            {
                return mAccessMode;
            }

            // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline DataConnector& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline DataConnector& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline DataConnector& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

            /// \brief Set getter via extrnal memory.
            DataConnector& setGetter(T* target)
            {
                deleteGetValue();
                mOwnGetValue = false;
                mGetValue = target;
                mGetPtrAccess = true;
                onReconfig(true, false);

                return *this;
            }
            /// \brief Set setter via extrnal memory.
            DataConnector& setSetter(T* target)
            {
                deleteSetValue();
                mOwnSetValue = false;
                mSetValue = target;
                mSetPtrAccess = true;
                onReconfig(false, true);
                return *this;
            }
            /// \brief Set getter and setter via extrnal memory.
            DataConnector& setAccessors(T* target)
            {
                setGetter(target);
                setSetter(target);
                onReconfig(true, true);
                return *this;
            }
            /// \brief Set getter via functor.
            DataConnector& setGetter(std::function<void(T& result)> getter)
            {
                mGetter = getter;
                // mGetValue used in get()
                sureInternalGetValueExists();
                mGetPtrAccess = false;
                onReconfig(true, false);
                return *this;
            }
            /// \brief Set setter via functor.
            DataConnector& setSetter(std::function<void(const T&)> setter)
            {
                mSetter = setter;
                // deleteSetValue();
                mSetPtrAccess = false;
                onReconfig(false, true);
                return *this;
            }

            DataConnector& onValueChange(std::function<void()> callback)
            {
                SigValueChanged.add(callback);
                return *this;
            }

            /// \brief Set event, that will be raised in CValue::set method.
            /// Event will be raised on any value change, made over CValue interface. If CValue
            /// points to some external memory, it cant monitor all changes in that memory, making by
            /// some external code.
            /// Setting nullptr value disables event generation.
            DataConnector& setValueChangedEvent(CEvent* ev)
            {
                SigValueChanged.add([ev]{ ev->raise(); });
                return *this;
            }
            DataConnector& setValueChangedEvent(CEvent& ev)
            {
                setValueChangedEvent(&ev);
                return *this;
            }

             /// \brief Fluent setter, allowing to set default value of a connector.
            inline DataConnector& defval(ValueType&& defaultValue){
                auto accessMode = mAccessMode;
                mAccessMode = READ_WRITE;
                set(defaultValue);
                mAccessMode = accessMode;
                return *this;
            }
            /// \brief Same as CValue::defval
            inline DataConnector& setDefaultValue(ValueType&& defaultValue)
            {
                auto accessMode = mAccessMode;
                mAccessMode = READ_WRITE;
                set(defaultValue);
                mAccessMode = accessMode;
                return *this;
            }
            // --------------------------------------------------------------

             /// \brief Switch getter and setter to internal value.
            void resetAccessors()
            {
                deleteSetValue();
                deleteGetValue();
                mSetValue = new T();
                mOwnSetValue = true;
                mSetPtrAccess = true;
                mGetValue = mSetValue;
                mOwnGetValue = true;
                mGetPtrAccess = true;
                onReconfig(true, true);
            }

            /// \brief Calls resetAccessors and base implementation.
            virtual void resetParent(bool deinitialize = true) override
            {
                resetAccessors();
                Connector::resetParent(deinitialize);
            }

            using DataConnectorBase::connect;

            virtual void connect(DataConnectorBase* source, ConvType* conversion)
            {
                ASSERT(conversion->canConvert(source, this), "Invalid conversion");
                ASSERT(conversion->canConvert(this, source) || mAccessMode == READ_ONLY, "Invalid conversion");

                mConvConnection = conversion->connect(source, this);
                if (!toConversionBind.isInitialized()) toConversionBind.set(conversion);
                // mGetValue used in get()
                resetAccessors();

                DataConnectorBase::connect(source);
            }

            virtual void disconnect() override
            {
                if (mConvConnection != nullptr)
                {
                    getConv()->disconnect(mConvConnection);
                    mConvConnection = nullptr;
                }

                DataConnectorBase::disconnect();
            }

            void update() override
            {
                ASSERT(prmTransferEveryStep(), "");

                if (mConvConnection != nullptr)
                {
                    T temp; // If we transfer every step, then excepted, that sizeof(T) is not very big
                    getConv()->getFromSource(mConvConnection, temp);
                    set(temp);
                }
                else
                {
                    set(static_cast<DataConnector*>(mSource)->get());
                }
            }

        protected:
            T* mGetValue;       ///< \brief Pointer getter.
            bool mOwnGetValue;  ///< \brief Is mGetValue internal value (created by this) or pointer to external memory.
            bool mGetPtrAccess; ///< \brief Is connectoer setup to use mGetValue or mGetter.
            std::function<void(T& result)> mGetter; ///< \brief Functor getter.
            T* mSetValue;       ///< \brief Pointer setter.
            bool mOwnSetValue;  ///< \brief Is mSetValue internal value (created by this) or pointer to external memory.
            bool mSetPtrAccess; ///< \brief Is connectoer setup to use mSetValue or mSetter.
            std::function<void (const T&)> mSetter; ///< \brief Functor setter.
            AccessModes mAccessMode;
            IConversionConnection* mConvConnection;

            inline ConvType* getConv()
            {
                return static_cast<ConvType*>(mConvConnection->getConv());
            }

            /// \brief Delete internal value if there is some.
            void deleteSetValue()
            {
                if (mSetValue == mGetValue)
                {
                    mGetValue = nullptr;
                    mOwnGetValue = false;
                }
                if (mOwnSetValue)
                {
                    delete mSetValue;
                }
                mOwnSetValue = false;
                mSetValue = nullptr;
            }
            /// \brief Delete internal value if there is some.
            void deleteGetValue()
            {
                if (mGetValue == mSetValue)
                {
                    mSetValue = nullptr;
                    mOwnSetValue = false;
                }
                if (mOwnGetValue)
                {
                    delete mGetValue;
                }
                mOwnGetValue = false;
                mGetValue = nullptr;
            }

            void sureInternalGetValueExists()
            {
                if (!mOwnGetValue || mGetValue == nullptr)
                {
                    mGetValue = new T();
                    mOwnGetValue = true;
                }
            }

            virtual void onReconfig(bool getterChanged, bool setterChanged)
            {
                ASSERT(!isInitialized(), "Configure connector before initialization.");
                setInitOrder(AFTER_PARENT);
            }

            virtual void onPreInit() override
            {
                if (conversion.containsValue())
                {
                    if (!toConversionBind.path().empty())
                    {
                        ERR("Set both, `conversion` and toConversionBind.path. Value of `conversion` ignored.");
                    }
                    else
                    {
                        toConversionBind.path = conversion();
                    }
                }
                DataConnectorBase::onPreInit();
            }

            /// \brief Read parameters and setup source if it specified.
            virtual InitState onInit()
            {
                if (toConversionBind.containsValue())
                {
                    if (!toSourceBind.containsValue())
                    {
                        return NOT_READY("Converter specified, but source not set.");
                    }
                    // Check, if converter is compatible
                    string converterMsg;
                    if (!toConversionBind->canConvert(toSourceBind.Get(), this, &converterMsg))
                    {
                        return NOT_READY("Converter is not compatible with specified source, because of next: %s", converterMsg);
                    }
                    if (mAccessMode == READ_WRITE)
                    {
                       if (!toConversionBind->canConvert(this, toSourceBind.Get(), &converterMsg))
                       {
                           return NOT_READY("AccessMode is READ_WRITE, but converter "
                               "does not support backward conversion because of next: %s", converterMsg);
                       }
                    }

                    connect(toSourceBind(), toConversionBind());
                }
                else
                {
                    if (toSourceBind.containsValue())
                    {
                        if (!core()->rtti()->is(type(toSourceBind()), type<DataConnector>()))
                        {
                            return NOT_READY("Source '%s' has uncompatible type (type is %s, but needed %s (or derived from it))",
                                toSourceBind.getPathString(), type(toSourceBind()).fullName, type<DataConnector>().fullName);
                        }

                        connect(toSourceBind());
                    }
                }

                if (prmTransferEveryStep)
                {
                    if (!toSourceBind.containsValue())
                    {
                         return NOT_READY("prmTransferEveryStep set to true, but source not specified");
                    }
                    core()->loop()->subscribeToUpdate(this);
                }

                return READY;
            }

            virtual void onDeinit() override
            {
                if (toSourceBind.containsValue())
                {
                    disconnect();
                }
                if (prmTransferEveryStep)
                {
                    core()->loop()->unsubscribeFromUpdate(this);
                }
            }
    };

    template<typename T, AccessModes AccessMode = READ_WRITE, bool SourceRequired = false>
    class CValue: public DataConnector<T>
    {
        public:
            THE_ENTITY(CValue<T COMMA AccessMode COMMA SourceRequired>, DataConnector<T>)

            CValue()
            {
                this->mAccessMode = AccessMode;
                this->toSourceBind.setRequired(SourceRequired);
            }

            inline CValue& operator=(const T& val)
            {
                this->set(val);
                return *this;
            }
            inline CValue& operator=(CValue& val)
            {
                this->set(val());
                return *this;
            }
    };

    /// \brief Support of streaming for CValue.
    /// Puts to stream connector value.
    template<typename T>
    std::ostream& operator << (std::ostream &o,const CValue<T> &a)
    {
        return o << a.get();
    }

    template<typename T, AccessModes AccessMode = READ_WRITE>
    using ToValue = CValue<T, AccessMode, true>;

    template<typename T>
    using CValueR = CValue<T, READ_ONLY, false>;

    template<typename T>
    using ToValueR = CValue<T, READ_ONLY, true>;

    typedef CValue<int> CInt;
    typedef CValue<float> CFloat;
    typedef CValue<bool> CBool;
    typedef CValue<string> CString;
    typedef CValue<Vec2> CVec2;

    typedef CValueR<int> CIntR;
    typedef CValueR<float> CFloatR;
    typedef CValueR<bool> CBoolR;
    typedef CValueR<string> CStringR;
    typedef CValueR<Vec2> CVec2R;

    typedef ToValue<int> ToInt;
    typedef ToValue<float> ToFloat;
    typedef ToValue<bool> ToBool;
    typedef ToValue<string> ToString;
    typedef ToValue<Vec2> ToVec2;

    typedef ToValueR<int> ToIntR;
    typedef ToValueR<float> ToFloatR;
    typedef ToValueR<bool> ToBoolR;
    typedef ToValueR<string> ToStringR;
    typedef ToValueR<Vec2> ToVec2R;


    template<typename T>
    class IFuncConversion;

    template<typename R, typename ...Args>
    class IFuncConversion<R(Args...)> : public IConversion
    {
        public:
            THE_ABSTRACT_ENTITY(IFuncConversion<R(Args...)>, IConversion);

            virtual R callSource(IConversionConnection* con, Args ... args) = 0;
    };

    template<typename T, bool sourceRequried = false>
    class CFuncBase;

    /// \brief Connector, representing some action, that can perform a component.
    /// Command should be binded via functor to some code, performing an action. Code can be executed
    /// manually by call of CFuncBase::exec, or by subscribing command to some CEvent.
    /// Subscribing can be done from code or from xml.
    ///
    /// Arguments of command passes via children connectors. Access these argumens in convinient way one canal
    /// through CParametrized interface.
    template<typename ReturnT, typename ... ArgsT, bool sourceRequried>
    class CFuncBase<ReturnT(ArgsT...), sourceRequried> : public Connector
    {
        public:
            THE_ENTITY(CFuncBase<ReturnT(ArgsT...)>, Connector);

            typedef IFuncConversion<ReturnT(ArgsT...)> ConvType;

            /// \brief Event, that executes a command.
            /// If event is compatible with a command (has corresponding child connectors),
            /// then it became data provider. In any case it became an initiator.
            /// \sa toDataProvider, toInitiators.
            ChildList<Bind<CArgEvent<ReturnT(ArgsT...)>>> toEvents;

            PString source;
            /// \brief Bind to source connector, from which should copy value on occurance
            ///        of event, specified in prmTransferCause.
            /// \note There uses Bind, so converters are awailable. But using them harm productivity.
            /// \sa prmTransferCause.
            Bind<Connector> toSourceBind;
            /// \brief Information about converter.
            /// \see PConverter.
            Bind<ConvType> toConversionBind;
            PString conversion;

            CFuncBase(): mConvConnection(nullptr), mSource(nullptr)
            {
                REGISTER(toEvents);
                REGISTER(source).setRequired(false);
                REGISTER(toSourceBind).setRequired(sourceRequried);
                REGISTER(toConversionBind).setRequired(false);
                REGISTER(conversion); conversion.defval("");
            }
            virtual ~CFuncBase()
            {
            }

            /// \brief Execute a command.
            /// Execution is execution of underlying callback. Before calling this callback,
            /// data copies to command from mDataProvider.
            inline ReturnT exec(ArgsT... args) const
            {
                if (mSource != nullptr)
                {
                    if (mConvConnection != nullptr)
                    {
                        return getConv()->callSource(mConvConnection, args...);
                    }
                    else
                    {
                        return static_cast<CFuncBase*>(mSource)->exec(args...);
                    }
                }
                else
                {
                    return mAction(args...);
                }

            }

            inline ReturnT operator()(ArgsT... args) const
            {
                return exec(args...);
            }

            /// \brief See CFuncBase::toEvent.
            void connect(CArgEvent<ReturnT(ArgsT...)>* ev)
            {
                ev->SigRaised.add(this, mAction);
            }

            /// \brief See CFuncBase::toEvent.
            void disconnect(CArgEvent<ReturnT(ArgsT...)>* ev)
            {
                ev->SigRaised.remove(this);
            }

            /// \brief Get code, that repersenting a command.
            const std::function<ReturnT(ArgsT...)>& getAction() const
            {
                return mAction;
            }

            // Fluent support -------------------------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline CFuncBase& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline CFuncBase& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline CFuncBase& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }

            /// \brief Set function implementation.
            inline CFuncBase& setFunc(std::function<ReturnT(ArgsT...)> action)
            {
                mAction = action;
                return *this;
            }
            // -------------------------------------------------------------------------------

            virtual void connect(CFuncBase* source)
            {
                mSource = source;
                if (!toSourceBind.isInitialized()) toSourceBind.set(source);
            }

            virtual void connect(Connector* connector, ConvType* conv)
            {
                mConvConnection = conv->connect(connector, this);
                if (!toConversionBind.isInitialized()) toConversionBind.set(conv);
                connect(static_cast<CFuncBase*>(connector));
            }

            virtual void disconnect()
            {
                if (mConvConnection != nullptr)
                {
                    getConv()->disconnect(mConvConnection);
                    mConvConnection = nullptr;
                }
                mSource = nullptr;
            }

        protected:

            std::function<ReturnT(ArgsT...)> mAction; ///< See CFuncBase::setFunc
            IConversionConnection* mConvConnection;
            Connector* mSource;

            ConvType* getConv() const
            {
                return static_cast<ConvType*>(mConvConnection->getConv());
            }

            virtual void onPreInit() override
            {
                if (source.containsValue())
                {
                    if (!toSourceBind.path().empty())
                    {
                        ERR("Set both, `source` and toSource.path. Value of `source` ignored.");
                    }
                    else
                    {
                        toSourceBind.path = source();
                    }
                }
                if (conversion.containsValue())
                {
                    if (!toConversionBind.path().empty())
                    {
                        ERR("Set both, `conversion` and toConversionBind.path. Value of `conversion` ignored.");
                    }
                    else
                    {
                        toConversionBind.path = conversion();
                    }
                }
            }

            /// \brief Reads parameters.
            InitState onInit() override
            {
                if (toConversionBind.containsValue())
                {
                    if (!toSourceBind.containsValue())
                    {
                        return NOT_READY("Converter specified, but source not set.");
                    }
                    // Check, if converter is compatible
                    string converterMsg;
                    if (!toConversionBind->canConvert(toSourceBind.Get(), this, &converterMsg))
                    {
                        return NOT_READY("Converter is not compatible with specified source, because of next: %s", converterMsg);
                    }
                    connect(toSourceBind(), toConversionBind());
                }
                else
                {
                    if (toSourceBind.containsValue())
                    {
                        if (!core()->rtti()->is(type(toSourceBind()), type<CFuncBase>()))
                        {
                            return NOT_READY("Source '%s' has uncompatible type (type is %s, but needed %s (or derived from it))",
                                toSourceBind.getPathString(), type(toSourceBind()).fullName, type<CFuncBase>().fullName);
                        }
                        connect(static_cast<CFuncBase*>(toSourceBind()));
                    }
                }
                if (toEvents.size() > 0)
                {
                    for (auto bindPtr : toEvents())
                    {
                        connect(bindPtr->Get());
                    }
                }
                return READY;
            }
            /// \brief Resets data provider and initiators.
            void onDeinit() override
            {
                if (toEvents.size() > 0)
                {
                    for (auto bindPtr : toEvents())
                    {
                        disconnect(bindPtr->Get());
                    }
                }
            }
    };

    template<typename T>
    using ToFunc = CFuncBase<T, true>;

    template<typename T>
    using CFunc = CFuncBase<T, false>;

    typedef CFunc<void()> CCommand;

    /// @}

}; // namespace the



////////////////////////////////////////////////////////////