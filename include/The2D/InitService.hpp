////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <list>
#include <The2D/Utils/Signals.hpp>
#include <The2D/ChunkList.hpp>
#include <The2D/GameEntity.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class InfrastructureCore;

    /// \class InitService InitService "InitService.hpp"
	/// \ingroup Core
    /// \brief Helps to init game tree.
    class InitService : private NonCopyable<InitService>
    {
        public:
            Signal<void()> SigTreeInitBegin;
            Signal<void()> SigTreeInitEnd;

            InitService(InfrastructureCore* core);

            bool init(GameEntity* ent);
            void deinit(GameEntity* ent);

            void enableDebugLog(bool enable);
            bool debugLogEnabled();

        private:
            InfrastructureCore* mCore;
            int mInitLevel;
            int mDeinitLevel;
            bool mDebugLog;

            typedef ChunkList<GameEntity*> EntitiesList;

            void buildInitListRec(GameEntity* current, EntitiesList& initList);
            void buildDeinitListRec(GameEntity* current, EntitiesList& deinitList);
            bool automaticInitOrder(const GameEntity* ent) const;
            bool containsRemove(EntitiesList& list, GameEntity* ent);

    };

} // namespace the
////////////////////////////////////////////////////////////
