////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Connectors.hpp>
#include <The2D/Attached.hpp>
#include <The2D/CValueVector.hpp>
#include <The2D/Converters.hpp>

////////////////////////////////////////////////////////////
namespace the {

    template<typename SourceT, typename TargetT>
    class CValueVectorConversion: public IValueConversion<eastl::vector<TargetT>>
    {
        public:
            THE_ENTITY(CValueVectorConversion<SourceT COMMA TargetT>, IValueConversion<eastl::vector<TargetT>>);

            typedef CValueVectorConversion<SourceT, TargetT> ThisType;
            typedef IValueConversion<eastl::vector<TargetT>> BaseType;
            typedef DataConnector<eastl::vector<SourceT>> SourceConnectorT;
            typedef DataConnector<eastl::vector<TargetT>> TargetConnectorT;

            virtual IConversionConnection* connect(Connector* from, Connector* to)
            {
                ASSERT(canConvert(from, to), "Unsupported source or target");

                auto con = BaseType::connect(from, to);

                CValueVector<SourceT>* source = static_cast<CValueVector<SourceT>*>(from);
                eastl::vector<SourceT>* sourceVec = &const_cast<eastl::vector<SourceT>&>(source->get());
                CValueVector<TargetT>* target = static_cast<CValueVector<TargetT>*>(to);
                eastl::vector<TargetT>* targetVec = &const_cast<eastl::vector<TargetT>&>(target->get());

                // lambdas there doesn't work, internal compiler error :(
                source->SigVectorContentChanged.add(con, std::bind(
                    &CValueVectorConversion::synchronize<SourceT, TargetT>,
                    this, sourceVec, std::placeholders::_1, targetVec, &mForwardItemConversion));

                return con;
            }
            virtual void disconnect(IConversionConnection* con)
            {
                CValueVector<SourceT>* source = static_cast<CValueVector<SourceT>*>(con->getSource());
                source->SigVectorContentChanged.remove(con);

                BaseType::disconnect(con);
            }

            void getFromSource(IConversionConnection*, eastl::vector<TargetT>&) override
            {
                // All synchronization makes on vectors content change
            }

            void setToSource(IConversionConnection*, const eastl::vector<TargetT>&) override
            {
                // All synchronization makes on vectors content change
            }

            // Fluent support -----------------------------------------------
            /// \brief Fluent overload of GameEntity::setInitOrder
            inline ThisType& setInitOrder(InitOrder order) { GameEntity::setInitOrder(order); return *this; }
            /// \brief Fluent overload of GameEntity::setVisibility
            inline ThisType& setVisibility(Visibility vis) { GameEntity::setVisibility(vis); return *this; }
            /// \brief Fluent overload of GameEntity::setXmlTagName
            inline ThisType& setXmlTagName(the::string xmlTag) { GameEntity::setXmlTagName(xmlTag); return *this; }


            /// \brief Set function, that realize conversion logic.
            ThisType& setForwardItemConv(std::function<TargetT(const SourceT&)> conversion)
            {
                mForwardItemConversion = conversion;
                return *this;
            }
            // --------------------------------------------------------------

            bool canConvert(Connector* from, Connector* to, string* errMsg = nullptr) override
            {
                bool fwd_sourceTypeOk = type(from).is<SourceConnectorT>();
                bool fwd_targetTypeOk = type(to).is<TargetConnectorT>();
                bool fwd_convOk = mForwardItemConversion;
                if (fwd_sourceTypeOk && fwd_targetTypeOk && fwd_convOk) return true;
                else if (errMsg != nullptr)
                {
                    if (!fwd_sourceTypeOk)
                    {
                        *errMsg += format("wrong source type for forward conversion (excepted '%s', got '%s'); ",
                                    type<SourceConnectorT>().fullName, type(from).fullName);
                    }
                    if (!fwd_targetTypeOk)
                    {
                        *errMsg += format("wrong target type for forward conversion (excepted '%s', got '%s'); ",
                                    type<TargetConnectorT>().fullName, type(to).fullName);
                    }
                    if (!fwd_convOk)
                    {
                        *errMsg += format("forward conversion not configured (use setForwardConv method);");
                    }
                }
                return false;
            }


        protected:
            std::function<TargetT(const SourceT&)> mForwardItemConversion;

            template<typename SourceType, typename TargetType>
            void synchronize(const eastl::vector<SourceType>* psource, VectorChange change,
                eastl::vector<TargetType>* ptarget, std::function<TargetType(const SourceType&)>* pconv)
            {
                eastl::vector<TargetType>& target = *ptarget;
                const eastl::vector<SourceType>& source = *psource;
                auto conv = *pconv;
                switch(change.type)
                {
                    CASE(PUSH_BACK) target.push_back(conv(source[change.pos])); END_CASE;
                    CASE(INSERT) target.insert(target.begin() + change.pos, conv(source[change.pos])); END_CASE;
                    CASE(REMOVE) target.erase(target.begin()  + change.pos); END_CASE;
                    CASE(UPDATE) target[change.pos] = conv(source[change.pos]); END_CASE;
                    CASE(FULL_UPDATE)
                        target.clear();
                        for (auto& el : source)
                        {
                            auto val = conv(el);
                            target.push_back(val);
                        }
                    END_CASE;
                };
            }
    };

}; // namespace the



////////////////////////////////////////////////////////////