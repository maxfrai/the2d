////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/RttiSupport.hpp>

#include <functional>
#include <vector>

////////////////////////////////////////////////////////////
namespace the
{
    class InfrastructureCore;
    class GameEntity;

    enum WatchAction
    {
        ENTITY_REGISTERED = 1,
        ENTITY_UNREGISTERED = 2,
        ENTITY_INITIALIZING = 4,
        ENTITY_INITIALIZED = 8,
        ENTITY_DEINITIALIZED = 16
    };

    enum WatchStrategy
    {
        WATCH_EACH_ENTITY,
        WATCH_SUBTREE
    };

    /// \brief For internal use in ObserverService.
    class Watcher
    {
        public:
            std::function<void (GameEntity*, WatchAction)> watcher;
            WatchAction actionFlags;
            WatchStrategy strategy;


            Watcher(std::function<void (GameEntity*, WatchAction)> _watcher,
                    WatchAction _actionFlags,
                    WatchStrategy _strategy);
        private:
            int handleCount;

            friend class WatcherHandle;
            friend class ObserverService;
    };

    class WatcherHandle
    {
        public:
            WatcherHandle();
            WatcherHandle(const WatcherHandle& cpy);
            ~WatcherHandle();

            bool operator==(const WatcherHandle& handle) const;
            bool operator!=(const WatcherHandle& handle) const;
            bool equal(const WatcherHandle& handle) const;

            WatcherHandle& operator=(const WatcherHandle& handle);

            void reset();
            bool valid();
            operator bool();

            Watcher* get();

        private:
            Watcher* mWatcher;
            std::vector<Watcher*>* mWatcherList;

            void set(Watcher* id, std::vector<Watcher*>* watchersList);

            WatcherHandle(Watcher* id, std::vector<Watcher*>* watchersList);

            friend class ObserverService;
    };

    /// \brief Service, that allow finding of desired entities.
    class ObserverService: private the::NonCopyable<ObserverService>
    {
        public:

            ObserverService(InfrastructureCore* core);
            virtual ~ObserverService();

            virtual WatcherHandle beginWatch(
                std::function<void (GameEntity*, WatchAction)> watcher,
                WatchAction actionFlags,
                WatchStrategy strategy = WATCH_EACH_ENTITY);
            virtual void endWatch(WatcherHandle& handle);

            virtual const std::vector<GameEntity*>& getEntityList() const;
            virtual GameEntity* getEntity(int id) const;
            virtual const std::vector<GameEntity*>& getManagerList() const;
            virtual int getRegisteredEntityCount() const;

        protected:
            InfrastructureCore* mCore;
            std::vector<Watcher*> mWatchers;
            std::vector<GameEntity*> mEntityList;
            std::vector<GameEntity*> mManagerList;
            int mEntityCounter;

            virtual void onRegister(GameEntity* ent);
            virtual void onUnregister(GameEntity* ent);

            virtual void onRegisterManager(GameEntity* ent);
            virtual void onUnregisterManager(GameEntity* ent);

            virtual void onInitializing(GameEntity* ent);
            virtual void onInitialized(GameEntity* ent);
            virtual void onDeinitialized(GameEntity* ent);

            void noticeWatchers(GameEntity* ent, WatchAction action, WatchStrategy strategy = WATCH_EACH_ENTITY);

            void addToList(GameEntity* ent);
            void removeFromList(GameEntity* ent);

            friend class GameEntity;
            friend class InitService;
            friend class Manager;
            friend class InfrastructureCore;
    };
}; // namespace the
////////////////////////////////////////////////////////////
