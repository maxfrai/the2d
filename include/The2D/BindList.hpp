////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/ChildList.hpp>
#include <The2D/Utils/Tinyxml/tinyxml.h>
#include <The2D/Utils/Signals.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \class BindList BindList "The2D/BindList.hpp"
    template<typename TargetT>
    class BindList: public ChildList<Bind<TargetT>>
    {
        public:
            THE_ENTITY(the::BindList<TargetT>, the::ChildList<Bind<TargetT>>);

            typedef the::ChildList<Bind<TargetT>> BaseType;

            BindList()
            {
                this->mChildInitOrder = BEFORE_PARENT;
            }

            using BaseType::add;

            virtual void addTarget(TargetT* ent)
            {
                Bind<TargetT>* bind = new Bind<TargetT>();
                bind->set(ent);
                add(make_sptr(bind));
            }

            virtual void addTarget(TargetT* ent, InitOrder initOrder)
            {
                Bind<TargetT>* bind = new Bind<TargetT>();
                bind->set(ent);
                add(make_sptr(bind), initOrder);
            }

            using BaseType::remove;

            virtual void removeTarget(TargetT* ent)
            {
                eastl::vector<Bind<TargetT>*> toRemove;
                for (auto child : this->mAllChildren)
                {
                    if (child->Get() == ent)
                    {
                        toRemove.push_back(child);
                        break;
                    }
                }

                for (auto child : toRemove)
                {
                    remove(child);
                }
            }

            virtual TargetT* targetAt(int i) const
            {
                return mTargets[i];
            }

            virtual const eastl::vector<TargetT*>& getTargets() const
            {
                return mTargets;
            }

        protected:
            eastl::vector<TargetT*> mTargets;

            virtual InitState onInit() override
            {
                for (auto bind : this->mAllChildren)
                {
                    if (bind->containsValue())
                    {
                        mTargets.push_back(bind->Get());
                    }
                }

                return BaseType::onInit();
            }

            virtual void onDeinit() override
            {
                mTargets.clear();

                BaseType::onDeinit();
            }
    };

} // namespace the
////////////////////////////////////////////////////////////
