////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/Config.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \struct IEntityCreator IEntityCreator "EntityCreator.hpp"
    /// \brief Interface of the class, that realizes creation of game enities by type name.
    /// For example, one can realize class, that will interact with script system to create objects by type name.
    class IEntityCreator
    {
        public:
            /// \brief Returns pointer to new instance of specified type.
            /// \param typeName String which identifies game-object type
            /// \return Shared pointer to created GameEntity or nullptr, if `typeName` is not valid type.
            virtual std::shared_ptr<GameEntity> createEntity(const the::string& typeName) const = 0;
    };
}; // namespace the
////////////////////////////////////////////////////////////
