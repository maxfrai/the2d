////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/GameEntity.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/Message.hpp>
#include <The2D/MessageCore.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \ingroup Core
    /// \class Msg Msg "MessageHelper.hpp"
    /// \brief Set of functions, that makes messaging sweet.
    class Msg
    {
        public:

            // !!!!!!!!!!!!! Notify via Message !!!!!!!!!!!!!!!!!!!!

            /// \brief Send Message via MessageManager.
            static inline uint16 notifyAll(uint32 msgType, GameEntity* sender, InfrastructureCore* core)
            {
                Message msg(msgType, Message::MESSAGE_CORE, sender);
                return core->msg()->broadcast(&msg);
            }

            /// \brief Send Message up to tree.
            static inline void notifyMyParents(uint32 msgType, GameEntity* sender)
            {
                Message msg(msgType, Message::UP, sender);
                if (sender->getParent() != nullptr)
                {
                    sender->getParent()->processMessage(&msg);
                }
            }

            /// \brief Send Message down to tree.
            static inline void notifyMyChildren(uint32 msgType, GameEntity* sender)
            {
                Message msg(msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = sender->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
            }

            /// \brief Send Message up to some branch of the tree.
            static inline void notifyParentsOf(GameEntity* parentsOf, uint32 msgType, GameEntity* sender)
            {
                Message msg(msgType, Message::UP, sender);
                if (parentsOf->getParent() != nullptr)
                {
                    parentsOf->getParent()->processMessage(&msg);
                }
            }

            /// \brief Send Message down to some branch of the tree.
            static inline void notifyChildrenOf(GameEntity* childrenOf, uint32 msgType, GameEntity* sender)
            {
                Message msg(msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = childrenOf->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
            }

            // !!!!!!!!!!!!! Notify via MessageSendData<T> !!!!!!!!!!!!!!!!!!!!

            /// \brief Send MessageSendData via MessageManager.
            template<typename MsgDataType>
            static inline uint16 notifyAll(MsgDataType data, uint32 msgType, GameEntity* sender, InfrastructureCore* core)
            {
                MessageSendData<MsgDataType> msg(data, msgType, Message::MESSAGE_CORE, sender);
                return core->msg()->broadcast(&msg);
            }

            /// \brief Send MessageSendData up to tree.
            template<typename MsgDataType>
            static inline void notifyMyParents(MsgDataType data, uint32 msgType, GameEntity* sender)
            {
                MessageSendData<MsgDataType> msg(data, msgType, Message::UP, sender);
                if (sender->getParent() != nullptr)
                {
                    sender->getParent()->processMessage(&msg);
                }
            }

            /// \brief Send MessageSendData down to tree.
            template<typename MsgDataType>
            static inline void notifyMyChildren(MsgDataType data, uint32 msgType, GameEntity* sender)
            {
                MessageSendData<MsgDataType> msg(data, msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = sender->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
            }

            /// \brief Send MessageSendData up to some branch of the tree.
             template<typename MsgDataType>
            static inline void notifyParentsOf(GameEntity* parentsOf, MsgDataType data, uint32 msgType, GameEntity* sender)
            {
                MessageSendData<MsgDataType> msg(data, msgType, Message::UP, sender);
                if (parentsOf->getParent() != nullptr)
                {
                    parentsOf->getParent()->processMessage(&msg);
                }
            }

             /// \brief Send MessageSendData down to some branch of the tree.
            template<typename MsgDataType>
            static inline void notifyChildrenOf(GameEntity* childrenOf, MsgDataType data, uint32 msgType, GameEntity* sender)
            {
                MessageSendData<MsgDataType> msg(data, msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = childrenOf->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
            }

             // !!!!!!!!!!!!! Get data via MessageReceiveData !!!!!!!!!!!!!!!!!!!!

             /// \brief Send MessageRequestData via MessageManager.
            template<typename MsgDataType>
            static inline uint16 getFromSomeone(MsgDataType& data, uint32 msgType, GameEntity* sender, InfrastructureCore* core)
            {
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::MESSAGE_CORE, sender);
                return core->msg()->broadcast(&msg);
            }

            /// \brief Send MessageRequestData via MessageManager.
            template<typename MsgDataType>
            static inline MsgDataType getFromSomeone(uint32 msgType, GameEntity* sender, InfrastructureCore* core,  MsgDataType defaultValue = MsgDataType())
            {
                MsgDataType data = defaultValue;
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::MESSAGE_CORE, sender);
                core->msg()->broadcast(&msg);
                return data;
            }

            /// \brief Send MessageRequistData up to tree.
            template<typename MsgDataType>
            static inline void getFromMyParents(MsgDataType& data, uint32 msgType, GameEntity* sender)
            {
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::UP, sender);
                if (sender->getParent() != nullptr)
                {
                    sender->getParent()->processMessage(&msg);
                }
            }

            /// \brief Send MessageRequistData up to tree.
            template<typename MsgDataType>
            static inline MsgDataType getFromMyParents(uint32 msgType, GameEntity* sender, MsgDataType defaultValue = MsgDataType())
            {
                MsgDataType data = defaultValue;
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::UP, sender);
                if (sender->getParent() != nullptr)
                {
                    sender->getParent()->processMessage(&msg);
                }
                return data;
            }

            /// \brief Send MessageRequestData down to tree.
            template<typename MsgDataType>
            static inline void getFromMyChildren(MsgDataType& data, uint32 msgType, GameEntity* sender)
            {
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = sender->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
            }

            /// \brief Send MessageRequestData down to tree.
            template<typename MsgDataType>
            static inline MsgDataType getFromMyChildren(uint32 msgType, GameEntity* sender, MsgDataType defaultValue = MsgDataType())
            {
                MsgDataType data = defaultValue;
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = sender->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
                return data;
            }

            /// \brief Send MessageRequestData up to some branch of the tree.
            template<typename MsgDataType>
            static inline void getFromParentsOf(GameEntity* parentsOf, MsgDataType& data, uint32 msgType, GameEntity* sender)
            {
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::UP, sender);
                if (parentsOf->getParent() != nullptr)
                {
                    parentsOf->getParent()->processMessage(&msg);
                }
            }

            /// \brief Send MessageRequestData up to some branch of the tree.
            template<typename MsgDataType>
            static inline MsgDataType getFromParentsOf(GameEntity* parentsOf, uint32 msgType, GameEntity* sender, MsgDataType defaultValue = MsgDataType())
            {
                MsgDataType data = defaultValue;
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::UP, sender);
                if (parentsOf->getParent() != nullptr)
                {
                    parentsOf->getParent()->processMessage(&msg);
                }
                return data;
            }

             /// \brief Send MessageRequestData down to some branch of the tree.
            template<typename MsgDataType>
            static inline void getFromChildrenOf(GameEntity* childrenOf, MsgDataType& data, uint32 msgType, GameEntity* sender)
            {
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = childrenOf->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
            }

            /// \brief Send MessageRequestData down to some branch of the tree.
            template<typename MsgDataType>
            static inline MsgDataType getFromChildrenOf(GameEntity* childrenOf, uint32 msgType, GameEntity* sender, MsgDataType defaultValue = MsgDataType())
            {
                MsgDataType data = defaultValue;
                MessageReceiveData<MsgDataType> msg(data, msgType, Message::DOWN, sender);
                const std::list<the::GameEntity*>& children = childrenOf->getChildren();
                for (auto it = children.begin(); it != children.end(); it++)
                {
                    (*it)->processMessage(&msg);
                }
                return data;
            }

            // !!!!!!!!!!!!!!!!!!!! Working with data of message !!!!!!!!!!!!!!!!!!!!!!!

            /// \brief Casts msg to MessageSendData<MsgDataType> and returns mData.
            template<typename MsgDataType>
            static inline MsgDataType& getSendedData(Message* msg)
            {
                return ((MessageSendData<MsgDataType>*)msg)->mData;
            }

            /// \brief Casts msg to MessageRequestData<MsgDataType> and sets mVariable with data.
            template<typename MsgDataType>
            static inline void setRequistedData(Message* msg, MsgDataType data)
            {
                ((MessageReceiveData<MsgDataType>*)msg)->mVariable = data;
            }
    };

} // namespace the
////////////////////////////////////////////////////////////
