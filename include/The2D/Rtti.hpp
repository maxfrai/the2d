////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#include <The2D/XmlLoader.hpp>
#include <The2D/RttiSupport.hpp>
#include <The2D/Utils/Signals.hpp>
#include <bitset>

namespace the
{

    /// \brief Run time type information service.
    /// \ingroup Engine
    /// Provides fast rtti operations and engine-unique type ids.
    class Rtti: public IEntityCreator
    {
        public:

            Signal<void(const The2DTypeInfo*)> SigTypeRegistered;

            /// \brief Initialized new instance of Rtti clas with specified inheritance matrix capacity.
            /// \pram inheritanceMatrixCapacity Max count of registered types.
            Rtti(uint inheritanceMatrixCapacity = 700);

            virtual ~Rtti();

            /// \brief Making awailable rtti service for this type and all its parents.
            /// \param type Type info object of the type to register. Use the type<T>() or type(ISupportRtti*) method to receive it.
            /// \example
            ///       core()->rtti()->registerType(type<ICharacter>());
            /// \note Types of game entities which added to the main tree registered automaticly.
            virtual void registerType(const The2DTypeInfo& type);

            /// \brief Making awailable rtti service for this type and all its parents.
            /// \param T Type to register.
            /// \example
            ///       core()->rtti()->registerType<ICharacter>();
            /// \note Types of game entities which added to the main tree registered automaticly.
            /// \note Type must contains macro THE_ENTITY at declaration and macro  in implementation.
            template<typename T>
            void registerType()
            {
                registerType(type<T>());
            }

            /// \brief Allow to determine if the type registered in this instace of Rtti class.
            /// \param type Type info object of the type to register. Use the type<T>() or type(ISupportRtti*) method to receive it.
            /// \return  Is the type registered in this instace of Rtti class.
            /// Complexity: several memory accesses and comparisions, i.e. O(1).
            virtual bool typeRegistered(const The2DTypeInfo& type);

            /// \brief The method is used to check whether the one type is compatible with another.
            /// \param type Type info object of the first type. Use the type<T>() or type(ISupportRtti*) method to receive it.
            /// \param base Type info object of the first type. Use the type<T>() or type(ISupportRtti*) method to receive it.
            /// \return Is the first type compatible with second.
            /// Types are compatible, if second type is explicit or implicit parent of the frist type, or equal to it.
            ///
            /// Complexity of the call is O(1). More specifically:
            ///
            /// * if types are not registered, registration of the types
            /// * Two memory access (to get engineID values of the type info objects)
            /// * One multiply and one summ (to calculate index in inheritance matrix)
            /// * Another one memory access (to read value from inheritance matrix)
			/// * One interger comparision with zero
            virtual inline bool is(const The2DTypeInfo& type, const The2DTypeInfo& base)
            {
                return getInheritanceDistance(type, base) > 0;
            }

            virtual inline int getInheritanceDistance(const The2DTypeInfo& type, const The2DTypeInfo& base)
            {
                if (type.engineID == 0) registerType(type);
                if (base.engineID == 0) registerType(base);
                ASSERT(type.engineID <= mRegisteredTypes.size(), "Invalid type info object. Maybe, type registered in another Rtti instance.");
                ASSERT(base.engineID <= mRegisteredTypes.size(), "Invalid type info object. Maybe, type registered in another Rtti instance.");
                ASSERT(*(mRegisteredTypes[type.engineID]) == type, "Invalid type info object. Maybe, type registered in another Rtti instance.");
                ASSERT(*(mRegisteredTypes[base.engineID]) == base, "Invalid type info object. Maybe, type registered in another Rtti instance.");

                // Acces type.engineID row and base.engineID column in matrix, stored as flat array.
                return mInheritanceMatrix[type.engineID*mInheritanceMatrixCapacity + base.engineID];
            }

            /// \brief The method is used to check whether the type of an game entity is compatible with a given type.
            /// \param obj Game entity object wich type needed to check.
            /// \param Type Type to check compatible with.
            /// \return Is the type of entity is compatible with template parameter type.
            /// Types are compatible, if Type is explicit or implicit parent of the game entity type, or equal to it.
            ///
            /// Complexity of the call is:
            /// * one virtual method call (to get type info object of the `obj`)
            /// * two memory access (to get engineID values of the type info objects)
            /// * One multiply and one summ (to calculate index in inheritance matrix)
            /// * another one memory access (to read value from inheritance matrix)
            template<typename Type>
            inline bool is(const ISupportRtti* obj)
            {
                if (obj != nullptr)
                {
                    return is(type(obj), type<Type>());
                }
                else
                {
                    return false;
                }
            }

            /// \brief Implementation of IEntityCreator::createEntity.
            virtual std::shared_ptr<GameEntity> createEntity(const the::string& typeName) const;

            virtual const std::vector<const The2DTypeInfo*>& getRegisteredTypes();

            virtual const The2DTypeInfo* getType(const string& typeName) const;

        private:
            // engineID of the The2DTypeInfo is the index in mRegisteredTypes. But, zero value means
            // that type does not registered. So, wee need to add some stuf to mRegisteredTypes
            // before use it.
            const The2DTypeInfo mFirstTypeStub;
            // engineID of the The2DTypeInfo is the index in mRegisteredTypes
            std::vector<const The2DTypeInfo*> mRegisteredTypes;
            std::map<size_t, const The2DTypeInfo*> mNameHashToType;
            // Two dimesional square matrix, where for any two types stores information about
            // their inheritance relation. Types represented by their engineID number.
            int8* mInheritanceMatrix;
            // Number of rows and columns in the mInheritanceMatrix (matrix is square).
            int mInheritanceMatrixCapacity;

            // Recursively register all base types of the type, and specify the inheritance relations.
            void __registerTypeRec(const The2DTypeInfo* type, const The2DTypeInfo* base, int deep);
    };
}

