////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/ChunkVectorIndex.hpp>
#include <EASTL/vector.h>
#include <EASTL/map.h>

////////////////////////////////////////////////////////////
namespace the {

    /// \brief Objects of classes, derived from this class can have attached properties.
    class ISupportAttachedInternal
    {
        public:
            virtual ~ISupportAttachedInternal() {}
            
            static constexpr int INITIAL_ATTACHED_INDEXES_CAPACITY = 2;

            inline ChunkVectorIndex getAttachedIndex(int id, int classID)
            {
                auto it = mAttachedIndexes.find(classID);
                if (it != mAttachedIndexes.end())
                {
                    eastl::vector<ChunkVectorIndex>& indexes = it->second;
                    if (id < (int)indexes.size())
                    {
                        return indexes[id];
                    }
                    else
                    {
                        return INVALID_INDEX;
                    }
                }
                else
                {
                    return INVALID_INDEX;
                }
            }
            void setAttachedIndex(int id, int classID, ChunkVectorIndex value);

            void shrinkAttachedIndexes();

            size_t getMemoryWastedByAttached();

            size_t getMemoryUsedByAttached();

        private:
            eastl::map<int, eastl::vector<ChunkVectorIndex>> mAttachedIndexes;

            friend class AttachedService;
    };

}; // namespace the



////////////////////////////////////////////////////////////
