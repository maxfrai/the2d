////////////////////////////////////////////////////////////
//
// Themisto - 2D physic based game with flexible engine
// Copyright (C) 2010-2012 Pavel Bogatirev, Max Tyslenko (Wincode team), max.tyslenko@gmail.com, pfight77@gmail.com
//
// This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/EntityGroup.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \class GameTree GameTree "GameTree.hpp"
    /// \brief Root of game tree or subtree.
    /// Nearest GameTree can be pointed by bind via "~" symbol.
    class GameTree: public EntityGroup
    {
        public:
            THE_ENTITY(the::GameTree, the::EntityGroup);

            GameTree();
            virtual ~GameTree();
    };
}; // namespace the
////////////////////////////////////////////////////////////
