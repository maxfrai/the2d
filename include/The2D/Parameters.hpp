////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#include <The2D/ParametersBase.hpp>
#include <map>
#include <algorithm>
#include <The2D/Vec2.hpp>

namespace the
{
    class PFloat: public TValueParameter<float>
    {
        public:
            virtual ~PFloat();

            virtual string serialize() const override;
            virtual ParameterTypes getType() const override;
            void operator=(float val);
        protected:
            virtual bool onParse(string& strValue) override;

    };

    class PDouble: public TValueParameter<double>
    {
        public:
            virtual ~PDouble();

            virtual string serialize() const override;
            virtual ParameterTypes getType() const override;
            void operator=(double val);
        protected:
            virtual bool onParse(string& strValue) override;

    };

    class PInt: public TValueParameter<int>
    {
        public:
            virtual ~PInt();

            virtual string serialize() const override;
            virtual ParameterTypes getType() const override;
            void operator=(int val);
        protected:
            virtual bool onParse(string& strValue) override;
    };

    class PUInt: public TValueParameter<uint>
    {
        public:
            virtual ~PUInt();

            virtual string serialize() const;
            virtual ParameterTypes getType() const  override;
            void operator=(uint val);
        protected:
            virtual bool onParse(string& strValue) override;
    };

    class PBool: public TValueParameter<bool>
    {
        public:
            virtual ~PBool();

            virtual string serialize() const override;
            virtual ParameterTypes getType() const  override;
            void operator=(bool val);
        protected:
            virtual bool onParse(string& strValue) override;

    };

    class PString: public TValueParameter<string>
    {
        public:
            virtual ~PString();

            virtual string serialize() const override;
            virtual ParameterTypes getType() const override;

            void operator=(const char* val);
            void operator=(string val);

        protected:
            virtual bool onParse(string& strValue) override;

    };

    template<typename T>
    inline std::shared_ptr<TValueParameter<T>> createDefaultParameter()
    {
        TH_FAIL("There is no default parameter for this type");
        return std::shared_ptr<TValueParameter<T>>();
    };
    template<>
    inline std::shared_ptr<the::TValueParameter<float>> createDefaultParameter()
    {
        auto param = new PFloat();
        return std::shared_ptr<TValueParameter<float>>(param);
    };

    template<>
    inline std::shared_ptr<the::TValueParameter<double>> createDefaultParameter()
    {
        auto param = new PDouble();
        return std::shared_ptr<TValueParameter<double>>(param);
    };

    template<>
    inline std::shared_ptr<the::TValueParameter<int>> createDefaultParameter()
    {
        auto param = new PInt();
        return std::shared_ptr<TValueParameter<int>>(param);
    };
    template<>
    inline std::shared_ptr<the::TValueParameter<bool>> createDefaultParameter()
    {
        auto param = new PBool();
        return std::shared_ptr<TValueParameter<bool>>(param);
    };
    template<>
    inline std::shared_ptr<the::TValueParameter<string>> createDefaultParameter()
    {
        auto param = new PString();
        return std::shared_ptr<TValueParameter<string>>(param);
    };
    template<>
    inline std::shared_ptr<the::TValueParameter<uint>> createDefaultParameter()
    {
        auto param = new PUInt();
        return std::shared_ptr<TValueParameter<uint>>(param);
    };

    template<typename T>
    class PMapped: public TValueParameter<T>, public MappedValueParameter
    {
        public:

            PMapped(std::initializer_list<std::pair<the::string, T>> map)
            {
                setMap(map);
            }

            PMapped(std::map<the::string, T> map)
            {
                setMap(map);
            }

            PMapped()
            {
            }

            virtual ~PMapped()
            {
            }

            virtual void clearMap()
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                mMap.clear();
                mOriginalMap.clear();
            }


            virtual the::string serialize() const
            {
                for (auto &it : mOriginalMap)
                {
                    if (it.second == TValueParameter<T>::getValue())
                    {
                        return it.first;
                    }
                }
                return "";
            }

            virtual std::list<string> getMappedValues()
            {
                std::list<string> list;
                for(auto &pair : mOriginalMap)
                {
                    list.push_back(pair.first);
                }
                return list;
            }

            virtual ParameterTypes getType() const
            {
                return MAPPED_PARAM;
            }

            void operator=(T val)
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                this->setValue(val);
            }

            // Fluent support ----------------------------------
            /// \brief Fluent overload of Parameter::setXmlName
            inline PMapped& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
            /// \brief Fluent overload of Parameter::setRequired
            inline PMapped& setRequired(bool required) { Parameter::setRequired(required); return *this; }
            /// \brief Fluent overload of Parameter::setLocal
            inline PMapped& setLocal(Local local) { Parameter::setLocal(local); return *this; }
            /// \brief Fluent overload of Parameter::setRelativeTo
            inline PMapped& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }
            /// \brief Fluent overload of TValueParameter::defval
            inline PMapped& defval(T defaultValue, bool req = false){ return setDefaultValue(defaultValue, req); }
            /// \brief Fluent overload of TValueParameter::setDefaultValue
            virtual PMapped& setDefaultValue(T value, bool req = false){
                TValueParameter<T>::setDefaultValue(value, req); return *this; }

            virtual PMapped& addToMap(string name, T value)
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                string strLowerName = name;
                std::transform(strLowerName.begin(), strLowerName.end(), strLowerName.begin(), ::tolower);
                mMap.insert(std::pair<string, T>(strLowerName, value));
                mOriginalMap.insert(std::pair<string, T>(name, value));
                return *this;
            }
            virtual PMapped& setMap(std::initializer_list<std::pair<the::string, T>> map)
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                clearMap();

                for (auto it=map.begin(); it!=map.end(); it++)
                {
                    addToMap(it->first, it->second);
                }
                return *this;
            }
            virtual PMapped& setMap(std::map<the::string, T> map)
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                clearMap();

                for (auto it=map.begin(); it!=map.end(); it++)
                {
                    addToMap(it->first, it->second);
                }
                return *this;
            }
            // ----------------------------------------------------

            const std::map<string, T>& getOriginalMap()
            {
                return mOriginalMap;
            }

        protected:
            std::map<the::string, T> mMap;
            // Stores values in original letters register for nice serialization.
            std::map<the::string, T> mOriginalMap;

            virtual bool onParse(string& strValue) override
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                string strLowerValue = strValue;
                std::transform(strLowerValue.begin(), strLowerValue.end(), strLowerValue.begin(), ::tolower);
                auto it = mMap.find(strLowerValue);
                if (it != mMap.end())
                {
                    this->setValue(it->second);
                    return true;
                }
                else
                {
                    return false;
                }
            }
    };

    class PColor: public ParameterGroup
    {
        public:
            PInt a;
            PInt r;
            PInt g;
            PInt b;

            PColor();
            virtual ~PColor();
            virtual ParameterTypes getType() const;
    };

    class PTimeSpan: public ParameterGroup
    {
        public:
            PInt h;
            PInt m;
            PInt s;
            PInt ms;

            PTimeSpan();
            virtual ~PTimeSpan();

            float getTotalHours() const;
            float getTotalMinutes() const;
            float getTotalSeconds() const;
            int getTotalMiliseconds() const;

            virtual ParameterTypes getType() const;
    };

    class PDate: public ParameterGroup
    {
        public:
            PInt year;
            PInt month;
            PInt day;

            PDate();
            virtual ~PDate();
            virtual ParameterTypes getType() const;
    };

    class PTime: public ParameterGroup
    {
        public:
            PInt h;
            PInt m;
            PInt s;
            PInt ms;

            PTime();
            virtual ~PTime();
            virtual ParameterTypes getType() const;
    };

    class PTimeStamp: public ParameterGroup
    {
        public:
            PInt year;
            PInt month;
            PInt day;
            PInt h;
            PInt m;
            PInt s;
            PInt ms;

            PTimeStamp();
            virtual ~PTimeStamp();
            virtual ParameterTypes getType() const;
    };

    class PPoint: public ParameterGroup
    {
        public:
            PFloat x;
            PFloat y;

            PPoint();
            virtual ~PPoint();
            void set(float x, float y);
            virtual ParameterTypes getType() const;

            // Fluent support ----------------------------------
            /// \brief Fluent overload of Parameter::setXmlName
            inline PPoint& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
            /// \brief Fluent overload of Parameter::setRequired
            inline PPoint& setRequired(bool required) { Parameter::setRequired(required); return *this; }
            /// \brief Fluent overload of Parameter::setLocal
            inline PPoint& setLocal(Local local) { Parameter::setLocal(local); return *this; }
            /// \brief Fluent overload of Parameter::setRelativeTo
            inline PPoint& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }


            inline PPoint& defval(float defaultX, float defaultY, bool req = false){
                return setDefaultValue(defaultX, defaultY, req); }
            inline PPoint& setDefaultValue(float defaultX, float defaultY, bool req = false){
                x.setDefaultValue(defaultX, req); y.setDefaultValue(defaultY, req); return *this; }
            // ----------------------------------------------------

    };
    class PPosition: public PPoint
    {
        public:
            virtual ~PPosition();
            virtual ParameterTypes getType() const;
    };
    class PVector: public PPoint
    {
        public:
            virtual ~PVector();
            virtual ParameterTypes getType() const;
    };
    class PSize: public ParameterGroup
    {
        public:
            PFloat width;
            PFloat height;

            PSize();
            virtual ~PSize();
            virtual ParameterTypes getType() const;
            void set(float width, float height);

            // Fluent support ----------------------------------
            /// \brief Fluent overload of Parameter::setXmlName
            inline PSize& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
            /// \brief Fluent overload of Parameter::setRequired
            inline PSize& setRequired(bool required) { Parameter::setRequired(required); return *this; }
            /// \brief Fluent overload of Parameter::setLocal
            inline PSize& setLocal(Local local) { Parameter::setLocal(local); return *this; }
            /// \brief Fluent overload of Parameter::setRelativeTo
            inline PSize& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }


            inline PSize& defval(float defaultWidth, float defaultHeight, bool req = false){
                return setDefaultValue(defaultWidth, defaultHeight, req); }
            inline PSize& setDefaultValue(float defaultWidth, float defaultHeight, bool req = false){
                width.setDefaultValue(defaultWidth, req); height.setDefaultValue(defaultHeight, req); return *this; }
            // ----------------------------------------------------


    };


    class PRadius: public PFloat
    {
        public:
            virtual ~PRadius();
            virtual ParameterTypes getType() const;
            void operator=(float val);
    };
    class PAngle: public PFloat
    {
        public:
            virtual ~PAngle();
            virtual ParameterTypes getType() const;
            void operator=(float val);
    };
    class PPercent: public PFloat
    {
        public:
            virtual ~PPercent();
            virtual ParameterTypes getType() const;
            void operator=(float val);
    };
    class PMedia: public PString
    {
        public:
            virtual ~PMedia();
            virtual ParameterTypes getType() const;
            virtual the::string getValue() const override;
            void operator=(string val);
    };

    class PRect: public ParameterGroup
    {
         public:
            PPoint leftLower;
            PPoint rightUpper;

            PRect();
            virtual ~PRect();

            virtual ParameterTypes getType() const;

            virtual void set(const Vec2& leftDown, const Vec2& rightTop);
            virtual float left() const;
            virtual float right() const;
            virtual float top() const;
            virtual float bottom() const;
            virtual void setLeft(float val);
            virtual void setRight(float val);
            virtual void setTop(float val);
            virtual void setBottom(float val);

            // Fluent support ----------------------------------
            /// \brief Fluent overload of Parameter::setXmlName
            inline PRect& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
            /// \brief Fluent overload of Parameter::setRequired
            inline PRect& setRequired(bool required) { Parameter::setRequired(required); return *this; }
            /// \brief Fluent overload of Parameter::setLocal
            inline PRect& setLocal(Local local) { Parameter::setLocal(local); return *this; }
            /// \brief Fluent overload of Parameter::setRelativeTo
            inline PRect& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }


            inline PRect& defval(Vec2 leftDownDefault, Vec2 rightTopDefault, bool req = false){
                return setDefaultValue(leftDownDefault, rightTopDefault, req); }
            inline PRect& setDefaultValue(Vec2 leftDownDefault, Vec2 rightTopDefault, bool req = false){
                leftLower.setDefaultValue(leftDownDefault.x, leftDownDefault.y, req);
                rightUpper.setDefaultValue(rightTopDefault.x, rightTopDefault.y, req); return *this; }
            // ----------------------------------------------------
    };

    template<typename ParamT>
    class PParamList: public ParameterList
    {
        public:

            PParamList(string itemTagName): ParameterList(itemTagName)
            {
            }

            PParamList(): ParameterList()
            {
            }

            virtual ~PParamList()
            {
            }

            virtual std::shared_ptr<Parameter> createItem()
            {
                return std::shared_ptr<Parameter>(new ParamT());
            }

            virtual std::vector<ParamT*> getTypedItems() const
            {
                std::vector<ParamT*> list;
                for(auto &p: getItems())
                {
                    list.push_back(static_cast<ParamT*>(p));
                }
                return list;
            }
    };

    class PPolygon: public PParamList<PPoint>
    {
        public:
            PPolygon(string pointTagName);
            PPolygon();
            virtual ~PPolygon();
            virtual ParameterTypes getType() const;
            virtual void addPoint(float x, float y);
            virtual std::vector<PPoint*> getPoints() const;
    };


    template<typename ValueT>
    class PValueList: public ParameterList
    {
        public:

            PValueList(string itemTagName): ParameterList(itemTagName)
            {
            }

            PValueList(): ParameterList("Value")
            {
            }

            virtual ~PValueList()
            {
            }

            ValueT getValue(int index) const
            {
                TH_ASSERT(0 <= index && index <= ParameterList::getItems().size());
                Parameter* p = ParameterList::getItems()[index];
                return static_cast<TValueParameter<ValueT>*>(p)->getValue();
            }

            ValueT operator[](int index) const
            {
                return getValue(index);
            }

            void addValue(ValueT value)
            {
                ASSERT(!this->getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                auto item = createDefaultParameter<ValueT>();
                item->setParent(this, value, mItemTagName);
                ParameterList::addItem(item);
            }

            std::vector<ValueT> getValues() const
            {
                std::vector<ValueT> values;
                for(auto &param : ParameterList::getItems())
                {
                    values.push_back(static_cast<TValueParameter<ValueT>*>(param)->getValue());
                }
                return values;
            }

            virtual std::shared_ptr<Parameter> createItem()
            {
                return createDefaultParameter<ValueT>();
            }
    };

    struct PBitMask: ParameterGroup
    {
        PUInt mask;

        PBitMask();
        virtual ~PBitMask();

        virtual bool getBit(int number) const;
        virtual void setBit(int number);
        virtual void resetBit(int number);

        virtual TiXmlElement* serialize();
        virtual void readDataFrom(const Parameter* src);
        virtual ParameterTypes getType() const;

        // Fluent support ----------------------------------
        /// \brief Fluent overload of Parameter::setXmlName
        inline PBitMask& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
        /// \brief Fluent overload of Parameter::setRequired
        inline PBitMask& setRequired(bool required) { Parameter::setRequired(required); return *this; }
        /// \brief Fluent overload of Parameter::setLocal
        inline PBitMask& setLocal(Local local) { Parameter::setLocal(local); return *this; }
        /// \brief Fluent overload of Parameter::setRelativeTo
        inline PBitMask& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }

        inline PBitMask& defval(uint32 defaultMask, bool req = false){
            return setDefaultValue(defaultMask, req); }
        inline PBitMask& setDefaultValue(uint32 defaultMask, bool req = false){
            mask.setDefaultValue(defaultMask, req); return *this; }
        // ----------------------------------------------------
        protected:
            virtual std::list<ParsingError> onParse(TiXmlElement* element);

    };

    struct PFlagMask: PBitMask
    {
        PFlagMask();
        virtual ~PFlagMask();


        virtual void removeFlagName(uint32 mask);
        virtual const std::map<string, uint32>& getFlagsNames() const;

        virtual void appendFlags(uint32 mask);
        virtual void takeAwayFlags(uint32 mask);
        virtual void clearFlags();

        virtual TiXmlElement* serialize();
        virtual void readDataFrom(const Parameter* src);
        virtual ParameterTypes getType() const;

        // Fluent support ----------------------------------
        /// \brief Fluent overload of Parameter::setXmlName
        inline PFlagMask& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
        /// \brief Fluent overload of Parameter::setRequired
        inline PFlagMask& setRequired(bool required) { Parameter::setRequired(required); return *this; }
        /// \brief Fluent overload of Parameter::setLocal
        inline PFlagMask& setLocal(Local local) { Parameter::setLocal(local); return *this; }
        /// \brief Fluent overload of Parameter::setRelativeTo
        inline PFlagMask& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }
        /// \brief Fluent overload of PBitMask::defval
        inline PFlagMask& defval(uint32 defaultMask, bool req = false){
            return setDefaultValue(defaultMask, req); }
        /// \brief Fluent overload of PBitMask::setDefaultValue
        inline PFlagMask& setDefaultValue(uint32 defaultMask, bool req = false){
            PBitMask::setDefaultValue(defaultMask, req); return *this; }

        virtual PFlagMask& addFlagName(string name, uint32 mask);
        // ----------------------------------------------------

        protected:
           PString flags;
           std::map<string, uint32> mFlags;

           virtual std::list<ParsingError> onParse(TiXmlElement* element);
    };
}
