////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#define TIXML_USE_STL

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <string>
#include <memory>
#include <float.h>
#include <stdarg.h>

#include <The2D/Utils/TinyFormat.hpp>

////////////////////////////////////////////////////////////
/// \namespace the Themisto main namespace
///
/// \defgroup Utils
/// \defgroup Core3
////////////////////////////////////////////////////////////
namespace the
{
    typedef std::string string;

    /// \brief Shortening for eastl::shared_ptr.
    template<typename T>
    using sptr = std::shared_ptr<T>;

    template<typename T>
    inline sptr<T> make_sptr(T* ptr)
    {
        return sptr<T>(ptr);
    }

    // TODO:
    // Replace with ConfigManager
    extern bool __THE_RUN_SIMULATION;
};

////////////////////////////////////////////////////////////
#ifndef NONPC_BUILD_PLATFORM
    #define PC_PLATFORM_BUILD
    #include <iostream>
#else
    #include <android/log.h>
#endif

#ifdef _DEBUG
    #ifndef DEBUG
        #define DEBUG 1
    #endif
#endif

#ifndef NDEBUG
    #ifndef DEBUG
        #define DEBUG
    #endif
#endif

#ifdef _WIN32
    #ifndef WIN32
        #define WIN32
    #endif
#endif

#ifdef _WIN64
    #ifndef WIN64
        #define WIN64
    #endif
#endif

#if defined linux || defined __linux || defined __linux__
    #ifndef NIX
        #define NIX 1
    #endif

    #if defined __x86_64 || defined __amd64 || defined __x86_64__
        #define NIX64 1
    #endif
#endif
#ifdef WIN32 // WINDOWS DEFINES
    #include <sys/stat.h>
    namespace the
    {
        typedef unsigned char uint8;
        typedef char int8;
        typedef unsigned short uint16;
        typedef short int16;
        typedef unsigned int uint32;
        typedef int int32;
        typedef unsigned long long uint64;
        typedef long long int64;
		typedef unsigned int uint;
    };

	#define M_PI 3.14159265359

    #define DIRS_STR "\\"
    #define DIRS_CHAR '\\'
    #define DIRS_WSTR L"\\"
    #define DIRS_WCHAR L'\\'

    #define CEXPORT __declspec(dllexport)
    #define CIMPORT __declspec(dllimport)
#endif

#ifdef NIX // LINUX
    // #include <sys/types.h>
    // #include <sys/stat.h>
    // #include <sys/un.h>
    // #include <sys/sysinfo.h>

    #include <unistd.h>
    #include <limits.h>

    #include <stdint.h>
    // #include <stdarg.h>
    // #include <strings.h>

    // #include <fstream>
    #include <sstream>
    // #include <regex>
    // #include <string>

    namespace the
    {
        typedef uint8_t uint8;
        typedef int8_t int8;
        typedef uint16_t uint16;
        typedef int16_t int16;
        typedef uint32_t uint32;
        typedef int32_t int32;
        typedef uint64_t uint64;
        typedef int64_t int64;
    }

    #define DIRS_STR "/"
    #define DIRS_CHAR '/'
    #define DIRS_WSTR L"/"
    #define DIRS_WCHAR L'/'

    #define CEXPORT __attribute__((visibility("default")))
    #define CIMPORT
#endif

//#include <iostream>
//#include <conio.h>
#include <iosfwd>
#include <vector>

namespace the
{
    const float PixelsPerMeter = 100.0f;               ///< How many pixels are there in one game meter
    const float Gravity = 9.81 * PixelsPerMeter;       ///< Gravity power, calculated as real one
    const float TimeKoef = 300.0f;                     ///< Game speed time, higher - faster
    const float Pi = 3.1415926535898f;                 ///< Math constant

    // This isn't const because could be changed with sf::ResizeEvent
    const int ScreenSizeX = 1152;   ///< Window screen width in pixels
    const int ScreenSizeY = 864;    ///< Window screen height in pixels

    const float GameVersion = 0.1;  ///< Version of application (used in scene loader to check levels compability)

    ////////////////////////////////////////////////////////////
    /// \brief Conver number of game meters into screen pixels
    ///
    /// \param meters  Number of pixels
    ////////////////////////////////////////////////////////////
    inline float Meters2Pixels(float meters) { return meters * PixelsPerMeter; }

    ////////////////////////////////////////////////////////////
    /// \brief Conver number of scren pixels into game meters
    ///
    /// \param pixels  Number of meters
    ////////////////////////////////////////////////////////////
    inline float Pixels2Meters(float pixels) { return pixels / PixelsPerMeter; }

    ////////////////////////////////////////////////////////////
    /// \brief Math function which converts radians into degrees
    ///
    /// \param rad  Number of radians to convert
    ////////////////////////////////////////////////////////////
    inline float Rad2Deg(float rad) { return rad * 57.2957795130823; }

    ////////////////////////////////////////////////////////////
    /// \brief Math function which converts degrees into radians
    ///
    /// \param rad  Number of degrees to convert
    ////////////////////////////////////////////////////////////
    inline float Deg2Rad(float deg) { return deg * 0.0174532925199433; }

    //---------- Real time → game time

    ////////////////////////////////////////////////////////////
    /// \brief How many game-ms is in passed real microseconds
    ///
    /// \param ms  Microseconds from real time into game one
    ////////////////////////////////////////////////////////////
    inline float GameMicroseconds(float ms) { return ms * TimeKoef; }

    ////////////////////////////////////////////////////////////
    /// \brief How many game-seconds is in passed real microseconds
    ///
    /// \param ms  Microseconds from real time into game seconds
    ////////////////////////////////////////////////////////////
    inline float GameSeconds(float ms) { return GameMicroseconds(ms) / 1000.0f; }

    ////////////////////////////////////////////////////////////
    /// \brief How many game-minutes is in passed real microseconds
    ///
    /// \param ms  Microseconds from real time into game minutes
    ////////////////////////////////////////////////////////////
    inline float GameMinutes(float ms) { return GameSeconds(ms) / 60.0f; }

    ////////////////////////////////////////////////////////////
    /// \brief How many game-hours is in passed real microseconds
    ///
    /// \param ms  Microseconds from real time into game hours
    ////////////////////////////////////////////////////////////
    inline float GameHours(float ms) { return GameMinutes(ms) / 60.0f; }

    //---------- Real time → game time

    ////////////////////////////////////////////////////////////
    /// \brief Get number of game ms to achive passed real minutes
    ///
    /// \param realMs  Microseconds from real time into game seconds
    ////////////////////////////////////////////////////////////
    inline float Microseconds2Seconds(float realMs) { return realMs * TimeKoef / 1000.0f; }

    ////////////////////////////////////////////////////////////
    /// \brief Get number of game seconds to achive passed real minutes
    ///
    /// \param realSeconds  Seconds from real time into game seconds
    ////////////////////////////////////////////////////////////
    inline float Seconds2Seconds(float realSeconds) { return realSeconds * TimeKoef; }

    ////////////////////////////////////////////////////////////
    /// \brief Get number of game seconds to achive passed real minutes
    ///
    /// \param realMinutes  Minutes from real time into game seconds
    ////////////////////////////////////////////////////////////
    inline float Minutes2Seconds(float realMinutes) { return Seconds2Seconds(realMinutes * 60.0f); }

    ////////////////////////////////////////////////////////////
    /// \brief Get number of game seconds to achive passed real hours
    ///
    /// \param realHours  Hours from real time into game seconds
    ////////////////////////////////////////////////////////////
    inline float Hours2Seconds(float realHours) { return Minutes2Seconds(realHours * 60.0f); }

    inline std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string> &elems)
    {
        // Got from http://stackoverflow.com/a/236803/486516
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim))
        {
            elems.push_back(item);
        }
        return elems;
    }

    inline std::vector<std::string> split(const std::string &s, char delim)
    {
        // Got from http://stackoverflow.com/a/236803/486516
        std::vector<std::string> elems;
        return split(s, delim, elems);
    }

    /// \brief Stub for not implemeted methods.
    /// \note Macro do smth only if DEBUG defined.
    /// \note On linux macro prints callstack.
    #define TH_NOT_IMPLEMENTED ASSERT(false, "Not implemented");

    /// \brief Runtime fail with SIGSEV. Use to mark unreachable blocks of code.
    /// \param msg Some comment from developer to user, why this code can't be reached.
    /// \note Macro do smth only if DEBUG defined.
    /// \note On linux macro prints callstack.
    #define TH_FAIL(msg) ASSERT(false, msg)

    /// \brief ID list of the classes of engine.
    enum EngineClassesID: uint8
    {
        MessageCoreID,
        AttachedID,
        ManagerID,
        ComponentID,
        BindID
    };

    /// \brief Engine indetificator, wich uses in messaging.
    const uint16 ENGINE_ID = 1;

    /// \brief Macro, to declare component type indetificator.
    /// \param id ID of component. Should be unical inside library.
    /// \param ParentClass Name of base class of component.
    #define MY_ID(id, ParentClass) static const the::uint8 ID = id; \
                                   virtual the::TypeID getTypeId(){ return the::TypeID(ID, ParentClass::getTypeId()); }

    /// \brief Macro, wich helps to declare XXX_MSG macros in libraries.
    /// \param MsgID ID of the message. Must be unical inside component.
    /// \param LibID ID of the library. Each library must have unical indetificator.
    #define LIB_MSG(MsgID, LibID) static const the::uint32 MsgID = ((((((the::uint32)LibID) << 8) | ID) << 8) | (the::uint8)(__COUNTER__+1));

    /// \brief Macro, which helps to declare messages in engine.
    /// \param MsgID ID of the message. Must be unical inside the class.
    #define ENGINE_MSG(MsgID) LIB_MSG(MsgID, ENGINE_ID)

    /// \brief Uses std::find to determine, if val contains in arr.
    /// \note arr must have begin() and end() methods.
    #define CONTAINS(arr, val) (std::find(arr.begin(), arr.end(), val) != arr.end())

    #define CASE(variant) case variant: {
    #define END_CASE } break;

    #define THE_EPSILON FLT_EPSILON

    #define THE_NOT_USED(x) ((void)(x))
};
