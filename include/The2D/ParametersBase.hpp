////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

#include <vector>

#include <The2D/Config.hpp>
#include <The2D/IXmlParser.hpp>
#include <The2D/LogService.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/Utils/Utils.hpp>
#include <The2D/Utils/Signals.hpp>

#define NODE_VIEWED ((void*)42)

namespace the
{
    /// \brief Awailable parameter types, that has special representation in GUI.
    enum ParameterTypes
    {
        /// \brief Init data of some GameEntity.
        GAME_ENTITY_INIT_DATA_PARAM,
        /// \brief Floating point value.
        FLOAT_PARAM,
        /// \brief Floating point value with high precission.
        DOUBLE_PARAM,
        /// \brief Integer value.
        INT_PARAM,
        /// \brief Logical value.
        BOOL_PARAM,
        /// \brief Text value.
        STRING_PARAM,
        /// \brief The set of predefined values.
        /// Parameters of this types must inherit from MappedValueParameter.
        MAPPED_PARAM,
        /// \brief Parameter group that contains two children parameters.
        PAIR_PARAM,
        /// \brief Parameter group that contains three children parameters.
        TRIAD_PARAM,
        /// \brief Parameter group that contains four children parameters.
        QUAD_PARAM,
        /// \brief Color value, represented in ARGB model.
        COLOR_PARAM,
        /// \brief Time interval value.
        TIME_SPAN_PARAM,
        /// \brief Time represented by four values: hour, minute, second and milisecond.
        TIME_PARAM,
        /// \brief Date represented by three values: year, month and day.
        /// Year numbering from borning of the Chirst.
        DATE_PARAM,
        /// \brief Date and time.
        /// Date represented by three values: year, month and day.
        /// Time by four values: hour, minute, second and milisecond.
        TIME_STAMP_PARAM,
        /// \brief Pair of the float values, representing point in 2D cartesian coordinat system.
        /// \note Coordinate values are in pixels.
        POINT_PARAM,
        /// \brief Pair of the float values, representing position of the GameEntity in world.
        /// \note Coordinate values are in pixels.
        POSITION_PARAM,
        /// \brief Pair of the float values, representing vector in 2D cartesian coordinat system.
        /// \note Coordinate values are in pixels.
        VECTOR_PARAM,
        /// \brief Pair of the float values, representing size of the game entity.
        /// \note Values are in pixels.
        SIZE_PARAM,
        /// \brief Radius for the game entities, that has circle form.
        /// \note Coordinate values are in pixels.
        RADIUS_PARAM,
        /// \brief Angle in grades.
        ANGLE_PARAM,
        /// \brief Float value in range [0, 100]
        PERCENT_PARAM,
        /// \brief Path to the some kind of resource, like image, sound, font.
        /// Param has string format. Its value determined by resource system.
        MEDIA_PARAM,
        /// \brief Consist of two POSITION_PARAMs - leftDown and rightTop, representing corners of a rectangle.
        RECTANGLE_PARAM,
        /// \brief Any count of some parameters.
        LIST_PARAM,
        /// \brief Sequence of the points, that describes some polygon.
        POLYGON_PARAM,
        /// \brief Set of 32 bits.
        BIT_MASK_PARAM,
        /// \brief Set of flags, that detemines bits in some bitmask.
        FLAG_MASK_PARAM,
        /// \brief Binding to some entity in game tree.
        BIND_PARAM,
        /// \brief Unknown type of parameter group.
        /// \note Parameters of this type must derive from ParameterGroup.
        USER_GROUP_PARAM,
        /// \brief Unknown type of value parameter.
        /// \note Parameters of this type must derive from ValueParameter.
        USER_VALUE_PARAM
    };

    enum Local
    {
        LOCAL,
        NOT_LOCAL
    };

    class GameEntity;

    /// \brief Base class for all parameter types.
    /// \warning Do not use it as a base class. To create your own parameter types derive from ValueParameter and ParameterGroup.
    class Parameter: private NonCopyable<Parameter>
    {
        public:
            /// \brief Enables saving xml data inside parameter after parsing (for debug purposes).
            /// If this value is true (default), then ParameterGroup::getParsedXml and ValueParameter::getParsedXml
            /// will return parsed xml data.
            static bool StoreParsedXml;

            virtual ~Parameter();

            /// \brief Determine, whether parameter can be modified.
            /// \note Parameter can't be changed after game entity
            ///       initialization until its deinitialization.
            virtual bool getLocked() const;


            /// \brief Get parent parameter group.
            /// \return Parameter group, where this parameter is a member.
            virtual Parameter* getParent() const;

            virtual void setParent(Parameter* p);

            virtual GameEntity* tryFindParentEntity() const;

            /// \brief Is it instance of class derived from ParameterGroup, or from ValueParameter.
            virtual bool isGroup() const = 0;
            /// \brief Get meanable for GUI type of parameter.
            virtual ParameterTypes getType() const = 0;

            virtual void readDataFrom(const Parameter* src) = 0;

            // Fluent support ----------------------------------
            /// \brief Set xml name of the xml element or attribute
            /// \return Name of the xml element or attribute.
            Parameter& setXmlName(string name);
            /// \brief Set importance of the parameter.
            Parameter& setRequired(bool required);
            /// \brief Note users, that parameter has relative value.
            /// \see setRelativeTo
            Parameter& setLocal(Local local);
            /// \brief Parameter, relative what this parameter, or some of its child has a value.
            Parameter& setRelativeTo(Parameter* relativeTo);
            // ----------------------------------------------------

            /// \brief Disable checking for unregistered parameter.
            void setNoParent();

            string formXmlName(string fieldName) const;

            /// \brief Get name of the xml element or attribute for this parameter.
            /// \return Name of the xml element or attribute.
            string getXmlName() const;
            /// \brief Determine, whether parameter value must be specified.
            /// \return If the true, then paramter has no acceptable default value.
            bool getRequired() const;
            bool getLocal();
            Parameter* getRelativeTo();

            void setParsed(bool parsed);
            bool getParsed() const;

        protected:
            string mXmlName;
            Parameter* mParent;
            Parameter* mRelativeTo;
            bool mNoParent;
            bool mRequired;
            bool mLocal;
            bool mHadParent;
            bool mParsed;

            Parameter* findRelativeToRec(Parameter* current);
            bool findLocalRec(Parameter* current);
        private:
            Parameter();
            friend class ValueParameter;
            friend class ParameterGroup;

    };

    /// \brief Base class for parameters, that stores simple value.
    /// Value is simple if it repesented by single row in xml murkup.
    /// Complex values represented by several xml elements or attributes.
    class ValueParameter: public Parameter
    {
        public:
            /// \brief Signal raises after parameter parsing (in postParse method).
            /// In callback you can look, how parameter parsed xml, and if needed,
            /// override parsing result. Feel free to change parsedOk, new value will be interpreted as a result.
            Signal<void (string& strVal, bool& parsedOk)> SigParsed;

            ValueParameter();
            virtual ~ValueParameter();


            /// \brief Get current parameter value as a string.
            virtual string serialize() const = 0;

            /// \brief Parse string value.
            /// \note For overriding parsing logic use onParse method.
            virtual bool parse(string& strValue) final;

            virtual bool isGroup() const;
            virtual void reset() = 0;
            virtual bool containsValue() const = 0;
            virtual bool modified() const = 0;
            virtual ParameterTypes getType() const;

            string getParsedXml();

        protected:
            string mParsedXml;
            bool mInOnParse;

            /// \brief Parse value from string and set it as a current parameter value.
            /// \return Do parsing string had done successfuly.
            virtual bool onParse(string& strValue) = 0;
            virtual void postParse(string& strVal, bool& parsedOk);

    };

    class ParameterGroup: public Parameter, public IXmlParser
    {
        public:
            /// \brief Signal raises after parameter parsing (in postParse method).
            /// In callback you can look, how parameter parsed xml, and if needed,
            /// override parsing result. Feel free to change errors, new value will be interpreted as a result.
            Signal<void (TiXmlElement* elem, std::list<ParsingError>& errors)> SigParsed;

            ParameterGroup();
            virtual ~ParameterGroup();

            virtual bool isGroup() const;
            virtual ParameterGroup* getParentGroup() const;
            virtual const std::vector<Parameter*>& getItems() const;
            virtual void addItem(Parameter* item);
            virtual void addItem(std::shared_ptr<Parameter> param);
            virtual void removeItem(Parameter* item);
            virtual void clear();
            virtual void resetAll();
            virtual ParameterTypes getType() const;

            /// \brief REGISTER macro support
            virtual void registerChild(Parameter* p, string xmlName);
            /// \brief UNREGISTER macro support
            virtual void unregisterChild(Parameter* p);


            virtual bool isMyTag(TiXmlElement *element);
            virtual TiXmlElement* serialize();

            /// \brief Parse xml data.
            /// \note For overriding parsing logic use onParse method.
            virtual std::list<the::ParsingError> parse(TiXmlElement* el) final;

            virtual void readDataFrom(const Parameter* src);

            const std::list<ParsingError>& getParsingErrors();
            std::shared_ptr<const TiXmlElement> getParsedXml();

        protected:
            std::vector<Parameter*> mItems;
            std::vector<std::shared_ptr<Parameter>> mStoredItems;
            std::list<ParsingError> mParsingErrors;
            std::shared_ptr<const TiXmlElement> mParsedXml;
            bool mInOnParse;

            virtual std::list<ParsingError> onParse(TiXmlElement* element);
            virtual void postParse(TiXmlElement* el, std::list<ParsingError>& err);

    };

    class GameEntityInitData: public ParameterGroup
    {
        public:
            GameEntityInitData(GameEntity* parent);
            virtual ~GameEntityInitData();
            virtual ParameterTypes getType() const;
            virtual bool getLocked() const;
            virtual GameEntity* tryFindParentEntity() const;
            virtual void setParentEntity(GameEntity* ent);

            virtual TiXmlElement* serialize();

        protected:
            GameEntity* mParentEntity;

            virtual std::list<ParsingError> onParse(TiXmlElement* element);
    };

    template<typename T>
    class TValueParameter: public ValueParameter
    {
        public:
            Signal<void (T& newVal)> SigValueChanged;

            TValueParameter(): mContainsValue(false), mHasDefaultValue(false), mModified(false)
            {
                setRequired(true);
            }

            virtual ~TValueParameter()
            {
            }

            virtual bool containsValue() const
            {
                return mContainsValue;
            }
            virtual bool modified() const
            {
                return mModified;
            }
            virtual bool hasDefaultValue() const
            {
                return mHasDefaultValue;
            }
            virtual T operator()() const
            {
                return getValue();
            }
            virtual T getValue() const
            {
                ASSERT(containsValue(), "Parameter does not contain value");
                return mValue;
            }
            virtual T getDefaultValue() const
            {
                ASSERT(hasDefaultValue(), "Parameter does not have default value");
                return mDefaultValue;
            }

            virtual void setValue(T val)
            {
                ASSERT(!getLocked(), "Parameter is locked. Value can't be changed right now.");

                mModified = true;
                mContainsValue = true;
                mValue = val;

                SigValueChanged(mValue);
            }
            virtual void reset()
            {
                ASSERT(!getLocked(), "Parameter is locked. Value can't be changed right now.");

                mModified = false;
                mContainsValue = mHasDefaultValue;
                mValue = mDefaultValue;

                SigValueChanged(mValue);
            }



            operator T() const
            {
                return getValue();
            }

            void operator=(T val)
            {
                ASSERT(!getLocked(), "Parameter is locked. It can't be changed right now.");
                setValue(val);
            }
            bool operator==(T val) const
            {
                return getValue() == val;
            }
            bool operator!=(T val) const
            {
                return getValue() != val;
            }

            void operator=(const TValueParameter<T>& val)
            {
                ASSERT(!getLocked(), "Parameter is locked. It can't be changed right now.");
                setValue(val);
            }
            bool operator==(const TValueParameter<T>& val) const
            {
                return getValue() == val;
            }
            bool operator!=(const TValueParameter<T>& val) const
            {
                return getValue() != val;
            }

            virtual void readDataFrom(const Parameter* src)
            {
                ASSERT(!src->isGroup(), "Value parameter can't read data from group parameter.");
                ASSERT(dynamic_cast<const TValueParameter<T>*>(src) != 0, "Invalid source parameter type.");
                const TValueParameter<T>* srcParam = static_cast<const TValueParameter<T>*>(src);
                mContainsValue = srcParam->mContainsValue;
                mHasDefaultValue = srcParam->mHasDefaultValue;
                mModified = srcParam->mModified;
                mDefaultValue = srcParam->mDefaultValue;
                mValue = srcParam->mValue;

                SigValueChanged(mValue);
            }

            // Fluent support ----------------------------------
            /// \brief Fluent overload of Parameter::setXmlName
            inline TValueParameter<T>& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
            /// \brief Fluent overload of Parameter::setRequired
            inline TValueParameter<T>& setRequired(bool required) { Parameter::setRequired(required); return *this; }
            /// \brief Fluent overload of Parameter::setLocal
            inline TValueParameter<T>& setLocal(Local local) { Parameter::setLocal(local); return *this; }
            /// \brief Fluent overload of Parameter::setRelativeTo
            inline TValueParameter<T>& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }

            inline TValueParameter<T>& defval(T defaultValue, bool req = false){ return setDefaultValue(defaultValue, req); }
            virtual TValueParameter<T>& setDefaultValue(T value, bool req = false)
            {
                ASSERT(!getLocked(), "Parent entity is initialized, parameter can't be changed right now.");

                // If we call setDefaultValue several times, mValue should contains value of the last call
                if (mHasDefaultValue && mContainsValue && mValue == mDefaultValue)
                {
                    // Value is set by previous call of setDefaultValue, so reset it
                    mContainsValue = false;
                }
                mDefaultValue = value;
                mHasDefaultValue = true;
                setRequired(req);
                if (!mContainsValue)
                {
                    mValue = mDefaultValue;
                    mContainsValue = true;

                    SigValueChanged(mValue);
                }
                return *this;
            }
            // ----------------------------------------------------

        protected:

            bool mContainsValue;
            bool mHasDefaultValue;
            bool mModified;
            T mDefaultValue;
            T mValue;
    };

    template<typename T>
    std::ostream& operator << (std::ostream &o,const TValueParameter<T> &a)
    {
        return o << a.getValue();
    }

    class ParameterList: public ParameterGroup
    {
        public:
            ParameterList(string itemTagName);
            ParameterList();
            virtual ~ParameterList();

            virtual string getItemTagName();

            virtual int size() const;

            virtual void readDataFrom(const Parameter* src);

            virtual std::shared_ptr<Parameter> createItem() = 0;

            virtual ParameterTypes getType() const;

            // Fluent support ----------------------------------
            /// \brief Fluent overload of Parameter::setXmlName
            inline ParameterList& setXmlName(string name) { Parameter::setXmlName(name); return *this; }
            /// \brief Fluent overload of Parameter::setRequired
            inline ParameterList& setRequired(bool required) { Parameter::setRequired(required); return *this; }
            /// \brief Fluent overload of Parameter::setLocal
            inline ParameterList& setLocal(Local local) { Parameter::setLocal(local); return *this; }
            /// \brief Fluent overload of Parameter::setRelativeTo
            inline ParameterList& setRelativeTo(Parameter* relativeTo) { Parameter::setRelativeTo(relativeTo); return *this; }

            virtual ParameterList& setItemTagName(string itemTagName);
            // ----------------------------------------------------

        protected:
            string mItemTagName;

            virtual std::list<ParsingError> onParse(TiXmlElement* el);
    };

    class MappedValueParameter
    {
        public:
            virtual ~MappedValueParameter();
            virtual std::list<string> getMappedValues() = 0;
    };

}
