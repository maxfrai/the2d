////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <cstring>
#include <The2D/Utils/TinyFormat.hpp>
#include <The2D/Utils/Stacktrace.h>

#include <The2D/Utils/Enet/enet.h>
#include <thread>
#include <chrono>
#include <list>
#include <mutex>
#include <deque>

// \brief This macros allow us to remove full path and leave only filename
#define THE_FILE (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

// \brief cross compiler definition of current function
#if defined(__GNUC__) || (defined(__MWERKS__) && (__MWERKS__ >= 0x3000)) || (defined(__ICC) && (__ICC >= 600))
    #define THE_CURRENT_FUNCTION __PRETTY_FUNCTION__
#elif defined(__DMC__) && (__DMC__ >= 0x810)
    #define THE_CURRENT_FUNCTION __PRETTY_FUNCTION__
#elif defined(__FUNCSIG__)
    #define THE_CURRENT_FUNCTION __FUNCSIG__
#elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) || (defined(__IBMCPP__) && (__IBMCPP__ >= 500))
    #define THE_CURRENT_FUNCTION __FUNCTION__
#elif defined(__BORLANDC__) && (__BORLANDC__ >= 0x550)
    #define THE_CURRENT_FUNCTION __FUNC__
#elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901)
    #define THE_CURRENT_FUNCTION __func__
#else
    #define THE_CURRENT_FUNCTION "(unknown)"
#endif

// #define NUMARGS(...)  (sizeof((int[]){0, ##__VA_ARGS__})/sizeof(int)-1)
// #define COUNT_ARGS(...) COUNT_ARGS_(,##__VA_ARGS__,6,5,4,3,2,1,0)
// #define COUNT_ARGS_(z,a,b,c,d,e,f,cnt,...) cnt

////////////////////////////////////////////////////////////
// Logging macros
////////////////////////////////////////////////////////////
#define INFO(...) logData(THE_CURRENT_FUNCTION, THE_FILE, __DATE__, __TIME__, __LINE__, the::LogType::LT_INFO, __VA_ARGS__)
#define WARN(...) logDataWarn(THE_CURRENT_FUNCTION, THE_FILE, __DATE__, __TIME__, __LINE__, the::LogType::LT_INFO, __VA_ARGS__)
#define ERR(...) logDataErr(THE_CURRENT_FUNCTION, THE_FILE, __DATE__, __TIME__, __LINE__, the::LogType::LT_INFO, __VA_ARGS__)
#define CRITICAL(...) logDataErr(THE_CURRENT_FUNCTION, THE_FILE, __DATE__, __TIME__, __LINE__, the::LogType::LT_INFO, __VA_ARGS__)
#define SRVCMD(cmd) logServerCmd(cmd)

#define ASSERT(condition, ...) (condition) ? (void)0  : logDataAssert(#condition, THE_CURRENT_FUNCTION, THE_FILE, __DATE__, __TIME__, __LINE__, the::LogType::LT_ERROR, __VA_ARGS__)

#ifdef ANDROID
    #include <android/log.h>
    #define  LOG_TAG  "main"
    #define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#endif

////////////////////////////////////////////////////////////
namespace the
{
    enum class LogType : char
    {
        LT_ERROR = 'E',
        LT_WARNING = 'W',
        LT_INFO = 'I'
    };

    // Forward Declaration
    class LogService;
    extern LogService* _the_globalLog;

    class LogInterface
    {
        public:
            /**
             * \brief This function is called from macros and shouldn't be
             * called directly
             */
            template<typename... Args>
            void logData(const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
                LogType lt, const char* fmt, const Args&... args) const
            {
                std::string result = the::format(fmt, args...);
                logLogic(result, LogType::LT_INFO, func, file, cmdate, cmtime, line);
            }

            template<typename... Args>
            void logDataErr(const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
                LogType lt, const char* fmt, const Args&... args) const
            {
                std::string result = the::format(fmt, args...);
                logLogic(result, LogType::LT_ERROR, func, file, cmdate, cmtime, line);
            }

            template<typename... Args>
            void logDataWarn(const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
                LogType lt, const char* fmt, const Args&... args) const
            {
                std::string result = the::format(fmt, args...);
                logLogic(result, LogType::LT_WARNING, func, file, cmdate, cmtime, line);
            }

            template<typename... Args>
            void logDataAssert(const char* statement,
                const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
                LogType lt, const char* fmt, const Args&... args) const
            {
                std::string result = the::format(fmt, args...);
                std::string finalResult = the::format("ASSERT: %s; %s", statement, result);
                logLogic(finalResult, LogType::LT_ERROR, func, file, cmdate, cmtime, line);

                // Uncomment to fail
                #ifdef NONPC_BUILD_PLATFORM
                        assert(false);
                #else
                        *((int*)0) = 42;
                #endif
            }

            /// \brief This is used for splitting title and additional information of the log message in formatting string.
            /// By default the value is symbol |
            static std::string ADDITIONAL_INFO_SEPARATOR;

        protected:
            virtual void logLogic(const std::string& formattedLine, LogType type,
                const char* func, const char* file, const char* cmdate, const char* cmtime, int line) const;
    };

    // Forward declaration
    class LogCoroutine;

    /**
     * \class LogService LogService.hpp "The2D/LogService.hpp"
     * \brief Log information into file with different level
     * \ingroup Utils
     * Available macros: DBUG, INFO, WARN, ERR, CRITICAL
     */
    class LogService
    {
        public:

            LogService();
            virtual ~LogService();

            void pushMessage(const the::string& title, const the::string& data);
            void stopService();
            void waitAllMessagesSent(int maxTimeToWaitMs = 5000);

            static void logLogic(const std::string& formattedLine, LogType type,
                const char* func, const char* file, const char* cmdate, const char* cmtime, int line);

        private:
            LogCoroutine* mCoroutineHandle;
    };

    inline void logLogic(const std::string& formattedLine, LogType type,
        const char* func, const char* file, const char* cmdate, const char* cmtime, int line)
    {
        the::LogService::logLogic(formattedLine, type, func, file, cmdate, cmtime, line);
    }

    // Global version of logging functions
    template<typename... Args>
    void logData(const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
        LogType lt, const char* fmt, const Args&... args)
    {
        std::string result = the::format(fmt, args...);
        logLogic(result, LogType::LT_INFO, func, file, cmdate, cmtime, line);
    }

    template<typename... Args>
    void logDataErr(const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
        LogType lt, const char* fmt, const Args&... args)
    {
        std::string result = the::format(fmt, args...);
        logLogic(result, LogType::LT_ERROR, func, file, cmdate, cmtime, line);
    }

    template<typename... Args>
    void logDataWarn(const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
        LogType lt, const char* fmt, const Args&... args)
    {
        std::string result = the::format(fmt, args...);
        logLogic(result, LogType::LT_WARNING, func, file, cmdate, cmtime, line);
    }

    template<typename... Args>
    void logDataAssert(const char* statement,
        const char* func, const char* file, const char* cmdate, const char* cmtime, int line,
        LogType lt, const char* fmt, const Args&... args)
    {
        std::string result = the::format(fmt, args...);
        std::string finalResult = the::format("ASSERT!: %s; %s", statement, result);
        logLogic(finalResult, LogType::LT_ERROR, func, file, cmdate, cmtime, line);

        std::cout << finalResult << std::endl;

#ifdef NONPC_BUILD_PLATFORM
        assert(false);
#else
        *((int*)0) = 42;
#endif
    }

    inline void logServerCmd(const char* cmd)
    {
        std::string result = the::format("SRVCMD:%s", cmd);
        _the_globalLog->pushMessage(result, "");
    }


    // -------------------------------------------------------------
    // Hidden interface
    struct LogMessage
    {
        std::string title;
        std::string message;
    };

    class LogCoroutine
    {
        public:
            void pushMessage(const LogMessage& message);
            int messageQueueSize();
            void stopLogic();

            LogCoroutine();
            ~LogCoroutine();

            LogCoroutine(const LogCoroutine&) = delete;
            void operator=(const LogCoroutine&) = delete;

            const int SERVER_PORT     = 1234;
            const int MAX_CONNECTIONS = 32;
            const int WAIT_TIME_MS    = 10;

            static std::string SEPARATOR;

        private:
            // -----------------------------------------------------
            bool onInit();
            void onDeinit();
            // -----------------------------------------------------
            void createServer();
            void runLogic();
            // -----------------------------------------------------
            void logic();
            void _serverLogic();
            void _senderLogic();
            // -----------------------------------------------------
            void pushPeer(ENetPeer* pPeer);
            void removePeer(ENetPeer* pPeer);
            // -----------------------------------------------------
            void _sendMessage(const LogMessage& message);
            void _sendData(ENetPeer* pPeer, ENetPacket* pData);

            ENetAddress mAddress;
            ENetHost* mServer;

            std::thread mBackgroundWorker;
            std::mutex mMutex;

            std::list<ENetPeer*> mClients;
            std::deque<LogMessage> mMessages;

            bool mInited;
            bool mRunning;
    };

}; // namespace the
////////////////////////////////////////////////////////////
