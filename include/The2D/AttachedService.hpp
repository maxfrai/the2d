////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/ISupportAttached.hpp>
#include <The2D/Utils/Utils.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class GameEntity;
    class AttachedBase;

    /// \brief Service, that allow to attach data to entities.
    class AttachedService: private the::NonCopyable<AttachedService>
    {
        public:

            AttachedService(InfrastructureCore* core);
            virtual ~AttachedService();

            void registerProperty(AttachedBase* property);
            void unregisterProperty(AttachedBase* property);

            AttachedBase* getAttached(int id, int classID);

            /// \brief Free all memory, that allocated, but not used currently by attached system.
            /// Quit slow operation if there are many properties and attachiers. It should be performed
            /// from time to time, in moments when application is not care about perfomance. Otherwise,
            /// some possible some growing ammount of wasted memory in case of frequently registering
            /// and unregistering attached properties.
            void freeAllUnusedMemory();

            /// \brief Get total amount of memory, allocated by attached system
            /// \warn Very slow function, use for investigate purposes only.
            size_t getTotalUsedMemory();
            /// \brief Get total amount of memory, allocated, but not used by attached system
            /// \warn Very slow function, use for investigate purposes only.
            size_t getTotalWastedMemory();

            /// \brief Return collection of all simple (not static) attached properties, registered in the system.
            const eastl::map<int, eastl::vector<AttachedBase*>>& getProperties();

            /// \brief Reset values of all attached to obj properties to its defaults.
            void resetAllAttached(ISupportAttached* obj);



        protected:
            InfrastructureCore* mCore;
            eastl::map<int, eastl::vector<AttachedBase*>> mProperties;
            eastl::vector<string> mClasses;

            struct StaticAttachedID
            {
                HashCode parentType;
                HashCode name;
                HashCode valueType;
                HashCode targetType;

                bool operator==(const StaticAttachedID& other)
                {
                    return parentType == other.parentType &&
                        name == other.name &&
                        valueType == other.valueType &&
                        targetType == other.targetType;
                }
            };
            struct StaticAttachedInfo
            {
                sptr<AttachedBase> source;
                int id;
                int classID;
            };
            eastl::vector<eastl::pair<StaticAttachedID, StaticAttachedInfo>> mStaticInfo;

            void spreadAttachedDownToHierarchyRec(const The2DTypeInfo* type);


    };
}; // namespace the
////////////////////////////////////////////////////////////
