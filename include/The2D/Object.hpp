////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Component.hpp>

#include <unordered_map>
#include <stdarg.h>

namespace the
{
    /// \class Object Object "Object.hpp"
	/// \ingroup Core
    /// \brief Base class for objects.
    /// Object declared in the scripts, and serve for binding components.
    class Object :
        public GameEntity,
        private NonCopyable<Object>
    {
        public:
            THE_ENTITY(the::Object, the::GameEntity)

            Object();
            virtual ~Object();

            virtual bool getAlwaysDraw() const;
            virtual void setAlwaysDraw(bool state);
            void onIncomingMessage(Message* msg);

        protected:
            bool mAlwaysDraw;

    };
};
////////////////////////////////////////////////////////////
