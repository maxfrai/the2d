////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/EntityGroup.hpp>

// Forward declaration
namespace the
{
    class Attached;
}

////////////////////////////////////////////////////////////
namespace the
{
    /// \class Component Component "Component.hpp"
    /// \brief Base class for components.
    /// Components stores in C++ libraries, and contains specified logic.
    class Component: public GameEntity
    {
        public:
            THE_ENTITY(Component, GameEntity)

            /// \brief Support for attached properties and logic.
            Attached* mAttachedProcessor;
            EntityGroup mAttachedChildren;

            Component();
            virtual ~Component();
    };

} // namespace the
////////////////////////////////////////////////////////////
