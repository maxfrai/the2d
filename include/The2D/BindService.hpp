////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/Rtti.hpp>
#include <The2D/InfrastructureCore.hpp>
#include <The2D/Service.hpp>
#include <The2D/Attached.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    class GameEntity;

    struct PathItem
    {
        string rawString;
        HashCode name;
        eastl::vector<HashCode> groups;
        HashCode type;
        bool goChildrenRec;
        bool goParent;

        PathItem(bool _goChildrenRec):
            name(INVALID_HASH), groups(INVALID_HASH), type(INVALID_HASH), goChildrenRec(_goChildrenRec), goParent(false)
        {
        }
        PathItem(HashCode _name, const eastl::vector<HashCode>& _groups, HashCode _type)
            : name(_name), groups(_groups), type(_type), goChildrenRec(false), goParent(false)
        {
        }
    };

    struct ParsedPath
    {
        std::vector<PathItem> pathItems;
        GameEntity* searchRoot;

        PathItem& operator[](int i)
        {
            return pathItems[i];
        }
    };

    class BindBase;

    /// \brief Service, that allow finding of desired entities.
    class BindService: public Service
    {
        public:
            THE_ENTITY(BindService, Service)

            AttachedProperty<GameEntity, eastl::list<BindBase*>> atIncomingBindings;

            BindService();
            virtual ~BindService();


            virtual ParsedPath parsePath(string path, GameEntity* relativeTo);

            virtual ParsedPath parsePath(std::string path)
            {
                return parsePath(path, core()->tree());
            }

            virtual ParsedPath parsePathIn(GameEntity* searchRoot, std::string path)
            {
                ParsedPath result = parsePath(path, core()->tree());
                result.pathItems.front().name = searchRoot->getNameHash();
                result.pathItems.front().groups = searchRoot->getGroupsHashes();
                result.pathItems.front().type = type(searchRoot).nameHash;
                result.pathItems.front().rawString = "set_by_user";
                result.searchRoot = searchRoot;
                return result;
            }


            virtual bool matchPath(GameEntity* ent, ParsedPath path, string* debugLog=nullptr);

            virtual bool matchPath(GameEntity* ent, string path, string* debugLog=nullptr)
            {
                return matchPath(ent, parsePath(path), debugLog);
            }

            virtual eastl::list<GameEntity*> findAll(ParsedPath path, std::function<bool(GameEntity* item)> visitor, string* debugLog=nullptr);


            virtual GameEntity* find(ParsedPath path, string* debugLog=nullptr)
            {
                eastl::list<GameEntity*> result = findAll(path, [](GameEntity* item){ return false; }, debugLog);
                if (result.size() > 0) return result.front();
                else return nullptr;
            }

            virtual eastl::list<GameEntity*> findAll(ParsedPath path, string* debugLog=nullptr)
            {
                return findAll(path, [](GameEntity* item){ return true; }, debugLog);
            }

            template<typename T>
            T* find(ParsedPath path, string* debugLog=nullptr)
            {
                GameEntity* found = find(path, debugLog);
                if (core()->rtti()->is<T>(found))
                {
                    return static_cast<T*>(found);
                }
                else
                {
                    return nullptr;
                }
            }

            virtual GameEntity* findIn(GameEntity* searchRoot, string path, string* debugLog=nullptr)
            {
                return find(parsePathIn(searchRoot, path), debugLog);
            }
            virtual GameEntity* find(string path, string* debugLog=nullptr)
            {
                return find(parsePath(path), debugLog);
            }

            virtual eastl::list<GameEntity*> findAllIn(GameEntity* searchRoot, string path, string* debugLog=nullptr)
            {
                return findAll(parsePathIn(searchRoot, path), debugLog);
            }
            virtual eastl::list<GameEntity*> findAll(string path, string* debugLog=nullptr)
            {
                return findAll(parsePath(path), debugLog);
            }
            virtual eastl::list<GameEntity*> findAllIn(GameEntity* searchRoot, string path, std::function<bool(GameEntity* item)> visitor, string* debugLog=nullptr)
            {
                return findAll(parsePathIn(searchRoot, path), visitor, debugLog);
            }
            virtual eastl::list<GameEntity*> findAll(string path, std::function<bool(GameEntity* item)> visitor, string* debugLog=nullptr)
            {
                return findAll(parsePath(path), visitor, debugLog);
            }

            template<typename T>
            T* findIn(GameEntity* searchRoot, string path, string* debugLog=nullptr)
            {
                GameEntity* found = findIn(searchRoot, path, debugLog);
                if (core()->rtti()->is<T>(found))
                {
                    return static_cast<T*>(found);
                }
                else
                {
                    return nullptr;
                }
            }
            template<typename T>
            T* find(string path, string* debugLog=nullptr)
            {
                return findIn<T>(core()->tree(), path, debugLog);
            }



        protected:
            int mDebugOutputLevel;
            int mMaxDeep;

            virtual bool _findAll(ParsedPath& path,
                                    int current,
                                    GameEntity* object,
                                    eastl::list<GameEntity*>& result,
                                    std::function<bool(GameEntity* item)>& visitor,
                                    string* debugLog);
            virtual bool _matchPath(ParsedPath& path, int current, GameEntity* object, string* debugLog);
    };
}; // namespace the
////////////////////////////////////////////////////////////