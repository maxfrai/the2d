////////////////////////////////////////////////////////////
//
// Themisto - 2D physic based game with flexible engine
// Copyright (C) 2010-2012 Pavel Bogatirev, Max Tyslenko (Wincode team), max.tyslenko@gmail.com, pfight77@gmail.com
//
// This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>
#include <The2D/IXmlParser.hpp>

////////////////////////////////////////////////////////////
namespace the
{
    /// \class EntityGroup EntityGroup <The2D/EntityGroup.hpp>
    /// \brief Collection of entities, that can create children via xml service during parsing.
    /// Class can store shared pointers to children.
    class EntityGroup: public GameEntity, public IXmlParser
    {
        public:
            THE_ENTITY(the::EntityGroup, the::GameEntity);

            EntityGroup();
            virtual ~EntityGroup();

            /// \brief Register child entity and store shared pointer.
            void addChild(std::shared_ptr<GameEntity> entity, InitOrder initOrder);
            /// \brief Unregister child entity and delete shared pointer.
            void removeChild(GameEntity* entity);

            virtual IXmlParser* getParser();
            virtual std::list<ParsingError> parse(TiXmlElement* element);
            virtual bool isMyTag(TiXmlElement *element);
            virtual TiXmlElement* serialize();

            InitOrder getDefaultInitOrder();
            void setDefaultInitOrder(InitOrder order);

        protected:
            std::list<std::shared_ptr<GameEntity>> mStoredChildren;
            InitOrder mDefaultInitOrder;
            bool mInDctor;

            virtual void onUnregisterChild(GameEntity* child) override;

    };
}; // namespace the
////////////////////////////////////////////////////////////
