////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Connectors.hpp>
#include <EASTL/vector.h>

////////////////////////////////////////////////////////////
namespace the {

    enum VectorChangeType
    {
        PUSH_BACK,
        INSERT,
        REMOVE,
        UPDATE,
        FULL_UPDATE
    };

    struct VectorChange
    {
        VectorChangeType type;
        int pos;
    };

    class CValueVectorTransaction
    {
    };

    template<typename T, AccessModes AccessMode = READ_WRITE, bool SourceRequired = false>
    class CValueVector: public CValue<eastl::vector<T>, AccessMode, SourceRequired>
    {
         public:

            THE_ENTITY(CValueVector<T COMMA AccessMode COMMA SourceRequired>,
                CValue<eastl::vector<T> COMMA AccessMode COMMA SourceRequired>);

            typedef CValue<eastl::vector<T>, AccessMode, SourceRequired> BaseType;


            CValueVector()
            {
                this->SigValueChanged.add([this]{ this->onVectorContentChanged(FULL_UPDATE, -2); });
            }

            Signal<void(VectorChange)> SigVectorContentChanged;

            using BaseType::get;

            inline const T& get(int index)
            {
                return this->at(index);
            }

            using BaseType::set;

            inline void set(int index, const T& value, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().at(index) = value;
                if (tr == nullptr) onVectorContentChanged(UPDATE, index);
            }

            /// \name Vector functions
            /// @{
            inline const T& operator[](int index)
            {
                return this->at(index);
            }

            typedef typename eastl::vector<T> vector_type;
            typedef typename eastl::vector<T>::value_type value_type;
            typedef typename eastl::vector<T>::const_pointer const_pointer;
            typedef typename eastl::vector<T>::const_reference const_reference;  // Maintainer note: We want to leave iterator defined as T* -- at least in release builds -- as this gives some algorithms an advantage that optimizers cannot get around.
            typedef typename eastl::vector<T>::const_iterator const_iterator;   //       Do not write code that relies on iterator being T*. The reason it will
            typedef typename eastl::vector<T>::const_reverse_iterator const_reverse_iterator;
            typedef typename eastl::vector<T>::size_type size_type;

            const_iterator begin() { return this->get().begin(); }
            const_iterator end() { return this->get().end(); }

            CValueVector& operator=(const vector_type& x)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().operator=(x);
                onVectorContentChanged(FULL_UPDATE);
                return *this;
            }
            void swap(vector_type& x, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().swap(x);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }

            void assign(size_type n, const value_type& value, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().assign(n, value);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }

            void resize(size_type n, const value_type& value, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().resize(n, value);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }
            void resize(size_type n, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().resize(n);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }

            bool empty() { return this->get().empty(); }
            int size() { return (int)this->get().size(); }
            int capacity() { return this->get().capacity(); }
            void reserve(size_type n) { getMutable().reserve(n); }
            const_reference at(size_type n) { return this->get().at(n); }
            const_reference front() { return this->get().front(); }
            const_reference back() { return this->get().back(); }

            void set_capacity(size_type n = vector_type::base_type::npos, CValueVectorTransaction* tr = nullptr)   // Revises the capacity to the user-specified value. Resizes the container to match the capacity if the requested capacity n is less than the current size. If n == npos then the capacity is reallocated (if necessary) such that capacity == size.
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().set_capacity(n);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }
            void set_capacity(size_type n, const value_type& value, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().set_capacity(n, value);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }

            void push_back(const value_type& value, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().push_back(value);
                if (tr == nullptr) onVectorContentChanged(PUSH_BACK, size()-1);
            }
            void pop_back(CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().pop_back();
                if (tr == nullptr) onVectorContentChanged(REMOVE, size());
            }
            void erase(int position, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().erase(getMutable().begin() + position);
                if (tr == nullptr) onVectorContentChanged(REMOVE, position);
            }
            void erase(int first, int last, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().erase(getMutable().begin() + first, getMutable().begin() + last);
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }
            void insert(int position, const value_type& value, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().insert(getMutable().begin() + position, value);
                if (tr == nullptr) onVectorContentChanged(INSERT, position);
            }
            void clear(CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                getMutable().clear();
                if (tr == nullptr) onVectorContentChanged(FULL_UPDATE);
            }
            /// \brief Remove value from vector.
            /// \returns True if value was removed, or false if there is no such value found in vector.
            inline bool remove(const_reference val, CValueVectorTransaction* tr = nullptr)
            {
                ASSERT(this->mAccessMode == READ_WRITE, "");
                int pos = find(val);
                if (pos >= 0)
                {
                    getMutable().erase(getMutable().begin() + pos);
                    if (tr == nullptr) onVectorContentChanged(REMOVE, pos);
                }
                return pos >= 0;
            }
            inline bool contains(const_reference val)
            {
                return this->get().contains(val);
            }
            inline int find(const_reference val)
            {
                vector_type& vector = getMutable();
                for (int i=0; i< (int)vector.size(); i++)
                {
                    if (vector[i] == val)
                    {
                        return i;
                    }
                }
                return -1;
            }
            /// @} Vector functions -------------------------

            CValueVectorTransaction* beginTransaction()
            {
                return &mTrans;
            }

            void endTransaction()
            {
                onVectorContentChanged(FULL_UPDATE);
            }

        protected:
            CValueVectorTransaction mTrans;

            inline vector_type& getMutable()
            {
                return const_cast<vector_type&>(this->get());
            }

            void onVectorContentChanged(VectorChangeType type, int pos = -1)
            {
                SigVectorContentChanged({type, pos});
                if (pos != -2) // -2 is from onValueChanged (see ctor)
                {
                    this->onValueChanged();
                }
            }

    };

    template<typename T, AccessModes AccessMode = READ_WRITE>
    using ToValueVector = CValueVector<T, AccessMode, true>;

    template<typename T>
    using CValueVectorR = CValueVector<T, READ_ONLY, false>;

    template<typename T>
    using ToValueVectorR = CValueVector<T, READ_ONLY, true>;

    /// @}

}; // namespace the



////////////////////////////////////////////////////////////
