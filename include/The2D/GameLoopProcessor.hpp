////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Config.hpp>
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/Utils/Signals.hpp>
#include <The2D/RttiSupport.hpp>
#include <The2D/ChunkList.hpp>
#include <The2D/StatisticService.hpp>
#include <EASTL/map.h>

#include <iostream>


////////////////////////////////////////////////////////////
namespace the
{
    class InfrastructureCore;
    class GameLoopProcessor;

    class LoopSubscriber: public StatisticTarget
    {
        public:
            THE_ABSTRACT_ENTITY(the::LoopSubscriber, the::StatisticTarget)

            LoopSubscriber();
            virtual ~LoopSubscriber();

            virtual void update(){}

            virtual void draw(){};

            bool subscribedToUpdate() const;
            bool subscribedToDraw() const;
            bool active() const;

        protected:
            virtual void onActiveChanged();

        private:
            ChunkItem<LoopSubscriber*>* mPosInUpdateList;
            ChunkItem<LoopSubscriber*>* mPosInDrawList;
            bool mActive;

            friend class GameLoopProcessor;
    };

    /// \brief Class which manages main game cycle.
    /// \ingroup Core
    class GameLoopProcessor : private the::NonCopyable<GameLoopProcessor>
    {
        public:

            the::Signal<void (float)> SigPreUpdateLogic;
            the::Signal<void (float)> SigPostUpdateLogic;

            the::Signal<void (float)> SigPreUpdateVisual;
            the::Signal<void (float)> SigPostUpdateVisual;

            the::Signal<void ()> SigPostStep;

            float prmTimeStep;
            bool prmAdaptiveTimeStep;
            float prmTimeStepCorrectionInterval;
            float prmTimeStepMaxDifferencePercent;
            float prmMaxTimeStep;
            int prmSkipFirstIntervalsCount;

            GameLoopProcessor(InfrastructureCore* core);
            virtual ~GameLoopProcessor();

            /// \brief Runs game loop. Method do not return until stop() called.
            virtual void start();
            /// \brief Stop game loop.
            virtual void stop();

            /// \brief update cycle manually, dt is measured in ms
            virtual void step(float timeStep);

            /// \brief Initializes internal structures.
            /// \note After initialization, GameTree look up over the tree,
            /// and check, if all nodes were initialized. If don't: warning printed.
            /// To surpress this warning, set Init order to NEVER, BY_PARENT,
            virtual bool init();
            virtual void deinit();

            /// \brief Updates visual of tree and state.
            virtual void draw(float elapsed);
            /// \brief Updates logic of tree and state.
            virtual void update(float elapsed);

            virtual void subscribeToUpdate(LoopSubscriber* subscriber);
            virtual void unsubscribeFromUpdate(LoopSubscriber* subscriber);
            virtual bool subscribedToUpdate(LoopSubscriber* sub) const;

            virtual void subscribeToDraw(LoopSubscriber* subscriber);
            virtual void unsubscribeFromDraw(LoopSubscriber* subscriber);
            virtual bool subscribedToDraw(LoopSubscriber* sub) const;

            virtual void setActive(LoopSubscriber* subscriber, bool active);
            virtual bool active(LoopSubscriber* sub) const;

            virtual ChunkList<LoopSubscriber*>& getUpdateList();
            virtual ChunkList<LoopSubscriber*>& getDrawList();

            inline float timeStep() const
            {
                return mTimeStep;
            }

            inline float invTimeStep() const
            {
                return mInvTimeStep;
            }

        protected:
            bool mRunning;
            InfrastructureCore* mCore;
            float mTimeStep;
            float mInvTimeStep;
            double mAccomulatedDiff;
            double mAccomulatedTimeStep;
            int mStepCount;
            double mMilisecFromLastSync;
            long mIntervalNum;
            /// \brief List of entities, that needs to update logic.
            ChunkList<LoopSubscriber*> mUpdateList;
            ChunkItem<LoopSubscriber*>* mCurrentUpdateItem;
            /// \brief List of entities, that need to update visual.
            ChunkList<LoopSubscriber*> mDrawList;
            ChunkItem<LoopSubscriber*>* mCurrentDrawItem;
            /// \brief Used to prevent calculating update lists during initialization
            bool mObjectsInitalized;
            // Used for prevent update defrag during first udpate step, after loading.
            // On first step entites, that not using update or draw unsubscribes from loop.
            bool mInitializing;

            virtual void onTreeInitBegin();

            virtual void shrinkAndOrderLists();

            virtual void processCellMoveUpdate(ChunkItem<LoopSubscriber*>* oldCellPos, ChunkItem<LoopSubscriber*>* newCellPos);
            virtual void processCellMoveDraw(ChunkItem<LoopSubscriber*>* oldCellPos, ChunkItem<LoopSubscriber*>* newCellPos);

    };
}; // namespace the
////////////////////////////////////////////////////////////
