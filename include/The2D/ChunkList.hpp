/*
    Copyright 2011 Pavel Bogatirev

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <The2D/Config.hpp>
#include <The2D/Utils/Utils.hpp>
#include "Utils/NonCopyable.hpp"
#include <The2D/LogService.hpp>
#include <vector>

namespace the
{
    template<typename T>
    class Chunk;

    template<typename T>
    class ChunkList;

    template<typename T>
    class ChunkItem
    {
        public:
            T data;

            inline ChunkItem<T>* prev() const { return mPrev; }
            inline ChunkItem<T>* next() const { return mNext; }

        private:
            ChunkItem<T>* mPrev;
            ChunkItem<T>* mNext;

            friend class ChunkList<T>;
    };

    template<typename T>
    class Chunk: private NonCopyable<ChunkList<T>>
    {
        public:
            Chunk(int capacity = 10)
                : mCapacity(capacity), mCurrent(-1)
            {
                 mList = (ChunkItem<T>*)std::malloc(capacity*sizeof(ChunkItem<T>));
            }

            ~Chunk()
            {
                std::free(mList);
            }

            inline int capacity() const
            {
                return mCapacity;
            }

        private:
            int mCapacity;
            int mCurrent;
            ChunkItem<T>* mList;

            friend class ChunkList<T>;
    };

    template<typename T>
    class ChunkList: private NonCopyable<ChunkList<T>>
    {
        public:

            Signal<void(ChunkItem<T>* oldCellPos, ChunkItem<T>* newCellPos)> SigCellMoved;

            ChunkList(int firstChunkCapacity = 8, float sizeGrow = 2.f, int maxChunkSize=1024)
                : mSizeGrowKoef(sizeGrow), mSize(0), mCapacity(firstChunkCapacity), mMaxChunkSize(maxChunkSize)
            {
                mZero.mPrev = &mZero;
                mZero.mNext = &mZero;

                mRemovedZero.mPrev = &mRemovedZero;
                mRemovedZero.mNext = &mRemovedZero;

                mChunks.emplace_back(firstChunkCapacity);
                mCurrentChunk = mChunks.begin();
            }

            inline ChunkItem<T>* insert_after(ChunkItem<T>* prev)
            {
                ASSERT(prev != nullptr, "Invalid arg");

                ChunkItem<T>* next = prev->mNext;

                Chunk<T>* chunk = getCurrentChunk();

                chunk->mCurrent++;
                mSize++;
                ChunkItem<T>* cur = &chunk->mList[chunk->mCurrent];
                cur->mPrev = prev;
                cur->mNext = next;

                prev->mNext = cur;
                next->mPrev = cur;
                return cur;
            }
            inline ChunkItem<T>* insert_after(ChunkItem<T>* prev, T data)
            {
                ASSERT(prev != nullptr, "Invalid arg");
                ChunkItem<T>* newItem = insert_after(prev);
                newItem->data = data;
                return newItem;

            }
            inline ChunkItem<T>* insert_before(ChunkItem<T>* next, T data)
            {
                ASSERT(next != nullptr, "Invalid arg");
                return insert_after(next->mPrev, data);
            }

            inline ChunkItem<T>* push_back(T data)
            {
                return insert_after(last(), data);
            }

            inline void pop_back()
            {
                erase(last());
            }
            inline ChunkItem<T>* push_front(T data)
            {
                return insert_before(first(), data);
            }
            inline void pop_front()
            {
                erase(first());
            }
            template<typename ...Types>
            inline ChunkItem<T>* emplace_after(ChunkItem<T>* prev, Types ... args)
            {
                ASSERT(prev != nullptr, "Invalid arg");
                ChunkItem<T>* newItem = insert_after(prev);
                newItem->data->T(args...);
                return newItem;
            }
            template<typename ...Types>
            inline ChunkItem<T>* emplace_before(ChunkItem<T>* next, Types ... args)
            {
                ASSERT(next != nullptr, "Invalid arg");
                ChunkItem<T>* newItem = insert_after(next->mPrev);
                newItem->data->T(args...);
                return newItem;
            }
            template<typename ...Types>
            inline ChunkItem<T>* emplace_back(Types ... args)
            {
                ChunkItem<T>* newItem = insert_after(last());
                newItem->data->T(args...);
                return newItem;
            }
            template<typename ...Types>
            inline ChunkItem<T>* emplace_front(Types ... args)
            {
                ChunkItem<T>* newItem = insert_after(first());
                newItem->data->T(args...);
                return newItem;
            }

            inline ChunkItem<T>* insert_before(ChunkItem<T>* next)
            {
                ASSERT(next != nullptr, "Invalid arg");
                return insert_after(next->mPrev);
            }
            inline ChunkItem<T>* push_back()
            {
                return insert_after(last());
            }
            inline ChunkItem<T>* push_front()
            {
                return insert_before(first());
            }

            inline void move_after(ChunkItem<T>* after, ChunkItem<T>* itemToMove)
            {
                ASSERT(after != nullptr, "Invalid arg");
                ASSERT(itemToMove != nullptr, "Invalid arg");

                if (after != itemToMove)
                {
                    // Remove from previous position
                    {
                        ChunkItem<T>* prev = itemToMove->mPrev;
                        ChunkItem<T>* next = itemToMove->mNext;
                        prev->mNext = next;
                        next->mPrev = prev;
                    }
                    // Insert into new position
                    {
                        ChunkItem<T>* prev = after;
                        ChunkItem<T>* next = prev->mNext;
                        itemToMove->mPrev = prev;
                        itemToMove->mNext = next;
                        prev->mNext = itemToMove;
                        next->mPrev = itemToMove;
                    }
                }
                // else nothing to do
            }

            inline void move_before(ChunkItem<T>* before, ChunkItem<T>* itemToMove)
            {
                ASSERT(before != nullptr, "Invalid arg");

                move_after(before->mPrev, itemToMove);
            }
            inline void move_back(ChunkItem<T>* itemToMove)
            {
                move_after(last(), itemToMove);
            }
            inline void move_front(ChunkItem<T>* itemToMove)
            {
                move_before(first(), itemToMove);
            }

            inline void erase(ChunkItem<T>* cur)
            {
                ASSERT(cur != zero(), "Zero item is internal structure, you cant erase it");
                ASSERT(cur != nullptr, "Invalid argument");

                // Remove from list
                ChunkItem<T>* prev = cur->mPrev;
                ChunkItem<T>* next = cur->mNext;
                prev->mNext = next;
                next->mPrev = prev;

                // Insert into removed list
                prev = &mRemovedZero;
                next = prev->mNext;
                cur->mNext = next;
                prev->mNext = cur;
                cur->mPrev = nullptr; // Need for defrag, thats why removed list forward only
                mSize--;
            }

            inline int size() const
            {
                return mSize;
            }

            inline ChunkItem<T>* zero(){ return &mZero; }
            inline ChunkItem<T>* first() { return zero()->next(); }
            inline ChunkItem<T>* last() { return zero()->prev(); }
            inline ChunkItem<T>* begin() { return first(); }
            inline ChunkItem<T>* end() { return zero(); }

            inline ChunkItem<T>* removed_zero(){ return &mRemovedZero; }
            inline ChunkItem<T>* first_removed() { return removed_zero()->next(); }
            inline ChunkItem<T>* last_removed() { return removed_zero()->prev(); }
            inline ChunkItem<T>* removed_begin() { return first_removed(); }
            inline ChunkItem<T>* removed_end() { return removed_zero(); }

            inline T& front()
            {
                ASSERT(!empty(), "No front element");
                return first()->data;
            }
            inline T& back()
            {
                ASSERT(!empty(), "No back element");
                return last()->data;
            }

            inline bool empty() const
            {
                return mSize == 0;
            }

            inline int capacity() const
            {
                return mCapacity;
            }

            inline bool remove(const T& value)
            {
                auto it = find(value);
                if (it != nullptr)
                {
                    erase(it);
                    return true;
                }
                else
                {
                    return false;
                }
            }

            inline bool contains(const T& value)
            {
                return find(value) != nullptr;
            }

            inline ChunkItem<T>* find(const T& value)
            {
                for (auto it = begin(); it != end(); it = it->next())
                {
                    if (it->data == value)
                    {
                        return it;
                    }
                }
                return nullptr;
            }

            inline void clear()
            {
                zero()->mNext = zero();
                zero()->mPrev = zero();

                // Chunks will be reused, so removed items can be overwrited
                mRemovedZero.mPrev = &mRemovedZero;
                mRemovedZero.mNext = &mRemovedZero;

                for (auto& chunk : mChunks)
                {
                    chunk.mCurrent = -1;
                }
                mCurrentChunk = mChunks.begin();
            }

            inline size_t usedMemory()
            {
                return capacity() * sizeof(ChunkItem<T>) + sizeof(ChunkList<T>);
            }

            inline bool defragAndShrink()
            {
                typename std::list<Chunk<T>>::iterator freeChunkIt= mChunks.begin();
                int indexInChunk = 0;
                bool defragged = false;
                while ((size() < (capacity() - mChunks.back().capacity())) && mChunks.size() > 1)
                {
                    // Removed items will become unavailable
                    mRemovedZero.mPrev = &mRemovedZero;
                    mRemovedZero.mNext = &mRemovedZero;

                    // Move all items from last chunk to some another
                    Chunk<T>& chunk = mChunks.back();
                    for (int i=0; i <= chunk.mCurrent; i++)
                    {
                        // if item not removed, move it somewhere
                        // mPrev == nullptr, mean that item erased, and cell is free
                        if (chunk.mList[i].mPrev != nullptr)
                        {
                            // Find free cell
                            bool found = false;
                            for (; freeChunkIt != --mChunks.end(); )
                            {
                                Chunk<T>& freeChunk = *freeChunkIt;

                                for (; indexInChunk <= freeChunk.mCurrent; indexInChunk++)
                                {
                                    // mPrev == nullptr, mean that item erased, and cell is free
                                    if (freeChunk.mList[indexInChunk].mPrev == nullptr)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                {
                                    indexInChunk = 0;
                                    freeChunkIt++;
                                }
                                else
                                {
                                    break;
                                }
                            }

                            // Move item to found cell
                            Chunk<T>& freeChunk = *freeChunkIt;
                            ChunkItem<T>* item = &freeChunk.mList[indexInChunk];
                            ChunkItem<T>* oldItem = &chunk.mList[i];
                            *item = *oldItem;
                            item->mPrev->mNext = item;
                            item->mNext->mPrev = item;
                            SigCellMoved(oldItem, item);
                            // Special notation, to say users where moved item
                            oldItem->mPrev = nullptr;
                            oldItem->mNext = item;
                            defragged = true;
                        }
                    }
                    removeLastChunk();
                }
                return defragged;
            }

            /// \brief Returns count of chunks, that will be freed on call of defragAndShrink.
            inline int getDefragNeed() const
            {
                int unused = mCapacity - mSize;
                int canBeFreed = 0;
                int chunksSize = 0;
                // Note, first chunk cant be freed
                for (auto chunkIt = mChunks.cend(); chunkIt != mChunks.cbegin(); chunkIt--)
                {
                    chunksSize += chunkIt->capacity();
                    if (chunksSize > unused)
                    {
                        break;
                    }
                    canBeFreed++;
                }
                return canBeFreed;
            }

            inline const std::list<Chunk<T>>& getChunks() const
            {
                return mChunks;
            }

            inline void splice(ChunkItem<T>* before, ChunkList<T>& list, bool keepSourceValid = true)
            {
                if (list.size() > 0)
                {
                    mChunks.splice(mChunks.begin(), list.mChunks);

                    auto prev = before->prev();
                    auto next = before;
                    prev->mNext = list.first();
                    list.first()->mPrev = prev;
                    next->mPrev = list.last();
                    list.last()->mNext = next;
                    mSize += list.mSize;
                    mCapacity += list.mCapacity;

                    // Restore list
                    if (keepSourceValid)
                    {
                        list.mSize = 0;
                        list.mChunks.emplace_back(mChunks.front().capacity());
                        list.mCapacity = mChunks.front().capacity();
                        list.mCurrentChunk = mChunks.begin();
                        list.zero()->mNext = list.zero();
                        list.zero()->mPrev = list.zero();
                    }
                }
            }
        private:
            float mSizeGrowKoef;
            std::list<Chunk<T>> mChunks;
            typename std::list<Chunk<T>>::iterator mCurrentChunk;
            ChunkItem<T> mZero;
            ChunkItem<T> mRemovedZero;
            int mSize;
            int mCapacity;
            int mMaxChunkSize;

            inline Chunk<T>* getCurrentChunk()
            {
                Chunk<T>* chunk = &(*mCurrentChunk);
                while (chunk->mCurrent >= chunk->mCapacity-1)
                {
                    mCurrentChunk++;
                    if (mCurrentChunk == mChunks.end())
                    {
                        chunk = addChunk();
                        mCurrentChunk = mChunks.end();
                        mCurrentChunk--;
                    }
                }
                return chunk;
            }

            inline Chunk<T>* addChunk()
            {
                int size = mChunks.back().capacity() * mSizeGrowKoef + 0.5f;
                if (size > mMaxChunkSize)
                {
                    size = mMaxChunkSize;
                }
                mChunks.emplace_back(size);
                mCapacity += size;
                return &mChunks.back();
            }
            inline void removeLastChunk()
            {
                ASSERT(mChunks.size() > 1, "Removing last chunk will break list logic");

                mCapacity -= mChunks.back().capacity();
                auto it = mChunks.end();
                it--;
                if (it == mCurrentChunk)
                {
                    mCurrentChunk--;
                }
                mChunks.erase(it);
            }
    };
}