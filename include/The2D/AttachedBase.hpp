////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/GameEntity.hpp>

#include "AttachedService.hpp"

////////////////////////////////////////////////////////////
namespace the
{
    class AttachedBase: public GameEntity
    {
        public:
            THE_ABSTRACT_ENTITY(AttachedBase, GameEntity);

            constexpr static uint16 INVALID_ID = (uint16) -1;

            virtual ~AttachedBase();

            virtual bool isStatic() const;
            virtual const string& getTargetType();

            inline bool propertyRegistered() const
            {
                return mID != INVALID_ID || mClassID != INVALID_ID;
            }

            inline int getID() const
            {
                ASSERT(propertyRegistered(), "Register property in AttachedService first");
                
                return mID;
            }
            inline int getClassID() const
            {
                ASSERT(propertyRegistered(), "Register property in AttachedService first");
                
                return mClassID;
            }
            void setID(int id, int classID);

            virtual void reset(ISupportAttachedInternal* ent) = 0;

            virtual sptr<AttachedBase> getSource();
            virtual void setSource(sptr<AttachedBase> source);
            virtual void createSource();
            virtual string getStaticAttachedParentType();
            virtual string getValueType() = 0;

            virtual sptr<eastl::vector<ISupportAttachedInternal*>> getAttachiers() const = 0;
            virtual void resetAll(bool shrinkAttachiers = false) = 0;
            virtual void defragAndShrink() = 0;
            virtual int getFreeCellsCount() const = 0;
            /// \brief Get count of attachiers, that has not default values of this property.
            /// Complexity is O(1).
            /// \see getAttachiers
            virtual int getValuesCount() const = 0;
            virtual size_t getUsedMemory() const = 0;
            virtual size_t getWastedMemory() const = 0;

        protected:
            AttachedBase(const string& targetType, bool isStatic);

            virtual void onSetCore(InfrastructureCore* old, InfrastructureCore* newVal) override;
            
        private:
            string mName;
            HashCode mNameHash;
            string mTargetType;
            bool mIsStatic;
            uint16 mID;
            uint16 mClassID;
            

            ISupportAttachedInternal* mParent;
    };
}; // namespace the
////////////////////////////////////////////////////////////
