////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Utils/NonCopyable.hpp>
#include <The2D/Config.hpp>
#include <The2D/LogService.hpp>

#include <memory>

namespace the
{
    class ResourceInterface
    {
        public:
            virtual const std::string getPath(const std::string& relPath) const = 0;
            virtual bool isPathExists(const std::string& relPath) const = 0;
    };

    // Forward Declaration
    class ResourceService;
    extern ResourceService* _the_globalResource;

    class ResourceService : private NonCopyable<ResourceService>, public ResourceInterface
    {
        public:

            void setInterface(std::shared_ptr<ResourceInterface> interf);

            // Interface realization
            const std::string getPath(const std::string& relPath) const override;
            bool isPathExists(const std::string& relPath) const override;

        private:
            std::shared_ptr<ResourceInterface> mInterface;
    };
}
