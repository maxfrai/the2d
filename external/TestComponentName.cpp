////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "./TestComponentName.hpp"

using namespace the;



TestComponentName::InitData::InitData()
{
    REGISTER(test);
    REGISTER(test2);
    REGISTER(someBody);
    REGISTER(physicManager)
    REGISTER(childStub);
    REGISTER(multiBindTargets);

}

TestComponentName::TestComponentName():
    mInitData(this), mSomeBody(nullptr)
{
}

void TestComponentName::onDeinit()
{
    mSomeBody = nullptr;
    mMultiBindTargets.clear();

}

the::InitState TestComponentName::onInit()
{
    

    mSomeBody = mInitData->someBody.Get();
    for(MultiBindTargetType* cmp : mInitData->multiBindTargets.getBindsT())
    {
        mMultiBindTargets.push_back(cmp);
    }

    return true;
}

unsigned int TestComponentName::ThisIsPublic() const
{
}

void TestComponentName::update()
{
}

int TestComponentName::ThisIsPRIVATEEE() const
{
}

