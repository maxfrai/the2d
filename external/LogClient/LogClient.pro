#-------------------------------------------------
#
# Project created by QtCreator 2013-07-31T23:22:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LogClient
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CFLAGS += -DHAS_SOCKLEN_T
DEFINES += DHAS_SOCKLEN_T

SOURCES += main.cpp\
    mainwindow.cpp\
    networkservice.cpp \
    highlighter.cpp

INCLUDEPATH += "../../include/"

HEADERS  += mainwindow.h \
    networkservice.h \
    highlighter.h

FORMS    += mainwindow.ui
LIBS += -pthread -L$$PWD/../../../Bin/ -lThe2D
win32 {
    LIBS += -lws2_32
    LIBS += -lwinmm
}
