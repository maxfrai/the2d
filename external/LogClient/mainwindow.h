#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QSplitter>
#include <QVBoxLayout>
#include <QLayout>
#include "highlighter.h"
#include "networkservice.h"
#include <vector>
#include <QTextDocument>
#include <QLabel>
#include <QTimer>
#include <QPushButton>
#include <QMouseEvent>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    typedef std::vector<Message> MessagesQueue;
    bool eventFilter(QObject *watched, QEvent *e);

private slots:
    void on_connectButton_clicked();
    void on_newMessage(const Message& msg);
    void on_cursorPosChanged();
    void changeStatusIcon(bool connected);
    void clearButtonPressed();
    void timerEvent();
    void on_newCmd(QString cmdData);
    void on_doBeep();

    void on_titleFilter_textEdited(const QString &arg1);

    void on_contentFilter_textEdited(const QString &arg1);

    void on_enableInfo_clicked();

    void on_enableWarnings_clicked();

    void on_enableErrors_clicked();

private:
    void generateMessages();
    QString generateTitle(const Message& msg);

    bool filterMessage(Message msg);
    bool filterByCheckbox(LogType type);
    bool filterByString(QString data, QString filter);
    void closeEvent(QCloseEvent *) override;

    void loadSettings();

    QString mStartTime;
    QString mSettingsFile;
    QTimer* mTimer;
    Ui::MainWindow *ui;
    Highlighter* mHighlighter;

    QTextEdit* mGeneralEdit;
    QTextEdit* mMoreEdit;
    QLabel* mConnectionStatus;
    QPushButton* mClearButton;

    QVBoxLayout* mLayout;
    QHBoxLayout* mButtonsLayout;
    QSplitter* mSplitter;

    bool mWereChanges;

    MessagesQueue mQueue;
};

#endif // MAINWINDOW_H
