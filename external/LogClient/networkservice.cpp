#include "networkservice.h"

using namespace std;

NetworkService::NetworkService()
    :  peer(nullptr), server(nullptr), mCounter(0), mConnect(false), mConnected(false)
{
    qDebug() << "Running main logic";
    mBackgroundWorker = std::thread(&NetworkService::logic, this);
    mBackgroundWorker.detach();
}

NetworkService::~NetworkService()
{
    deinit();
};

NetworkService& NetworkService::instance()
{
    static NetworkService handle;
    return handle;
}

void NetworkService::init()
{
    if (enet_initialize () != 0)
    {
       cout << "Error initialising enet";
       exit (EXIT_FAILURE);
   }

   client = enet_host_create (NULL, /* create a client host */
                              1,    /* number of clients */
                              2,    /* number of channels */
                              0,    /* incoming bandwith */
                              0);   /* outgoing bandwith */

   if (client == NULL)
   {
       cout << "Can't create host =(\n";
       exit (EXIT_FAILURE);
   }
}

void NetworkService::deinit()
{
    enet_host_destroy (client);
    enet_deinitialize ();
}

void NetworkService::connect(const char *ip, int port)
{
    //std::lock_guard<std::mutex> lock(mMutex);
    qDebug() << "mConnect is true now";
    mInfo.ip = ip;
    mInfo.port = port;
    mConnect = true;
    mConnected = false;
}

const NetworkService::MessagesList& NetworkService::getMessages() const
{
    //std::lock_guard<std::mutex> lock(mMutex);
    return mMessages;
}

void NetworkService::logic()
{
    while(true)
    {
        if (!mConnected)
        {
            if (!mConnect)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
                continue;
            }

            qDebug() << "Trying to connect...";
            while (true)
            {
                init();
                enet_address_set_host (&address, mInfo.ip.toLatin1().data());
                address.port = mInfo.port;

                peer = enet_host_connect (client, &address, 2, 0);

                // Try to connect each second
                if (enet_host_service (client, &event, 2000) > 0 && event.type == ENET_EVENT_TYPE_CONNECT)
                {
                    qDebug() << "Gon connect event, breaking";
                    mConnected = true;

                    emit connectionStatusChanged(true);
//                    mMessages.clear();
                    server = event.peer;
                    break;
                }
                else
                {
                    //qDebug() << "Failed to connect in 2 second, retrying";
                    enet_peer_reset(peer);
                    deinit();
                }
            }
        }

        while (enet_host_service (client, & event, 100) > 0)
        {
            switch (event.type)
            {
                 case ENET_EVENT_TYPE_DISCONNECT:
                 {
                    qDebug() << "DISCONNECT\n";
                    mConnected = false;
                    emit connectionStatusChanged(false);
                    break;
                 }
                 case ENET_EVENT_TYPE_RECEIVE:
                    {
                        QString data = reinterpret_cast<const char*>(event.packet->data);

                        QStringList parts = data.split("[:]");

                        if (parts[0].isEmpty())
                            break;

                        QString title = parts[0];

                        if (title.startsWith("SRVCMD:"))
                        {
                            QString cmdData = title.mid(title.indexOf(":")+1);
                            emit newCmd(cmdData);
                        }
                        else
                        {
                            char firstSym = parts[1][0].toLatin1();
                            LogType type = static_cast<LogType>(firstSym);
                            if (type == LogType::LT_ERROR)
                            {
                                emit doBeep();
                            }

                            parts[1].remove(0, 1);
                            mMutex.lock();
                            Message msg = Message { title, parts[1], type };
                            mMessages[mCounter++] = msg;
                            mMutex.unlock();
                            emit newMessage(msg);
                        }
                    }
                    break;
                default:
                    break;
            }
            enet_peer_ping(server);
        }
}
}

void NetworkService::clearMessages()
{
    std::lock_guard<std::mutex> lock(mMutex);
    mMessages.clear();
}
