#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mSettingsFile = QApplication::applicationDirPath() + "/settings.ini";

    mTimer = new QTimer(this);
    mTimer->start(700);
    mWereChanges = false;

    mGeneralEdit = new QTextEdit();
    mMoreEdit = new QTextEdit();
    mGeneralEdit->setReadOnly(true);
    mGeneralEdit->setLineWrapMode(QTextEdit::NoWrap);
    mMoreEdit->setReadOnly(true);

    mLayout = new QVBoxLayout();
    mButtonsLayout = new QHBoxLayout();

    mSplitter = new QSplitter();
    mSplitter->setOrientation(Qt::Vertical);
    mSplitter->addWidget(mGeneralEdit);
    mSplitter->addWidget(mMoreEdit);
    mSplitter->setStretchFactor(0, 5);

    mConnectionStatus = new QLabel();
    mClearButton = new QPushButton();
    mClearButton->setIcon(QIcon("clear.png"));
    mClearButton->setToolTip("Clear messages");

    mButtonsLayout->addWidget(mConnectionStatus);
    mButtonsLayout->addWidget(ui->ipInput);
    mButtonsLayout->addWidget(ui->portInput);
    mButtonsLayout->addWidget(ui->connectButton);
    mButtonsLayout->addWidget(mClearButton);
    mButtonsLayout->addWidget(ui->titleFilter);
    mButtonsLayout->addWidget(ui->contentFilter);
    mButtonsLayout->addWidget(ui->enableInfo);
    mButtonsLayout->addWidget(ui->enableWarnings);
    mButtonsLayout->addWidget(ui->enableErrors);

    mLayout->addLayout(mButtonsLayout);
    mLayout->addWidget(mSplitter);
    ui->centralWidget->setLayout(mLayout);

    mHighlighter = new Highlighter(mMoreEdit->document());

    changeStatusIcon(false);

    mGeneralEdit->viewport()->installEventFilter(this);

    qRegisterMetaType<Message>("Message");

    connect(&NetworkService::instance(), SIGNAL(newMessage(Message)), this, SLOT(on_newMessage(Message)));
    connect(&NetworkService::instance(), SIGNAL(newCmd(QString)), this, SLOT(on_newCmd(QString)));
    connect(&NetworkService::instance(), SIGNAL(connectionStatusChanged(bool)), this, SLOT(changeStatusIcon(bool)));
    connect(&NetworkService::instance(), SIGNAL(doBeep()), this, SLOT(on_doBeep()));
    //connect(mGeneralEdit, SIGNAL(cursorPositionChanged()), this, SLOT(on_cursorPosChanged()));
    connect(mClearButton, SIGNAL(clicked()), this, SLOT(clearButtonPressed()));
    connect(mTimer, SIGNAL(timeout()), this, SLOT(timerEvent()));

    loadSettings();
    ui->connectButton->click();
}

bool MainWindow::eventFilter(QObject *watched, QEvent* e)
{
    if (watched == mGeneralEdit->viewport() && e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *ev = reinterpret_cast<QMouseEvent*>(e);
        if (ev->button() == Qt::LeftButton)
        {
            QTimer::singleShot(50, this, SLOT(on_cursorPosChanged()));
        }
    }
    return QWidget::eventFilter(watched, e);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeStatusIcon(bool connected)
{
    QPixmap* image;
    if (!connected)
    {
//        ui->connectButton->setDisabled(false);
        image = new QPixmap("disconnected.png");
        mConnectionStatus->setToolTip("Disconnected");
    }
    else
    {
        image = new QPixmap("connected.png");
        mConnectionStatus->setToolTip("Connected");
    }
    mConnectionStatus->setPixmap(*image);
}

void MainWindow::clearButtonPressed()
{
    NetworkService::instance().clearMessages();
    generateMessages();
    mMoreEdit->clear();
}

void MainWindow::on_cursorPosChanged()
{
    QTextCursor cur = mGeneralEdit->textCursor();

    size_t line = cur.blockNumber() - 1; // -1 because the first line is always timestamp
    if (mQueue.empty() || line > mQueue.size())
        return;

    Message msg = mQueue[line];
    mMoreEdit->clear();

    QString result;
    result.append(generateTitle(msg));

    QString data = msg.data;

    // Simple markdown
    data.replace(QRegularExpression("\\*\\*(.*)\\*\\*"), "<b>\\1</b>");
    data.replace(QRegularExpression("__(.*)__"), "<i>\\1</i>");

    QStringList lines = data.split('\n');
    for (QString line : lines)
    {
        if (line.contains("Additional information:"))
        {
            //result.append("<span style=\"color: #bdbcbc\"> ===================== Additional information: ===================== </span></p>");
            result.append("<hr>");
            continue;
        }
        else if (line.contains("Stack Trace"))
        {
            result.append("<hr>");
            continue;
            //result.append("<br><span style=\"color: #bdbcbc\"> ===================== Stack Trace: ===================== </span></p>");
        }

        result.append("<p style=\"margin:0px; padding: 0px; \">");
        if (line.contains("compiled at"))
        {
            QString fmt= "[<b>%1</b>] <i>%2</i></p>";
            int index = line.indexOf("]");
            result += fmt.arg(line.mid(1, index-1), line.mid(index+1));
            // [XmlLoader.cpp:187] compiled at Aug  3 2013 - 23:44:34
        }
        else
        {
            line.replace(" ", "&nbsp;");
            result.append(line);
            result.append("</p>");
        }
    }

    mMoreEdit->insertHtml(result);

    QColor selectionColor = QColor(Qt::blue).lighter(190);

    // Highlight the whole line in DESCRIPTION view
    QTextCursor curDesc = mMoreEdit->textCursor();
    QList<QTextEdit::ExtraSelection> extrasDesc;

    curDesc.movePosition(QTextCursor::Start);

    QTextEdit::ExtraSelection highlightDesc;

    highlightDesc.cursor = curDesc;
    highlightDesc.format.setProperty(QTextFormat::FullWidthSelection, true);
    highlightDesc.format.setBackground(selectionColor);
    extrasDesc.append(highlightDesc);

    curDesc.clearSelection();
    mMoreEdit->setTextCursor(curDesc);

    mMoreEdit->setExtraSelections(extrasDesc);

    // Highlight the whole line in MAIN view
    QTextEdit::ExtraSelection highlight;
    highlight.cursor = cur;
    highlight.format.setProperty(QTextFormat::FullWidthSelection, true);
    highlight.format.setBackground(selectionColor);

    QList<QTextEdit::ExtraSelection> extras;
    extras << highlight;
    mGeneralEdit->setExtraSelections(extras);
}

void MainWindow::on_connectButton_clicked()
{
//    ui->connectButton->setDisablesd(true);
    QString port = ui->portInput->text();
    QString ip   = ui->ipInput->text();
    NetworkService::instance().connect(ip.toStdString().c_str(), port.toInt());

    QSettings settings(mSettingsFile, QSettings::NativeFormat);
    settings.setValue("ip", ip);
    settings.setValue("port", port);
}

void MainWindow::on_newMessage(const Message& msg)
{
    if (!filterMessage(msg))
        return;

    mQueue.push_back(msg);
    QString title = generateTitle(msg);

    mGeneralEdit->append(title);

    QTextCursor cur = mGeneralEdit->textCursor();
    cur.movePosition(QTextCursor::End);
    cur.movePosition(QTextCursor::StartOfLine);
    mGeneralEdit->setTextCursor(cur);
}

void MainWindow::on_titleFilter_textEdited(const QString &arg1)
{
    (void)arg1;
    mWereChanges = true;
}


void MainWindow::on_contentFilter_textEdited(const QString &arg1)
{
    (void)arg1;
    mWereChanges = true;
}

bool MainWindow::filterByCheckbox(LogType type)
{
    switch (type)
    {
        case LogType::LT_INFO:
        {
            if (!ui->enableInfo->isChecked())
                return false; break;
        }
        case LogType::LT_WARNING:
        {
            if (!ui->enableWarnings->isChecked())
                return false; break;
        }
        case LogType::LT_ERROR:
        {
            if (!ui->enableErrors->isChecked())
                return false; break;
        }
    }

    return true;
}

bool MainWindow::filterByString(QString data, QString filterData)
{
    if (!data.isEmpty() && !filterData.isEmpty())
    {
        bool negative = filterData[0] == '!';
        if (negative)
            filterData.remove(0, 1);

        QRegExp filter(filterData);
        filter.setCaseSensitivity(Qt::CaseInsensitive);

        if (!negative)
            return data.contains(QRegExp(filter));
        else
            return !data.contains(QRegExp(filter));
    }

    return true;
}

void MainWindow::closeEvent(QCloseEvent *)
{
    QSettings settings(mSettingsFile, QSettings::NativeFormat);
    settings.setValue("geometry", saveGeometry());
    settings.setValue("saveState", saveState());
    settings.setValue("maximized", isMaximized());
    settings.setValue("splitterState", mSplitter->saveState());

    if (!isMaximized())
    {
        settings.setValue("pos", pos());
        settings.setValue("size", size());
    }
}

void MainWindow::loadSettings()
{
    QSettings settings(mSettingsFile, QSettings::NativeFormat);
    QString ip   = settings.value("ip", "127.0.0.1").toString();
    QString port = settings.value("port", "1234").toString();

    ui->ipInput->setText(ip);
    ui->portInput->setText(port);

    QString titleFilter   = settings.value("titleFilter", "").toString();
    QString contentFilter = settings.value("contentFilter", "").toString();

    ui->titleFilter->setText(titleFilter);
    ui->contentFilter->setText(contentFilter);

    restoreGeometry(settings.value("geometry", saveGeometry()).toByteArray());
    restoreState(settings.value("saveState", saveState()).toByteArray());
    move(settings.value("pos", pos()).toPoint());
    resize(settings.value("size", size()).toSize());
    mSplitter->restoreState(settings.value("splitterState", mSplitter->saveState()).toByteArray());
    if (settings.value("maximized", isMaximized()).toBool())
        showMaximized();
}

bool MainWindow::filterMessage(Message msg)
{
    QString title = msg.title;
    QString data  = msg.data;

    if (!filterByCheckbox(msg.type))
        return false;

    if (!filterByString(title, ui->titleFilter->text()))
        return false;

    if (!filterByString(title+data, ui->contentFilter->text()))
        return false;

    return true;
}

void MainWindow::timerEvent()
{
    if (mWereChanges)
    {
        generateMessages();

        QSettings settings(mSettingsFile, QSettings::NativeFormat);
        settings.setValue("titleFilter", ui->titleFilter->text());
        settings.setValue("contentFilter", ui->contentFilter->text());

        mWereChanges = false;
    }
}

void MainWindow::on_newCmd(QString cmdData)
{
    if (cmdData == "StartGame")
    {
        mStartTime = QDateTime::currentDateTime().toString("hh:mm:ss");
        clearButtonPressed();
    }
}

void MainWindow::on_doBeep()
{
    qDebug() << "BEEEEEEEEEEEEP motherfucker";
    QApplication::beep();

    qDebug() << (QCoreApplication::applicationDirPath() + "/err.mp3");
}

void MainWindow::generateMessages()
{
    mGeneralEdit->clear();
    mQueue.clear();

    QString fullText;
    fullText.append("<span style=\"font-weight: bold;\">\t Start time: " + mStartTime + "</span>");
    for (auto msg : NetworkService::instance().getMessages())
    {
        if (!filterMessage(msg.second))
            continue;

        mQueue.push_back(msg.second);
        fullText.append(generateTitle(msg.second));
    }

    mGeneralEdit->insertHtml(fullText);

    QTextCursor cur = mGeneralEdit->textCursor();
    cur.movePosition(QTextCursor::End);
    mGeneralEdit->setTextCursor(cur);
}

QString MainWindow::generateTitle(const Message &msg)
{
    QString color = "black";
    if (msg.type == LogType::LT_ERROR)
        color = "#e22d2b";
    else if (msg.type == LogType::LT_WARNING)
        color = "#a96338";
    else if (msg.type == LogType::LT_INFO)
        color = "#5172c5";

    QColor lineColor;
    int r = ((rand() % 255 + 1) + 140)/2;
    int g = ((rand() % 255 + 1) + 160)/2;
    int b = ((rand() % 255 + 1) + 250)/2;
    lineColor.setRgb(r, g, b);
    QString colored = QString("<span style=\"color:%1\">❭</span>").arg(lineColor.name());

    QString fullText;
    if (msg.type == LogType::LT_ERROR)
        fullText.append(QString("<p style=\"margin:0px; padding: 0px; color: %1; font-weight: bold\">").arg(color));
    else
        fullText.append(QString("<p style=\"margin:0px; padding: 0px; color: %1; \">").arg(color));

    fullText.append(colored);
    fullText.append(msg.title.toHtmlEscaped());
    fullText.append("</p>");

    return fullText;
}

void MainWindow::on_enableInfo_clicked()
{
    mWereChanges = true;
}

void MainWindow::on_enableWarnings_clicked()
{
    mWereChanges = true;
}

void MainWindow::on_enableErrors_clicked()
{
    mWereChanges = true;
}
