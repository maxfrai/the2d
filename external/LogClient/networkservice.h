#ifndef NETWORKSERVICE_H
#define NETWORKSERVICE_H

#ifdef _WIN32
    #ifndef WIN32
        #define WIN32
    #endif
#endif

#ifdef _WIN64
    #ifndef WIN64
        #define WIN64
    #endif
#endif

#include "The2D/Utils/Enet/enet.h"
#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <map>
#include <QtCore>

enum class LogType : char
{
    LT_ERROR = 'E',
    LT_WARNING = 'W',
    LT_INFO = 'I'
};

struct Message
{
    QString title;
    QString data;
    LogType type;
};

struct ConnectInfo
{
    QString ip;
    int port;
};

class NetworkService : public QObject
{
    Q_OBJECT
public:
    NetworkService();
    ~NetworkService();

    void init();
    void deinit();
    void connect(const char* ip, int port);
    void logic();
    void clearMessages();

    static NetworkService& instance();

    typedef std::map<int, Message> MessagesList;
    const MessagesList& getMessages() const;

private:
    NetworkService(const NetworkService&) = delete;
    void operator=(const NetworkService&) = delete;

    void newMessage(void* data);

    ENetAddress address;
    ENetEvent event;
    ENetPeer* peer;
    ENetHost* client;

    ENetPeer* server;

    std::thread mBackgroundWorker;
    std::mutex mMutex;
    MessagesList mMessages;
    ConnectInfo mInfo;
    int mCounter;
    bool mConnect;
    bool mConnected;

signals:
    void newMessage(const Message& msg);
    void doBeep();
    void newCmd(QString cmd);
    void connectionStatusChanged(bool connected);
};

#endif // NETWORKSERVICE_H
