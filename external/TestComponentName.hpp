////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>

#include "File1.hpp"
#include "File2.h"

#include "Box2DSupport/Body.hpp"
#include <The2D/GameLoopProcessor.hpp>

////////////////////////////////////////////////////////////
namespace the {

    class TestComponentName : public Component
    {
        public:
            THE_ENTITY(TestComponentName, Component)

            TestComponentName();
            virtual ~TestComponentName();

            struct InitData: Component::InitData
            {
                bind<something> test;
                bind<another> test2;
                ManagerBind<PhysicManager> physicManager;
                Bind<Body> someBody;
                ChildTypeStub::InitData childStub;
                MultiBind<MultiBindTargetType> multiBindTargets;

                InitData();
            };
            InitObj<InitData> mInitData;

            // List of signals
            the::Signal<void (int)> mySignalName;

            unsigned int ThisIsPublic() const;
            virtual void update() override;

        private:
            InitState onInit() override;
            void onDeinit() override;

            int ThisIsPRIVATEEE() const;

            Body* mSomeBody;
            ChildTypeStub mChildStub;
            std::vector<MultiBindTargetType*> mMultiBindTargets;

    };
}; // namespace the
////////////////////////////////////////////////////////////
