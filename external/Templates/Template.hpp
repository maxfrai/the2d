////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#pragma once

////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <The2D/Bind.hpp>
#include <The2D/Component.hpp>

using namespace the;

{IncludeList}
////////////////////////////////////////////////////////////
namespace {Namespace} {{

    class {Name} : public {BaseName}
    {{
        public:
            THE_ENTITY({Name}, {BaseName})

{ParametersPlaceholder}

            {Name}();
            virtual ~{Name}();

{SignalsPlaceholder}
{PublicFunctionsPlaceholder}
        private:{PrivateFieldsPlaceholder}
            InitState onInit() override;
            void onDeinit() override;
{PrivateFunctionsPlaceholder}
    }};
}}; // namespace {Namespace}
////////////////////////////////////////////////////////////
