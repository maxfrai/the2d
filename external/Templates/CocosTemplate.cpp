////////////////////////////////////////////////////////////
//
// The2D is a framework for easy creation of 2D applications
// Copyright (C) 2010-2013 WinCode team: Pavel Bogatirev, Max Tyslenko
//
// The source may not be copied or distributed without asking WinCode team before
//
////////////////////////////////////////////////////////////

#include <The2D/Config.hpp>
#include "{HeaderMapInclude}"

using namespace {Namespace};
using namespace the;
using namespace cocos2d;



{Name}::{Name}() {CtorInitPlaceholder}
{{
{ParametersDeclarationPlaceholder}
{CtorPlaceholder}
}}

{Name}::~{Name}()
{{
}}

void {Name}::onDeinit()
{{
{OnDeinitPlaceholder}
}}

the::InitState {Name}::onInit()
{{
{OnInitPlaceholder}
    return true;
}}

{PublicFunctionsImplPlaceholder}
{PrivateFunctionsImplPlaceholder}
