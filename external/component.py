from lxml import etree
import json
import sys # cmd arguments
import ConfigParser
import os

# Configuration part -------------------------------------------------------------------------------
configFile = 'default_generator.cfg'
templatesFolder = ''

numArgs = len(sys.argv)

if numArgs is 2:
    configFile = sys.argv[1]

def Str2Bool(string):
    return string.lower() == 'true'

# --------------------------------------------------------------------------------------------------
class ComponentGenerator:
    def __init__(self):
        self.main()

    # ----------------------------------------------------------------------------------------------
    def getConfigName(self):
        return configFile

    def readConfiguration(self):
        config = ConfigParser.RawConfigParser(allow_no_value=True)
        config.read(self.getConfigName())

        generalMap = {}
        headerMap = {}
        sourceMap = {}

        singleTabulation = '    '
        tabulation = '            '

        headerMap['ParametersPlaceholder'] = '';
        headerMap['PublicFunctionsPlaceholder'] = ''
        headerMap['PrivateFunctionsPlaceholder'] = ''
        headerMap['SignalsPlaceholder'] = ''
        headerMap['PrivateFieldsPlaceholder'] = ''

        sourceMap['PublicFunctionsImplPlaceholder'] = ''
        sourceMap['ParametersDeclarationPlaceholder'] = ''
        sourceMap['CtorPlaceholder'] = ''
        sourceMap['PrivateFunctionsImplPlaceholder'] = ''
        sourceMap['CtorInitPlaceholder'] = ''
        sourceMap['OnInitPlaceholder'] = ''
        sourceMap['OnDeinitPlaceholder'] = ''
       

        # ----------------------------
        generalItems = [
            'Name',
            'BaseName',
            'Namespace',
            'TemplateName',
            'HeaderPath',
            'SourcePath',
            'UpdateLogic',
            'UpdateVisual',
            'UsePhysic',
            'ChildStub',
            'MultiBindStub'
        ]

        headerItems = [
            'IncludeList'
        ]

        for item in generalItems:
            generalMap[item] = config.get('General', item)

        generalMap['HeaderMapInclude'] = os.path.join(generalMap['HeaderPath'], generalMap['Name'] + ".hpp")

        # ----------------------------
        for item in headerItems:
            headerMap[item] = config.get('Header', item)

        includes = filter(None, headerMap['IncludeList'].split('\n'))
        headerMap['IncludeList'] = ''
        for include in includes:
            headerMap['IncludeList'] += '#include ' + include + '\n'

        # PUBLIC FUNCTIONS -----------------------------------------------------------------------------
        publicFunctionsList = config.items('PublicFunctions')

        for func in publicFunctionsList:
            headerMap['PublicFunctionsPlaceholder'] += tabulation + func[0] + ' ' + func[1] + ';\n'
            sourceMap['PublicFunctionsImplPlaceholder'] += func[0] + ' ' + generalMap['Name'] + '::' + func[1] + '\n{\n}\n\n'

        if Str2Bool(generalMap['UpdateLogic']):
            headerMap['PublicFunctionsPlaceholder'] += tabulation + 'void updateLogic(float elapsed);\n'
            sourceMap['PublicFunctionsImplPlaceholder'] += 'void ' + generalMap['Name'] + '::updateLogic(float elapsed)\n{\n}\n\n'
            sourceMap['CtorPlaceholder'] += singleTabulation + 'enableUpdateLogic(true);\n'

        if Str2Bool(generalMap['UpdateVisual']):
            headerMap['PublicFunctionsPlaceholder'] += tabulation + 'void updateVisual();\n'
            sourceMap['PublicFunctionsImplPlaceholder'] += 'void ' + generalMap['Name'] + '::updateVisual()\n{\n}\n\n'
            sourceMap['CtorPlaceholder'] += singleTabulation + 'enableUpdateVisual(true);\n'

        # PRIVATE FUNCTIONS -----------------------------------------------------------------------------
        privateFunctionsList = config.items('PrivateFunctions')  

        for func in privateFunctionsList:
            headerMap['PrivateFunctionsPlaceholder'] += tabulation + func[0] + ' ' + func[1] + ';\n'
            sourceMap['PrivateFunctionsImplPlaceholder'] += func[0] + ' ' + generalMap['Name'] + '::' + func[1] + '\n{\n}\n\n'

        # SIGNALS ---------------------------------------------------------------------------------------
        signalsList = config.items('Signals')  

        for sig in signalsList:
            headerMap['SignalsPlaceholder'] += tabulation + 'the::Signal<void (' + sig[0] + ')> ' + sig[1] + ';\n'

        # PARAMETERS ------------------------------------------------------------------------------------
        parametersList = config.items('Parameters') 

        for param in parametersList:
            headerMap['ParametersPlaceholder'] += tabulation + singleTabulation + param[0] + ' ' + param[1] + ';\n'
            sourceMap['ParametersDeclarationPlaceholder'] += singleTabulation + 'THE_REGISTER_PARAM(' + param[1] + ');\n'

        # General
        if Str2Bool(generalMap['UsePhysic']):
            headerMap['IncludeList'] += '\n#include "Box2DSupport/Body.hpp"\n'
            headerMap['ParametersPlaceholder'] += tabulation + singleTabulation + 'ManagerBind<PhysicManager> physicManager;\n' + tabulation + singleTabulation + 'Bind<Body> someBody;\n'
            headerMap['PrivateFieldsPlaceholder'] += '\n' + tabulation + 'Body* mSomeBody;\n';

            sourceMap['ParametersDeclarationPlaceholder'] += singleTabulation + 'THE_REGISTER_PARAM(someBody);\n' + singleTabulation + 'THE_REGISTER_PARAM(physicManager)\n'
            sourceMap['CtorInitPlaceholder'] += ', mSomeBody(nullptr)'
            sourceMap['OnInitPlaceholder'] += singleTabulation + 'mSomeBody = mInitData->someBody.Get();\n'
            sourceMap['OnDeinitPlaceholder'] += singleTabulation + 'mSomeBody = nullptr;\n'

        if Str2Bool(generalMap['ChildStub']):
            headerMap['PrivateFieldsPlaceholder'] += tabulation + 'ChildTypeStub mChildStub;\n';
            headerMap['ParametersPlaceholder'] += tabulation + singleTabulation + 'ChildTypeStub::InitData childStub;\n'

            sourceMap['ParametersDeclarationPlaceholder'] += singleTabulation + 'THE_REGISTER_PARAM(childStub);\n'

        if Str2Bool(generalMap['MultiBindStub']):
            headerMap['ParametersPlaceholder'] += tabulation + singleTabulation + 'MultiBind<MultiBindTargetType> multiBindTargets;\n'
            headerMap['PrivateFieldsPlaceholder'] += tabulation + 'std::vector<MultiBindTargetType*> mMultiBindTargets;\n';

            sourceMap['ParametersDeclarationPlaceholder'] += singleTabulation + 'THE_REGISTER_PARAM(multiBindTargets);\n'
            sourceMap['OnInitPlaceholder'] += \
                singleTabulation + 'for(MultiBindTargetType* cmp : mInitData->multiBindTargets.getBindsT())\n' + \
                singleTabulation + '{\n' + \
                singleTabulation + singleTabulation + 'mMultiBindTargets.push_back(cmp);\n' + \
                singleTabulation + '}\n'
            sourceMap['OnDeinitPlaceholder'] += singleTabulation + 'mMultiBindTargets.clear();\n'

        # ----------------------------
        self.globalMap = dict(generalMap.items() + headerMap.items() + sourceMap.items())

    def generate(self):
        # Templates should be located in same dir with script
        inputHeaderTpl = os.path.join(self.globalMap['TemplateName'] + ".hpp")
        inputSourceTpl = os.path.join(self.globalMap['TemplateName'] + ".cpp")

        # Content of the components templates
        inputHeaderData = open(inputHeaderTpl, 'r').read()
        inputSourceData = open(inputSourceTpl, 'r').read()

        headerResult = inputHeaderData.format(**self.globalMap)
        sourceResult = inputSourceData.format(**self.globalMap)

        outputHeaderPath = os.path.join(self.globalMap['HeaderPath'], self.globalMap['Name'] + ".hpp")
        outputSourcePath = os.path.join(self.globalMap['SourcePath'], self.globalMap['Name'] + ".cpp")
        outputHeaderData = open(outputHeaderPath, 'w+')
        outputSourceData = open(outputSourcePath, 'w+')

        outputHeaderData.writelines(headerResult)
        outputSourceData.writelines(sourceResult)

        print 'Generated OK'

    def main(self):
        self.readConfiguration()
        self.generate()
# --------------------------------------------------------------------------------------------------

# !!!!!!!!!!!!!!!
ComponentGenerator()
# !!!!!!!!!!!!!!!
