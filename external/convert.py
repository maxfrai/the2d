from lxml import etree
import json
import ntpath
import sys # cmd arguments

# Configuration part -------------------------------------------------------------------------------
inputFile  = 'input.json'
outputFile = 'result.xml'
showMessages = False

def Message(data):
    if showMessages:
        print data

numArgs = len(sys.argv) # minimum 1, script filename

if numArgs < 4:
    print '[Warning] Script takes 2+1 arguments: convert.py +input.json +output.xml ~true/false'

if numArgs >= 2:
    inputFile = sys.argv[1]

if numArgs >= 3:
    outputFile = sys.argv[2]

if numArgs >= 4:
    showMessages = (sys.argv[3].lower() == 'true')

print 'Used configuration:'
print '\t Input file : ' + inputFile
print '\t Output file: ' + outputFile
print '\t Show messages: ' + str(showMessages)

# --------------------------------------------------------------------------------------------------
class Converter:
    def __init__(self):
        self.xmlData  = etree.Element('GameTree')
        self.jsonData = self.getJson()

        self.main()

    def getJson(self):
        # Read json-input and return single string made by joining list values
        fileHandle = open(inputFile, 'r')
        jsonData = fileHandle.readlines()
        fileHandle.close()

        return json.loads(''.join(jsonData))

    def writeXml(self):
        fileHandle = open(outputFile, 'w')
        fileHandle.write(etree.tostring(self.xmlData, pretty_print=True))
        fileHandle.close()
    # ----------------------------------------------------------------------------------------------
    def checkKey(self, keyName, array):
        if type(array) is not dict:
            return False

        return array.has_key(keyName)

    def getKey(self, keyName, array):
        if self.checkKey(keyName, array):
            return str(array[keyName])
        else:
            return ''

    def getVector(self, keyName, array):
        # Returns map like: {'x': 0, 'y' : 0}
        if self.checkKey(keyName, array):
            # If we got not a map but single value, make an map from it
            if type(array[keyName]) is int:
                return {'x': array[keyName], 'y': array[keyName]}
            else:
                return array[keyName]
        else:
            #Message('\t[Warning] No `' + keyName + '` vector in json data!')
            return None

    # ----------------------------------------------------------------------------------------------
    def convertBodyType(self, id):
        # We receive string, so get the number first
        id = int(id)
        if id is 0:
            return 'b2_staticBody'
        elif id is 1:
            return 'b2_kinematicBody'
        elif id is 2:
            return 'b2_dynamicBody'
    # ----------------------------------------------------------------------------------------------
    def addChild(self, elementName, parent, array, callback = None, newName = None):
        if newName is None:
            handle = etree.Element(elementName)
        else:
            handle = etree.Element(newName)

        result = self.getKey(elementName, array)

       # Skip when getKey returned None
        if result:
            if callback is not None:
                handle.text = callback(result)
            else:
                handle.text = result

            parent.append(handle)
        #else:
            #Message('\t[Warning] Element: `' + elementName + '` not found in json data!')

    def addVector(self, elementName, parent, array, newName = None):
        vectorMap = self.getVector(elementName, array)

        if vectorMap:
            if newName is None:
                vectorNode = etree.Element(elementName)
            else:
                vectorNode = etree.Element(newName)

            if len(vectorMap) is 2:
                vectorNode.set('x', str(vectorMap['x']))
                vectorNode.set('y', str(vectorMap['y']))

            parent.append(vectorNode)
        #else:
            #Message('\t[Warning] Element(vector): `' + elementName + '` not found in json data!')

    def addVector1(self, elementName, parent, array, newName = None, attrName = None):
        val = self.getKey(elementName, array)

        if val:
            if newName is None:
                vectorNode = etree.Element(elementName)
            else:
                vectorNode = etree.Element(newName)

            if attrName is None:
                vectorNode.set('x', str(val))
            else:
                vectorNode.set(attrName, str(val))

            parent.append(vectorNode)
        #else:
            #Message('\t[Warning] Element(vector1): `' + elementName + '` not found in json data!')

    def addAttached(self, parentJson, parentNode):
        # Custom properties
        if self.checkKey('customProperties', parentJson):
            customPropJson = parentJson['customProperties']

            if len(customPropJson) is not 0:
                attachedNode = etree.Element('Attached')

                # Avoid adding Attached node to fixture if there was no real child
                # Or the children was zindex or group
                numAddedChildren = 0

                # item is a map: {'name': val, 'type' : val}
                for item in customPropJson:
                    # Process zindex and group manually later
                    storedName = item['name']

                    # Process vec2 type separately
                    if item.has_key('vec2'):
                        self.addVector('vec2', attachedNode, item, storedName)
                        continue

                    # Remove name property
                    tempNode = etree.Element(item['name'])
                    item.pop('name', None)
                    tempNode.text = str(item.itervalues().next())

                    if storedName == 'zindex' or storedName == 'group':
                        # Add to local parent node
                        parentNode.append(tempNode)
                    else:
                        # Add to `Attached` node
                        attachedNode.append(tempNode)
                        numAddedChildren += 1

                if numAddedChildren > 0:
                    parentNode.append(attachedNode)

    def addBind(self, jointJson, parentNode):
        # Indecies of bodies
        if self.checkKey('bodyA', jointJson) and self.checkKey('bodyB', jointJson):
            bodyA = jointJson['bodyA']
            bodyB = jointJson['bodyB']

            # Associated names
            if self.jsonData['body'][bodyA].has_key('name') and \
               self.jsonData['body'][bodyB].has_key('name'):
                bodyNameA = self.jsonData['body'][bodyA]['name']
                bodyNameB = self.jsonData['body'][bodyB]['name']

                aNode = etree.Element('bodyA')
                aNode.set('path', bodyNameA)
                bNode = etree.Element('bodyB')
                bNode.set('path', bodyNameB)

                parentNode.append(aNode)
                parentNode.append(bNode)
            else:
                Message('[Error] Joint is attached to body without name')

    # ----------------------------------------------------------------------------------------------
    def processPhysicManager(self):
        """parent = etree.Element('PhysicManager')

        Message('+-- Parsing `PhysicManager`')

        self.addChild('allowSleep',                 parent, self.jsonData)
        self.addChild('autoClearForces',            parent, self.jsonData)
        self.addChild('positionIterations',         parent, self.jsonData)
        self.addChild('velocityIterations',         parent, self.jsonData)
        self.addChild('stepsPerSecond',             parent, self.jsonData)
        self.addChild('warmStarting',               parent, self.jsonData)
        self.addChild('continuousPhysics',          parent, self.jsonData)
        self.addChild('subStepping',                parent, self.jsonData)
        self.addVector('gravity',                   parent, self.jsonData)

        self.xmlData.append(parent)"""
    # ----------------------------------------------------------------------------------------------
    def processImagePath(self, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)

    def processImages(self):
        if not self.checkKey('image', self.jsonData):
            # Message(' No images in input file...')
            return

        for image in self.jsonData['image']:
            imageNode = etree.Element('Image')

            self.addChild('name',               imageNode, image)
            self.addChild('opacity',            imageNode, image)
            self.addChild('renderOrder',        imageNode, image)
            self.addChild('scale',              imageNode, image)
            self.addChild('angle',              imageNode, image)
            self.addChild('file',               imageNode, image, self.processImagePath)
            self.addChild('filter',             imageNode, image)
            self.addChild('flip',               imageNode, image)
            self.addVector('center',            imageNode, image)

            # Process local bind to body
            if self.checkKey('body', image):
                body = image['body']

                # Associated names
                if body != -1 and self.jsonData['body'][body].has_key('name'):
                    bodyName = self.jsonData['body'][body]['name']

                    temp = etree.Element('bodyBind')
                    temp.set('path', bodyName)

                    imageNode.append(temp)

            cornersNode = etree.Element('corners')

            if self.checkKey('corners', image):
                json = image['corners']
                x = json['x']
                y = json['y']

                counter = 0
                for point in x:
                    vertexNode = etree.Element('Vertex')

                    vertexNode.set('x', str(point))
                    vertexNode.set('y', str(y[counter]))

                    counter += 1

                    cornersNode.append(vertexNode)

                # CORNERS ARE DISABLED!
                # imageNode.append(cornersNode)

            # Custom properties
            self.addAttached(image, imageNode)

            self.xmlData.append(imageNode)
    # ----------------------------------------------------------------------------------------------
    def processJoints(self):
        if not self.checkKey('joint', self.jsonData):
            Message('\t[Warning] No joints in input file, skipping...')
            return

        for joint in self.jsonData['joint']:
            jointContainerNode = self.xmlData;

            typeName = joint['type']

            # ========================
            if typeName == 'revolute':
                revoluteNode = etree.Element('RevoluteJoint')

                self.addChild('name',             revoluteNode, joint)
                self.addChild('collideConnected', revoluteNode, joint)
                self.addChild('enableLimit',      revoluteNode, joint)
                self.addChild('enableMotor',      revoluteNode, joint)
                # self.addChild('jointSpeed',       revoluteNode, joint)
                self.addChild('lowerLimit',       revoluteNode, joint, None, 'lowerAngle')
                self.addChild('maxMotorTorque',   revoluteNode, joint)
                self.addChild('motorSpeed',       revoluteNode, joint)
                self.addChild('refAngle',         revoluteNode, joint, None, 'referenceAngle')
                self.addChild('upperLimit',       revoluteNode, joint, None, 'upperAngle')

                self.addVector('anchorA', revoluteNode, joint, 'localAnchorA')
                self.addVector('anchorB', revoluteNode, joint, 'localAnchorB')

                self.addBind(joint, revoluteNode)
                self.addAttached(joint, revoluteNode)

                jointContainerNode.append(revoluteNode)
            # ========================
            elif typeName == 'distance':
                distanceNode = etree.Element('DistanceJoint')

                self.addChild('name',             distanceNode, joint)
                self.addChild('collideConnected', distanceNode, joint)
                self.addChild('dampingRatio',     distanceNode, joint)
                self.addChild('frequency',        distanceNode, joint, None, 'frequencyHz')
                self.addChild('length',           distanceNode, joint)

                self.addVector('anchorA', distanceNode, joint, 'localAnchorA')
                self.addVector('anchorB', distanceNode, joint, 'localAnchorB')

                self.addBind(joint, distanceNode)
                self.addAttached(joint, distanceNode)

                jointContainerNode.append(distanceNode)
            # ========================
            elif typeName == 'prismatic':
                prismaticNode = etree.Element('PrismaticJoint')

                self.addChild('name',             prismaticNode, joint)
                self.addChild('collideConnected', prismaticNode, joint)
                self.addChild('enableLimit',      prismaticNode, joint)
                self.addChild('enableMotor',      prismaticNode, joint)
                self.addChild('lowerLimit',       prismaticNode, joint, None, 'lowerTranslation')
                self.addChild('maxMotorForce',    prismaticNode, joint)
                self.addChild('motorSpeed',       prismaticNode, joint)
                self.addChild('refAngle',         prismaticNode, joint, None, 'referenceAngle')
                self.addChild('upperLimit',       prismaticNode, joint, None, 'upperTranslation')

                self.addVector('localAxisA', prismaticNode, joint, 'localAxisA')
                self.addVector('anchorA',    prismaticNode, joint, 'localAnchorA')
                self.addVector('anchorB',    prismaticNode, joint, 'localAnchorB')

                self.addBind(joint, prismaticNode)
                self.addAttached(joint, prismaticNode)

                jointContainerNode.append(prismaticNode)
            # ========================
            elif typeName == 'wheel':
                wheelNode = etree.Element('WheelJoint')

                self.addChild('name',               wheelNode, joint)
                self.addChild('collideConnected',   wheelNode, joint)
                self.addChild('enableMotor',        wheelNode, joint)
                self.addChild('maxMotorTorque',     wheelNode, joint)
                self.addChild('motorSpeed',         wheelNode, joint)
                self.addChild('springDampingRatio', wheelNode, joint, None, 'dampingRatio')
                self.addChild('springFrequency',    wheelNode, joint, None, 'frequencyHz')

                self.addVector('localAxisA', wheelNode, joint, 'localAxisA')
                self.addVector('anchorA',    wheelNode, joint, 'localAnchorA')
                self.addVector('anchorB',    wheelNode, joint, 'localAnchorB')

                self.addBind(joint, wheelNode)
                self.addAttached(joint, wheelNode)

                jointContainerNode.append(wheelNode)
            # ========================
            elif typeName == 'rope':
                ropeNode = etree.Element('RopeJoint')

                self.addChild('name',               ropeNode, joint)
                self.addChild('collideConnected',   ropeNode, joint)
                self.addChild('maxLength',          ropeNode, joint)

                self.addVector('anchorA',    ropeNode, joint, 'localAnchorA')
                self.addVector('anchorB',    ropeNode, joint, 'localAnchorB')

                self.addBind(joint, ropeNode)
                self.addAttached(joint, ropeNode)

                jointContainerNode.append(ropeNode)
            # ========================
            elif typeName == 'motor':
                motorNode = etree.Element('MotorJoint')

                self.addChild('name',               motorNode, joint)
                self.addChild('collideConnected',   motorNode, joint)
                self.addChild('maxForce',           motorNode, joint)
                self.addChild('maxTorque',          motorNode, joint)
                self.addChild('correctionFactor',   motorNode, joint)

                self.addVector('anchorA',    motorNode, joint, 'localAnchorA')
                self.addVector('anchorB',    motorNode, joint, 'localAnchorB')

                self.addBind(joint, wheelNode)
                self.addAttached(joint, wheelNode)

                jointContainerNode.append(motorNode)
            # ========================
            elif typeName == 'weld':
                weldNode = etree.Element('WeldJoint')

                self.addChild('name',               weldNode, joint)
                self.addChild('collideConnected',   weldNode, joint)
                self.addChild('refAngle',           weldNode, joint, None, 'referenceAngle')
                self.addChild('dampingRatio',       weldNode, joint)
                self.addChild('frequency',          weldNode, joint, None, 'frequencyHz')

                self.addVector('anchorA',    weldNode, joint, 'localAnchorA')
                self.addVector('anchorB',    weldNode, joint, 'localAnchorB')

                self.addBind(joint, weldNode)
                self.addAttached(joint, weldNode)

                jointContainerNode.append(weldNode)
            # ========================
            elif typeName == 'friction':
                frictNode = etree.Element('FrictionJoint')

                self.addChild('name',               frictNode, joint)
                self.addChild('collideConnected',   frictNode, joint)
                self.addChild('maxForce',           frictNode, joint)
                self.addChild('maxTorque',          frictNode, joint)

                self.addVector('anchorA',    frictNode, joint, 'localAnchorA')
                self.addVector('anchorB',    frictNode, joint, 'localAnchorB')

                self.addBind(joint, frictNode)
                self.addAttached(joint, frictNode)

                jointContainerNode.append(frictNode)

            #self.xmlData.append(jointContainerNode)
    # ----------------------------------------------------------------------------------------------
    def _processFixtures(self, bodyNode, bodyJson):
        # Is called inside procesBodies
        if not self.checkKey('fixture', bodyJson):
            #Message('[Warning] No fixture tag in `' + self.getKey('name', bodyJson) + '` body!')
            return

        fixturesRootNode = etree.Element('Fixtures')
        for fixtureJson in bodyJson['fixture']:
            fixtureNode = etree.Element('Fixture')

            self.addChild('name',                    fixtureNode, fixtureJson)
            self.addChild('density',                 fixtureNode, fixtureJson)
            self.addChild('friction',                fixtureNode, fixtureJson)
            self.addChild('restitution',             fixtureNode, fixtureJson)
            self.addChild('sensor',                  fixtureNode, fixtureJson, None, 'isSensor')

            # Chek filter manually
            categoryBits = self.checkKey('filter-categoryBits', fixtureJson)
            maskBits   = self.checkKey('filter-maskBits', fixtureJson)
            groupIndex = self.checkKey('filter-groupIndex', fixtureJson)

            if categoryBits or maskBits or groupIndex:
                filterNode = etree.Element('filter')

                if categoryBits:
                    self.addVector1('filter-categoryBits', filterNode, fixtureJson, 'categoryBits', 'mask')

                if maskBits:
                    self.addVector1('filter-maskBits', filterNode, fixtureJson, 'maskBits', 'mask')

                if groupIndex:
                    self.addChild('filter-groupIndex', filterNode, fixtureJson, None, 'groupIndex')

                fixtureNode.append(filterNode)

            fixturesRootNode.append(fixtureNode)

            # Fixture shape objects
            # Circle or Polygon or Chain
            circle  = self.checkKey('circle', fixtureJson)
            polygon = self.checkKey('polygon', fixtureJson)
            chain   = self.checkKey('chain', fixtureJson)

            shapeNode = etree.Element('Shape')
            if circle:
                circleNode = etree.Element('circleData')
                self.addVector('center', circleNode, fixtureJson['circle'])
                self.addChild('radius', circleNode, fixtureJson['circle'])
                shapeNode.append(circleNode)

            elif polygon:
                polygonNode  = etree.Element('polygonData')
                verticesNode = etree.Element('vertices')

                json = fixtureJson['polygon']['vertices']
                x = json['x']
                y = json['y']

                counter = 0
                for point in x:
                    vertexNode = etree.Element('Vertex')

                    vertexNode.set('x', str(point))
                    vertexNode.set('y', str(y[counter]))

                    counter += 1

                    verticesNode.append(vertexNode)

                polygonNode.append(verticesNode)
                shapeNode.append(polygonNode)

            elif chain:
                chainNode  = etree.Element('chainData')
                verticesNode = etree.Element('vertices')

                json = fixtureJson['chain']['vertices']
                x = json['x']
                y = json['y']

                counter = 0
                for point in x:
                    vertexNode = etree.Element('Vertex')

                    vertexNode.set('x', str(point))
                    vertexNode.set('y', str(y[counter]))

                    counter += 1

                    verticesNode.append(vertexNode)

                chainNode.append(verticesNode)

                self.addVector('hasNextVertex', chainNode, fixtureJson['chain'])
                self.addVector('hasPrevVertex', chainNode, fixtureJson['chain'])
                self.addVector('nextVertex', chainNode, fixtureJson['chain'])
                self.addVector('prevVertex', chainNode, fixtureJson['chain'])

                shapeNode.append(chainNode)

            fixtureNode.append(shapeNode)

            # Custom properties for fixtures
            self.addAttached(fixtureJson, fixtureNode)

        bodyNode.append(fixturesRootNode)
    # ----------------------------------------------------------------------------------------------
    def getCustomStringProperty(self, bodyJson, propertyName):
        if self.checkKey('customProperties', bodyJson):
                customPropJson = bodyJson['customProperties']

                # item is a map: {'name': val, 'string' : val}
                for item in customPropJson:
                    if item['name'] == propertyName:
                        return item['string']
        return None

    def _findPhysicPlaceholder(self, bodyJson):
        placeholderFound = False
        if self.getCustomStringProperty(bodyJson, 'type') == 'PhysicPlaceholder':
            placeholderFound = True
            Message('+-- Parsing placeholder `' + self.getKey('name', bodyJson) + '`')
            filePath = self.getCustomStringProperty(bodyJson, 'file');
            if filePath != None:
                placeholderNode = etree.Element('PhysicPlaceholder')
                placeholderNode.set('file',  filePath)
                positionNode = etree.Element('position')
                positionNode.set('x', str(bodyJson['position']['x']))
                positionNode.set('y', str(bodyJson['position']['y']))
                placeholderNode.append(positionNode)

                self.xmlData.append(placeholderNode)
            else:
                Message('[Error] Missed property \'file\' in placeholder')

        return placeholderFound

    def _findTemplate(self, bodyJson):
        templateFound = False

        if self.getCustomStringProperty(bodyJson, 'type') == 'GameTreeTemplate':
            templateFound = True
            Message('+-- Parsing game tree template `' + self.getKey('name', bodyJson) + '`')
            tplPath = self.getCustomStringProperty(bodyJson, 'template');
            if tplPath != None:
                tplNode = etree.Element('Mechanism')
                tplNode.set('template', tplPath)

                if self.checkKey('customProperties', bodyJson):
                        customPropJson = bodyJson['customProperties']

                        # Process all params except the "system-defined"
                        for prop in customPropJson:
                            name = prop['name']
                            if name in ['type', 'template']:
                                continue
                            val  = prop['string']

                            paramNode = etree.Element('Param')
                            paramNode.set('id', '@' + name)
                            if '<' in val and '>' in val:
                                paramNode.text = val
                            else:
                                paramNode.set('value', val)

                            tplNode.append(paramNode)

                        # Process position separately
                        paramNode = etree.Element('Param')
                        paramNode.set('id', '@Position')

                        positionNode = etree.Element('position')
                        positionNode.set('x', str(bodyJson['position']['x']))
                        positionNode.set('y', str(bodyJson['position']['y']))

                        paramNode.append(positionNode)
                        tplNode.append(paramNode)

                self.xmlData.append(tplNode)
            else:
                Message('[Error] Missed property \'template\' in game tree template')

        return templateFound

    def processBodies(self):
        if not self.checkKey('body', self.jsonData):
            Message('\t No bodies in input file, skipping...')
            return

        # Iterate through bodies list
        for bodyJson in self.jsonData['body']:

            if self._findPhysicPlaceholder(bodyJson):
                continue

            if self._findTemplate(bodyJson):
                continue

            bodyNode = etree.Element('Body')

            Message('+-- Parsing body `' + self.getKey('name', bodyJson) + '`')

            self.addChild('name',                       bodyNode, bodyJson)
            self.addChild('type',                       bodyNode, bodyJson, self.convertBodyType)
            self.addChild('angle',                      bodyNode, bodyJson)
            self.addChild('angularDumping',             bodyNode, bodyJson)
            self.addChild('angularVelocity',            bodyNode, bodyJson)
            self.addChild('awake',                      bodyNode, bodyJson)
            self.addChild('active',                     bodyNode, bodyJson)
            self.addChild('bullet',                     bodyNode, bodyJson)
            self.addChild('fixedRotation',              bodyNode, bodyJson)
            self.addChild('linearDamping',              bodyNode, bodyJson)
            self.addChild('gravityScale',               bodyNode, bodyJson)
            self.addVector('linearVelocity',            bodyNode, bodyJson)
            self.addVector('position',                  bodyNode, bodyJson)

            # Chek massData manually
            mass = self.checkKey('massData-mass', bodyJson)
            massCenter = self.checkKey('massData-center', bodyJson)
            massI = self.checkKey('massData-I', bodyJson)

            if mass or massCenter or massI:
                massNode = etree.Element('massData')

                if mass:
                    self.addChild('massData-mass',    massNode, bodyJson, None, 'mass')
                if massCenter:
                    self.addVector('massData-center', massNode, bodyJson, 'center')
                if massI:
                    self.addChild('massData-I',       massNode, bodyJson, None, 'I')

                bodyNode.append(massNode)
            #else:
                #Message('[Warning] No mass data in `' + self.getKey('name', bodyJson) + '` body!')

            # Check fixtures manually
            self._processFixtures(bodyNode, bodyJson)
            self.addAttached(bodyJson, bodyNode)

            self.xmlData.append(bodyNode)
    # ----------------------------------------------------------------------------------------------
    def main(self):
        print ''

        self.processPhysicManager()
        self.processBodies()
        self.processJoints()
        self.processImages()

        self.writeXml()

        print 'Conversion finished.'
# --------------------------------------------------------------------------------------------------

# !!!!!!!!!!!!!!!
Converter()
# !!!!!!!!!!!!!!!
